<?php
define("DS","/",true);
define('BASE_PATH',realpath(dirname(__FILE__)).DS,true);
set_include_path(BASE_PATH);
include('Crypt/RSA.php');
$production = 0;
$nonce_str = generate_nonce_str(32);
$refund_no = date("YmdHis").time();

$data = array(
    'appid' => 'mch26248',
    'refund_no' => $refund_no,
    'mch_refund_no' => $refund_no,
    'order_no' => '90020190402195135182641',
    'trade_no' => '90020190402195135182641',
    'total_fee' => 500,
    'refund_fee' => 100,
    'refund_fee_type' => 'THB',
    'fee_type' => 'THB',
    'nonce_str' => $nonce_str
);

$encoded_sign = "";
$pemdata = BASE_PATH."../../mch_privkey.pem";

$privatekey_content = file_get_contents($pemdata);
#echo "Private key contents: <br>".$privatekey; echo "<hr>";

$message = "";
ksort($data); # array key sorting
foreach ($data as $key => $value) $message .= $key."=".$value;

$message = mb_convert_encoding($message, "UTF-8");

$rsa = new Crypt_RSA();
$rsa->loadKey($privatekey_content); 
$rsa->setSignatureMode(CRYPT_RSA_SIGNATURE_PKCS1);
$rsa->setHash("MD5");
$signature = $rsa->sign($message);

#echo "Sign with MD5: <br>".$signature; echo "<hr>";
#$signature_encoding = mb_convert_encoding($signature, "UTF-8");
#echo "Sign UTF-8 encoding: <br>".$signature_encoding; echo "<hr>";

$encoded_sign = bin2hex($signature); # supposed to be binascii.hexlify(signature) in Python. Please help to check!!
$timestamp = date("YmdHis").time();
$data_json = json_encode($data);
$dict = array(
		'version' => '2.0.0',
		'time_stamp' => '20170117180215', #$timestamp,
		'sign' => $encoded_sign,
		// 'data' => $data_json
	);
#echo "Sign binascii.hexlify:<pre>"; print_r($dict); echo "</pre>"; echo "<hr>";

print_r($dict);

$data += $dict;

$micro_getrefund_url = "http://api.mch.ksher.net/KsherPay/order_refund";

$ch = curl_init();
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($ch, CURLOPT_POST, 1);
curl_setopt($ch, CURLOPT_URL, $micro_getrefund_url);
curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
$output = curl_exec($ch);

$itemreturn = json_decode($output,true);
echo "*** RETURNS API BELOW ***";
echo "<pre>"; print_r($itemreturn); echo "</pre>";


function generate_nonce_str($len) {
	$nonce_str = "";
	$chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"; 

	for ( $i = 0; $i < $len; $i++ ) { 
		$nonce_str .= $chars[ mt_rand(0, strlen($chars) - 1) ]; 
	} 
	return $nonce_str;
}

