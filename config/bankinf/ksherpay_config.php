<?php 
return (object) array(
	'notify_url' => 'https://bankinf.mcpay.in:22447/bankinf/v1.0/ksherpay/notifypayment',
	'generate_qrcode_url' => 'http://api.mch.ksher.net/KsherPay/native_pay',
	'appid' => 'mch26495', //cannot_modify_amount
	'mch_id' => "26495", //can_modify_amount
	'version' => '2.0.0'
);

