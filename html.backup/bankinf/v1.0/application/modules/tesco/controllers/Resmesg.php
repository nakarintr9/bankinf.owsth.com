<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Resmesg extends MY_Controller {

  function __construct()
  {
    parent::__construct();
  }

  function get_response_mesg($resCode){
    $resMesg['0000'] = 'Success';
    $resMesg['0001'] = 'System Payment Success But System Loan Error';
    $resMesg['1001'] = 'Invalid reference1';
    $resMesg['1002'] = 'Invalid reference2';
    $resMesg['1003'] = 'Invalid amount';
    $resMesg['1004'] = 'Invalid key/passKey';
    $resMesg['2001'] = 'Duplicate transaction';
    $resMesg['2002'] = 'Not found transaction';
    $resMesg['2003'] = 'Not found debt';
    $resMesg['3001'] = 'System Loan Error';
    $resMesg['3002'] = 'System Payment Error';
    return $resMesg[$resCode];
  }


}
