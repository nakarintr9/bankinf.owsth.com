<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Billpayment extends MY_Controller {

  function __construct()
  {
    parent::__construct();
    $this->load->module('tesco/qrypayment');
    $this->load->module('tesco/resmesg');
    $this->load->module('common/cancel_payment');
  }

  function check_key($key, $passKey){
    if($key  == '0817768258' && $passKey == 'FbC13009xxx'){
      return true;
    }else{
      return false;
    }
  }

  function inquiry(){
    $postdata = file_get_contents("php://input");
    $body = json_decode($postdata, true);

    $key = isset($body['key']) ? $body['key'] : '';
    $passKey = isset($body['passKey']) ? $body['passKey'] : '';
    $res_check_key = $this->check_key($key, $passKey);
    if($res_check_key){
      $resCode = '0000';
    }else{
      $resCode = '1004';
    }
    
    $Min = 0;
    $Max = 0;
    $amount = 0;
    $ref1 = $body['reference1'];
    $ref2 = $body['reference2'];
    $transid = $body['tranID'];
    $api_type = $this->qrypayment->get_api_type($ref1, $ref2);
    $action = 'inquiry';
    $paymenttype = 'member';

    $insert_log = array(
      'api_type' => $api_type,
      'action' => $action,
      'ref1' => $ref1,
      'ref2' => $ref2,
      'amount' => $amount,
      'min_amount' => $Min,
      'max_amount' => $Max,
      'request_transid' => $transid,
      'paymenttype' => $paymenttype,
      'status' => 0,
      'postdata' => json_encode($body),
      'adddate' => date('Y-m-d'),
      'addtime' => date('H:i:s')
    );
    $logid = $this->qrypayment->insert_log($insert_log);

    if($resCode == '0000'){
      $log = $this->qrypayment->get_log($transid, $ref1, $ref2, $api_type, $action, $logid);
      if(empty($log)){
        $resCode = '0000';
      }else{
        $resCode = '2001';
      }
    }

    if($resCode == '0000'){
      $partner = $this->qrypayment->get_partner($ref1);
      if(!empty($partner)){
        $resCode = '0000';
      }else{
        $resCode = '1001';
      }
    }

    if($resCode == '0000'){
      $member = $this->qrypayment->get_member($ref1, $ref2);
      if(!empty($member)){
        $resCode = '0000';
      }else{
        $resCode = '1002';
      }
    }

    if($resCode == '0000'){
      if($api_type == 'loan'){
        $kioskcode = "K".$member->membercode;
        $kioskid = $ref2;
        $res = $this->qrypayment->inquiry_loan($kioskcode, $kioskid);
        if($res['error_code'] == 'E00'){
          $resCode = '0000';
          $Min = $res['loan_detail']['pay_per_month'];
          $Max = $res['loan_detail']['pay_per_month'];
          $amount = $res['loan_detail']['pay_per_month'];
          $this->load->module('common/payment');
          $member_fee = $this->payment->get_member_fee('tesco', $ref1, $amount);
          $amount = $amount+$member_fee;
          $Min = $amount;
          $Max = $amount;
        }else if($res['error_code'] == 'E15'){
          $resCode = '3001';
        }else{
          $resCode = '2003';
        }
      }else{
        $resCode = '0000';
        $Min = 100;
        $Max = 30000;
      }
    }

    $response = array(
      'resCode' => $resCode,
      'resMesg' => $this->resmesg->get_response_mesg($resCode),
      'tranID' => $transid,
      'Min'=> $Min,
      'Max'=> $Max
    );

    if($resCode == '0000'){
      $status = 0;
    }else{
      $status = 2;
    }

    $update_log = array(
      'amount' => $amount,
      'min_amount' => $Min,
      'max_amount' => $Max,
      'response_transid' => $transid,
      'res_code' => $resCode,
      'res_message' => $this->resmesg->get_response_mesg($resCode),
      'status' => $status,
      'returndata' => json_encode($response),
      'returndate' => date('Y-m-d'),
      'returntime' => date('H:i:s')
    );
    $this->qrypayment->update_log_id($update_log, $logid);

    header('Content-Type: application/json');
    echo json_encode($response);
  }

  function confirm(){
    $postdata = file_get_contents("php://input");
    $body = json_decode($postdata, true);

    $key = isset($body['key']) ? $body['key'] : '';
    $passKey = isset($body['passKey']) ? $body['passKey'] : '';
    $res_check_key = $this->check_key($key, $passKey);
    if($res_check_key){
      $resCode = '0000';
    }else{
      $resCode = '1004';
    }

    $amount = $body['amount'];
    $ref1 = $body['reference1'];
    $ref2 = $body['reference2'];
    $transid = $body['tranID'];
    $api_type = $this->qrypayment->get_api_type($ref1, $ref2);
    $action = 'confirm';
    $paymenttype = 'member';

    if($api_type == 'loan'){
      $Min = $amount;
      $Max = $amount;
    }else{
      $Min = 100;
      $Max = 30000;
    }

    $insert_log = array(
      'api_type' => $api_type,
      'action' => $action,
      'ref1' => $ref1,
      'ref2' => $ref2,
      'amount' => $amount,
      'min_amount' => $Min,
      'max_amount' => $Max,
      'paymenttype' => $paymenttype,
      'status' => 0,
      'request_transid' => $transid,
      'postdata' => json_encode($body),
      'adddate' => date('Y-m-d'),
      'addtime' => date('H:i:s')
    );
    $logid = $this->qrypayment->insert_log($insert_log);

    if($resCode == '0000'){
      $logquiry = $this->qrypayment->get_log($transid, $ref1, $ref2, $api_type, 'inquiry');
      if(!empty($logquiry)){
        if($logquiry->status == 0){
          $resCode = '0000';
          $status = 1;
          $update_log = array(
            'status' => $status
          );
          $this->qrypayment->update_log($update_log, $transid, $ref1, $ref2, $api_type, 'inquiry');
        }else{
          $resCode = '2001';
        }
      }else{
        $resCode = '2002';
      }
    }

    if($resCode == '0000'){
      $logconfirm = $this->qrypayment->get_log($transid, $ref1, $ref2, $api_type, $action, $logid);
      if(empty($logconfirm)){
        $resCode = '0000';
      }else{
        $resCode = '2001';
      }
    }

    if($resCode == '0000'){
      $partner = $this->qrypayment->get_partner($ref1);
      if(!empty($partner)){
        $resCode = '0000';
      }else{
        $resCode = '1001';
      }
    }

    if($resCode == '0000'){
      $member = $this->qrypayment->get_member($ref1, $ref2);
      if(!empty($member)){
        $resCode = '0000';
      }else{
        $resCode = '1002';
      }
    }

    if($resCode == '0000' && $api_type != 'loan'){
      $res = $this->qrypayment->payment_credit_member($ref1, $ref2, $amount, $transid, $member, $partner);
      if($res['error_code'] == 'E00'){
        $resCode = '0000';
        $partnerpaymentid = $res['partnerpaymentid'];
        $memberpaymentid = $res['memberpaymentid'];
      }else{
        $resCode = '3002';
      }
    }

    if($resCode == '0000' && $api_type == 'loan'){
      $kioskcode = "K".$member->membercode;
      $kioskid = $ref2;
      $res = $this->qrypayment->inquiry_loan($kioskcode, $kioskid);
      if($res['error_code'] == 'E00'){
        $resCode = '0000';
        $loan_amount = $res['loan_detail']['pay_per_month'];
        $this->load->module('common/payment');
        $member_fee = $this->payment->get_member_fee('thanachart', $ref1, $amount);
        $loan_amount = $loan_amount+$member_fee;
        if($amount < $loan_amount){
          $resCode = '1003';
        }else{
          $res = $this->qrypayment->payment_credit_member($ref1, $ref2, $amount, $transid, $member, $partner);
          if($res['error_code'] == 'E00'){
            $resCode = '0000';
            $partnerpaymentid = $res['partnerpaymentid'];
            $memberpaymentid = $res['memberpaymentid'];
            $res = $this->qrypayment->pay_loan($kioskcode, $kioskid);
            if($res['error_code'] == 'E00'){
              $resCode = '0000';
            }else{
              $resCode = '0000';
            }
          }else{
            $resCode = '3002';
          }
        }
      }else if($res['error_code'] == 'E15'){
        $resCode = '3001';
      }else{
        $resCode = '2003';
      }
    }

    $response = array(
      'resCode' => $resCode,
      'resMesg' => $this->resmesg->get_response_mesg($resCode),
      'tranID' => $body['tranID']
    );

    if($resCode == '0000'){
      $status = 1;
    }else{
      $status = 2;
    }

    $update_log = array(
      'partnerpaymentid' => isset($partnerpaymentid) ? $partnerpaymentid : NULL,
      'memberpaymentid' => isset($memberpaymentid) ? $memberpaymentid : NULL,
      'response_transid' => $transid,
      'res_code' => $resCode,
      'res_message' => $this->resmesg->get_response_mesg($resCode),
      'status' => $status,
      'returndata' => json_encode($response),
      'returndate' => date('Y-m-d'),
      'returntime' => date('H:i:s')
    );
    $this->qrypayment->update_log_id($update_log, $logid);

    
    header('Content-Type: application/json');
    echo json_encode($response);
  }

  function reverse(){
    $postdata = file_get_contents("php://input");
    $body = json_decode($postdata, true);

    $key = isset($body['key']) ? $body['key'] : '';
    $passKey = isset($body['passKey']) ? $body['passKey'] : '';
    $res_check_key = $this->check_key($key, $passKey);
    if($res_check_key){
      $resCode = '0000';
    }else{
      $resCode = '1004';
    }
    
    $amount = $body['amount'];
    $ref1 = $body['reference1'];
    $ref2 = $body['reference2'];
    $transid = $body['tranID'];
    $api_type = $this->qrypayment->get_api_type($ref1, $ref2);
    $action = 'reverse';
    $paymenttype = 'member';
    $addby = 'Tesco Bill Payment';

    $this->load->module('common/alert_line');
    $status_cancel = "FAIL";
    $alert_message = "\r\nPayment Cancel: Tesco\r\nRef1: $ref1\r\nRef2: $ref2\r\nAmount: $amount\r\nStatus Cancel: $status_cancel";

    $insert_log = array(
      'api_type' => $api_type,
      'action' => $action,
      'ref1' => $ref1,
      'ref2' => $ref2,
      'amount' => $amount,
      'min_amount' => 0,
      'max_amount' => 0,
      'paymenttype' => $paymenttype,
      'status' => 0,
      'request_transid' => $transid,
      'postdata' => json_encode($body),
      'adddate' => date('Y-m-d'),
      'addtime' => date('H:i:s')
    );
    $logid = $this->qrypayment->insert_log($insert_log);

    if($resCode == '0000'){
      $logconfirm = $this->qrypayment->get_log($transid, $ref1, $ref2, $api_type, 'confirm');
      if(!empty($logconfirm)){
        if($logconfirm->status == 1){
          $resCode = '0000';
          $status = 3;
          $update_log = array(
            'status' => $status
          );
          $this->qrypayment->update_log($update_log, $transid, $ref1, $ref2, $api_type, 'confirm');
          $partnerpaymentid = $logconfirm->partnerpaymentid;
          $memberpaymentid = $logconfirm->memberpaymentid;
          $amount_log = $logconfirm->amount;
          if($amount_log != $amount){
            $resCode = '1003';
          }
        }else{
          $resCode = '2001';
        }
      }else{
        $resCode = '2002';
      }
    }

    if($resCode == '0000'){
      $logreverse = $this->qrypayment->get_log($transid, $ref1, $ref2, $api_type, $action, $logid);
      if(empty($logreverse)){
        $resCode = '0000';
      }else{
        $resCode = '2001';
      }
    }

    if($resCode == '0000'){
      $partner = $this->qrypayment->get_partner($ref1);
      if(!empty($partner)){
        $resCode = '0000';
      }else{
        $resCode = '1001';
      }
    }

    if($resCode == '0000'){
      $member = $this->qrypayment->get_member($ref1, $ref2);
      if(!empty($member)){
        $resCode = '0000';
      }else{
        $resCode = '1002';
      }
    }

    if($resCode == '0000'){
      $res = $this->cancel_payment->cancel_payment_credit_member($partnerpaymentid, $memberpaymentid, $amount, $addby, $member, $partner);
      if($res['error_code'] == 'E00'){
        $resCode = '0000';
        $status_cancel = "SUCCESS";
      }else{
        $resCode = '3002';
      }
    }

    if($resCode == '0000' && $api_type == 'loan'){
      $kioskcode = "K".$member->membercode;
      $kioskid = $ref2;
      $res = $this->qrypayment->inquiry_loan($kioskcode, $kioskid);
      if($res['error_code'] == 'E00'){
        $resCode = '0000';

        $sql = "select * from payment where ID = '".$memberpaymentid."' and status=1 ";
        $qry = $this->multipledb->owstopup->query($sql)->row();
        $payment_member = (array)$qry;
        $member_fee = ($payment_member['fee'] != NULL) ? $payment_member['fee'] : 10;
        $loan_amount = $amount - $member_fee;
        $res = $this->qrypayment->cancel_loan($kioskcode, $kioskid, $loan_amount);
        if($res['error_code'] == 'E00'){
          $resCode = '0000';
          $status_cancel = "SUCCESS";
        }else{
          $resCode = '0001';
          $status_cancel = "CANCEL LOAN FAIL";
        }
      }else if($res['error_code'] == 'E15'){
        $resCode = '3001';
      }else{
        $resCode = '2003';
      }
    }

    $response = array(
      'resCode' => $resCode,
      'resMesg' => $this->resmesg->get_response_mesg($resCode),
      'tranID' => $body['tranID']
    );

    if($resCode == '0000'){
      $status = 1;
    }else{
      $status = 2;
    }

    $alert_message = "\r\nPayment Cancel: Tesco\r\nPartner Payment ID: $partnerpaymentid\r\nMember Payment ID: $memberpaymentid\r\nAmount: $amount\r\nStatus Cancel: $status_cancel";

    $this->alert_line->notify_message($alert_message);

    $update_log = array(
      'partnerpaymentid' => isset($partnerpaymentid) ? $partnerpaymentid : NULL,
      'memberpaymentid' => isset($memberpaymentid) ? $memberpaymentid : NULL,
      'response_transid' => $transid,
      'res_code' => $resCode,
      'res_message' => $this->resmesg->get_response_mesg($resCode),
      'status' => $status,
      'returndata' => json_encode($response),
      'returndate' => date('Y-m-d'),
      'returntime' => date('H:i:s')
    );
    $this->qrypayment->update_log_id($update_log, $logid);

    header('Content-Type: application/json');
    echo json_encode($response);
  }

}