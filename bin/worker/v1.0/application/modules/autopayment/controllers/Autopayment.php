<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Autopayment extends MY_Controller {

	function __construct()
	{
    parent::__construct();
    $this->load->module('common/statement');
    $this->load->module('common/cashbag');
    $this->load->module('common/payment_migrate');
    $this->load->module('common/payment_fee');
    $this->load->module('sms/smsscript');
    $this->detail_auto = 'Auto approve by Autopaymentform from payment id ';
  }

  function do_payment_member(){
    $path_file = getcwd()."/../../../log/payment/logautopayment-".date('Ymd').".txt";
    file_put_contents($path_file, "START:".date('Y-m-d H:i:s')."\r\n", FILE_APPEND);

    $date = date_create(date('Y-m-d H:i:s'));

    // date_sub($date,date_interval_create_from_date_string("5 minute"));
    $adddate = date_format($date,"Y-m-d");
    $addtime = date_format($date,"H:i:s");
    $where = array(
      'addby' => 'AutoPaymentForm',
      'fromtype' => 'member',
      'status' => 0,
      'adddate' => $adddate,
      // 'addtime <=' => $addtime
    );
    $member_payment = $this->get_member_payment($where);

    $path_file = getcwd()."/../../../log/payment/logautopayment-".date('Ymd').".txt";
    file_put_contents($path_file, "COUNT PAYMENT:".count($member_payment)."\r\n", FILE_APPEND);

    if(!empty($member_payment)){
      foreach ($member_payment as $key => $value) {
        $exbank = explode(" ", $value['detail']);
        $bank = isset($exbank[2]) ? $exbank[2] : '';
        // $payin = $this->check_smspayin($value['transferdate'], $value['transfertime'], $value['amount'], $bank);
        $payin = array();
        $payin['isOk'] = false;
        if($payin['isOk'] == true){
          $path_file = getcwd()."/../../../log/payment/logautopayment-".date('Ymd').".txt";
          file_put_contents($path_file, "APPROVED:".$value['ID']."\r\n", FILE_APPEND);

          $this->approve_credit_member($value);
          $this->del_member_fee($value);

          $value['partner_fee'] = $this->payment_fee->get_partner_fee("autosms", $value['toid'], $value['amount']);
          $payment_partner =  $this->add_payment_partner_to_admin($value);
          $this->approve_credit_partner($payment_partner);
          $this->del_partner_fee($payment_partner);

          $insert = array(
            'smspayin_id' => $payin['payin_id'],
            'smspayment_id' => $value['ID'],
            'autoby' => 'Autopaymentform',
            'autodate' => date('Y-m-d'),
            'autotime' => date('H:i:s')
          );
          $insert = $this->multipledb->owstopup->insert('list_smspayin_auto', $insert);

          $update = array(
            'status' => 1,
            'statustext' => 'Auto Check OK : '.$value['ID']
          );
          $where = array('ID' => $payin['payin_id']);
          $this->multipledb->owstopup->where($where);
          $this->multipledb->owstopup->set($update);
          $update = $this->multipledb->owstopup->update('list_smspayin');

          $data = array(
            'fromcode' => $value['fromcode'],
            'fromid' => $value['fromid'],
            'fromtype' => $value['fromtype'],
            'membercode' => $value['fromcode'],
            'memberid' => $value['fromid'],
            'partnerid' => $value['toid'],
            'orderid' => $value['ID'],
            'amount' => $value['amount'],
            'detail' => 'โอนย้ายเครดิตสมาชิกจากการเติมเครดิตผ่านระบบ Auto Payment Form ไปยังระบบใหม่',
            'fee' => $value['fee']
          );
          $res_migrate = $this->payment_migrate->migrate_member_credit($data);
          $where = array(
            'membercode' => $data['membercode'],
            'memberid' => $data['memberid']
          );

          $member = $this->multipledb->owstopup->select('*')->from('member')->where($where)->get()->row(); 
          $username = $data['membercode'].str_pad($data['memberid'],5,"0",STR_PAD_LEFT);
          if(isset($res_migrate['error_code'])){
            if($res_migrate['error_code'] == 'E00'){
              $sms = $username." อนุมัติเติมเครดิต ".number_format($data['amount'],2)." บ. คงเหลือ ".number_format($res_migrate['m_credit']+$member->credit,2)." บ. ขอบคุณค่ะ";
            }
            else{
              $sms = $username." อนุมัติเติมเครดิต ".number_format($data['amount'],2)." บ. คงเหลือ ".number_format($member->credit,2)." บ. ขอบคุณค่ะ";
            }
          }else{
            $sms = $username." อนุมัติเติมเครดิต ".number_format($data['amount'],2)." บ. คงเหลือ ".number_format($member->credit,2)." บ. ขอบคุณค่ะ";
          }

          $this->send_sms($member->mobile, $sms, $data['membercode'], $data['memberid']);

          echo "<pre>";
          echo $sms;
        }
        else{
          $create_time = $value['adddate']." ".$value['addtime'];
          $date1 = $create_time;
          $date2 = date('Y-m-d H:i:s');
           
          //Convert them to timestamps.
          $date1Timestamp = strtotime($date1);
          $date2Timestamp = strtotime($date2);
           
          //Calculate the difference.
          $difference = $date2Timestamp - $date1Timestamp;
           
          if($difference > 600){
            $path_file = getcwd()."/../../../log/payment/logautopayment-".date('Ymd').".txt";
            file_put_contents($path_file, "CANCEL:".$value['ID']."\r\n", FILE_APPEND);
            //cancel payment
            $update = array(
              'cancelby' => 'Autopaymentform', 
              'canceldate' => date('Y-m-d'), 
              'canceltime' => date('H:i:s'), 
              'status' => 2, 
            );
            $where = array('ID' => $value['ID']);
            $this->multipledb->owstopup->where($where);
            $this->multipledb->owstopup->set($update);
            $this->multipledb->owstopup->update('payment');

            $where = array(
              'membercode' => $value['fromcode'],
              'memberid' => $value['fromid']
            );

            $member = $this->multipledb->owstopup->select('*')->from('member')->where($where)->get()->row(); 
            $sms = $member->username." ไม่อนุมัติเติมเครดิตผ่านฟอร์ม AUTO ".$bank." กรุณาติดต่อ Call Center ค่ะ.";
            
            $this->send_sms($member->mobile, $sms, $value['membercode'], $value['memberid']);
          }else{
            $path_file = getcwd()."/../../../log/payment/logautopayment-".date('Ymd').".txt";
            file_put_contents($path_file, "NOT FOUND PAYIN:".$value['ID']."\r\n", FILE_APPEND);
          }
        }
      }
    }
    $path_file = getcwd()."/../../../log/payment/logautopayment-".date('Ymd').".txt";
    file_put_contents($path_file, "STOP:".date('Y-m-d H:i:s')."\r\n", FILE_APPEND);
  }

  function del_member_fee($payment){
    $fee = $payment['fee'];
    $operator = 'del';
    $array = array(
      'membercode' => $payment['fromcode'],
      'memberid' => $payment['fromid'],
      'broker' => '',
      'ordertype' => 'payment',
      'detail' => 'หักค่าบริการจากการเติมเครดิตผ่านระบบ AUTO PAYMENT FORM',
      'orderid' => $payment['ID'],
    );
    $this->statement->create_statement_member($payment['fromcode'], $payment['fromid'], $operator, $fee, $array);
  }

  function del_partner_fee($payment){
    $operator = 'del';
    $fee = $payment['fee'];
    $array = array(
      'partnercode' => 'PN',
      'partnerid' => $payment['fromid'],
      'broker' => '',
      'ordertype' => 'payment',
      'detail' => 'หักค่าบริการจากการเติมเครดิตผ่านระบบ AUTO PAYMENT FORM',
      'orderid' => $payment['ID'],
    );
    $this->statement->create_statement_partner($payment['fromid'], $operator, $fee, $array);
  }

  function get_member_payment($where, $limit=''){
    $sql = $this->multipledb->owstopup
      ->select('*')
      ->from('payment')
      ->where($where);

    if(!empty($limit)){
      $sql = $sql->limit($liimt);
    }
    $payment = $sql->get()->result_array();
    return $payment;
  }

  function check_smspayin($adddate, $addtime, $amount, $bank){
    $ret = array(
      'isOk' => false,
      'isUsed' => false,
      'payin_id' => 0
    );

    list($year, $month, $day) = explode('-', $adddate);

    list($hour, $min, $sec) = explode(':', $addtime);
    $addtime_sub1 = (new DateTime($addtime))->sub(DateInterval::createFromDateString('1 minutes'))->format('H:i:s');
    list($hour_sub1, $min_sub1, $sec_sub1) = explode(':', $addtime_sub1);
    $addtime_sub2 = (new DateTime($addtime))->sub(DateInterval::createFromDateString('2 minutes'))->format('H:i:s');
    list($hour_sub2, $min_sub2, $sec_sub2) = explode(':', $addtime_sub2);
    $addtime_sub3 = (new DateTime($addtime))->sub(DateInterval::createFromDateString('3 minutes'))->format('H:i:s');
    list($hour_sub3, $min_sub3, $sec_sub3) = explode(':', $addtime_sub3);
    $addtime_sub4 = (new DateTime($addtime))->sub(DateInterval::createFromDateString('4 minutes'))->format('H:i:s');
    list($hour_sub4, $min_sub4, $sec_sub4) = explode(':', $addtime_sub4);
    $addtime_sub5 = (new DateTime($addtime))->sub(DateInterval::createFromDateString('5 minutes'))->format('H:i:s');
    list($hour_sub5, $min_sub5, $sec_sub5) = explode(':', $addtime_sub5);
    
    $addtime_add1 = (new DateTime($addtime))->add(DateInterval::createFromDateString('1 minutes'))->format('H:i:s');
    list($hour_add1, $min_add1, $sec_add1) = explode(':', $addtime_add1);
    $addtime_add2 = (new DateTime($addtime))->add(DateInterval::createFromDateString('2 minutes'))->format('H:i:s');
    list($hour_add2, $min_add2, $sec_add2) = explode(':', $addtime_add2);
    $addtime_add3 = (new DateTime($addtime))->add(DateInterval::createFromDateString('3 minutes'))->format('H:i:s');
    list($hour_add3, $min_add3, $sec_add3) = explode(':', $addtime_add3);
    $addtime_add4 = (new DateTime($addtime))->add(DateInterval::createFromDateString('4 minutes'))->format('H:i:s');
    list($hour_add4, $min_add4, $sec_add4) = explode(':', $addtime_add4);
    $addtime_add5 = (new DateTime($addtime))->add(DateInterval::createFromDateString('5 minutes'))->format('H:i:s');
    list($hour_add5, $min_add5, $sec_add5) = explode(':', $addtime_add5);
    
    $short_year = substr($year, 2, 2);
    $thai_year = $year + 543;
    $short_thai_year = substr($thai_year, 2, 2);
    $short_thai_year = str_pad(''.$short_thai_year, 2, "0", STR_PAD_LEFT);
    $month = str_pad(''.$month, 2, "0", STR_PAD_LEFT);
    $day = str_pad(''.$day, 2, "0", STR_PAD_LEFT);
    $sms_date = $day.'/'.$month.'/'.$short_thai_year; 
    $sms_day_month = $day.'/'.$month; 
    $sms_date_dash = $day.'-'.$month.'-'.$short_year; 
    $sms_time = $hour.':'.$min;
    $sms_time_sub1 = $hour_sub1.':'.$min_sub1;
    $sms_time_sub2 = $hour_sub2.':'.$min_sub2;
    $sms_time_sub3 = $hour_sub3.':'.$min_sub3;
    $sms_time_sub4 = $hour_sub4.':'.$min_sub4;
    $sms_time_sub5 = $hour_sub5.':'.$min_sub5;
    $sms_time_add1 = $hour_add1.':'.$min_add1;
    $sms_time_add2 = $hour_add2.':'.$min_add2;
    $sms_time_add3 = $hour_add3.':'.$min_add3;
    $sms_time_add4 = $hour_add4.':'.$min_add4;
    $sms_time_add5 = $hour_add5.':'.$min_add5;
    
    // sms payin
    $isBank = false;
    if($bank == 'ธนาคารกสิกรไทย'){
      // 25/04/59 16:28 บชX278322X รับโอนจากX220874X 10.00 คงเหลือ408867.57บ >> K-Mobile
      // 25/04/59 14:51 บชX230018X รับโอนจากX281830X 7000.00 คงเหลือ100112.01บ >> k-cyber
      $payin_sql = "select * from list_smspayin where 
      (sms like '".$sms_date." ".$sms_time." บชX278322X รับโอนจาก%'
      or sms like '".$sms_date." ".$sms_time_sub1." บชX278322X รับโอนจาก%'
      or sms like '".$sms_date." ".$sms_time_add1." บชX278322X รับโอนจาก%'
      or sms like '".$sms_date." ".$sms_time_sub2." บชX278322X รับโอนจาก%'
      or sms like '".$sms_date." ".$sms_time_add2." บชX278322X รับโอนจาก%'
      or sms like '".$sms_date." ".$sms_time_sub3." บชX278322X รับโอนจาก%'
      or sms like '".$sms_date." ".$sms_time_add3." บชX278322X รับโอนจาก%'
      or sms like '".$sms_date." ".$sms_time_sub4." บชX278322X รับโอนจาก%'
      or sms like '".$sms_date." ".$sms_time_add4." บชX278322X รับโอนจาก%'
      or sms like '".$sms_date." ".$sms_time_sub5." บชX278322X รับโอนจาก%'
      or sms like '".$sms_date." ".$sms_time_add5." บชX278322X รับโอนจาก%'
      )
      or 
      (sms like '".$sms_date." ".$sms_time." บชX230018X รับโอนจาก%'
      or sms like '".$sms_date." ".$sms_time_sub1." บชX230018X รับโอนจาก%'
      or sms like '".$sms_date." ".$sms_time_add1." บชX230018X รับโอนจาก%'
      or sms like '".$sms_date." ".$sms_time_sub2." บชX230018X รับโอนจาก%'
      or sms like '".$sms_date." ".$sms_time_add2." บชX230018X รับโอนจาก%'
      or sms like '".$sms_date." ".$sms_time_sub3." บชX230018X รับโอนจาก%'
      or sms like '".$sms_date." ".$sms_time_add3." บชX230018X รับโอนจาก%'
      or sms like '".$sms_date." ".$sms_time_sub4." บชX230018X รับโอนจาก%'
      or sms like '".$sms_date." ".$sms_time_add4." บชX230018X รับโอนจาก%'
      or sms like '".$sms_date." ".$sms_time_sub5." บชX230018X รับโอนจาก%'
      or sms like '".$sms_date." ".$sms_time_add5." บชX230018X รับโอนจาก%'
      )
      or 
      (sms like '".$sms_date." ".$sms_time." บชX278322X เงินเข้า%'
      or sms like '".$sms_date." ".$sms_time_sub1." บชX278322X เงินเข้า%'
      or sms like '".$sms_date." ".$sms_time_add1." บชX278322X เงินเข้า%'
      or sms like '".$sms_date." ".$sms_time_sub2." บชX278322X เงินเข้า%'
      or sms like '".$sms_date." ".$sms_time_add2." บชX278322X เงินเข้า%'
      or sms like '".$sms_date." ".$sms_time_sub3." บชX278322X เงินเข้า%'
      or sms like '".$sms_date." ".$sms_time_add3." บชX278322X เงินเข้า%'
      or sms like '".$sms_date." ".$sms_time_sub4." บชX278322X เงินเข้า%'
      or sms like '".$sms_date." ".$sms_time_add4." บชX278322X เงินเข้า%'
      or sms like '".$sms_date." ".$sms_time_sub5." บชX278322X เงินเข้า%'
      or sms like '".$sms_date." ".$sms_time_add5." บชX278322X เงินเข้า%'
      )
      and
      status = 0
      ";
      $isBank = true;
    }
    else if($bank == 'ธนาคารกรุงไทย'){
      // 28-04-16@22:00 บช X72836X:เงินเข้า  250.00 บ. ใช้ได้ 101,886.55 บ.
      // $chkbank = "9810728360";
      $payin_sql = "select * from list_smspayin where 
      (
      sms like '".$sms_date_dash."@".$sms_time." บช X72836X:เงินเข้า%' 
      or sms like '".$sms_date_dash."@".$sms_time_sub1." บช X72836X:เงินเข้า%' 
      or sms like '".$sms_date_dash."@".$sms_time_add1." บช X72836X:เงินเข้า%' 
      or sms like '".$sms_date_dash."@".$sms_time_sub2." บช X72836X:เงินเข้า%' 
      or sms like '".$sms_date_dash."@".$sms_time_add2." บช X72836X:เงินเข้า%' 
      or sms like '".$sms_date_dash."@".$sms_time_sub3." บช X72836X:เงินเข้า%' 
      or sms like '".$sms_date_dash."@".$sms_time_add3." บช X72836X:เงินเข้า%' 
      or sms like '".$sms_date_dash."@".$sms_time_sub4." บช X72836X:เงินเข้า%' 
      or sms like '".$sms_date_dash."@".$sms_time_add4." บช X72836X:เงินเข้า%' 
      or sms like '".$sms_date_dash."@".$sms_time_sub5." บช X72836X:เงินเข้า%' 
      or sms like '".$sms_date_dash."@".$sms_time_add5." บช X72836X:เงินเข้า%' 
      )
      and
      status = 0
      ";
      $isBank = true;   
    }
    else if($bank == 'ธนาคารไทยพาณิชย์'){
      // SCB
      // เงินโอนจาก PRASONG PHANP ยอด 3,000.00 บ. เข้าบ/ชx376989 วันที่ 28/04@06:16  ใช้ได้ 84,878.39 บ.
      // $chkbank = "0324376989";

      $payin_sql = "select * from list_smspayin where 
      (
      sms like '%เข้าบ/ชx376989 วันที่ ".$sms_day_month."@".$sms_time."%' 
      or sms like '%เข้าบ/ชx376989 วันที่ ".$sms_day_month."@".$sms_time_sub1."%' 
      or sms like '%เข้าบ/ชx376989 วันที่ ".$sms_day_month."@".$sms_time_add1."%' 
      or sms like '%เข้าบ/ชx376989 วันที่ ".$sms_day_month."@".$sms_time_sub2."%' 
      or sms like '%เข้าบ/ชx376989 วันที่ ".$sms_day_month."@".$sms_time_add2."%' 
      or sms like '%เข้าบ/ชx376989 วันที่ ".$sms_day_month."@".$sms_time_sub3."%' 
      or sms like '%เข้าบ/ชx376989 วันที่ ".$sms_day_month."@".$sms_time_add3."%' 
      or sms like '%เข้าบ/ชx376989 วันที่ ".$sms_day_month."@".$sms_time_sub4."%' 
      or sms like '%เข้าบ/ชx376989 วันที่ ".$sms_day_month."@".$sms_time_add4."%' 
      or sms like '%เข้าบ/ชx376989 วันที่ ".$sms_day_month."@".$sms_time_sub5."%' 
      or sms like '%เข้าบ/ชx376989 วันที่ ".$sms_day_month."@".$sms_time_add5."%' 
      )
      and
      status = 0
      ";
      $isBank = true;   
    }
    else{
      // not found bank
      $ret = array(
        'isOk' => false,
        'isUsed' => false,
        'payin_id' => 0
      );
      $isBank = false;
    }

    if($isBank){
      // continue
      $payin_id = 0;

      $isUsed = false;
      $isOk = false;
      $smspayin_date = "";
      $smspayin_time = "";

      $payin_row = $this->multipledb->owstopup->query($payin_sql)->result_array();
      
      foreach ($payin_row as $key => $value) {
       
        $payin_id = $value['ID'];
        $payin_sms = $value['sms'];

        // check amount
        if($bank == 'ธนาคารกสิกรไทย'){
          // 25/04/59 16:28 บชX278322X รับโอนจากX220874X 10.00 คงเหลือ408867.57บ >> K-Mobile
          // 25/04/59 14:51 บชX230018X รับโอนจากX281830X 7000.00 คงเหลือ100112.01บ >> k-cyber
          $payin_cut_sms = explode(' ', $payin_sms);
          if(count($payin_cut_sms)>=5){
            $smspayin_date = $payin_cut_sms[0];
            $smspayin_time = $payin_cut_sms[1];
            $payin_amount = "";
            $pos = strpos($payin_sms, 'รับโอนจาก');
            if($pos !== false){
              // found
              $payin_amount = $payin_cut_sms[4];                  
            }
            else{
              $pos = strpos($payin_sms, 'เงินเข้า');
              if($pos !== false){
                // found                      
                $payin_amount = $payin_cut_sms[3];    
              }
              else{
                // error
              }
            }

            if($payin_amount != ""){
              // index array for amount in sms
              $payin_amount = str_replace("เงินเข้า", "", $payin_amount);
              $payin_amount = str_replace("บ", "", $payin_amount);
              $payin_amount = str_replace(",", "", $payin_amount);

              if($payin_amount == $amount){
                // ok (is equal)
                $auto_sql = "select * from list_smspayin_auto where 
                  smspayin_id = $payin_id
                  ";
                $auto_qry = $this->multipledb->owstopup->query($auto_sql)->result_array();
                $auto_cnt_row = count($auto_qry);
                if($auto_cnt_row > 0){
                  // found 
                  // used
                  $isUsed = true;
                }
                else{
                  // not found
                  $isOk = true;
                  $isUsed = false;
                  break;
                }
              }
              else{
                // not equal
                // skip to error
                $isOk = false;
              }
            }
            else{
              // not found amount
              $isOk = false;
            }
          }
          else{
            // can not split sms
            // skip to error
            $isOk = false;
          }
        }
        else if($bank == 'ธนาคารกรุงไทย'){
          // ktb
          // 28-04-16@22:00 บช X72836X:เงินเข้า  250.00 บ. ใช้ได้ 101,886.55 บ.
          $smspayin_cut = explode(' ', $payin_sms);
          if(count($smspayin_cut)>0){
            if(strpos($smspayin_cut[0], '@') !== false){
              list($smspayin_date, $smspayin_time) = explode('@', $smspayin_cut[0]);
            }
          }
          $payin_cut_sms = explode('เงินเข้า', $payin_sms);
          if(count($payin_cut_sms)>1){
            $payin_cut_amount = explode('บ.', $payin_cut_sms[1]);
            if(count($payin_cut_amount)>1){
              $payin_amount = $payin_cut_amount[0];
              $payin_amount = str_replace(' ', '', $payin_amount);
              echo $payin_amount = str_replace(',', '', $payin_amount);
              if($payin_amount != ''){
                if($payin_amount == $amount){
                  echo '<br>equal:'.$amount;
                  // ok (is equal)
                  $auto_sql = "select * from list_smspayin_auto where 
                    smspayin_id = $payin_id
                    ";
                  $auto_qry = $this->multipledb->owstopup->query($auto_sql)->result_array();
                  $auto_cnt_row = count($auto_qry);
                  if($auto_cnt_row > 0){
                    // found 
                    // used
                    $isUsed = true;
                  }
                  else{
                    // not found
                    $isOk = true;
                    $isUsed = false;
                    break;
                  }
                }
                else{
                  // not equal
                  // skip to error
                  $isOk = false;
                }
              }
              else{             
                // not find amount
                // skip to error
                $isOk = false;
              }
            }
            else{
              // not find amount
              // skip to error
              $isOk = false;
            }
          }
          else{
            // not detect sms format
            // skip to error
            $isOk = false;
          }       
        }
        else if($bank == 'ธนาคารไทยพาณิชย์'){
          // scb
          // เงินโอนจาก PRASONG PHANP ยอด 3,000.00 บ. เข้าบ/ชx376989 วันที่ 28/04@06:16  ใช้ได้ 84,878.39 บ.
          $smspayin_cut = explode('วันที่ ', $payin_sms);
          if(count($smspayin_cut)>1){       
            $smspayin_cut = explode(' ', $smspayin_cut[1]);
            if(count($smspayin_cut)>0){
              if(strpos($smspayin_cut[0], '@') !== false){
                list($smspayin_date, $smspayin_time) = explode('@', $smspayin_cut[0]);
              }
            }
          }
          $payin_cut_sms = explode('ยอด', $payin_sms);
          if(count($payin_cut_sms)>1){
            $payin_cut_amount = explode('บ.', $payin_cut_sms[1]);
            if(count($payin_cut_amount)>1){
              $payin_amount = $payin_cut_amount[0];
              $payin_amount = str_replace(' ', '', $payin_amount);
              $payin_amount = str_replace(',', '', $payin_amount);
              if($payin_amount != ''){
                if($payin_amount == $amount){
                  // ok (is equal)
                  $auto_sql = "select * from list_smspayin_auto where 
                    smspayin_id = $payin_id
                    ";
                  $auto_qry = $this->multipledb->owstopup->query($auto_sql)->result_array();
                  $auto_cnt_row = count($auto_qry);
                  if($auto_cnt_row > 0){
                    // found 
                    // used
                    $isUsed = true;
                  }
                  else{
                    // not found
                    $isOk = true;
                    $isUsed = false;
                    break;
                  }
                }
                else{
                  // not equal
                  // skip to error
                  $isOk = false;
                }
              }
              else{             
                // not find amount
                // skip to error
                $isOk = false;
              }
            }
            else{
              // not find amount
              // skip to error
              $isOk = false;
            }
          }
          else{
            // not detect sms format
            // skip to error
            $isOk = false;
          }       
        }

      }
      

      $ret = array(
        'isOk' => $isOk,
        'isUsed' => $isUsed,
        'payin_id' => $payin_id,
        'smspayin_date' => $smspayin_date,
        'smspayin_time' => $smspayin_time
      );
    }

    return $ret;
  }

  function approve_credit_member($payment){
    $date=date('Y-m-d');
    $time=date('H:i:s');
 
    //check member inactive
    $member_close = false;
    $where = array(
      'membercode' => $payment['fromcode'], 
      'memberid' => $payment['fromid'],
      'credit <' => 10,
      'status' => 1,
    );
    $get_member_inactive = $this->multipledb->owstopup
      ->select('*')
      ->from('member')
      ->where($where)
      ->get()
      ->result_array();
    if(count($get_member_inactive) > 0){
      $member_close = true;
    }

    //update payment
    $update = array(
      'approveby' => 'Autopaymentform', 
      'approvedate' => date('Y-m-d'), 
      'approvetime' => date('H:i:s'), 
      'status' => 1, 
    );
    $where = array('ID' => $payment['ID']);
    $this->multipledb->owstopup->where($where);
    $this->multipledb->owstopup->set($update);
    $this->multipledb->owstopup->update('payment');

    //statement_member
    $operator = 'add';
    $array = array(
      'membercode' => $payment['fromcode'],
      'memberid' => $payment['fromid'],
      'broker' => '',
      'ordertype' => 'payment',
      'detail' => "เครดิต : อนุมัติเครดิต orderid ".$payment['ID'],
      'orderid' => $payment['ID'],
    );
    $this->statement->create_statement_member($payment['fromcode'], $payment['fromid'], $operator, $payment['amount'], $array);

    //cashbag_partner
    $partnerid = $payment['toid'];
    $bank = $payment['channel'];
    $operator = 'add';
    $amount = $payment['amount'];
    $array = array(
      'partnercode' => $payment['tocode'],
      'partnerid' => $payment['toid'],
      'bank' => 'อื่นๆ',
      'cashtype' => 'payment',
      'membercode' => $payment['tocode'],
      'memberid' => $payment['toid'],
      'orderid' => $payment['ID'],
      'detail' => "เครดิต : อนุมัติเครดิต orderid ".$payment['ID'],
      'amount' => $payment['amount'],
    );
    $this->cashbag->create_cashbag_partner($partnerid, $bank, $operator, $amount, $array);

    //clear call
    $update = array(
      'callone' => 0,
      'calltwo' => 0,
      'callthree' => 0
    );
    if($member_close == true){
      $update['status'] =0;
      $update['useweb'] = 0;
      $update['usetopupmobile'] = 0;
      $update['usetopupcard'] = 0; 
      $update['usetopuppublic'] = 0;
    }
    $where = array(
      'membercode' => $payment['fromcode'], 
      'memberid' => $payment['fromid'],
    );
    $this->multipledb->owstopup->where($where);
    $this->multipledb->owstopup->set($update);
    $this->multipledb->owstopup->update('member');
        
    //log_payment
    $insert = array(
      'paymentid' => $payment['ID'], 
      'action' => 'approve', 
      'addby' => $payment['addby'], 
      'adddate' => date('Y-m-d'), 
      'addtime' => date('H:i:s'),
    );
    $this->multipledb->owstopup->insert('log_payment', $insert);
  }

  function add_payment_partner_to_admin($payment){

    $where = array('ID' => $payment['ID']);

    $where = array('partnerid' => $payment['toid']);
    $get_partner = $this->multipledb->owstopup
      ->select('*')
      ->from('partner')
      ->where($where)
      ->get()
      ->row();

    $account_bank = (isset($payment['account_bank'])) ? $payment['account_bank'] : '';
    $transfer_date = (isset($payment['transfer_date'])) ? $payment['transfer_date'] : date('Y-m-d');
    $transfer_time = (isset($payment['transfer_time'])) ? $payment['transfer_time'] : date('H:i:s');

    $insert = array(
      'fromcode' => 'PN',
      'fromid' => $payment['toid'],
      'fromtype' => 'partner',
      'banktransfer' => 'อื่นๆ',
      'tocode' => 'AM',
      'toid' => 1,
      'totype' => 'admin',
      'channel' => $payment['channel'],
      'paymentauto' => 0,
      'detail' => $this->detail_auto.$payment['ID'].' '.$account_bank,
      'amount' => $payment['amount'],
      'creditbefore' => $get_partner->credit,
      'creditafter' => ($get_partner->credit) + ($payment['amount']),
      'addby' => $payment['addby'],
      'transferdate' => $transfer_date,
      'transfertime' => $transfer_time,
      'adddate' => date('Y-m-d'),
      'addtime' => date('H:i:s'),
      'fee' => $payment['partner_fee'],
    );
    $res = $this->insert_payment($insert);
    $payment_partner = $insert;
    $payment_partner['ID'] = $res['id'];
    return $payment_partner;
  }

  function approve_credit_partner($payment){
    $date=date('Y-m-d');
    $time=date('H:i:s');

    //update payment
    $update = array(
      'approveby' => $payment['addby'], 
      'approvedate' => date('Y-m-d'), 
      'approvetime' => date('H:i:s'), 
      'status' => 1, 
    );
    $where = array('ID' => $payment['ID']);
    $this->multipledb->owstopup->where($where);
    $this->multipledb->owstopup->set($update);
    $this->multipledb->owstopup->update('payment');

    //statement_partner
    $partnerid = $payment['fromid'];
    $operator = 'add';
    $amount = $payment['amount'];
    $array = array(
      'partnercode' => 'PN',
      'partnerid' => $payment['fromid'],
      'broker' => '',
      'ordertype' => 'payment',
      'detail' => "เครดิต : อนุมัติเครดิต orderid ".$payment['ID'],
      'orderid' => $payment['ID'],
    );
    $this->statement->create_statement_partner($partnerid, $operator, $amount, $array);

    //cashbag_admin
    $partnerid = $payment['toid'];
    $bank = $payment['channel'];
    $operator = 'add';
    $amount = $payment['amount'];
    $array = array(
      'bank' => $payment['channel'],
      'cashtype' => 'payment',
      'partnercode' => $payment['fromcode'],
      'partnerid' => $payment['fromid'],
      'orderid' => $payment['ID'],
      'detail' => "เครดิต : อนุมัติเครดิต orderid ".$payment['ID'],
      'amount' => $payment['amount'],
    );
    $this->cashbag->create_cashbag_admin($bank, $operator, $amount, $array);

    //cashbag_partner
    $partnerid = $payment['toid'];
    $bank = $payment['channel'];
    $operator = 'del';
    $amount = $payment['amount'];
    $array = array(
      'partnercode' => $payment['tocode'],
      'partnerid' => $payment['toid'],
      'bank' => 'อื่นๆ',
      'cashtype' => 'payment',
      'membercode' => '',
      'memberid' => 0,
      'orderid' => $payment['ID'],
      'detail' => "เครดิต : อนุมัติเครดิต orderid ".$payment['ID'],
      'amount' => $payment['amount'],
    );
    $this->cashbag->create_cashbag_partner($partnerid, $bank, $operator, $amount, $array);

    //clear call
    $update = array(
      'callone' => 0,
      'calltwo' => 0,
      'callthree' => 0,
    );
    $where = array('partnerid' => $payment['fromid']);
    $this->multipledb->owstopup->where($where);
    $this->multipledb->owstopup->set($update);
    $this->multipledb->owstopup->update('partner');

    //log_payment
    $insert = array(
      'paymentid' => $payment['ID'], 
      'action' => 'approve', 
      'addby' => $payment['addby'], 
      'adddate' => date('Y-m-d'), 
      'addtime' => date('H:i:s'), 
    );
    $this->multipledb->owstopup->insert('log_payment', $insert);
  }

  function insert_payment($insert){
    $query = $this->multipledb->owstopup->insert('payment', $insert);
    if($query){
      $id = $this->multipledb->owstopup->insert_id();
      $ret['status'] = true;
      $ret['id'] = $id;
    }
    else{
      $ret['status'] = false;
    }
    return $ret;
  }

  function send_sms($member_mobile,$message,$membercode,$memberid){
    $obj = new ArrayObject();
    $obj->mobile = $member_mobile;
    // $obj->mobile = '0957386086';
    $obj->message = $message;
    if($obj->mobile != '' && strlen($obj->mobile) == 10){
      $response = $this->smsscript->send_sms($obj);
      // echo "send sms : ".$message;
    }    
  }

}