﻿<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Payment_migrate extends MY_Controller {

  function __construct() {
    parent::__construct();
    $this->load->model('common/payment_migrate_m');
    $this->url_api_migrate = $this->url_v3.'/api/v1.0/api_migrate_json/app_api';
  }

  function migrate_member_credit($data){

    $fromcode = $data['fromcode'];
    $fromid = $data['fromid'];
    $fromtype = $data['fromtype'];
    $membercode = $data['membercode'];
    $memberid = $data['memberid'];
    $partnerid = $data['partnerid'];

    $orderid = $data['orderid'];
    $amount = $data['amount'];
    $detail = $data['detail'];

    $fee = !empty($data['fee']) ? $data['fee'] : 0;
    $amount = $amount - $fee;

    //(amount * kiosk_v3/kiosk_v2) --------------------------------------------
    /*
    $where = array(
      'membercode' => $membercode,
      'memberid' => $memberid,
    );

    $kiosk_v2 = 0;
    $get_tk = $this->payment_migrate_m->get_total_kiosk($where);
    if(count($get_tk) > 0){
      $kiosk_v2 = $get_tk->total;
    }

    $kiosk_v3 = 0;
    $get_lm = $this->payment_migrate_m->get_total_list_migrate($where);
    if(count($get_lm) > 0){
      $kiosk_v3 = $get_lm->total;
    }

    if($kiosk_v2>0 && $kiosk_v3>0){
      $amount =  $amount * $kiosk_v3/$kiosk_v2; 
      $data['amount'] = $amount;
    }
    */
    //--------------------------------------------------------------------------

    //Top2sim
    $isTop2sim = $this->check_member_is_topuptosim_v2($membercode, $memberid);

    //member is not topuptosim
    if(!$isTop2sim){
      $res = $this->get_member_v3($data);
      if($res != 'ERROR'){
        if($res['error_code'] == 'E00'){
          $res = $this->migrate_member_credit_to_v3($data);
          if($res != 'ERROR'){
            if($res['error_code'] == 'E00'){

              //statement_member
              $operator = 'del';
              $array = array(
                'membercode' => $membercode,
                'memberid' => $memberid,
                'broker' => '',
                'ordertype' => 'migrate',
                'detail' => $detail,
                'orderid' => $orderid,
              );
              $this->statement->create_statement_member($membercode, $memberid, $operator, $amount, $array);

              //statement_partner
              $operator = 'del';
              $array = array(
                'partnercode' => 'PN',
                'partnerid' => $partnerid,
                'broker' => '',
                'ordertype' => 'migrate',
                'detail' => $detail,
                'orderid' => $orderid,
              );
              $this->statement->create_statement_partner($partnerid, $operator, $amount, $array);

              //ปิดแจ้งเตือน เครดิต member ใกล้หมด หลัง migrate เสร็จ
              $update = array(
                'callone' => 1, 
                'calltwo' => 1, 
                'callthree' => 1,
                'alertone' => -1000,
                'alerttwo' => -1000,
                'alertthree' => -1000,
              );
              $where = array(
                'membercode' => $membercode, 
                'memberid' => $memberid, 
              );
              $this->payment_migrate_m->update_member($update, $where);
            }
          }
        }
      }
    }
    else{
      $res = array(
        "error_code" => 'E99',
        "error_text" => 'Member is Topuptosim cannot migrate credit',
      );
    }
    
    return $res;
  }

  function get_member_v3($data){
    $post_data = array(
      'service' => 'getmemberdetail',
      'key' => $this->key_v3,
      'fromcode' => $data['fromcode'],
      'fromid' => $data['fromid'],
      'fromtype' => $data['fromtype'],
    );

    $ch = curl_init($this->url_api_migrate);                                                                      
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($post_data));
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
     'Content-Type: application/json', 
     'Content-Length: ' . strlen(json_encode($post_data)))
    );
    $content = curl_exec($ch);
    if($content){
      $return = json_decode($content, true);
    }else{
      $return = 'ERROR';
    }
    curl_close($ch);
    return $return;
  }

  function migrate_member_credit_to_v3($data){
    $post_data = array(
     'service' => 'migratecredit',
     'key' => $this->key_v3,
     'fromcode' => $data['fromcode'],
     'fromid' => $data['fromid'],
     'fromtype' => $data['fromtype'],
     'amount' => $data['amount'],
     'orderid' =>$data['orderid'],
    );

    $ch = curl_init($this->url_api_migrate);                                                                      
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($post_data));
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
     'Content-Type: application/json', 
     'Content-Length: ' . strlen(json_encode($post_data)))
    );
    $content = curl_exec($ch);
    if($content){
      $return = json_decode($content, true);
    }else{
      $return = 'ERROR';
    }
    curl_close($ch);
    return $return;
  }

  function check_member_is_topuptosim_v2($membercode, $memberid){
    $where = array(
      'membercode' => $membercode,
      'memberid' => $memberid,
      'feeinternetset' => 1,
    );
    $get_member = $this->payment_migrate_m->get_member($where);
    if(count($get_member) > 0){
      return true;
    }
    else{
      return false;
    }
  }

}
