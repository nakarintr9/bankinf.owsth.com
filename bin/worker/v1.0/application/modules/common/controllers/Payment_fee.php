﻿<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Payment_fee extends MY_Controller {

  function __construct() {
    parent::__construct();
  }

  function get_partner_fee($bankcode, $partnerid, $amount){
    $config_fee['scb']['partner_mc'] = 0;
    $config_fee['scb']['partner_non_mc'] = 10;
    $config_fee['ktb']['partner_mc'] = 0;
    $config_fee['ktb']['partner_non_mc'] = 10;
    $config_fee['autosms']['partner_mc'] = 0;
    $config_fee['autosms']['partner_non_mc'] = 10;

    $sql = "SELECT * FROM partner WHERE partnerid=".$partnerid." AND firstname LIKE '%media%' ";
    $qry=$this->multipledb->owstopup->query($sql)->row();
    if(!empty($row)){
      if($amount >= 5000){
        return 0;
      }else{
        return $config_fee[$bankcode]['partner_mc'];
      }
    }else{
      return $config_fee[$bankcode]['partner_non_mc'];
    }
  }

  function get_member_fee($bankcode, $partnerid, $amount){
    $config_fee['scb']['member_mc'] = 10;
    $config_fee['scb']['member_non_mc'] = 10;
    $config_fee['ktb']['member_mc'] = 10;
    $config_fee['ktb']['member_non_mc'] = 10;
    $config_fee['autosms']['member_mc'] = 10;
    $config_fee['autosms']['member_non_mc'] = 10;

    $sql = "SELECT * FROM partner WHERE partnerid=".$partnerid." AND firstname LIKE '%media%' ";
    $qry=$this->multipledb->owstopup->query($sql)->row();
    if(!empty($row)){
      if($amount >= 5000){
        return 0;
      }else{
        return $config_fee[$bankcode]['member_mc'];
      }
    }else{
      return $config_fee[$bankcode]['member_non_mc'];
    }
  }

}
