﻿<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Cashbag extends MY_Controller {

  function __construct() {
    parent::__construct();

    $this->load->model('cashbag_m');
  }

  function create_cashbag_admin($bank, $operator, $amount, $array){

    //creditbank1 - creditbank8
    $bank_credit = 'credit'.$this->getCashbank($bank);

    //trans
    $this->multipledb->owstopup->trans_begin();

    //Select Bank Credit (For Update)
    $quote = $this->multipledb->owstopup->query("SELECT ".$bank_credit." FROM admin WHERE ID=1 FOR UPDATE ;")->row($bank_credit);
    $balance = 0;
    $is_credit_enough = true;

    //Check action
    if($operator == 'change'){
      if($quote >= $amount){
        $add_credit = 0;
        $del_credit = $quote - $amount;
      }
      else if($quote < $amount){
        $add_credit = $amount - $quote;
        $del_credit = 0;
      }
      $balance = $amount;
    }
    else if($operator == 'add'){
      $balance = $quote + $amount;
      $add_credit = $amount;
      $del_credit = 0;
    }
    else if($operator == 'del'){
      $balance = $quote - $amount;
      $add_credit = 0;
      $del_credit = $amount;
    }

    $return = false;
    if($is_credit_enough){

      //Update Bank Credit 
      $update_ok = $this->multipledb->owstopup->query("UPDATE admin SET ".$bank_credit." = ".$balance." WHERE ID=1;");

      //Insert Statement Cashbag Partner
      if($update_ok){
        $array['qouted'] = $quote;
        $array['addcredit'] = $add_credit;
        $array['delcredit'] = $del_credit;
        $array['balance'] = $balance;
        $array['adddate'] = date('Y-m-d');
        $array['addtime'] = date('H:i:s');

        $ret = $this->cashbag_m->insert_cashbag_admin($array);

        if($ret['status']){
          $this->multipledb->owstopup->trans_commit();
          $return = true;
        }
        else{
          $this->multipledb->owstopup->trans_rollback();
        }
      }
      else{
        $this->multipledb->owstopup->trans_rollback();
      }
    }

    return $return;
  }

  function create_cashbag_partner($partnerid, $bank, $operator, $amount, $array){

    //creditbank1 - creditbank8
    $bank_credit = ($bank!='') ? 'credit'.$this->getCashbank($bank) : 'creditbank8';

    //trans
    $this->multipledb->owstopup->trans_begin();

    //Select Bank Credit (For Update)
    $quote = $this->multipledb->owstopup->query("SELECT ".$bank_credit." FROM partner WHERE partnerid = '".$partnerid."' FOR UPDATE ;")->row($bank_credit);
    $balance = 0;
    $is_credit_enough = true;

    //Check action
    if($operator == 'change'){
      if($quote >= $amount){
        $add_credit = 0;
        $del_credit = $quote - $amount;
      }
      else if($quote < $amount){
        $add_credit = $amount - $quote;
        $del_credit = 0;
      }
      $balance = $amount;
    }
    else if($operator == 'add'){
      $balance = $quote + $amount;
      $add_credit = $amount;
      $del_credit = 0;
    }
    else if($operator == 'del'){
      $balance = $quote - $amount;
      $add_credit = 0;
      $del_credit = $amount;
    }

    $return = false;
    if($is_credit_enough){

      //Update Bank Credit 
      $update_ok = $this->multipledb->owstopup->query("UPDATE partner SET ".$bank_credit." = ".$balance." WHERE partnerid = '".$partnerid."';");

      //Insert Statement Cashbag Partner
      if($update_ok){
        $array['qouted'] = $quote;
        $array['addcredit'] = $add_credit;
        $array['delcredit'] = $del_credit;
        $array['balance'] = $balance;
        $array['adddate'] = date('Y-m-d');
        $array['addtime'] = date('H:i:s');

        $ret = $this->cashbag_m->insert_cashbag_partner($array);

        if($ret['status']){
          $this->multipledb->owstopup->trans_commit();
          $return = true;
        }
        else{
          $this->multipledb->owstopup->trans_rollback();
        }
      }
      else{
        $this->multipledb->owstopup->trans_rollback();
      }
    }

    return $return;
  }

  function getCashbank($bank){
    if($bank=='ธนาคารกรุงศรี'){return 'bank1';}
    else if($bank=='ธนาคารไทยพาณิชย์'){return 'bank2';}
    else if($bank=='ธนาคารกรุงเทพ'){return 'bank3';}
    else if($bank=='ธนาคารกสิกรไทย'){return 'bank4';}
    else if($bank=='ธนาคารทหารไทย'){return 'bank5';}
    else if($bank=='ธนาคารกรุงไทย'){return 'bank6';}
    else if($bank=='ธนาคาร ธกส'){return 'bank7';}
    else if($bank=='อื่นๆ'){return 'bank8';}
  }

}
