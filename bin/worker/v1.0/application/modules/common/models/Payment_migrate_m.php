<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Payment_migrate_m extends CI_Model 
{
  public function __construct()
	{
		parent::__construct();
  }

  public function get_member($where){
    $query = $this->multipledb->owstopup
      ->from('member')
      ->where($where)
      ->get()
      ->row();
    return $query;
  }

  public function update_member($update, $where){
    $this->multipledb->owstopup->set($update);
    $this->multipledb->owstopup->where($where);
    $ret = $this->multipledb->owstopup->update('member');
    return $ret;
  } 

  public function get_total_kiosk($where){
    $query = $this->multipledb->owstopup
      ->select('COUNT(ID) as total')
      ->from('kiosk')
      ->where($where)
      ->get()
      ->row();
    return $query;
  }

  public function get_total_list_migrate($where){
    $query = $this->multipledb->owstopup
      ->select('COUNT(ID) as total')
      ->from('list_migrate')
      ->where($where)
      ->get()
      ->row();
    return $query;
  }

}