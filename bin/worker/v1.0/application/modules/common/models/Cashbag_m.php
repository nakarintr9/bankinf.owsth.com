<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Cashbag_m extends CI_Model 
{
  public function __construct()
	{
		parent::__construct();
  }

  public function insert_cashbag_admin($insert){
    if($this->multipledb->owstopup->insert('cashbag_admin', $insert)){
      $ret['status'] = true;
      $ret['id'] = $this->multipledb->owstopup->insert_id();
    }    
    else{
      $ret['status'] = false;
    }
    return $ret;
  }

  public function insert_cashbag_partner($insert){
    if($this->multipledb->owstopup->insert('cashbag_partner', $insert)){
      $ret['status'] = true;
      $ret['id'] = $this->multipledb->owstopup->insert_id();
    }    
    else{
      $ret['status'] = false;
    }
    return $ret;
  }

  public function get_last_cashbag_admin($where){
    $query = $this->multipledb->owstopup
      ->from('cashbag_admin')
      ->where($where)
      ->order_by('ID','desc')
      ->limit(1)
      ->get()
      ->row();
    return $query;
  }

}