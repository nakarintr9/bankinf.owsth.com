<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Statement_m extends CI_Model 
{
  public function __construct()
	{
		parent::__construct();
  }

  public function insert_statement_partner($insert){
    if($this->multipledb->owstopup->insert('statement_partner', $insert)){
      $ret['status'] = true;
      $ret['id'] = $this->multipledb->owstopup->insert_id();
    }    
    else{
      $ret['status'] = false;
    }
    return $ret;
  }

  public function insert_statement_member($insert){
    if($this->multipledb->owstopup->insert('statement_member', $insert)){
      $ret['status'] = true;
      $ret['id'] = $this->multipledb->owstopup->insert_id();
    }    
    else{
      $ret['status'] = false;
    }
    return $ret;
  }

}