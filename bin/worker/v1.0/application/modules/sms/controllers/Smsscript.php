<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Smsscript extends MY_Controller
{
  function __construct()
  {
    parent::__construct();
  }

  public function send_sms($obj){
    $this->module = 'Smsscript/send_sms';
    $this->order_id = rand(1000,9999).date('Ymdhis');
    $url = $this->url_gw."/brkrcore/v1.0/service/sendsms";
    $data = array(
      'key' => $this->key_gw,
      'username' => $this->username_gw,
      'mobile' => $obj->mobile,
      'message' => $obj->message,
      'dest_ref_id' => $this->order_id,
      'callback_url' => $this->base_url."/bankinf/v1.0/sms/smsscript/callback_sms"
    );
    $this->apilog->log($this->module,'SEND', json_encode($data), $this->order_id);
    $response = $this->librarycurl->curl($url, json_encode($data));
    $this->apilog->log($this->module,'RECEIVE', json_encode($response), $this->order_id);
    return $response;
  }

  public function callback_sms(){
    $this->module = 'Smsscript/callback_sms';
    $this->order_id = rand(10000,99999).date('Ymdhis');
    $callback = file_get_contents("php://input");
    $this->apilog->log($this->module,'REQUEST', $callback, $this->order_id);
    $response = "OK";
    $this->apilog->log($this->module,'RESPONSE', $response, $this->order_id);
    echo $response;
    return;
  }
}
