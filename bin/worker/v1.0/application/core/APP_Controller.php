<?php defined('BASEPATH') OR exit('No direct script access allowed');

class APP_Controller extends MY_Controller {

	function __construct()
  {
    parent::__construct();  
    date_default_timezone_set('Asia/Bangkok');
    
    $this->load->library('session');
    $this->load->helper('language');
    $site_lang = $this->session->userdata('site_lang');
    if($site_lang){
      $this->lang->load('message', $this->session->userdata('site_lang'));
    } 
    else{
      $this->lang->load('message', 'thai');
    }


    if($this->session->userdata('logged_in')=== FALSE ||
      $this->session->userdata('logged_in')=== NULL){
      redirect(base_url('login'));
    }
    else{
      $this->site_lang = $this->session->userdata('site_lang');
    }
  }

}
