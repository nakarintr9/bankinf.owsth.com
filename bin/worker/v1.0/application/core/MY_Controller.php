<?php defined('BASEPATH') OR exit('No direct script access allowed');

require_once APPPATH.'libraries/MX/Controller.php';

class MY_Controller extends MX_Controller {

  function __construct()
  {
    date_default_timezone_set('Asia/Bangkok');
    parent::__construct();   
    $this->load->library('apilog');     
    $this->load->library('librarycurl');

    $obj_base_url = include(getcwd().'/../../../config/worker/base_url.php');
    $base_url = (array)$obj_base_url;
    $this->url_v3 = $base_url['url_v3'];
    $this->key_v3 = $base_url['key_v3'];
    $this->url_gw = $base_url['url_gw'];
    $this->key_gw = $base_url['key_gw'];
    $this->username_gw = $base_url['username_gw'];
    $this->base_url = $base_url['base_url'];
  }

}
