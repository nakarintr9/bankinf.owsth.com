<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$this->ci =& get_instance();

$config['layout']['layout'] = 'layout';
$config['layout']['body-class'] = "page-md 
  page-header-top-fixed
  page-container-bg-solid 
  page-sidebar-closed-hide-logo 
  page-header-fixed-mobile 
  page-footer-fixed 
  page-quick-sidebar-over-content";

date_default_timezone_set('Asia/Bangkok');

$config['layout']['theme-color'] = 'blue-hoki';
$config['layout']['title'] = 'SINGER';
$config['layout']['copy'] = date('Y').' &copy; Media Center';
$config['layout']['logo'] = 'singer_logo.png';
$config['layout']['logo-login'] = 'singer_logo_login.png';
$config['layout']['login-logo-height'] = 100;
$config['layout']['topbar-logo-width'] = 210;
$config['layout']['topbar-logo-margin-left'] = 1;
$config['layout']['topbar-logo-margin-top'] = 1;
$config['layout']['footer'] = false;
