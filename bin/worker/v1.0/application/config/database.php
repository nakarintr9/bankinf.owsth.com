<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$active_group = 'bankinf';
$query_builder = TRUE;

$obj_db_user = include(getcwd().'/../../../config/worker/db_user.php');
$db_user = (array)$obj_db_user;

$db['bankinf'] = array(
  'dsn' => '',
  'hostname' => $db_user['bankinf']['hostname'],
  'username' => $db_user['bankinf']['username'],
  'password' => $db_user['bankinf']['password'],
  'database' => $db_user['bankinf']['database'],
  'dbdriver' => 'mysqli',
  'dbprefix' => '',
  'pconnect' => FALSE,
  'db_debug' => (ENVIRONMENT !== 'production'),
  'cache_on' => FALSE,
  'cachedir' => '',
  'char_set' => 'utf8',
  'dbcollat' => 'utf8_general_ci',
  'swap_pre' => '',
  'encrypt' => FALSE,
  'compress' => FALSE,
  'stricton' => FALSE,
  'failover' => array(),
  'save_queries' => TRUE
);

$db['owstopup'] = array(
  'dsn' => '',
  'hostname' => $db_user['owstopup']['hostname'],
  'username' => $db_user['owstopup']['username'],
  'password' => $db_user['owstopup']['password'],
  'database' => $db_user['owstopup']['database'],
  'dbdriver' => 'mysqli',
  'dbprefix' => '',
  'pconnect' => FALSE,
  'db_debug' => (ENVIRONMENT !== 'production'),
  'cache_on' => FALSE,
  'cachedir' => '',
  'char_set' => 'utf8',
  'dbcollat' => 'utf8_general_ci',
  'swap_pre' => '',
  'encrypt' => FALSE,
  'compress' => FALSE,
  'stricton' => FALSE,
  'failover' => array(),
  'save_queries' => TRUE
);




