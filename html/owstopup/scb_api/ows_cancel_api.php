<?php
	require_once("../nusoap/lib/nusoap.php");
	 
	//Create a new soap server
	$server = new soap_server();
	 
	//Define our namespace
	$base_url = include('../../../config/bankinf/base_url.php');
	$base_url = (array)$base_url;
	$namespace = $base_url['endpoint_scb']."/scb_api/ows_cancel_api.php";
	$server->wsdl->schemaTargetNamespace = $namespace;
	 
	//Configure our WSDL
	$server->configureWSDL("cancel");
	 
	// Register our method and argument parameters
  $varname = array(
   'tranID' => "xsd:string",
   'tranDate' => "xsd:string",
   'channel' => "xsd:string",
   'account' => "xsd:string",
   'amount' => "xsd:string",
   'reference1' => "xsd:string",
   'reference2' => "xsd:string",
   'reference3' => "xsd:string"
  );
	
	$server->wsdl->addComplexType('ResponseType', 'complexType', 'struct', 'all', '', array('resCode' => 'xsd:string', 'resMesg' => 'xsd:string', 'tranID' => 'xsd:string'));
	$server->register('cancel', $varname, array('response' => 'tns:ResponseType'));
	
	class responseType {
		var $resCode; // string
		var $resMesg; // string
		var $tranID; // string
	}
	 
	function cancel($request)
	{
		require_once("../connect/connect.php");
		
		require_once("../class/statement.php");
		require_once("../class/payment.php");
		require_once("../class/cashbag.php");
		require_once("../class/sms.php");
	  require_once("../class/payment_migrate.php");
		
		$resMesg['0000'] = 'Success';
		$resMesg['1000'] = 'Invalid data';
		$resMesg['1001'] = 'Invalid reference1';
		$resMesg['1002'] = 'Invalid reference2';
		$resMesg['1003'] = 'Invalid reference3';
		$resMesg['1004'] = 'Invalid amount';
		$resMesg['2001'] = 'Duplicate transaction';
		
		$resCode = '0000';
			
		$pid = $request['reference1']+0;
		$mid = $request['reference2']+0;
		$amt = $request['amount'];
		$transid = $request['tranID'];
		
		if($mid=='99999'){
			$paytype = 'partner';
		}else{
			$paytype = 'member';
		}
		
		if($paytype == 'partner'){
			
			//chk duplicate
			if($resCode=='0000'){
				$sql="select count(ID) as x from log_scb_ows where transid = '".$transid."' and api_type= 'cancel' ";
				$qry=$mysqli->query($sql);
				$row=mysqli_fetch_array($qry);
				
				if($row['x']!=0){
					$resCode = '2001';
				}
			}
			
			//chk transid
			if($resCode=='0000'){
				$sql="select count(ID) as x from log_scb_ows where transid = '".$transid."' and api_type= 'payment' ";
				$qry=$mysqli->query($sql);
				$row=mysqli_fetch_array($qry);
				
				if($row['x']==0){
					$resCode = '1000';
				}else{
					$sql="select * from log_scb_ows where transid = '".$transid."' and api_type= 'payment' ";
					$qry=$mysqli->query($sql);
					$row=mysqli_fetch_array($qry);
					
					$partnerpaymentid = $row['partnerpaymentid'];
				}
			}
			
			//chk partner
			if($resCode=='0000'){
				$sql="select count(ID) as x from partner where partnercode = 'PN' and partnerid = '".$mysqli->real_escape_string($pid)."' ";
				$qry=$mysqli->query($sql);
				$row=mysqli_fetch_array($qry);
				
				if($row['x']!=1){
					$resCode = '1001';
				}
			}
			
			//payment cancel
			if($resCode=='0000'){
				$frm = new payment;
				
				$frm->addby='SCB Bill Payment';
				$frm->orderid=$partnerpaymentid;
				$frm->cancelPaymentPartnerToAdmin();
			}
			
		}
		
		if($paytype == 'member'){
			
			//chk duplicate
			if($resCode=='0000'){
				$sql="select count(ID) as x from log_scb_ows where transid = '".$transid."' and api_type= 'cancel' ";
				$qry=$mysqli->query($sql);
				$row=mysqli_fetch_array($qry);
				
				if($row['x']!=0){
					$resCode = '2001';
				}
			}
			
			//chk transid
			if($resCode=='0000'){
				$sql="select count(ID) as x from log_scb_ows where transid = '".$transid."' and api_type= 'payment' ";
				$qry=$mysqli->query($sql);
				$row=mysqli_fetch_array($qry);
				
				if($row['x']==0){
					$resCode = '1000';
				}else{
					$sql="select * from log_scb_ows where transid = '".$transid."' and api_type= 'payment' ";
					$qry=$mysqli->query($sql);
					$row=mysqli_fetch_array($qry);
					
					$partnerpaymentid = $row['partnerpaymentid'];
					$memberpaymentid = $row['memberpaymentid'];
				}
			}
			
			//chk partner
			if($resCode=='0000'){
				$sql="select count(ID) as x from partner where partnercode = 'PN' and partnerid = '".$mysqli->real_escape_string($pid)."' ";
				$qry=$mysqli->query($sql);
				$row=mysqli_fetch_array($qry);
				
				if($row['x']!=1){
					$resCode = '1001';
				}
			}
			
			//chk member
			if($resCode=='0000'){
				$sql="select membercode from partner where partnercode = 'PN' and partnerid = '".$mysqli->real_escape_string($pid)."' ";
				$qry=$mysqli->query($sql);
				$row=mysqli_fetch_array($qry);
				
				$mcode = $row['membercode'];
				
				$sql="select count(ID) as x from member where membercode = '".$mysqli->real_escape_string($mcode)."' and memberid = '".$mysqli->real_escape_string($mid)."' ";
				$qry=$mysqli->query($sql);
				$row=mysqli_fetch_array($qry);
				
				if($row['x']!=1){
					$resCode = '1002';
				}
			}
			
			//payment cancel
			if($resCode=='0000'){

				$frm = new payment;
				
				$frm->addby='SCB Bill Payment';
				$frm->orderid=$partnerpaymentid;
				$frm->cancelPaymentPartnerToAdmin();
				
				$frm->orderid=$memberpaymentid;
				$frm->cancelPaymentMemberToPartner();
				
			 //คืนค่าบริการ 15 บาท
				$sta = new statement;
				$sta->membercode = $mcode;
				$sta->memberid = $mid;
				$sta->orderid = $memberpaymentid;
				$sta->ordertype = 'payment';
				$sta->detail = 'คืนค่าบริการจากการยกเลิกเติมเครดิตผ่านระบบ SCB Bill Payment';
				$fee = 15;
				$qouted = $sta->getBalanceStatementMember();
				$addcredit = $fee;
				$delcredit = 0;
				$balance = ($qouted+$addcredit)-$delcredit;
					
				$sta->qouted = $qouted;
				$sta->addcredit = $addcredit;
				$sta->delcredit = $delcredit;
				$sta->balance = $balance;
				
				$sta->addStatementMember();	

				//คืนค่าบริการ 15 บาท
				$sta = new statement;
				$sta->partnerid = $pid;
				$sta->orderid = $partnerpaymentid;
				$sta->ordertype = 'payment';
				$sta->detail = 'คืนค่าบริการจากการยกเลิกเติมเครดิตผ่านระบบ SCB Bill Payment';
				$fee = 15;
				$qouted = $sta->getBalanceStatementPartner();
				$addcredit = $fee;
				$delcredit = 0;
				$balance = ($qouted+$addcredit)-$delcredit;
					
				$sta->qouted = $qouted;
				$sta->addcredit = $addcredit;
				$sta->delcredit = $delcredit;
				$sta->balance = $balance;
				
				$sta->addStatementPartner();	

				$pmi = new payment_migrate;
				$pmi->fromcode = $mcode;
				$pmi->fromid = $mid;
				$pmi->fromtype = 'member';
				$pmi->orderid = $memberpaymentid;
				$pmi->partnerpaymentid = $partnerpaymentid;
				$pmi->memberpaymentid = $memberpaymentid;
				$pmi->membercode = $mcode;
				$pmi->memberid = $mid;
				$pmi->partnerid = $pid;
				$pmi->detail = 'ปรับเครดิต จากการยกเลิกการเติมเครดิต SCB Bill Payment ที่ระบบใหม่';
				$pmi->cancelpayment_v3();

			}
			
		}
	
		$res = new responseType();
		$res->resCode=$resCode;
		$res->resMesg = $resMesg[$resCode];
		$res->tranID =  $transid;
		
		$sql="insert into log_scb_ows ";
		$sql.="(api_type,action,ref1,ref2,amount,transid,res_code,res_message,paymenttype,partnerpaymentid,memberpaymentid,adddate,addtime) ";
		$sql.="values ";
		$sql.="('cancel','process','".$request['reference1']."','".$request['reference2']."','".$request['amount']."','".$transid."','".$resCode."','".$resMesg[$resCode]."','".$paytype."','".$partnerpaymentid."','".$memberpaymentid."','".date('Y-m-d')."','".date('H:i:s')."')";
		$qry=$mysqli->query($sql);
		
		return $res;
	}
	 
	// Get our posted data if the service is being consumed
	// otherwise leave this data blank.
	$POST_DATA = isset($GLOBALS['HTTP_RAW_POST_DATA']) ? $GLOBALS['HTTP_RAW_POST_DATA'] : '';
		if(empty($POST_DATA)){
		$POST_DATA = (null != file_get_contents("php://input")) ? file_get_contents("php://input") : '';
	}
	
	// pass our posted data (or nothing) to the soap service
	$server->service($POST_DATA);
	exit(); 
?>