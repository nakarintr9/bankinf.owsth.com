<?php
	require_once("../nusoap/lib/nusoap.php");
	 
	//Create a new soap server
	$server = new soap_server();
	 
	//Define our namespace
	$base_url = include('../../../config/bankinf/base_url.php');
	$base_url = (array)$base_url;
	$namespace = $base_url['endpoint_scb']."/scb_api/ows_inquiry_api.php";
	$server->wsdl->schemaTargetNamespace = $namespace;
	 
	//Configure our WSDL
	$server->configureWSDL("verify");
	 
	// Register our method and argument parameters
  $varname = array(
	  'tranID' => "xsd:string",
	  'tranDate' => "xsd:string",
	  'channel' => "xsd:string",
	  'account' => "xsd:string",
	  'amount' => "xsd:string",
	  'reference1' => "xsd:string",
	  'reference2' => "xsd:string",
	  'reference3' => "xsd:string"
  );
	
	$server->wsdl->addComplexType('ResponseType', 'complexType', 'struct', 'all', '', array('resCode' => 'xsd:string', 'resMesg' => 'xsd:string', 'tranID' => 'xsd:string'));
	$server->register('verify', $varname, array('response' => 'tns:ResponseType'));
	
	class responseType {
		var $resCode; // string
		var $resMesg; // string
		var $tranID; // string	
	}
	
	function verify($request) {
		require_once("../connect/connect.php");
		
		$resMesg['0000'] = 'Success';
		$resMesg['1001'] = 'Invalid reference1';
		$resMesg['1002'] = 'Invalid reference2';
		$resMesg['1003'] = 'Invalid reference3';
		$resMesg['1004'] = 'Invalid amount';
		$resMesg['2001'] = 'Duplicate transaction';
		
		$resCode = '0000';
			
		$pid = $request['reference1']+0;
		$mid = $request['reference2']+0;
		$amt = $request['amount'];
    $amt = str_replace(",", "", $amt);
    $amt = floatval($amt);
		$transid = $request['tranID'];
		if($mid=='99999'){
			$paytype = 'partner';
		}else{
			$paytype = 'member';
		}
		
		if($paytype == 'partner'){
			//chk duplicate
			if($resCode=='0000'){
				$sql="select count(ID) as x from log_scb_ows where transid = '".$transid."' and api_type= 'inquiry' ";
				$qry=$mysqli->query($sql);
				$row=mysqli_fetch_array($qry);
				
				if($row['x']!=0){
					$resCode = '2001';
				}
			}
			
			//chk amount
			if($resCode=='0000'){
				if($amt != 0 && $amt < 20){
					$resCode = '1003';
				}
			}
			
			//chk amount
			/*
			if($resCode=='0000'){
				if($amt != 0 && $amt > 39999){
					$resCode = '1003';
				}
			}
			*/
			
			//chk partner
			if($resCode=='0000'){
				$sql="select count(ID) as x from partner where partnercode = 'PN' and partnerid = '".$mysqli->real_escape_string($pid)."' ";
				$qry=$mysqli->query($sql);
				$row=mysqli_fetch_array($qry);
				
				if($row['x']!=1){
					$resCode = '1001';
				}
			}
		}
		
		if($paytype == 'member'){
			//chk duplicate
			if($resCode=='0000'){
				$sql="select count(ID) as x from log_scb_ows where transid = '".$transid."' and api_type= 'inquiry' ";
				$qry=$mysqli->query($sql);
				$row=mysqli_fetch_array($qry);

				if($row['x']!=0){
					$resCode = '2001';
				}
			}

      //chk amount
			if($resCode=='0000'){
				if($amt != 0 && $amt < 300){
					$resCode = '1003';
				}
			}
			
			//chk amount
			/*
			if($resCode=='0000'){
				if($amt != 0 && $amt > 39999){
					$resCode = '1003';
				}
			}
			*/

			//chk partner
			if($resCode=='0000'){
				$sql="select count(ID) as x from partner where partnercode = 'PN' and partnerid = '".$mysqli->real_escape_string($pid)."' ";
				$qry=$mysqli->query($sql);
				$row=mysqli_fetch_array($qry);
				if($row['x']!=1){
					$resCode = '1001';
				}
			}
			
			//chk member
			
			if($resCode=='0000'){
				$sql="select membercode from partner where partnercode = 'PN' and partnerid = '".$mysqli->real_escape_string($pid)."' ";
				$qry=$mysqli->query($sql);
				$row=mysqli_fetch_array($qry);
				
				$mcode = $row['membercode'];
				
				$sql="select count(ID) as x from member where membercode = '".$mysqli->real_escape_string($mcode)."' and memberid = '".$mysqli->real_escape_string($mid)."' ";
				$qry=$mysqli->query($sql);
				$row=mysqli_fetch_array($qry);
				
				if($row['x']!=1){
					$resCode = '1002';
				}
			}
		}
	
		$res = new responseType();
		$res->resCode=$resCode;
		$res->resMesg = $resMesg[$resCode];
		$res->tranID =  $transid;
		
		$sql="insert into log_scb_ows ";
		$sql.="(api_type,action,ref1,ref2,amount,transid,res_code,res_message,paymenttype,adddate,addtime) ";
		$sql.="values ";
		$sql.="('inquiry','process','".$request['reference1']."','".$request['reference2']."','".$request['amount']."','".$transid."','".$resCode."','".$resMesg[$resCode]."','".$paytype."','".date('Y-m-d')."','".date('H:i:s')."')";
		$qry=$mysqli->query($sql);
		
		return $res;
	}
	 
	// Get our posted data if the service is being consumed
	// otherwise leave this data blank.
	$POST_DATA = isset($GLOBALS['HTTP_RAW_POST_DATA']) ? $GLOBALS['HTTP_RAW_POST_DATA'] : '';
	if(empty($POST_DATA)){
		$POST_DATA = (null != file_get_contents("php://input")) ? file_get_contents("php://input") : '';
	}
	
	// pass our posted data (or nothing) to the soap service
	$server->service($POST_DATA);
	exit(); 
?>