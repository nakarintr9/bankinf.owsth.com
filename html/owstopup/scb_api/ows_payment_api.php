<?php
	require_once("../nusoap/lib/nusoap.php");
	 
	//Create a new soap server
	$server = new soap_server();
	 
	//Define our namespace
	$base_url = include('../../../config/bankinf/base_url.php');
	$base_url = (array)$base_url;
	$namespace = $base_url['endpoint_scb']."/scb_api/ows_payment_api.php";
	$server->wsdl->schemaTargetNamespace = $namespace;
	 
	//Configure our WSDL
	$server->configureWSDL("confirm");
	 
	// Register our method and argument parameters
  $varname = array(
     'tranID' => "xsd:string",
	  'tranDate' => "xsd:string",
	  'channel' => "xsd:string",
	  'account' => "xsd:string",
	  'amount' => "xsd:string",
	  'reference1' => "xsd:string",
	  'reference2' => "xsd:string",
	  'reference3' => "xsd:string"
  );
	
	$server->wsdl->addComplexType('ResponseType', 'complexType', 'struct', 'all', '', array('resCode' => 'xsd:string', 'resMesg' => 'xsd:string', 'tranID' => 'xsd:string'));
	$server->register('confirm', $varname, array('response' => 'tns:ResponseType'));

	class responseType {
		var $resCode; // string
		var $resMesg; // string
		var $tranID; // string
	}
	 
	function confirm($request)
	{
		require_once("../connect/connect.php");

		require_once("../class/statement.php");
		require_once("../class/payment.php");
		require_once("../class/cashbag.php");
		require_once("../class/sms.php");
		require_once("../class/payment_migrate.php");
		

		$resMesg['0000'] = 'Success';
		$resMesg['1001'] = 'Invalid reference1';
		$resMesg['1002'] = 'Invalid reference2';
		$resMesg['1003'] = 'Invalid reference3';
		$resMesg['1004'] = 'Invalid amount';
		$resMesg['2001'] = 'Duplicate transaction';
		
		$resCode = '0000';
			
		$pid = $request['reference1']+0;
		$mid = $request['reference2']+0;
		$amt = $request['amount'];
    $amt = str_replace(",", "", $amt);
    $amt = floatval($amt);
   	$transid = $request['tranID'];
		
		if($mid=='99999'){
			$paytype = 'partner';
		}else{
			$paytype = 'member';
		}
		
		
		if($paytype == 'partner'){
			//chk duplicate
			if($resCode=='0000'){
				$sql="select count(ID) as x from log_scb_ows where transid = '".$transid."' and api_type= 'payment' ";
				$qry=$mysqli->query($sql);
				$row=mysqli_fetch_array($qry);
				
				if($row['x']!=0){
					$resCode = '2001';
				}
			}
			
			//chk amount
			if($resCode=='0000'){
				if($amt < 20){
					$resCode = '1003';
				}
			}

			//chk partner
			if($resCode=='0000'){
				$sql="select count(ID) as x from partner where partnercode = 'PN' and partnerid = '".$mysqli->real_escape_string($pid)."' ";
				$qry=$mysqli->query($sql);
				$row=mysqli_fetch_array($qry);
				
				if($row['x']!=1){
					$resCode = '1001';
				}
			}
			
			//payment
			if($resCode=='0000'){
				$sta = new statement;
				$sta->partnerid = $pid;
				
				$before = $sta->getBalanceStatementPartner();
				$after = $amt+$before;
				
				$frm = new payment;
				$frm->partnerid = $pid;
				
				$detail = 'เติมเครดิตด้วย SCB BILL PAYMENT ของ Partner';
				
				$frm->amount_v=$amt;
				$frm->transferdate_v=date('Y-m-d');
				$frm->transfertime_v=date('H:i:s');
				$frm->channel_v = 'อื่นๆ';
				$frm->detail_v=$detail;
				$frm->image_name_v="";
				$frm->image_temp_v="";
				$frm->before_v=$before;
				$frm->after_v=$after;
				$frm->banktransfer_v = 'อื่นๆ';
				$frm->addby = "SCB Bill Payment";
				$frm->paymentauto = '1';
		
				$error=$frm->addPaymentPartnerToAdmin();
		
				if($error=='')
				{
					$frm->addby="SCB Bill Payment";
				
					$sql="select max(ID) as x from payment where fromcode = 'PN' and fromid = '".$pid."' ";
					$qry=$mysqli->query($sql);
					$row=mysqli_fetch_array($qry);
					
					$partnerpaymentid = $row['x'];
					
					$frm->orderid=$partnerpaymentid;
					$frm->approvePaymentPartnerToAdmin();	
					
					//ค่าบริการ 15 บาท
					$sta->orderid = $partnerpaymentid;
					$sta->ordertype = 'payment';
					$sta->detail = 'หักค่าบริการจากการเติมเครดิตผ่านระบบ SCB Bill Payment';
					$fee = 15;
					$qouted = $sta->getBalanceStatementPartner();
					$addcredit = 0;
					$delcredit = $fee;
					$balance = ($qouted+$addcredit)-$delcredit;
						
					$sta->qouted = $qouted;
					$sta->addcredit = $addcredit;
					$sta->delcredit = $delcredit;
					$sta->balance = $balance;
					
					$sta->addStatementPartner();
				}
				//end partner to admin 
				
				//sms1
				$sms = new sms;
					
				$sms->addby = "Auto";
				
				$sql="select credit,username,firstname,lastname,email,mobile,alertphone,alertone,alerttwo,alertthree,callone,calltwo,callthree,status,membercode from partner where partnercode = 'PN' and partnerid = '".$mysqli->real_escape_string($pid)."' ";
				$qry=$mysqli->query($sql);
				$partner=mysqli_fetch_array($qry);
				
				$sms->mobile = $partner['alertphone'];
				$sms->message = "PN".sprintf("%05d",$pid)." อนุมัติเติมเครดิต คงเหลือ ".number_format($partner['credit'],2)." บาท ขอบคุณค่ะ";
				
				$sms->fromcode = "AM";
				$sms->fromid = "1";
				$sms->fromtype = 'admin';
				
				$sms->sendSMS();
			}
		}

		if($paytype == 'member'){
			//chk duplicate
			if($resCode=='0000'){
				$sql="select count(ID) as x from log_scb_ows where transid = '".$transid."' and api_type= 'payment' ";
				$qry=$mysqli->query($sql);
				$row=mysqli_fetch_array($qry);
				
				if($row['x']!=0){
					$resCode = '2001';
				}
			}
			
			//chk amount
			if($resCode=='0000'){
				if($amt < 300){
					$resCode = '1003';
				}
			}
			
			//chk partner
			if($resCode=='0000'){
				$sql="select count(ID) as x from partner where partnercode = 'PN' and partnerid = '".$mysqli->real_escape_string($pid)."' ";
				$qry=$mysqli->query($sql);
				$row=mysqli_fetch_array($qry);
				
				if($row['x']!=1){
					$resCode = '1001';
				}
			}
			
			//chk member
			if($resCode=='0000'){
				$sql="select membercode from partner where partnercode = 'PN' and partnerid = '".$mysqli->real_escape_string($pid)."' ";
				$qry=$mysqli->query($sql);
				$row=mysqli_fetch_array($qry);
				
				$mcode = $row['membercode'];
				
				$sql="select count(ID) as x from member where membercode = '".$mysqli->real_escape_string($mcode)."' and memberid = '".$mysqli->real_escape_string($mid)."' ";
				$qry=$mysqli->query($sql);
				$row=mysqli_fetch_array($qry);
				
				if($row['x']!=1){
					$resCode = '1002';
				}
			}
			
			//payment partner
			if($resCode=='0000'){
				$sta = new statement;
				$sta->partnerid = $pid;
				
				$before = $sta->getBalanceStatementPartner();
				$after = $amt+$before;
				
				$frm = new payment;
				$frm->partnerid = $pid;
				
				$detail = 'เติมเครดิตด้วย SCB BILL PAYMENT ของ Member';
				
				$frm->amount_v=$amt;
				$frm->transferdate_v=date('Y-m-d');
				$frm->transfertime_v=date('H:i:s');
				$frm->channel_v = 'อื่นๆ';
				$frm->detail_v=$detail;
				$frm->image_name_v="";
				$frm->image_temp_v="";
				$frm->before_v=$before;
				$frm->after_v=$after;
				$frm->banktransfer_v = 'อื่นๆ';
				$frm->addby = "SCB Bill Payment";
				$frm->paymentauto = '1';
		
				$error=$frm->addPaymentPartnerToAdmin();
				if($error=='')
				{
					$frm->addby="SCB Bill Payment";
				
					$sql="select max(ID) as x from payment where fromcode = 'PN' and fromid = '".$pid."' ";
					$qry=$mysqli->query($sql);
					$row=mysqli_fetch_array($qry);
					
					$partnerpaymentid = $row['x'];
					
					$frm->orderid=$partnerpaymentid;
					$frm->approvePaymentPartnerToAdmin();	
					
					//ค่าบริการ 15 บาท
					$sta->orderid = $partnerpaymentid;
					$sta->ordertype = 'payment';
					$sta->detail = 'หักค่าบริการจากการเติมเครดิตผ่านระบบ SCB Bill Payment';
					$fee = 15;
					$qouted = $sta->getBalanceStatementPartner();
					$addcredit = 0;
					$delcredit = $fee;
					$balance = ($qouted+$addcredit)-$delcredit;
						
					$sta->qouted = $qouted;
					$sta->addcredit = $addcredit;
					$sta->delcredit = $delcredit;
					$sta->balance = $balance;
					
					$sta->addStatementPartner();
				}
				
				//sms1
				$sms = new sms;
					
				$sms->addby = "Auto";
				
				$sql="select credit,username,firstname,lastname,email,mobile,alertphone,alertone,alerttwo,alertthree,callone,calltwo,callthree,status,membercode from partner where partnercode = 'PN' and partnerid = '".$mysqli->real_escape_string($pid)."' ";
				$qry=$mysqli->query($sql);
				$partner=mysqli_fetch_array($qry);
				
				$sms->mobile = $partner['alertphone'];
				
				$sms->message = "PN".sprintf("%05d",$pid)." อนุมัติเติมเครดิต คงเหลือ ".number_format($partner['credit'],2)." บาท ขอบคุณค่ะ";
				
				$sms->fromcode = "AM";
				$sms->fromid = "1";
				$sms->fromtype = 'admin';
				
				$sms->sendSMS();
			}//end payment partner
				
			//payment member
			if($resCode=='0000'){
				$sta = new statement;
				$sta->membercode = $mcode;
				$sta->memberid = $mid;
				
				$before = $sta->getBalanceStatementMember();
				$after = $amt+$before;
				
				$frm = new payment;
				$frm->membercode = $mcode;
				$frm->memberid = $mid;
				
				$frm->partnerid = $pid;
				
				$detail = 'เติมเครดิตด้วย SCB BILL PAYMENT ของ Member';
				
				$frm->amount_v=$amt;
				$frm->transferdate_v=date('Y-m-d');
				$frm->transfertime_v=date('H:i:s');
				$frm->channel_v = 'อื่นๆ';
				$frm->detail_v=$detail;
				$frm->image_name_v="";
				$frm->image_temp_v="";
				$frm->before_v=$before;
				$frm->after_v=$after;
				$frm->banktransfer_v = 'อื่นๆ';
				$frm->addby = "SCB Bill Payment";
				$frm->paymentauto = '1';
		
				$error=$frm->addPaymentMemberToPartner();
		
				if($error=='')
				{
					$frm->addby="SCB Bill Payment";
				
					$sql="select max(ID) as x from payment where fromcode = '".$mcode."' and fromid = '".$mid."' ";
					$qry=$mysqli->query($sql);
					$row=mysqli_fetch_array($qry);
					
					$memberpaymentid = $row['x'];
					
					$frm->orderid=$memberpaymentid;
					$frm->approvePaymentMemberToPartner();
					
					//ค่าบริการ 15 บาท
					$sta->orderid = $memberpaymentid;
					$sta->ordertype = 'payment';
					$sta->detail = 'หักค่าบริการจากการเติมเครดิตผ่านระบบ SCB Bill Payment';
					$fee = 15;
					$qouted = $sta->getBalanceStatementMember();
					$addcredit = 0;
					$delcredit = $fee;
					$balance = ($qouted+$addcredit)-$delcredit;
						
					$sta->qouted = $qouted;
					$sta->addcredit = $addcredit;
					$sta->delcredit = $delcredit;
					$sta->balance = $balance;
					
					$sta->addStatementMember();	
				}

				//start payment migrate
				if($error == ''){
					$pmi = new payment_migrate;
					$pmi->fromcode = $mcode;
					$pmi->fromid = $mid;
					$pmi->fromtype = 'member';
					$pmi->orderid = $memberpaymentid;
					$pmi->amount = $amt - $fee;
					$pmi->membercode = $mcode;
					$pmi->memberid = $mid;
					$pmi->partnerid = $pid;
					$pmi->detail = 'โอนย้ายเครดิตสมาชิกจากการเติมเครดิตผ่านระบบ SCB Bill Payment ไปยังระบบใหม่';
					$res_migrate = $pmi->migrate_member_credit();				
				}
				//end payment migrate

				//sms1
				$sms = new sms;
					
				$sms->addby = "Auto";
				
				$sql="select partnercode,partnerid,username,credit,firstname,lastname,email,mobile,alertphone,alertone,alerttwo,alertthree,callone,calltwo,callthree,chkip,ip,status from member where membercode = '".$mysqli->real_escape_string($mcode)."' and memberid = '".$mysqli->real_escape_string($mid)."' ";
				$qry=$mysqli->query($sql);
				$member=mysqli_fetch_array($qry);
				
				$sms->mobile = $member['alertphone'];

				//by Dear //sms แจ้งเครดิตคงเหลือ หลังการโอนเงิน
				if($res_migrate['error_code'] == 'E00'){
					$sms->message = $member['username']." อนุมัติเติมเครดิต คงเหลือ ".number_format($res_migrate['m_credit']+$member['credit'],2)." บาท ขอบคุณค่ะ";
				}
				else{
					$sms->message = $member['username']." อนุมัติเติมเครดิต คงเหลือ ".number_format($member['credit'],2)." บาท ขอบคุณค่ะ";
				}
				
				$sms->fromcode = "AM";
				$sms->fromid = "1";
				$sms->fromtype = 'admin';
				
				$sms->sendSMS();

				if($before < 10 && $member['status'] == 1){
					$sql="update member set status = 0 where membercode = '".$mcode."' and memberid = '".$mid."' ";
					$qry=$mysqli->query($sql);
				}
			}//end payment member
		}
	
		$res = new responseType();
		$res->resCode=$resCode;
		$res->resMesg = $resMesg[$resCode];
		$res->tranID =  $transid;
		
		$sql="insert into log_scb_ows ";
		$sql.="(api_type,action,ref1,ref2,amount,transid,res_code,res_message,paymenttype,partnerpaymentid,memberpaymentid,adddate,addtime) ";
		$sql.="values ";
		$sql.="('payment','process','".$request['reference1']."','".$request['reference2']."','".$request['amount']."','".$transid."','".$resCode."','".$resMesg[$resCode]."','".$paytype."','".$partnerpaymentid."','".$memberpaymentid."','".date('Y-m-d')."','".date('H:i:s')."')";
		$qry=$mysqli->query($sql);
	 
		return $res;
	}
	 
	// Get our posted data if the service is being consumed
	// otherwise leave this data blank.
	$POST_DATA = isset($GLOBALS['HTTP_RAW_POST_DATA']) ? $GLOBALS['HTTP_RAW_POST_DATA'] : '';
	if(empty($POST_DATA)){
		$POST_DATA = (null != file_get_contents("php://input")) ? file_get_contents("php://input") : '';
	}
	
	// pass our posted data (or nothing) to the soap service
	$server->service($POST_DATA);
	exit(); 
?>