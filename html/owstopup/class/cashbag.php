<?php
require_once '../core/core.php';
class cashbag extends core {
	
	public $membercode;
	public $memberid;
	private $partnercode = 'PN';
	public $partnerid;
	private $admincode = 'AM';
	private $adminid = '1';
	
	public $bank;
	public $cashtype;
	public $orderid;
	public $detail;
	public $amount;
	public $qouted;
	public $addcredit;
	public $delcredit;
	public $balance;

	function __construct() {
		parent::__construct();
  }
	
	public function getBalanceCashbagAdmin(){
		
		$sql="select ifnull(balance,0) as x from cashbag_admin where bank = '".$this->mysqli->real_escape_string($this->bank)."' order by ID DESC limit 1";
		$qry=$this->mysqli->query($sql);
		$row=mysqli_fetch_array($qry);
		
		return $row['x'];
	}
	
	public function getBalanceCashbagPartner(){
		
		$sql="select ifnull(balance,0) as x from cashbag_partner where partnercode = '".$this->mysqli->real_escape_string($this->partnercode)."' and partnerid = '".$this->mysqli->real_escape_string($this->partnerid)."' and  bank = '".$this->mysqli->real_escape_string($this->bank)."' order by ID DESC limit 1";
		$qry=$this->mysqli->query($sql);
		$row=mysqli_fetch_array($qry);

		return $row['x'];
	}
	
	public function addCashbagAdmin()
	{
		$date=date('Y-m-d');
		$time=date('H:i:s');
		
		$sql="insert into cashbag_admin ";
		$sql.="(bank,cashtype,partnercode";
		$sql.=",partnerid,orderid,detail,amount";
		$sql.=",qouted,addcredit,delcredit";
		$sql.=",balance,adddate,addtime)";
		$sql.=" values ";
		$sql.="('".$this->mysqli->real_escape_string($this->bank)."','".$this->mysqli->real_escape_string($this->cashtype)."','".$this->mysqli->real_escape_string($this->partnercode)."'";
		$sql.=",'".$this->mysqli->real_escape_string($this->partnerid)."','".$this->mysqli->real_escape_string($this->orderid)."','".$this->mysqli->real_escape_string($this->detail)."','".$this->mysqli->real_escape_string($this->amount)."'";
		$sql.=",'".$this->mysqli->real_escape_string($this->qouted)."','".$this->mysqli->real_escape_string($this->addcredit)."','".$this->mysqli->real_escape_string($this->delcredit)."'";
		$sql.=",'".$this->mysqli->real_escape_string($this->balance)."','".$date."','".$time."')";
		$qry=$this->mysqli->query($sql);
		
		$bank="credit".$this->getCashbank($this->bank);
		
		$sql="update admin set $bank = ($bank + $this->addcredit) - $this->delcredit where ID = '".$this->mysqli->real_escape_string($this->adminid)."' ";
		$qry=$this->mysqli->query($sql);
	}
	
	public function addCashbagPartner()
	{
		$date=date('Y-m-d');
		$time=date('H:i:s');
		
		$sql="insert into cashbag_partner ";
		$sql.="(partnercode,partnerid,bank,cashtype,membercode";
		$sql.=",memberid,orderid,detail,amount";
		$sql.=",qouted,addcredit,delcredit";
		$sql.=",balance,adddate,addtime)";
		$sql.=" values ";
		$sql.="('".$this->mysqli->real_escape_string($this->partnercode)."','".$this->mysqli->real_escape_string($this->partnerid)."','".$this->mysqli->real_escape_string($this->bank)."','".$this->mysqli->real_escape_string($this->cashtype)."','".$this->mysqli->real_escape_string($this->membercode)."'";
		$sql.=",'".$this->mysqli->real_escape_string($this->memberid)."','".$this->mysqli->real_escape_string($this->orderid)."','".$this->mysqli->real_escape_string($this->detail)."','".$this->mysqli->real_escape_string($this->amount)."'";
		$sql.=",'".$this->mysqli->real_escape_string($this->qouted)."','".$this->mysqli->real_escape_string($this->addcredit)."','".$this->mysqli->real_escape_string($this->delcredit)."'";
		$sql.=",'".$this->mysqli->real_escape_string($this->balance)."','".$date."','".$time."')";
		$qry=$this->mysqli->query($sql);
		
		if($this->bank!='')
		{
			$bank="credit".$this->getCashbank($this->bank);
		}
		else
		{
			$bank="creditbank8";
		}
		
		$sql="update partner set $bank = ($bank + $this->addcredit) - $this->delcredit where partnercode = '".$this->mysqli->real_escape_string($this->partnercode)."' and  partnerid = '".$this->mysqli->real_escape_string($this->partnerid)."' ";
		$qry=$this->mysqli->query($sql);
	}

	function getCashbank($bank)
	{
		if($bank=='ธนาคารกรุงศรี'){return 'bank1';}
		else if($bank=='ธนาคารไทยพาณิชย์'){return 'bank2';}
		else if($bank=='ธนาคารกรุงเทพ'){return 'bank3';}
		else if($bank=='ธนาคารกสิกรไทย'){return 'bank4';}
		else if($bank=='ธนาคารทหารไทย'){return 'bank5';}
		else if($bank=='ธนาคารกรุงไทย'){return 'bank6';}
		else if($bank=='ธนาคาร ธกส'){return 'bank7';}
		else if($bank=='อื่นๆ'){return 'bank8';}
	}
	
}

?>