<?php
require_once '../core/core.php';
class payment extends core {
	
	public $membercode;
	public $memberid;
	private $partnercode = 'PN';
	public $partnerid;
	private $admincode = 'AM';
	private $adminid = '1';
	
	public $mode = 1;
	public $broker = 1;
	public $bank = 1;
	public $amount = 1;
	public $transferdate = 1;
	public $transfertime = 1;
	public $channel = 1;
	public $channelpartner = 1;
	public $detail = 1;
	public $banktransfer = 1;
	public $passwork = 1;
	public $image = 1;
	
	public $amount_x;
	public $transferdate_x;
	public $transfertime_x;
	public $channel_x;
	public $channelpartner_x;
	public $detail_x;
	public $banktransfer_x;
	public $passwork_x;
	public $image_x;
	
	public $amount_v;
	public $transferdate_v;
	public $transfertime_v;
	public $channel_v;
	public $channelpartner_v;
	public $detail_v;
	public $banktransfer_v;
	public $passwork_v;
	public $broker_v;
	public $bank_v;
	public $image_name_v;
	public $image_temp_v;
	public $before_v;
	public $after_v;
	public $paymentauto_v;
	
	public $ID;
	public $form;
	public $addby;
	public $orderid;

	function __construct() {
		parent::__construct();
  }
	
	public function log_payment($action) {
		$sql="insert into log_payment (paymentid,action,addby,adddate,addtime) ";
		$sql.="values ('".$this->mysqli->real_escape_string($this->orderid)."','".$this->mysqli->real_escape_string($action)."','".$this->mysqli->real_escape_string($this->addby)."','".$this->mysqli->real_escape_string(date('Y-m-d'))."','".$this->mysqli->real_escape_string(date('H:i:s'))."')";
		$qry=$this->mysqli->query($sql);	
	}
	
	public function paymentBroker() {
		
		$date=date('Y-m-d');
		$time=date('H:i:s');
		
		$status=0;
		$error='';
		
		if($status==0)
		{	
			$this->transfertime_v = date('H:i:s');
			
			$sql="insert into payment ";
			$sql.="(fromcode,fromid,fromtype,channel";
			$sql.=",broker,brokerbank";
			$sql.=",amount,detail,transferdate,transfertime";
			$sql.=",addby,adddate,addtime)";
			$sql.=" values ";
			$sql.="('".$this->mysqli->real_escape_string($this->admincode)."','".$this->mysqli->real_escape_string($this->adminid)."','admin','".$this->mysqli->real_escape_string($this->channel_v)."'";
			$sql.=",'".$this->mysqli->real_escape_string($this->broker_v)."','".$this->mysqli->real_escape_string($this->bank_v)."'";
			$sql.=",'".$this->mysqli->real_escape_string($this->amount_v)."','".$this->mysqli->real_escape_string($this->detail_v)."','".$this->mysqli->real_escape_string($this->transferdate_v)."','".$this->mysqli->real_escape_string($this->transfertime_v)."'";
			$sql.=",'".$this->mysqli->real_escape_string($this->addby)."','".$this->mysqli->real_escape_string($date)."','".$this->mysqli->real_escape_string($time)."')";
			$sqlX=$sql;
			$qry=$this->mysqli->query($sql);	
			
			$sql="select ID from payment where broker = '".$this->mysqli->real_escape_string($this->broker_v)."' and amount = '".$this->mysqli->real_escape_string($this->amount_v)."' order by ID DESC ";
			$qry=$this->mysqli->query($sql);
			$row=mysqli_fetch_array($qry);
			
			$orderid = $row['ID'];
		}
		
		if($this->broker_v=='bk00001')
		{
			$api=new api_welovetopup;
			
			$api->bank = $this->bank_v;
			$api->amount= $this->amount_v;
	
			$datetrans = explode("-",$this->amount_v);
			$datetransfer = $datetrans[2]."-".$datetrans[1]."-".$datetrans[0];
			$api->transferdate= $datetransfer;
			$api->comment=$this->detail_v;
			
			$return = $api->api_banktransfer();
			
			$explode=explode(",",$return);
			
			$rest=$explode[0];
			
			if($rest == '1')
			{
				$transid=$explode[1];	
				$stx=0;
			}
			else if($rest == '0')
			{
				$status=1;$error="ไม่ทราบสาเหตุ1";
				$stx=2;
			}
			else if($rest == 'Error')
			{
				$status=1;$error="ไม่ทราบสาเหตุ2";
				$stx=2;
			}
			
			$sql="update payment set ";
			$sql.="notify = 1";
			$sql.=",notifydate = '".$this->mysqli->real_escape_string($date)."' ";
			$sql.=",notifytime = '".$this->mysqli->real_escape_string($time)."' ";
			$sql.=",notifystatus = '".$this->mysqli->real_escape_string($rest)."' ";
			$sql.=",notifyreturn = '".$this->mysqli->real_escape_string($return)."' ";
			$sql.=",brokerorderid = '".$this->mysqli->real_escape_string($transid)."' ";
			$sql.=",status = '".$this->mysqli->real_escape_string($stx)."' ";
			$sql.="where ID = '".$this->mysqli->real_escape_string($orderid)."' ";
			$qry=$this->mysqli->query($sql);
			
			
			if($status==0)
			{
				
			}
			else if($status==1)
			{
				return $error;
			}
		}
	}
	
	public function addPaymentMemberToPartner() {
		
		$date=date('Y-m-d');
		$time=date('H:i:s');
		
		$status=0;
		$error='';
		
		if($status==0)
		{	
			$sql="insert into payment ";
			$sql.="(fromcode,fromid,fromtype,channel";
			$sql.=",tocode,toid,totype,paymentauto";
			$sql.=",creditbefore,creditafter";
			$sql.=",amount,detail,transferdate,transfertime";
			$sql.=",addby,adddate,addtime)";
			$sql.=" values ";
			$sql.="('".$this->mysqli->real_escape_string($this->membercode)."','".$this->mysqli->real_escape_string($this->memberid)."','member','".$this->mysqli->real_escape_string($this->channel_v)."'";
			$sql.=",'".$this->mysqli->real_escape_string($this->partnercode)."','".$this->mysqli->real_escape_string($this->partnerid)."','partner','".$this->mysqli->real_escape_string($this->paymentauto_v)."'";
			$sql.=",'".$this->mysqli->real_escape_string($this->before_v)."','".$this->mysqli->real_escape_string($this->after_v)."'";
			$sql.=",'".$this->mysqli->real_escape_string($this->amount_v)."','".$this->mysqli->real_escape_string($this->detail_v)."','".$this->mysqli->real_escape_string($this->transferdate_v)."','".$this->mysqli->real_escape_string($this->transfertime_v)."'";
			$sql.=",'".$this->mysqli->real_escape_string($this->addby)."','".$this->mysqli->real_escape_string($date)."','".$this->mysqli->real_escape_string($time)."')";
			$qry=$this->mysqli->query($sql);	

			if($qry)
			{
				if($this->image_name_v != "")
				{
					$sql="select max(ID) as x from payment ";
					$qry=$this->mysqli->query($sql);
					$row=mysqli_fetch_array($qry);
					
					$max = $row['x'];
					
					$folder = '../images/slip/';
					
					$images = $this->image_temp_v;
					$new_images = "img".$max.".jpg";
					
					//echo $this->image_temp_v." : ".$folder."/".$this->image_name_v;
					
					/*
					if(!copy($this->image_temp_v,$folder."/".$this->image_name_v)){
						echo "NOT OK";
					}else{
						echo "OK";
					}
					*/
					copy($this->image_temp_v,$folder."/".$this->image_name_v);
					
					$size=GetimageSize($images);
					
					if($size[0]>800)
					{
						$width=800;
					}
					else
					{
						$width=$size[0];
					}
					$height=round($width*$size[1]/$size[0]);
					$images_orig = ImageCreateFromJPEG($images);
					$photoX = ImagesX($images_orig);
					$photoY = ImagesY($images_orig);
					$images_fin = ImageCreateTrueColor($width, $height);
					ImageCopyResampled($images_fin, $images_orig, 0, 0, 0, 0, $width+1, $height+1, $photoX, $photoY);
					ImageJPEG($images_fin,$folder."/".$new_images);
					ImageDestroy($images_orig);
					ImageDestroy($images_fin);
				
					$sql="update payment set image = '".$this->mysqli->real_escape_string($new_images)."' where ID = '".$this->mysqli->real_escape_string($max)."' ";
					$qry=$this->mysqli->query($sql);
					
					
					/*
					$sql="select max(ID) as x from payment ";
					$qry=$this->mysqli->query($sql);
					$row=mysqli_fetch_array($qry);
					
					$max = $row['x'];
					$img = "img".$max.".jpg";
					
					$folder = '../images/slip/';
					move_uploaded_file($this->image_temp_v,$folder."/".$this->image_name_v);
					rename($folder."/".$this->image_name_v,$folder."/".$img); 
					
					$sql="update payment set image = '".$img."' where ID = '".$max."' ";
					$qry=$this->mysqli->query($sql);
					*/
				}
				
			}else{
				$status = 1;
				$error = 'Not Success'; 
			}
		}
		
		if($status==0)
		{

		}
		else if($status==1)
		{
			return $error;
		}
	}
	
	public function approvePaymentMemberToPartner() {
		
		$date=date('Y-m-d');
		$time=date('H:i:s');
		
		$sql="select status from payment where ID = '".$this->mysqli->real_escape_string($this->orderid)."' ";
		$qry=$this->mysqli->query($sql);
		$row=mysqli_fetch_array($qry);
		
		if($row['status']!=1){
		
			$sql="update payment set ";
			$sql.="approveby = '".$this->mysqli->real_escape_string($this->addby)."' ";
			$sql.=",approvedate = '".$this->mysqli->real_escape_string($date)."' ";
			$sql.=",approvetime = '".$this->mysqli->real_escape_string($time)."' ";
			$sql.=",status = '1' ";
			$sql.="where ID = '".$this->mysqli->real_escape_string($this->orderid)."' ";
			$qry=$this->mysqli->query($sql);
			
			$detail="เครดิต : อนุมัติเครดิต orderid ".$this->mysqli->real_escape_string($this->orderid);
			
			$sql="select * from payment where ID = '".$this->mysqli->real_escape_string($this->orderid)."' ";
			$qry=$this->mysqli->query($sql);
			$row=mysqli_fetch_array($qry);
			
			$addcredit=$row['amount'];
			$delcredit=0;
			$this->partnerid=$row['toid'];
			$this->membercode=$row['fromcode'];
			$this->memberid=$row['fromid'];
			
			$str = new statement;
				
			$str->partnerid=$this->partnerid;
			$str->membercode=$this->membercode;
			$str->memberid=$this->memberid;
			
			$str->broker = '';
			$str->orderid = $this->orderid;
			$str->ordertype = 'payment';
			
			$qouted=$str->getBalanceStatementMember();
			
			$balance = ($qouted+$addcredit)-$delcredit;
			
			$str->detail = $detail;
			$str->qouted = $qouted;
			$str->addcredit = $addcredit;
			$str->delcredit = $delcredit;
			$str->balance = $balance;
			
			$str->addStatementMember();
			
			$csb = new cashbag;
			
			$csb->partnerid=$this->partnerid;
			$csb->membercode=$this->membercode;
			$csb->memberid=$this->memberid;
			$csb->bank=$row['channel'];
			$qouted=$csb->getBalanceCashbagPartner();
			
			$balance = ($qouted+$addcredit)-$delcredit;
			
			
			$csb->cashtype="payment";
			$csb->orderid=$this->orderid;
			$csb->detail = $detail;
			$csb->amount=$addcredit;
			$csb->qouted=$qouted;
			$csb->addcredit=$addcredit;
			$csb->delcredit=$delcredit;
			$csb->balance=$balance;
			
			$csb->addCashbagPartner();
			
			$sql="update member set callone = 0 , calltwo = 0 , callthree = 0 where membercode = '".$this->mysqli->real_escape_string($this->membercode)."' and memberid = '".$this->mysqli->real_escape_string($this->memberid)."' ";
			$qry=$this->mysqli->query($sql);
			
			$this->log_payment("approve");
		
		}
	}
	
	public function cancelPaymentMemberToPartner() {
		
		$date=date('Y-m-d');
		$time=date('H:i:s');
		
		$sql="select status from payment where ID = '".$this->mysqli->real_escape_string($this->orderid)."' ";
		$qry=$this->mysqli->query($sql);
		$row=mysqli_fetch_array($qry);
		
		if($row['status']!=2){
			$sql="update payment set ";
			$sql.="cancelby = '".$this->mysqli->real_escape_string($this->addby)."' ";
			$sql.=",canceldate = '".$this->mysqli->real_escape_string($date)."' ";
			$sql.=",canceltime = '".$this->mysqli->real_escape_string($time)."' ";
			$sql.=",status = '2' ";
			$sql.="where ID = '".$this->mysqli->real_escape_string($this->orderid)."' ";
			$qry=$this->mysqli->query($sql);
			
			$detail="เครดิต : ยกเลิกการอนุมัติเครดิต orderid ".$this->mysqli->real_escape_string($this->orderid);
			
			$sql="select * from payment where ID = '".$this->mysqli->real_escape_string($this->orderid)."' ";
			$qry=$this->mysqli->query($sql);
			$row=mysqli_fetch_array($qry);
			
			$addcredit=0;
			$delcredit=$row['amount'];
			$this->partnerid=$row['toid'];
			$this->membercode=$row['fromcode'];
			$this->memberid=$row['fromid'];
			
			$str = new statement;
				
			$str->partnerid=$this->partnerid;
			$str->membercode=$this->membercode;
			$str->memberid=$this->memberid;
			
			$str->broker = '';
			$str->orderid = $this->orderid;
			$str->ordertype = 'payment';
			
			$qouted=$str->getBalanceStatementMember();
			
			$balance = ($qouted+$addcredit)-$delcredit;
			
			$str->detail = $detail;
			$str->qouted = $qouted;
			$str->addcredit = $addcredit;
			$str->delcredit = $delcredit;
			$str->balance = $balance;
			
			$str->addStatementMember();
			
			$csb = new cashbag;
			
			$csb->partnerid=$this->partnerid;
			$csb->membercode=$this->membercode;
			$csb->memberid=$this->memberid;
			$csb->bank=$row['channel'];
			$qouted=$csb->getBalanceCashbagPartner();
			
			$balance = ($qouted+$addcredit)-$delcredit;
			
			
			$csb->cashtype="payment";
			$csb->orderid=$this->orderid;
			$csb->detail = $detail;
			$csb->amount=$addcredit;
			$csb->qouted=$qouted;
			$csb->addcredit=$addcredit;
			$csb->delcredit=$delcredit;
			$csb->balance=$balance;
			
			$csb->addCashbagPartner();
			
			$this->log_payment("cancel");
		}
	}
	
	public function deletePaymentMemberToPartner() {
		
		$date=date('Y-m-d');
		$time=date('H:i:s');
		
		$sql="select status from payment where ID = '".$this->mysqli->real_escape_string($this->orderid)."' ";
		$qry=$this->mysqli->query($sql);
		$row=mysqli_fetch_array($qry);
		
		if($row['status']!=2){
			$sql="update payment set ";
			$sql.="cancelby = '".$this->mysqli->real_escape_string($this->addby)."' ";
			$sql.=",canceldate = '".$this->mysqli->real_escape_string($date)."' ";
			$sql.=",canceltime = '".$this->mysqli->real_escape_string($time)."' ";
			$sql.=",status = '2' ";
			$sql.="where ID = '".$this->mysqli->real_escape_string($this->orderid)."' ";
			$qry=$this->mysqli->query($sql);
			
			$this->log_payment("delete");
		}
	}
	
	public function addPaymentPartnerToAdmin() {
		
		$date=date('Y-m-d');
		$time=date('H:i:s');
		
		$status=0;
		$error='';
		
		if($this->passwork==0)
		{
			//chkpasswork
			$chkpass=chkPasswork($this->passwork_v);
			if($chkpass==0)
			{

			}
			else
			{
				$status = 1;
				$error = 'รหัสทำงานนี้ไม่ถูกต้อง หรือหมดอายุแล้ว';
			}
		}
		
		if($status==0 and $this->banktransfer_v !='อื่นๆ')
		{
			$bank = "credit".getCashbank($this->banktransfer_v);
			$sql="select $bank from partner where partnercode = '".$this->mysqli->real_escape_string($this->partnercode)."' and partnerid = '".$this->mysqli->real_escape_string($this->partnerid)."' ";
			$qry=$this->mysqli->query($sql);
			$row=mysqli_fetch_array($qry);
			$creditbank=$row[$bank];
			if($creditbank<$_POST['amount'])
			{	
				$status = 1;
				$error = 'ยอดเงินในบัญชีที่หักไม่พอ'.$passwork_v;
			}
		}
		
		if($status==0)
		{	
			$sql="insert into payment ";
			$sql.="(fromcode,fromid,fromtype,channel";
			$sql.=",tocode,toid,totype,banktransfer,paymentauto";
			$sql.=",creditbefore,creditafter";
			$sql.=",amount,detail,transferdate,transfertime";
			$sql.=",addby,adddate,addtime)";
			$sql.=" values ";
			$sql.="('".$this->mysqli->real_escape_string($this->partnercode)."','".$this->mysqli->real_escape_string($this->partnerid)."','partner','".$this->mysqli->real_escape_string($this->channel_v)."'";
			$sql.=",'".$this->mysqli->real_escape_string($this->admincode)."','".$this->mysqli->real_escape_string($this->adminid)."','admin','".$this->mysqli->real_escape_string($this->banktransfer_v)."','".$this->mysqli->real_escape_string($this->paymentauto_v)."'";
			$sql.=",'".$this->mysqli->real_escape_string($this->before_v)."','".$this->mysqli->real_escape_string($this->after_v)."'";
			$sql.=",'".$this->mysqli->real_escape_string($this->amount_v)."','".$this->mysqli->real_escape_string($this->detail_v)."','".$this->mysqli->real_escape_string($this->transferdate_v)."','".$this->mysqli->real_escape_string($this->transfertime_v)."'";
			$sql.=",'".$this->mysqli->real_escape_string($this->addby)."','".$this->mysqli->real_escape_string($date)."','".$this->mysqli->real_escape_string($time)."')";
			$qry=$this->mysqli->query($sql);	
			
			if($qry)
			{
				if($this->image_name_v != "")
				{
					$sql="select max(ID) as x from payment ";
					$qry=$this->mysqli->query($sql);
					$row=mysqli_fetch_array($qry);
					
					$max = $row['x'];
					
					$folder = '../images/slip/';
					
					$images = $this->image_temp_v;
					$new_images = "img".$max.".jpg";
					copy($this->image_temp_v,$folder."/".$this->image_name_v);
					
					$size=GetimageSize($images);
					
					if($size[0]>800)
					{
						$width=800;
					}
					else
					{
						$width=$size[0];
					}
					$height=round($width*$size[1]/$size[0]);
					$images_orig = ImageCreateFromJPEG($images);
					$photoX = ImagesX($images_orig);
					$photoY = ImagesY($images_orig);
					$images_fin = ImageCreateTrueColor($width, $height);
					ImageCopyResampled($images_fin, $images_orig, 0, 0, 0, 0, $width+1, $height+1, $photoX, $photoY);
					ImageJPEG($images_fin,$folder."/".$new_images);
					ImageDestroy($images_orig);
					ImageDestroy($images_fin);
				
					$sql="update payment set image = '".$this->mysqli->real_escape_string($new_images)."' where ID = '".$this->mysqli->real_escape_string($max)."' ";
					$qry=$this->mysqli->query($sql);

					/*
					$sql="select max(ID) as x from payment ";
					$qry=$this->mysqli->query($sql);
					$row=mysqli_fetch_array($qry);
					
					$max = $row['x'];
					$img = "img".$max.".jpg";
					
					$folder = '../images/slip/';
					move_uploaded_file($this->image_temp_v,$folder."/".$this->image_name_v);
					rename($folder."/".$this->image_name_v,$folder."/".$img); 
					
					$sql="update payment set image = '".$img."' where ID = '".$max."' ";
					$qry=$this->mysqli->query($sql);
					*/
				}
				
			}else{
				$status = 1;
				$error = 'Not Success'; 
			}
		}
		
		if($status==0)
		{			
			
		}
		else if($status==1)
		{
			return $error;
		}
	}
	
	public function approvePaymentPartnerToAdmin() {
		
		$date=date('Y-m-d');
		$time=date('H:i:s');
		
		$sql="select status from payment where ID = '".$this->mysqli->real_escape_string($this->orderid)."' ";
		$qry=$this->mysqli->query($sql);
		$row=mysqli_fetch_array($qry);
		
		if($row['status']!=1){
			$sql="update payment set ";
			$sql.="approveby = '".$this->mysqli->real_escape_string($this->addby)."' ";
			$sql.=",approvedate = '".$this->mysqli->real_escape_string($date)."' ";
			$sql.=",approvetime = '".$this->mysqli->real_escape_string($time)."' ";
			$sql.=",status = '1' ";
			$sql.="where ID = '".$this->mysqli->real_escape_string($this->orderid)."' ";
			$qry=$this->mysqli->query($sql);
			
			$detail="เครดิต : อนุมัติเครดิต orderid ".$this->orderid;
			
			$sql="select * from payment where ID = '".$this->mysqli->real_escape_string($this->orderid)."' ";
			$qry=$this->mysqli->query($sql);
			$row=mysqli_fetch_array($qry);
			
			$addcredit=$row['amount'];
			$delcredit=0;
			$this->partnerid=$row['fromid'];
			
			$str = new statement;
				
			$str->partnerid=$this->partnerid;
			
			$str->broker = '';
			$str->orderid = $this->orderid;
			$str->ordertype = 'payment';
			
			$qouted=$str->getBalanceStatementPartner();
			
			$balance = ($qouted+$addcredit)-$delcredit;
			
			$str->detail = $detail;
			$str->qouted = $qouted;
			$str->addcredit = $addcredit;
			$str->delcredit = $delcredit;
			$str->balance = $balance;
			
			$str->addStatementPartner();
			
			$csb = new cashbag;
			$csb->bank=$row['channel'];
			$qouted=$csb->getBalanceCashbagAdmin();
			
			$balance = ($qouted+$addcredit)-$delcredit;
			
			$csb->partnerid=$this->partnerid;
			
			
			$csb->cashtype="payment";
			$csb->orderid=$this->orderid;
			$csb->detail = $detail;
			$csb->amount=$addcredit;
			$csb->qouted=$qouted;
			$csb->addcredit=$addcredit;
			$csb->delcredit=$delcredit;
			$csb->balance=$balance;
			
			$csb->addCashbagAdmin();
			
			$csb2 = new cashbag;
			$csb2->bank=$row['banktransfer'];
			$csb2->partnerid=$this->partnerid;
			$qouted=$csb2->getBalanceCashbagPartner();
			
			$balance = $qouted-$addcredit;
			
			$csb2->cashtype="payment";
			$csb2->orderid=$this->orderid;
			$csb2->detail = $detail;
			$csb2->amount=$addcredit;
			$csb2->qouted=$qouted;
			$csb2->addcredit=0;
			$csb2->delcredit=$addcredit;
			$csb2->balance=$balance;
			
			$csb2->addCashbagPartner();
			
			$sql="update partner set callone = 0 , calltwo = 0 , callthree = 0 where partnercode = '".$this->mysqli->real_escape_string($this->partnercode)."' and partnerid = '".$this->mysqli->real_escape_string($this->partnerid)."' ";
			$qry=$this->mysqli->query($sql);
			
			$this->log_payment("approve");
		}
	}
	
	public function cancelPaymentPartnerToAdmin() {
		
		$date=date('Y-m-d');
		$time=date('H:i:s');
		
		$sql="select status from payment where ID = '".$this->mysqli->real_escape_string($this->orderid)."' ";
		$qry=$this->mysqli->query($sql);
		$row=mysqli_fetch_array($qry);
		
		if($row['status']!=2){
			$sql="update payment set ";
			$sql.="cancelby = '".$this->mysqli->real_escape_string($this->addby)."' ";
			$sql.=",canceldate = '".$this->mysqli->real_escape_string($date)."' ";
			$sql.=",canceltime = '".$this->mysqli->real_escape_string($time)."' ";
			$sql.=",status = '2' ";
			$sql.="where ID = '".$this->mysqli->real_escape_string($this->orderid)."' ";
			$qry=$this->mysqli->query($sql);
			
			$detail="เครดิต : ยกเลิกการอนุมัติเครดิต orderid ".$this->orderid;
			
			$sql="select * from payment where ID = '".$this->mysqli->real_escape_string($this->orderid)."' ";
			$qry=$this->mysqli->query($sql);
			$row=mysqli_fetch_array($qry);
			
			$addcredit=0;
			$delcredit=$row['amount'];
			$amount=$row['amount'];
			$this->partnerid=$row['fromid'];
			
			$str = new statement;
				
			$str->partnerid=$this->partnerid;
			
			$str->broker = '';
			$str->orderid = $this->orderid;
			$str->ordertype = 'payment';
			
			$qouted=$str->getBalanceStatementPartner();
			
			$balance = ($qouted+$addcredit)-$delcredit;
			
			$str->detail = $detail;
			$str->qouted = $qouted;
			$str->addcredit = $addcredit;
			$str->delcredit = $delcredit;
			$str->balance = $balance;
			
			$str->addStatementPartner();
			
			$csb = new cashbag;
			
			$csb->bank=$row['channel'];
			$qouted=$csb->getBalanceCashbagAdmin();
			
			$balance = ($qouted+$addcredit)-$delcredit;
			
			$csb->partnerid=$this->partnerid;
			
			
			$csb->cashtype="payment";
			$csb->orderid=$this->orderid;
			$csb->detail = $detail;
			$csb->amount=$amount;
			$csb->qouted=$qouted;
			$csb->addcredit=$addcredit;
			$csb->delcredit=$delcredit;
			$csb->balance=$balance;
			
			$csb->addCashbagAdmin();
			
			$csb2 = new cashbag;
			$csb2->bank=$row['banktransfer'];
			$csb2->partnerid=$this->partnerid;
			$qouted=$csb2->getBalanceCashbagPartner();
			
			$balance = $qouted+$delcredit;
			
			$csb2->cashtype="payment";
			$csb2->orderid=$this->orderid;
			$csb2->detail = $detail;
			$csb2->amount=$amount;
			$csb2->qouted=$qouted;
			$csb2->addcredit=$delcredit;
			$csb2->delcredit=0;
			$csb2->balance=$balance;
			
			$csb2->addCashbagPartner();
			
			$this->log_payment("cancel");
		}
	}
	
	public function deletePaymentPartnerToAdmin() {
		
		$date=date('Y-m-d');
		$time=date('H:i:s');
		
		$sql="select status from payment where ID = '".$this->mysqli->real_escape_string($this->orderid)."' ";
		$qry=$this->mysqli->query($sql);
		$row=mysqli_fetch_array($qry);
		
		if($row['status']!=2){
			$sql="update payment set ";
			$sql.="cancelby = '".$this->mysqli->real_escape_string($this->addby)."' ";
			$sql.=",canceldate = '".$this->mysqli->real_escape_string($date)."' ";
			$sql.=",canceltime = '".$this->mysqli->real_escape_string($time)."' ";
			$sql.=",status = '2' ";
			$sql.="where ID = '".$this->mysqli->real_escape_string($this->orderid)."' ";
			$qry=$this->mysqli->query($sql);
			
			$this->log_payment("delete");
		}
	}
	
	public function showFrmPayment() {
	 
	  $date=date('Y-m-d');
	  $time=date('H:i:s');
	  
	  $timeexplode=explode(':',$time);
	  
	  $this->form = "<table style='line-height:35px;'>";
	  
	  
	  if($this->mode == 0){
		  
		  $this->form .= '<input name="mode" id="mode" type="hidden" class="txtbox1" value="'.$this->mode_v.'" autocomplete="off">';
	  }
	  
	  if($this->broker == 0){
			
			$this->form .= '<tr>';
			$this->form .= '<td><div class="fontblack">Broker<span class="fontred"> '.$this->broker_x.'</span></td>';
			$this->form .= '<td>';
			$this->form .= '<select name="broker" id="broker" style="width:205px;height:28px;">';
			
			$sql="select brokerid,brokername from broker ";
			$qry=$this->mysqli->query($sql);
			while($row=mysqli_fetch_array($qry))
			{
                $this->form .= '<option value="'.$row['brokerid'].'">'.$row['brokername'].'</option>';
			}
			$this->form .= '</select>';
			$this->form .= '</td>';
			$this->form .= '</tr>';
			
      }
	  
	  if($this->amount == 0){
		  
			$this->form .= '<tr>';
			$this->form .= '<td width="150"><div class="fontblack">จำนวนเงินที่โอน<span class="fontred"> '.$this->amount_x.'</span></td>';
			$this->form .= '<td width="300"><input name="amount" id="amount" type="text" class="txtbox1" value="" autocomplete="off"></td>';
			$this->form .= '</tr>';
			
	  }
		
	  if($this->transferdate == 0){
		  
			$this->form .= '<tr>';
			$this->form .= '<td><div class="fontblack">วันที่โอน<span class="fontred"> '.$this->transferdate_x.'</span></td>';
			$this->form .= '<td><input name="transferdate" id="transferdate" type="text" class="txtbox1" value="'.$date.'" autocomplete="off" readonly></td>';
			$this->form .= '</tr>';
			
      }
	  
	  if($this->transfertime == 0){
			
			$this->form .= '<tr>';
			$this->form .= '<td><div class="fontblack">เวลาที่โอน<span class="fontred"> '.$this->transfertime_x.'</span></td>';
			$this->form .= '<td>';
			$this->form .= '<select name="hr" id="hr" style="width:85px;height:28px;">';
			for($i=01;$i<=24;$i++)
			{
			if($i==$timeexplode[0]){$select='selected';}else{$select='';}
			$this->form .= '<option value="'.$i.'" '.$select.'>'.$i.'</option>';	
			}
			$this->form .= '</select> : ';
			$this->form .= '<select name="mn" id="mn" style="width:85px;height:28px;">';
			for($i=00;$i<=60;$i++)
			{
			if($i==$timeexplode[1]){$select='selected';}else{$select='';}
			$this->form .= '<option value="'.$i.'" '.$select.'>'.$i.'</option>';	
			}
			$this->form .= '</select> น.';
			$this->form .= '</td>';
			$this->form .= '</tr>';
			
      }
	  
	  if($this->bank == 0){
			
			$this->form .= '<tr>';
			$this->form .= '<td><div class="fontblack">ธนาคาร<span class="fontred"> '.$this->bank_x.'</span></td>';
			$this->form .= '<td>';
			$this->form .= '<select name="bank" id="bank" style="width:500px;height:28px;">';
		
			$this->form .='<option value="ไทยพาณิชย์">ไทยพาณิชย์</option>';
			$this->form .='<option value="กสิกรไทย">กสิกรไทย</option>';
			$this->form .='<option value="กรุงไทย">กรุงไทย</option>';
			$this->form .='<option value="กรุงศรีอยุธยา">กรุงศรีอยุธยา</option>';
			$this->form .='<option value="กรุงเทพ">กรุงเทพ</option>';
			$this->form .='<option value="ทหารไทย">ทหารไทย</option>';
			
			$this->form .= '</select>';
			$this->form .= '</td>';
			$this->form .= '</tr>';
			
      }
	  
	  if($this->channel == 0){
			
			$this->form .= '<tr>';
			$this->form .= '<td><div class="fontblack">บัญชี<span class="fontred"> '.$this->channel_x.'</span></td>';
			$this->form .= '<td>';
			$this->form .= '<select name="channel" id="channel" style="width:500px;height:28px;">';
			$this->form .= '<option value="">กรุณาเลือกบัญชีด้วยค่ะ</option>';
			$sql="select * from admin where ID = '".$this->mysqli->real_escape_string($this->adminid)."' ";
			$qry=$this->mysqli->query($sql);
			$row=mysqli_fetch_array($qry);
			$num=7;
			for($i=1;$i<=$num;$i++)
			{
				$bank='bank'.$i;
				$bankac='bankac'.$i;
				$bankno='bankno'.$i;
				$credit='creditbank'.$i;
				if($row[$bankac]!='')
				{	
                $this->form .= '<option value="'.$row[$bank].'">'.$row[$bank]." ".$row[$bankac]." ".$row[$bankno].'</option>';
				}
			}
			$this->form .='<option value="อื่นๆ">อื่นๆ กรุณาระบุที่ช่องรายละเอียดการเติมเงิน</option>';
			$this->form .= '</select>';
			$this->form .= '</td>';
			$this->form .= '</tr>';
			
      }
	  
	  if($this->channelpartner == 0){
			
			$this->form .= '<tr>';
			$this->form .= '<td><div class="fontblack">เข้าบัญชี<span class="fontred"> '.$this->channel_x.'</span></td>';
			$this->form .= '<td>';
			$this->form .= '<select name="channel" id="channel" style="width:500px;height:28px;">';
			
			$sql="select * from partner where partnercode = '".$this->mysqli->real_escape_string($this->partnercode)."' and partnerid = '".$this->mysqli->real_escape_string($this->partnerid)."'  ";
			$qry=$this->mysqli->query($sql);
			$row=mysqli_fetch_array($qry);
			$num=7;
			for($i=1;$i<=$num;$i++)
			{
				$bank='bank'.$i;
				$bankac='bankac'.$i;
				$bankno='bankno'.$i;
				$credit='creditbank'.$i;
				if($row[$bankac]!='')
				{	
                $this->form .= '<option value="'.$row[$bank].'">'.$row[$bank]." ".$row[$bankac]." ".$row[$bankno]." ".'</option>';
				}
			}
			$this->form .='<option value="อื่นๆ">อื่นๆ กรุณาระบุที่ช่องรายละเอียดการเติมเงิน</option>';
			$this->form .= '</select>';
			$this->form .= '</td>';
			$this->form .= '</tr>';
			
      }
	  
	   if($this->banktransfer == 0){
			
			$this->form .= '<tr>';
			$this->form .= '<td><div class="fontblack">หักเงินจากบัญชี<span class="fontred"> '.$this->banktransfer_x.'</span></td>';
			$this->form .= '<td>';
			$this->form .= '<select name="banktransfer" id="banktransfer" style="width:500px;height:28px;">';
			
			$sql="select * from partner where partnercode = '".$this->mysqli->real_escape_string($this->partnercode)."' and partnerid = '".$this->mysqli->real_escape_string($this->partnerid)."' ";
			$qry=$this->mysqli->query($sql);
			$row=mysqli_fetch_array($qry);
			$num=7;
			for($i=1;$i<=$num;$i++)
			{
				$bank='bank'.$i;
				$bankac='bankac'.$i;
				$bankno='bankno'.$i;
				$credit='creditbank'.$i;
				if($row[$bankac]!='')
				{	
                $this->form .= '<option value="'.$row[$bank].'">'.$row[$bank]." ".$row[$bankac]." ".$row[$bankno]." คงเหลือ ".$row[$credit].' บาท</option>';
				}
			}
			$this->form .= '<option value="อื่นๆ">บัญชีเงินสด</option>';
			$this->form .= '</select>';
			$this->form .= '</td>';
			$this->form .= '</tr>';
			
      }
	  
	   if($this->image == 0){
		  
			$this->form .= '<tr>';
			$this->form .= '<td><div class="fontblack">สลิปโอนเงิน<span class="fontred"> '.$this->image_x.'</span></td>';
			$this->form .= '<td><input name="img" id="img" type="file" /></td>';
			$this->form .= '</tr>';
			
      }
	   
	   if($this->detail == 0){
		  
			$this->form .= '<tr>';
			$this->form .= '<td><div class="fontblack">รายละเอียด<span class="fontred"> '.$this->detail_x.'</span></td>';
			$this->form .= '<td><textarea name="detail" id="detail" cols="" rows="" style="width:500px;height:100px;"></textarea></td>';
			$this->form .= '</tr>';
			
      }
	  
	  if($this->passwork == 0){
		  
			$this->form .= '<tr>';
			$this->form .= '<td width="150"><div class="fontblack">รหัสทำงาน<span class="fontred"> '.$this->passwork_x.'</span></td>';
			$this->form .= '<td width="300"><input name="passwork" id="passwork" type="password" class="txtbox1" value="" autocomplete="off"></td>';
			$this->form .= '</tr>';

	 }
	  
	  $this->form.="</table>";
	  echo $this->form;
	  
	}
	
	public function scriptFrm($formname,$fromtype) {
		$this->form = "";
		$i=0;
		$this->form .= '<script language="javascript"> function chkFrm() {';
		
		if($this->amount_x  != ""){
			if($i==1){$this->form .= " else ";}
			$this->form .= 'if(document.'.$formname.'.amount.value==""){alert("กรุณาใส่จำนวนเงินที่โอน");document.'.$formname.'.amount.focus();}';
			$i=1;
		}
		
		if($this->transferdate_x  != ""){
			if($i==1){$this->form .= " else ";}
			$this->form .= 'if(document.'.$formname.'.transferdate.value==""){alert("กรุณาใส่วันที่โอน");document.'.$formname.'.transferdate.focus();}';
			$i=1;
		}
		
		if($this->passwork_x  != ""){
			if($i==1){$this->form .= " else ";}
			$this->form .= 'if(document.'.$formname.'.passwork.value==""){alert("กรุณาใส่รหัสทำงาน");document.'.$formname.'.passwork.focus();}';
			$i=1;
		}
		
		if($this->channel_x  != ""){
			if($i==1){$this->form .= " else ";}
			$this->form .= 'if(document.'.$formname.'.channel.value==""){alert("กรุณาเลือกธนาคารที่โอนเงินค่ะ");document.'.$formname.'.channel.focus();}';
			$i=1;
		}
		
		if($i == 1)
		{
		$this->form .=' else{document.'.$formname.'.mode.value = "'.$fromtype.'";document.'.$formname.'.submit();} } </script>';
		}
		else
		{
		$this->form .=' document.'.$formname.'.mode.value = "'.$fromtype.'";document.'.$formname.'.submit(); } </script>';	
		}
		
		echo $this->form;
	}
	
}
?>