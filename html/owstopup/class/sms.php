<?php
require_once '../core/core.php';
class sms extends core {
	public $membercode;
	public $memberid;
	public $partnercode = 'PN';
	public $partnerid;
	public $admincode = 'AM';
	public $adminid = '1';

	private $url = 'https://www.welovetopup.com/api/level3/sendsms/';
	private $user = 'owstopup';
	private $pass = 'express15';
	private $amount;
	private $broker;
	private $postdata;

	public $fromcode;
	public $fromid;
	public $fromtype;
	public $mobile;
	public $message;

	public $keepadmin;

	public $api;
	public $ip;
	public $orderid;
	public $addby;

	function __construct() {
		parent::__construct();
  }

	public function sendSMS(){

		$status=0;
		$error="";

		$date=date('Y-m-d');
		$time=date('H:i:s');

		$sql="select brokerid,smsprice from broker where brokerid = 'bk00025'";
		$qry=$this->mysqli->query($sql);
		$row=mysqli_fetch_array($qry);

		$this->broker=$row['brokerid'];
		$this->amount=$row['smsprice'];

		// if($this->membercode == 'TPN' ||
		// 	$this->membercode == 'TSD'){
		// 	$this->broker = 'bk00023';
		// 	$this->amount = '0.4';
		// }

		$sql="insert into list_sms ";
		$sql.="(fromcode,fromid,fromtype";
		$sql.=",broker,mobile,message,amount";
		$sql.=",addby,adddate,addtime)";
		$sql.=" values ";
		$sql.="('".$this->mysqli->real_escape_string($this->fromcode)."','".$this->mysqli->real_escape_string($this->fromid)."','".$this->mysqli->real_escape_string($this->fromtype)."'";
		$sql.=",'".$this->mysqli->real_escape_string($this->broker)."','".$this->mysqli->real_escape_string($this->mobile)."','".$this->mysqli->real_escape_string($this->message)."','".$this->mysqli->real_escape_string($this->amount)."'";
		$sql.=",'".$this->mysqli->real_escape_string($this->addby)."','".$this->mysqli->real_escape_string($date)."','".$this->mysqli->real_escape_string($time)."')";
		$qry=$this->mysqli->query($sql);

		$sql="select max(ID) as x from list_sms where mobile = '".$this->mysqli->real_escape_string($this->mobile)."' ";
		$qry=$this->mysqli->query($sql);
		$row=mysqli_fetch_array($qry);

		$this->orderid = $row['x'];

		$this->addLogSMS();

		if($this->keepadmin==1){
			//statement หักเงินเต็ม
			if($this->adminid != '')
			{
				$ams = new statement;

				$ams->broker = $this->broker;
				$ams->orderid = $this->orderid;
				$ams->ordertype = 'sendsms';
				$ams->detail = 'sms : ส่ง sms ตาม orderid '.$ams->orderid;
				$ams->qouted = $ams->getBalanceStatementAdmin();
				$ams->addcredit = 0;
				$ams->delcredit = $this->amount;
				$ams->balance = ($ams->qouted+$ams->addcredit)-$ams->delcredit;

				$ams->addStatementAdmin();
			}

		}else{
			//statement หักเงินเต็ม
			if($this->adminid != '')
			{
				$ams = new statement;

				$ams->broker = $this->broker;
				$ams->orderid = $this->orderid;
				$ams->ordertype = 'sendsms';
				$ams->detail = 'sms : ส่ง sms ตาม orderid '.$ams->orderid;
				$ams->qouted = $ams->getBalanceStatementAdmin();
				$ams->addcredit = 0;
				$ams->delcredit = $this->amount;
				$ams->balance = ($ams->qouted+$ams->addcredit)-$ams->delcredit;

				$ams->addStatementAdmin();
			}

			if($this->partnerid != '')
			{
				$ams = new statement;

				$ams->partnerid = $this->partnerid;
				$ams->broker = $this->broker;
				$ams->orderid = $this->orderid;
				$ams->ordertype = 'sendsms';
				$ams->detail = 'sms : ส่ง sms ตาม orderid '.$ams->orderid;
				$ams->qouted = $ams->getBalanceStatementPartner();
				$ams->addcredit = 0;
				$ams->delcredit = $this->amount;
				$ams->balance = ($ams->qouted+$ams->addcredit)-$ams->delcredit;

				$ams->addStatementPartner();
			}

			if($this->memberid != '')
			{
				$sqlX="select credit from member where membercode = '".$this->mysqli->real_escape_string($this->membercode)."' and memberid = '".$this->mysqli->real_escape_string($this->memberid)."' ";
				$qryX=$this->mysqli->query($sqlX);
				$rowX=mysqli_fetch_array($qryX);
				$memcredit=$rowX['credit'];

				if($memcredit > 0)
				{
					$ams = new statement;

					$ams->membercode = $this->membercode;
					$ams->memberid = $this->memberid;
					$ams->broker = $this->broker;
					$ams->orderid = $this->orderid;
					$ams->ordertype = 'sendsms';
					$ams->detail = 'sms : ส่ง sms ตาม orderid '.$ams->orderid;
					$ams->qouted = $ams->getBalanceStatementMember();
					$ams->addcredit = 0;
					$ams->delcredit = $this->amount;
					$ams->balance = ($ams->qouted+$ams->addcredit)-$ams->delcredit;

					$ams->addStatementMember();
				}
			}

		}

		//bk00025
		if($this->broker == 'bk00025'){
			$broker_api = include('../../../config/bankinf/broker_api.php');
			$broker_api = (array)$broker_api;
	    //URL TO GATEWAY
	    $url = $broker_api['gw_base_url'].'/brkrcore/v1.0/service/sendsms';

	    //post_data
	    $data = array(
	     'key' => $broker_api['gw_key'],
	     'username' => $broker_api['gw_username'],
	     'mobile' => $this->mobile,
	     'message' => $this->message,
	     'dest_ref_id' => $this->orderid,
	     'callback_url' => $broker_api['gw_callback_url']
	    );

	    $notifypost = json_encode($data);
	   
	    //CURL
	    $ch = curl_init($url);                                                                      
	    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
	    curl_setopt($ch, CURLOPT_POSTFIELDS, $notifypost);
	    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
	     'Content-Type: application/json', 
	     'Content-Length: ' . strlen($notifypost))
	    );
	    $content = curl_exec($ch);
	    if($content === false){
         $return = "cURL Error: ".curl_error($ch);
         $notifystatus = $return;
         $status=1;
         $notify = 0;
      }
      else{
        $return = $content;
        $res = json_decode($return, true);
       	$notify = 1;
      	$notifystatus = isset($res['error_code']) ? $res['error_code'] : 'E64';   
      	$brokerorderid = isset($res['order_id']) ? $res['order_id'] : '';     
        if($res['error_code'] == 'E00'){
       		$status=0;
        }
        else{
        	$status=1;
        }
      }
	    curl_close($ch);
		}

		if($notify==1)
		{
			$sql="update list_sms set ";
			$sql.="notify = '".$this->mysqli->real_escape_string($notify)."'";
			$sql.=",notifydate = '".$this->mysqli->real_escape_string($date)."'";
			$sql.=",notifytime = '".$this->mysqli->real_escape_string($time)."'";
			$sql.=",notifystatus = '".$this->mysqli->real_escape_string($notifystatus)."'";
			$sql.=",notifypost = '".$this->mysqli->real_escape_string($notifypost)."' ";
			$sql.=",notifyreturn = '".$this->mysqli->real_escape_string($return)."'";
			$sql.=" where ID = '".$this->mysqli->real_escape_string($this->orderid)."' ";
			$qry=$this->mysqli->query($sql);
		}

		//สำเร็จ
		if($status==0)
		{
			$sql="update list_sms set ";
			$sql.="brokerorderid = '".$this->mysqli->real_escape_string($brokerorderid)."'";
			$sql.=",status=1";
			$sql.=",checker=1";
			$sql.=",checkerdate='".date('Y-m-d')."'";
			$sql.=",checkertime='".date('H:i:s')."'";
			$sql.=",checkerstatus='OK'";
			$sql.=" where ID = '".$this->mysqli->real_escape_string($this->orderid)."' ";
			$qry=$this->mysqli->query($sql);
		}

		//ไม่สำเร็จ
		if($status==1)
		{
			$sql="update list_sms set status = 2 where ID = '".$this->mysqli->real_escape_string($this->orderid)."' ";
			$qry=$this->mysqli->query($sql);

			if($this->keepadmin==1){

				if($this->adminid != '')
				{
					$ams = new statement;

					$ams->broker = $this->broker;
					$ams->orderid = $this->orderid;
					$ams->ordertype = 'sendsms';
					$ams->detail = 'sms : ส่ง sms ตาม orderid '.$ams->orderid.' ไม่สำเร็จ';
					$ams->qouted = $ams->getBalanceStatementAdmin();
					$ams->addcredit = $this->amount;
					$ams->delcredit = 0;
					$ams->balance = ($ams->qouted+$ams->addcredit)-$ams->delcredit;

					$ams->addStatementAdmin();
				}

			}else{

				if($this->adminid != '')
				{
					$ams = new statement;

					$ams->broker = $this->broker;
					$ams->orderid = $this->orderid;
					$ams->ordertype = 'sendsms';
					$ams->detail = 'sms : ส่ง sms ตาม orderid '.$ams->orderid.' ไม่สำเร็จ';
					$ams->qouted = $ams->getBalanceStatementAdmin();
					$ams->addcredit = $this->amount;
					$ams->delcredit = 0;
					$ams->balance = ($ams->qouted+$ams->addcredit)-$ams->delcredit;

					$ams->addStatementAdmin();
				}

				if($this->partnerid != '')
				{
					$ams = new statement;

					$ams->partnerid = $this->partnerid;
					$ams->broker = $this->broker;
					$ams->orderid = $this->orderid;
					$ams->ordertype = 'sendsms';
					$ams->detail = 'sms : ส่ง sms ตาม orderid '.$ams->orderid;
					$ams->qouted = $ams->getBalanceStatementPartner();
					$ams->addcredit = $this->amount;
					$ams->delcredit = 0;
					$ams->balance = ($ams->qouted+$ams->addcredit)-$ams->delcredit;

					$ams->addStatementPartner();
				}

				if($this->memberid != '')
				{
					if($memcredit > 0)
					{
						$ams = new statement;

						$ams->membercode = $this->membercode;
						$ams->memberid = $this->memberid;
						$ams->broker = $this->broker;
						$ams->orderid = $this->orderid;
						$ams->ordertype = 'sendsms';
						$ams->detail = 'sms : ส่ง sms ตาม orderid '.$ams->orderid;
						$ams->qouted = $ams->getBalanceStatementMember();
						$ams->addcredit = $this->amount;
						$ams->delcredit = 0;
						$ams->balance = ($ams->qouted+$ams->addcredit)-$ams->delcredit;

						$ams->addStatementMember();
					}
				}

			}

		}

	}

	private function addLogSMS(){
		$this->ip = $this->getUserIP();

		$date=date('Y-m-d');
		$time=date('H:i:s');

		$sql="insert into log_sms ";
		$sql.="(fromcode,fromid,fromtype,api,ip,orderid,adddate,addtime) ";
		$sql.="values ";
		$sql.="('".$this->mysqli->real_escape_string($this->fromcode)."','".$this->mysqli->real_escape_string($this->fromid)."','".$this->mysqli->real_escape_string($this->fromtype)."','".$this->mysqli->real_escape_string($this->api)."','".$this->mysqli->real_escape_string($this->ip)."','".$this->mysqli->real_escape_string($this->orderid)."','".$this->mysqli->real_escape_string($date)."','".$this->mysqli->real_escape_string($time)."') ";
		$qry=$this->mysqli->query($sql);
	}

	function getUserIP()
	{
	    $client  = @$_SERVER['HTTP_CLIENT_IP'];
	    $forward = @$_SERVER['HTTP_X_FORWARDED_FOR'];
	    $remote  = $_SERVER['REMOTE_ADDR'];

	    if(filter_var($client, FILTER_VALIDATE_IP))
	    {
	        $ip = $client;
	    }
	    elseif(filter_var($forward, FILTER_VALIDATE_IP))
	    {
	        $ip = $forward;
	    }
	    else
	    {
	        $ip = $remote;
	    }

	    return $ip;
	}


}
?>
