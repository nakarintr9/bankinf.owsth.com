<?php
require_once '../core/core.php';
class payment_migrate extends core {
	public $fromcode;
	public $fromid;
	public $fromtype;
	public $amount;
	public $orderid;
	public $partnerid;
	public $membercode;
	public $memberid;
	public $detail;
	public $partnerpaymentid;
	public $memberpaymentid;

	function __construct() {
		parent::__construct();
		$v3_api = include('../../../config/bankinf/v3_api.php');
		$v3_api = (array)$v3_api;
		$this->url = $v3_api['v3_base_url'];
		$this->key = $v3_api['v3_key'];
  }

	public function get_member_v3(){
    $post_data = array(
     'service' => 'getmemberdetail',
     'key' => $this->key,
     'fromcode' => $this->fromcode,
     'fromid' => $this->fromid,
     'fromtype' => $this->fromtype,
    );

    $ch = curl_init($this->url);                                                                      
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($post_data));
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
     'Content-Type: application/json', 
     'Content-Length: ' . strlen(json_encode($post_data)))
    );
    $content = curl_exec($ch);
    if($content){
    	$return = json_decode($content, true);
    }else{
      $return = 'ERROR';
    }
    curl_close($ch);
    return $return;
	}

	public function migrate_member_credit_to_v3(){
		$post_data = array(
     'service' => 'migratecredit',
     'key' => $this->key,
     'fromcode' => $this->fromcode,
     'fromid' => $this->fromid,
     'fromtype' => $this->fromtype,
     'amount' => $this->amount,
     'orderid' =>$this->orderid
    );

    $ch = curl_init($this->url);                                                                      
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($post_data));
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
     'Content-Type: application/json', 
     'Content-Length: ' . strlen(json_encode($post_data)))
    );
    $content = curl_exec($ch);
    if($content){
    	$return = json_decode($content, true);
    }else{
      $return = 'ERROR';
    }
    curl_close($ch);
    return $return;
	}

	public function check_member_is_topuptosim_v2($membercode, $memberid){
		$sql = "select * from member 
						where membercode = '".$membercode."' 
						and memberid = '".$memberid."' 
						and feeinternetset = 1";
		$qry = $this->mysqli->query($sql);
		$num = mysqli_num_rows($qry);
		if($num == 0){
			return false;
		}
		else{
			return true;
		}
	}

	public function migrate_member_credit(){
		$sql="select COUNT(ID) as total from kiosk where membercode = '".$this->membercode."' and memberid = '".$this->memberid."' ";
		$qry=$this->mysqli->query($sql);
		$row=mysqli_fetch_array($qry);
		$kiosk_v2=$row['total'];

		if($kiosk_v2 > 0){
			$sql="select COUNT(ID) as total from list_migrate where membercode = '".$this->membercode."' and memberid = '".$this->memberid."' ";
			$qry=$this->mysqli->query($sql);
			$row=mysqli_fetch_array($qry);
			$kiosk_v3=$row['total'];

			$amount_per_kiosk = $this->amount / $kiosk_v2;
			if($kiosk_v3 > 0){
				$this->amount = $kiosk_v3 * $amount_per_kiosk; 
			}
		}

		$isTop2sim = $this->check_member_is_topuptosim_v2($this->membercode, $this->memberid);

		//member is not topuptosim
		if(!$isTop2sim){
			$res = $this->get_member_v3();
			if($res != 'ERROR'){
				if($res['error_code'] == 'E00'){
					$res = $this->migrate_member_credit_to_v3();
					if($res != 'ERROR'){
						if($res['error_code'] == 'E00'){
							//member
							$sta = new statement;
							$sta->membercode = $this->membercode;
							$sta->memberid = $this->memberid;
							$sta->orderid = $this->orderid;
							$sta->ordertype = 'migrate';
							$sta->detail = $this->detail;
							$qouted = $sta->getBalanceStatementMember();
							$addcredit = 0;
							$delcredit = $this->amount;
							$balance = ($qouted+$addcredit)-$delcredit;											
							$sta->qouted = $qouted;
							$sta->addcredit = $addcredit;
							$sta->delcredit = $delcredit;
							$sta->balance = $balance;
							$sta->addStatementMember();

							//partner
							$sta = new statement;
							$sta->partnerid = $this->partnerid;
							$sta->orderid = $this->orderid;
							$sta->ordertype = 'migrate';
							$sta->detail = $this->detail;
							$qouted = $sta->getBalanceStatementPartner();
							$addcredit = 0;
							$delcredit = $this->amount;
							$balance = ($qouted+$addcredit)-$delcredit;											
							$sta->qouted = $qouted;
							$sta->addcredit = $addcredit;
							$sta->delcredit = $delcredit;
							$sta->balance = $balance;
							$sta->addStatementPartner();
						}
					}
				}
			}
		}
		else{
			$res = array(
				"error_code" => 'E99',
				"error_text" => 'Member is Topuptosim cannot migrate credit',
			);
		}
		
		return $res;
	}

 	public function cancelpayment_v3(){
    $post_data = array(
     'service' => 'cancelpayment',
     'key' => $this->key,
     'fromcode' => $this->fromcode,
     'fromid' => $this->fromid,
     'fromtype' => $this->fromtype,
     'payment_id' =>$this->orderid
    );

    $ch = curl_init($this->url);                                                                      
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($post_data));
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
     'Content-Type: application/json', 
     'Content-Length: ' . strlen(json_encode($post_data)))
    );
    $content = curl_exec($ch);
    if($content){
      $return = json_decode($content, true);
      if($return['error_code'] == 'E00'){
      	$amount_cancel_v3 = $return['amount_cancel'];
      	//member
				$sta = new statement;
				$sta->membercode = $this->membercode;
				$sta->memberid = $this->memberid;
				$sta->orderid = $this->memberpaymentid;
				$sta->ordertype = 'migrate';
				$sta->detail = $this->detail;
				$qouted = $sta->getBalanceStatementMember();
				$addcredit = $amount_cancel_v3;
				$delcredit = 0;
				$balance = ($qouted+$addcredit)-$delcredit;											
				$sta->qouted = $qouted;
				$sta->addcredit = $addcredit;
				$sta->delcredit = $delcredit;
				$sta->balance = $balance;
				$sta->addStatementMember();

				//partner
				$sta = new statement;
				$sta->partnerid = $this->partnerid;
				$sta->orderid = $this->partnerpaymentid;
				$sta->ordertype = 'migrate';
				$sta->detail = $this->detail;
				$qouted = $sta->getBalanceStatementPartner();
				$addcredit = $amount_cancel_v3;
				$delcredit = 0;
				$balance = ($qouted+$addcredit)-$delcredit;											
				$sta->qouted = $qouted;
				$sta->addcredit = $addcredit;
				$sta->delcredit = $delcredit;
				$sta->balance = $balance;
				$sta->addStatementPartner();
      }
    }else{
      $return = 'ERROR';
    }
    curl_close($ch);
    return $return;
  }
}
?>