<?php
require_once '../core/core.php';
class statement extends core {
	public $membercode;
	public $memberid;
	private $partnercode = 'PN';
	public $partnerid;
	private $admincode = 'AM';
	private $adminid = '1';
	public $kioskcode;
	public $kioskid;
	
	public $broker;
	public $orderid;
	public $ordertype;
	public $detail;
	public $qouted;
	public $addcredit;
	public $delcredit;
	public $balance;
	
	public $logic;
	//public $logic='';
	
	function __construct() {
		parent::__construct();
  }

	public function getCreditAdmin(){
		$sql="select  ifnull(credit,0) as x from broker where brokerid = '".$this->mysqli->real_escape_string($this->broker)."' ";
		$qry=$this->mysqli->query($sql);
		$row=mysqli_fetch_array($qry);
		
		return $row['x'];
	}
	
	public function getCreditPartner(){
		$sql="select ifnull(credit,0) as x from partner where partnercode = '".$this->mysqli->real_escape_string($this->partnercode)."' and partnerid = '".$this->mysqli->real_escape_string($this->partnerid)."' ";
		$qry=$this->mysqli->query($sql);
		$row=mysqli_fetch_array($qry);
		
		return $row['x'];
	}
	
	public function getCreditMember(){
		$sql="select ifnull(credit,0) as x from member where membercode = '".$this->mysqli->real_escape_string($this->membercode)."' and memberid = '".$this->mysqli->real_escape_string($this->memberid)."' ";
		$qry=$this->mysqli->query($sql);
		$row=mysqli_fetch_array($qry);
		
		return $row['x'];
	}
	
	public function getCreditKiosk(){
		$sql="select ifnull(credit,0) as x from kiosk where kioskcode = '".$this->mysqli->real_escape_string($this->kioskcode)."' and kioskid = '".$this->mysqli->real_escape_string($this->kioskid)."' ";
		$qry=$this->mysqli->query($sql);
		$row=mysqli_fetch_array($qry);
		
		return $row['x'];
	}
	
	public function getBalanceStatementAdmin(){		
		$sql="select ifnull(balance,0) as x from statement_admin where admincode = '".$this->mysqli->real_escape_string($this->admincode)."' and adminid = '".$this->mysqli->real_escape_string($this->adminid)."' and broker = '".$this->mysqli->real_escape_string($this->broker)."' order by ID DESC limit 1";
		$qry=$this->mysqli->query($sql);
		$row=mysqli_fetch_array($qry);
		
		//return $sql;
		return $row['x'];
	}
	
	public function getBalanceStatementPartner(){
		$sql="select ifnull(balance,0) as x from statement_partner where partnercode = '".$this->mysqli->real_escape_string($this->partnercode)."' and partnerid = '".$this->mysqli->real_escape_string($this->partnerid)."' order by ID DESC limit 1";
		$qry=$this->mysqli->query($sql);
		$row=mysqli_fetch_array($qry);
		
		//return $sql;
		return $row['x'];
	}

	public function getBalanceStatementMember(){
		$sql="select credit as x from member where membercode = '".$this->mysqli->real_escape_string($this->membercode)."' and memberid = '".$this->mysqli->real_escape_string($this->memberid)."'";
		$qry=$this->mysqli->query($sql);
		$row=mysqli_fetch_array($qry);

		return $row['x'];
	}
	
	public function getBalanceStatementMember_OLD(){
		$sql="select ifnull(balance,0) as x from statement_member where membercode = '".$this->mysqli->real_escape_string($this->membercode)."' and memberid = '".$this->mysqli->real_escape_string($this->memberid)."' order by ID DESC limit 1";
		$qry=$this->mysqli->query($sql);
		$row=mysqli_fetch_array($qry);

		return $row['x'];
	}
	
	public function getBalanceStatementKiosk(){
		$sql="select ifnull(balance,0) as x from statement_kiosk where kioskcode = '".$this->mysqli->real_escape_string($this->kioskcode)."' and kioskid = '".$this->mysqli->real_escape_string($this->kioskid)."' order by ID DESC limit 1";
		$qry=$this->mysqli->query($sql);
		$row=mysqli_fetch_array($qry);

		return $row['x'];
	}
	
	public function addStatementAdmin()
	{				
		$net = $this->addcredit - $this->delcredit;

		$this->mysqli->query("SET AUTOCOMMIT=0");
		$this->mysqli->query("START TRANSACTION");
		
		if($this->logic == ''){
			$sql = "select credit from broker where brokerid = '".$this->mysqli->real_escape_string($this->broker) ."' for update";
			$qry = $this->mysqli->query($sql);
			
			$row = mysqli_fetch_array($qry);
			$quote_credit = $row['credit'];
			
			$sql = "update broker set credit = credit + ". $net ." where brokerid = '".  $this->mysqli->real_escape_string($this->broker)."'";
			$qry1 = $this->mysqli->query($sql);
			$balance = $quote_credit + $net;			
	  }
		else{
			$sql = "select credit from broker where brokerid = '".$this->mysqli->real_escape_string($this->broker) ."' for update";
			$qry = $this->mysqli->query($sql);

			$quote_credit = $this->qouted;

	   	$sql = "update broker set ".  $this->logic ." where brokerid = '".$this->mysqli->real_escape_string($this->broker)."' ";
	   	$qry1 = $this->mysqli->query($sql);

	   	$balance = $this->balance;	   
		}
				
		$date=date('Y-m-d');
		$time=date('H:i:s');		
		
		$sql = "insert into statement_admin (admincode, adminid, broker, orderid, ordertype, detail, qouted, addcredit, delcredit, balance, adddate, addtime) values ";
		$sql .= "('".$this->mysqli->real_escape_string($this->admincode)."','".$this->mysqli->real_escape_string($this->adminid)."','".$this->mysqli->real_escape_string($this->broker)."'";
		$sql .= ",'".$this->mysqli->real_escape_string($this->orderid)."','".$this->mysqli->real_escape_string($this->ordertype)."','".$this->mysqli->real_escape_string($this->detail)."'";
		$sql .= ",'".$this->mysqli->real_escape_string($quote_credit)."','".$this->mysqli->real_escape_string($this->addcredit)."','".$this->mysqli->real_escape_string($this->delcredit)."'";
		$sql .= ",'".$this->mysqli->real_escape_string($balance)."','".$this->mysqli->real_escape_string($date)."','".$this->mysqli->real_escape_string($time)."')";
		$qry2 = $this->mysqli->query($sql);
	
		if($qry1 and $qry2){ 
			$this->mysqli->query("COMMIT");			
		} 
		else{
			$this->mysqli->query("ROLLBACK");			
		}
		$this->mysqli->query("SET AUTOCOMMIT=1");
	}
	
	public function addStatementPartner()
	{		
		$net = $this->addcredit - $this->delcredit;
		$this->mysqli->query("SET AUTOCOMMIT=0");
		$this->mysqli->query("START TRANSACTION");
		
		if($this->logic == ''){
			$sql = "select credit from partner where partnercode = '".$this->mysqli->real_escape_string($this->partnercode)."' and partnerid = ".$this->mysqli->real_escape_string($this->partnerid) ." for update";
			$qry = $this->mysqli->query($sql);
			
			$row = mysqli_fetch_array($qry);
			$quote_credit = $row['credit'];

			$sql = "update partner set credit = credit + ". $net ." where partnercode = '".$this->mysqli->real_escape_string($this->partnercode)."' and 	partnerid = ".  $this->mysqli->real_escape_string($this->partnerid);
			$qry1 = $this->mysqli->query($sql);
			$balance = $quote_credit + $net;
		}
		else{
			$sql = "select credit from partner where partnercode = '".$this->mysqli->real_escape_string($this->partnercode)."' and partnerid = ".$this->mysqli->real_escape_string($this->partnerid) ." for update";
			$qry = $this->mysqli->query($sql);
			
			$quote_credit = $this->qouted;

			$sql = "update partner set ".  $this->logic ." where partnercode = '".$this->mysqli->real_escape_string($this->partnercode)."' and 	partnerid = ".  $this->mysqli->real_escape_string($this->partnerid);
			$qry1 = $this->mysqli->query($sql);

			$balance = $this->balance;
		}

		$date=date('Y-m-d');
		$time=date('H:i:s');		

		$sql = "insert into statement_partner (partnercode, partnerid, broker, orderid, ordertype, detail, qouted, addcredit, delcredit,
			balance,adddate,addtime) values ";
		$sql .= "('".$this->mysqli->real_escape_string($this->partnercode)."','".$this->mysqli->real_escape_string($this->partnerid)."','".$this->mysqli->real_escape_string($this->broker)."'";
		$sql .= ",'".$this->mysqli->real_escape_string($this->orderid)."','".$this->mysqli->real_escape_string($this->ordertype)."','".$this->mysqli->real_escape_string($this->detail)."'";
		$sql .= ",'".$this->mysqli->real_escape_string($quote_credit)."','".$this->mysqli->real_escape_string($this->addcredit)."','".$this->mysqli->real_escape_string($this->delcredit)."'";
		$sql .= ",'".$this->mysqli->real_escape_string($balance)."','".$this->mysqli->real_escape_string($date)."','".$this->mysqli->real_escape_string($time)."');";
		$qry2 = $this->mysqli->query($sql);
		
		if($qry1 and $qry2){ 
			$this->mysqli->query("COMMIT");			
		} 
		else{
			$this->mysqli->query("ROLLBACK");			
		}		
		$this->mysqli->query("SET AUTOCOMMIT=1");
	}

	public function addStatementMember()
	{		
		$net = $this->addcredit - $this->delcredit;
	  $this->mysqli->query("SET AUTOCOMMIT=0");
		$this->mysqli->query("START TRANSACTION");
		
		if($this->logic=='')
		{
			$sql = "select credit from member where membercode = '".$this->mysqli->real_escape_string($this->membercode)."' and memberid = ".$this->mysqli->real_escape_string($this->memberid) ." for update";
			$qry = $this->mysqli->query($sql);

			// KH add for precaution check and if it is really can't get data it will be initial to -1 so we know it is this case of not able to get credit
			if(mysqli_num_rows($qry)== 0){
				$quote_credit = -0.00123;
				$balance = $quote_credit + $net;
			}
			else
			{
				$row = mysqli_fetch_array($qry);
				$quote_credit = $row['credit'];
				$balance = $quote_credit + $net;

				$sql="update member set credit = credit + ". $net ." where membercode = '".$this->mysqli->real_escape_string($this->membercode)."' and memberid = '".$this->mysqli->real_escape_string($this->memberid)."' ";
				$qry1=$this->mysqli->query($sql);
			}
		}
		else
		{
			$sql = "select credit from member where membercode = '".$this->mysqli->real_escape_string($this->membercode)."' and memberid = ".$this->mysqli->real_escape_string($this->memberid) ." for update";
			$qry = $this->mysqli->query($sql);
			$quote_credit = $this->qouted;

			$sql="update member set ".$this->logic." where membercode = '".$this->mysqli->real_escape_string($this->membercode)."' and memberid = '".$this->mysqli->real_escape_string($this->memberid)."' ";
			$qry1=$this->mysqli->query($sql);

			$balance = $this->balance;
		}

		$date=date('Y-m-d');
		$time=date('H:i:s');
		
		$sql="insert into statement_member ";
		$sql.="(membercode,memberid,broker";
		$sql.=",orderid,ordertype,detail";
		$sql.=",qouted,addcredit,delcredit";
		$sql.=",balance,adddate,addtime)";
		$sql.=" values ";
		$sql.="('".$this->mysqli->real_escape_string($this->membercode)."','".$this->mysqli->real_escape_string($this->memberid)."','".$this->mysqli->real_escape_string($this->broker)."'";
		$sql.=",'".$this->mysqli->real_escape_string($this->orderid)."','".$this->mysqli->real_escape_string($this->ordertype)."','".$this->mysqli->real_escape_string($this->detail)."'";
		$sql.=",'".$this->mysqli->real_escape_string($quote_credit)."','".$this->mysqli->real_escape_string($this->addcredit)."','".$this->mysqli->real_escape_string($this->delcredit)."'";
		$sql.=",'".$this->mysqli->real_escape_string($balance)."','".$this->mysqli->real_escape_string($date)."','".$this->mysqli->real_escape_string($time)."')";
		$qry2=$this->mysqli->query($sql);

		if($qry1 and $qry2){ 
			$this->mysqli->query("COMMIT");			
		} 
		else{
			$this->mysqli->query("ROLLBACK");			
		}		
		$this->mysqli->query("SET AUTOCOMMIT=1");
	}
	
	public function addStatementMember_OLD()
	{		
		$date=date('Y-m-d');
		$time=date('H:i:s');
		
		$sql="insert into statement_member ";
		$sql.="(membercode,memberid,broker";
		$sql.=",orderid,ordertype,detail";
		$sql.=",qouted,addcredit,delcredit";
		$sql.=",balance,adddate,addtime)";
		$sql.=" values ";
		$sql.="('".$this->mysqli->real_escape_string($this->membercode)."','".$this->mysqli->real_escape_string($this->memberid)."','".$this->mysqli->real_escape_string($this->broker)."'";
		$sql.=",'".$this->mysqli->real_escape_string($this->orderid)."','".$this->mysqli->real_escape_string($this->ordertype)."','".$this->mysqli->real_escape_string($this->detail)."'";
		$sql.=",'".$this->mysqli->real_escape_string($this->qouted)."','".$this->mysqli->real_escape_string($this->addcredit)."','".$this->mysqli->real_escape_string($this->delcredit)."'";
		$sql.=",'".$this->mysqli->real_escape_string($this->balance)."','".$this->mysqli->real_escape_string($date)."','".$this->mysqli->real_escape_string($time)."')";
		$qry=$this->mysqli->query($sql);
		
		if($this->logic=='')
		{
			//$sql="update member set credit = (credit + $this->addcredit) - $this->delcredit where membercode = '".$this->membercode."' and memberid = '".$this->memberid."' ";
			$sql="update member set credit = $this->balance where membercode = '".$this->mysqli->real_escape_string($this->membercode)."' and memberid = '".$this->mysqli->real_escape_string($this->memberid)."' ";
			$qry=$this->mysqli->query($sql);
		}
		else
		{
			$sql="update member set $this->logic where membercode = '".$this->mysqli->real_escape_string($this->membercode)."' and memberid = '".$this->mysqli->real_escape_string($this->memberid)."' ";
			$qry=$this->mysqli->query($sql);
		}
	}
	
	public function addStatementKiosk()
	{
		$date=date('Y-m-d');
	  $time=date('H:i:s');
	  
	  $sql="insert into statement_kiosk ";
	  $sql.="(kioskcode,kioskid,broker";
	  $sql.=",orderid,ordertype,detail";
	  $sql.=",qouted,addcredit,delcredit";
	  $sql.=",balance,adddate,addtime)";
	  $sql.=" values ";
	  $sql.="('".$this->mysqli->real_escape_string($this->kioskcode)."','".$this->mysqli->real_escape_string($this->kioskid)."','".$this->mysqli->real_escape_string($this->broker)."'";
	  $sql.=",'".$this->mysqli->real_escape_string($this->orderid)."','".$this->mysqli->real_escape_string($this->ordertype)."','".$this->mysqli->real_escape_string($this->detail)."'";
	  $sql.=",'".$this->mysqli->real_escape_string($this->qouted)."','".$this->mysqli->real_escape_string($this->addcredit)."','".$this->mysqli->real_escape_string($this->delcredit)."'";
	  $sql.=",'".$this->mysqli->real_escape_string($this->balance)."','".$this->mysqli->real_escape_string($date)."','".$this->mysqli->real_escape_string($time)."')";
	  $qry=$this->mysqli->query($sql);
	  
	  if($this->logic=='')
	  {
	   $sql="update kiosk set credit = $this->balance where kioskcode = '".$this->mysqli->real_escape_string($this->kioskcode)."' and kioskid = '".$this->mysqli->real_escape_string($this->kioskid)."' ";
	   $qry=$this->mysqli->query($sql);
	  }
	  else
	  {
	   $sql="update kiosk set $this->logic where kioskcode = '".$this->mysqli->real_escape_string($this->kioskcode)."' and kioskid = '".$this->mysqli->real_escape_string($this->kioskid)."' ";
	   $qry=$this->mysqli->query($sql);	  
	  }	   
	}
	
}
/*
$sta = new statement;

$sta->broker='bk00001';
$sta->orderid='1';
$sta->ordertype='topupmobile';
$sta->detail='เติมเงินมือถือ : เติมเงินให้ลูกค้าตาม orderid 12345 ';
$sta->qouted='100';
$sta->addcredit='0';
$sta->delcredit='10';
$sta->balance='90';

$sta->addStatementAdmin();
*/
?>