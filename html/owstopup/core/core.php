<?php
class core {
	function __construct() {
		date_default_timezone_set('Asia/Bangkok');

		$db_user = include(getcwd().'/../../../config/bankinf/db_user.php');
		$db_user = (array)$db_user;
		
		$user=$db_user['owstopup']['username'];
		$pass=$db_user['owstopup']['password'];
		$host=$db_user['owstopup']['hostname'];
		$db=$db_user['owstopup']['database'];

		$this->mysqli = new mysqli($host, $user, $pass, $db);
		if ($this->mysqli->connect_errno) {
		  printf("Connect failed: %s\n", $this->mysqli->connect_error);
		  exit();
		}
		else {
		  $this->mysqli->query("SET NAMES UTF8");
		  $this->mysqli->query("SET character_set_results=UTF8");
		  $this->mysqli->query("SET character_set_client=UTF8");
		  $this->mysqli->query("SET character_set_connection=UTF8");
		  setlocale(LC_ALL, 'th_TH');
		}
  }
}
?>