<?php defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Model extends CI_Model {
  function __construct()
  {
    date_default_timezone_set('Asia/Bangkok');
    parent::__construct();      
  }
}
