<?php defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH.'/libraries/REST_Controller.php';

class API_Controller extends REST_Controller {

	function __construct()
  {
    parent::__construct();  
    date_default_timezone_set('Asia/Bangkok');  
    $this->load->library('apilog');
    $this->load->library('errorcode'); 
  }

}
