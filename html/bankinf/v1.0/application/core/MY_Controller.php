<?php defined('BASEPATH') OR exit('No direct script access allowed');

require_once APPPATH.'libraries/MX/Controller.php';

class MY_Controller extends MX_Controller {

  function __construct()
  {
    date_default_timezone_set('Asia/Bangkok');
    parent::__construct();   
    $this->load->library('errorcode'); 
    $this->load->library('apilog');     
    $this->load->library('librarycurl');

    $loan_api = include(getcwd().'/../../../config/bankinf/loan_api.php');
    $loan_api = (array)$loan_api;
    $this->url_loan_api = $loan_api['url_loan_api'];

    $obj_base_url = include(getcwd().'/../../../config/bankinf/base_url.php');
    $base_url = (array)$obj_base_url;
    $this->url_v3 = $base_url['url_v3'];
    $this->url_v3_2 = $base_url['url_v3_2'];
    $this->key_v3 = $base_url['key_v3'];
    $this->url_gw = $base_url['url_gw'];
    $this->key_gw = $base_url['key_gw'];
    $this->base_url = $base_url['base_url'];
    $this->username_gw = $base_url['username_gw'];
    $this->line_token = $base_url['line_token'];
    $this->line_notify_url = $base_url['line_notify_url'];
    $this->line_token_powerbank = isset($base_url['line_token_powerbank']) ? $base_url['line_token_powerbank'] : '';
  }

}
