<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
$config['api_log']['is_save_log']['PUSHBACK'] = true;
$config['api_log']['is_save_log']['RESPONSE_PUSHBACK'] = true;
$config['api_log']['is_save_log']['CRITICAL'] = true;
$config['api_log']['is_save_log']['NOTICE'] = true;
$config['api_log']['is_save_log']['INFO'] = true;
$config['api_log']['is_save_log']['ERROR'] = true;
$config['api_log']['is_save_log']['RECEIVE'] = true;
$config['api_log']['is_save_log']['REQUEST'] = true;
$config['api_log']['is_save_log']['RESPONSE'] = true;
$config['api_log']['is_save_log']['SEND'] = true;
?>