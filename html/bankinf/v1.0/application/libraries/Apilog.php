<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

class Apilog{

  function __construct(){
    $CI = &get_instance();
    $this->api_log = $CI->config->item('api_log');
    $this->is_save_log = $this->api_log['is_save_log'];
  }

  function log($module, $type, $detail, $order_id ){
    if($this->is_save_log[$type]){
      $api_data = "('".$order_id."','".$type."','".$module."','".$detail."','".date('Y-m-d')."','".date('H:i:s')."','".get_client_ip()."')";
      $this->write_log($api_data);
    }
  }

  function write_log($contents){
    $path_file = getcwd()."/../../../log/apilog-active.txt";
    file_put_contents($path_file, $contents."\r\n", FILE_APPEND);
  }

  function get_client_ip() {
    $ipaddress = '';
    if (getenv('HTTP_CLIENT_IP'))
        $ipaddress = getenv('HTTP_CLIENT_IP');
    else if(getenv('HTTP_X_FORWARDED_FOR'))
        $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
    else if(getenv('HTTP_X_FORWARDED'))
        $ipaddress = getenv('HTTP_X_FORWARDED');
    else if(getenv('HTTP_FORWARDED_FOR'))
        $ipaddress = getenv('HTTP_FORWARDED_FOR');
    else if(getenv('HTTP_FORWARDED'))
       $ipaddress = getenv('HTTP_FORWARDED');
    else if(getenv('REMOTE_ADDR'))
        $ipaddress = getenv('REMOTE_ADDR');
    else
        $ipaddress = 'UNKNOWN';
    return $ipaddress;
  }
  
}

