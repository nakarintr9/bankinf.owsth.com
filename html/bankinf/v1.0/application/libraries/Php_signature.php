<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Php_signature {
  function __construct(){
  	define("DS","/",true);
		define('BASE_PATH',realpath(dirname(__FILE__)).DS,true);
		set_include_path(BASE_PATH.'php_signature/');
  	include('Crypt/RSA.php');
  }
}