<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Payment_migrate extends MY_Controller {

  function __construct() {
    parent::__construct();
    $this->load->model('common/payment_migrate_m');
    $this->load->module('common/logpaymentv3');
    // $this->url_api_migrate = $this->url_v3.'/api/v1.0/api_migrate_json/app_api';
    $this->url_api_migrate = $this->url_v3.'/api/v1.0/api_server_json/app_api';
    $this->url_api_server_v3 = $this->url_v3.'/api/v1.0/api_server_json/app_api';
  }

  function migrate_member_credit($data){

    $fromcode = $data['fromcode'];
    $fromid = $data['fromid'];
    $fromtype = $data['fromtype'];
    $membercode = $data['membercode'];
    $memberid = $data['memberid'];
    $partnerid = $data['partnerid'];

    $orderid = $data['orderid'];
    $amount = $data['amount'];
    $detail = $data['detail'];

    //(amount * kiosk_v3/kiosk_v2) --------------------------------------------
    /*
    $where = array(
      'membercode' => $membercode,
      'memberid' => $memberid,
    );

    $kiosk_v2 = 0;
    $get_tk = $this->payment_migrate_m->get_total_kiosk($where);
    if(count($get_tk) > 0){
      $kiosk_v2 = $get_tk->total;
    }

    $kiosk_v3 = 0;
    $get_lm = $this->payment_migrate_m->get_total_list_migrate($where);
    if(count($get_lm) > 0){
      $kiosk_v3 = $get_lm->total;
    }

    if($kiosk_v2>0 && $kiosk_v3>0){
      $amount =  $amount * $kiosk_v3/$kiosk_v2; 
      $data['amount'] = $amount;
    }
    */
    //--------------------------------------------------------------------------

    //Top2sim
    $isTop2sim = $this->check_member_is_topuptosim_v2($membercode, $memberid);

    //member is not topuptosim
    if(!$isTop2sim){
      $res = $this->get_member_v3($data);
      if($res != 'ERROR'){
        if($res['error_code'] == 'E00'){
          $res = $this->migrate_member_credit_to_v3($data);
          if($res != 'ERROR'){
            if($res['error_code'] == 'E00'){

              //statement_member
              $operator = 'del';
              $array = array(
                'membercode' => $membercode,
                'memberid' => $memberid,
                'broker' => '',
                'ordertype' => 'migrate',
                'detail' => $detail,
                'orderid' => $orderid,
              );
              $this->statement->create_statement_member($membercode, $memberid, $operator, $amount, $array);

              //statement_partner
              $operator = 'del';
              $array = array(
                'partnercode' => 'PN',
                'partnerid' => $partnerid,
                'broker' => '',
                'ordertype' => 'migrate',
                'detail' => $detail,
                'orderid' => $orderid,
              );
              $this->statement->create_statement_partner($partnerid, $operator, $amount, $array);

              //ปิดแจ้งเตือน เครดิต member ใกล้หมด หลัง migrate เสร็จ
              $update = array(
                'callone' => 1, 
                'calltwo' => 1, 
                'callthree' => 1,
                'alertone' => -1000,
                'alerttwo' => -1000,
                'alertthree' => -1000,
              );
              $where = array(
                'membercode' => $membercode, 
                'memberid' => $memberid, 
              );
              $this->payment_migrate_m->update_member($update, $where);
            }
          }
        }
      }
    }
    else{
      $res = array(
        "error_code" => 'E99',
        "error_text" => 'Member is Topuptosim cannot migrate credit',
      );
    }
    
    return $res;
  }

  function migrate_partner_credit($data){
    $fromcode = $data['fromcode'];
    $fromid = $data['fromid'];
    $fromtype = $data['fromtype'];
    $partnerid = $data['fromid'];
    $orderid = $data['orderid'];
    $amount = $data['amount'];
    $detail = $data['detail'];

    $res = $this->migrate_member_credit_to_v3($data);
    if($res != 'ERROR'){
      if($res['error_code'] == 'E00'){
        //statement_partner
        $operator = 'del';
        $array = array(
          'partnercode' => 'PN',
          'partnerid' => $partnerid,
          'broker' => '',
          'ordertype' => 'migrate',
          'detail' => $detail,
          'orderid' => $orderid,
        );
        $this->statement->create_statement_partner($partnerid, $operator, $amount, $array);
      }
    }
    return $res;
  }

  function get_member_v3($data, $trans_log_id=''){
    $post_data = array(
      'service' => 'getmemberdetail',
      'key' => $this->key_v3,
      'fromcode' => $data['fromcode'],
      'fromid' => $data['fromid'],
      'fromtype' => 'member'
    );

    $ch = curl_init($this->url_api_migrate);                                                                      
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($post_data));
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
    curl_setopt($ch, CURLOPT_TIMEOUT, 30);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
     'Content-Type: application/json', 
     'Content-Length: ' . strlen(json_encode($post_data)))
    );

    $insert = array(
      'api' => 'getmemberdetail',
      'url' => $this->url_api_migrate,
      'postdata' => json_encode($post_data),
      'fromcode' => $data['fromcode'],
      'fromid' => $data['fromid'],
      'fromchannel' => '',
      'transid' => $trans_log_id,
      'status' => 'inprogress',
      'postdatetime' => date('Y-m-d H:i:s')
    );
    $logid = $this->logpaymentv3->insert_log_v3($insert);

    $content = curl_exec($ch);

    if($content){
      $return = json_decode($content, true);
    }else{
      $return = 'ERROR';
      $content = "Curl Error: ".curl_error($ch);
    }

    $update_log = array(
      'returndata' => $content,
      'status' => 'complete',
      'returndatetime' => date('Y-m-d H:i:s')
    );
    $this->logpaymentv3->update_log_v3($update_log, $logid);

    curl_close($ch);
    return $return;
  }

  function migrate_member_credit_to_v3($data){
    $post_data = array(
     'service' => 'migratecredit',
     'key' => $this->key_v3,
     'fromcode' => $data['fromcode'],
     'fromid' => $data['fromid'],
     'fromtype' => $data['fromtype'],
     'amount' => $data['amount'],
     'orderid' =>$data['orderid'],
    );

    $ch = curl_init($this->url_api_migrate);                                                                      
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($post_data));
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
     'Content-Type: application/json', 
     'Content-Length: ' . strlen(json_encode($post_data)))
    );
    $content = curl_exec($ch);
    
    if($content){
      $return = json_decode($content, true);
    }else{
      $return = 'ERROR';
    }
    curl_close($ch);
    return $return;
  }

  function check_member_is_topuptosim_v2($membercode, $memberid){
    $where = array(
      'membercode' => $membercode,
      'memberid' => $memberid,
      'feeinternetset' => 1,
    );
    $get_member = $this->payment_migrate_m->get_member($where);
    if(count($get_member) > 0){
      return true;
    }
    else{
      return false;
    }
  }

  function cancel_payment_v3($fromcode, $fromid, $fromtype, $payment_id){
    $post_data = array(
      "service"=>"cancelpayment",
      "key" => $this->key_v3,
      "fromcode" => $fromcode,
      "fromid" => $fromid,
      "fromtype" => $fromtype,
      "payment_id" => $payment_id
    );

    $ch = curl_init($this->url_api_migrate);                                                                      
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($post_data));
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
    curl_setopt($ch, CURLOPT_TIMEOUT, 30);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
     'Content-Type: application/json', 
     'Content-Length: ' . strlen(json_encode($post_data)))
    );

    $content = curl_exec($ch);

    $insert = array(
      'api' => 'cancelpayment',
      'url' => $this->url_api_migrate,
      'postdata' => json_encode($post_data),
      'fromcode' => $fromcode,
      'fromid' => $fromid,
      'fromchannel' => '',
      'transid' => $payment_id,
      'status' => 'inprogress',
      'postdatetime' => date('Y-m-d H:i:s')
    );
    $logid = $this->logpaymentv3->insert_log_v3($insert);

    if($content){
      $return = json_decode($content, true);
    }else{
      $return = 'ERROR';
      $content = "Curl Error: ".curl_error($ch);
    }

    $update_log = array(
      'returndata' => $content,
      'status' => 'complete',
      'returndatetime' => date('Y-m-d H:i:s')
    );
    $this->logpaymentv3->update_log_v3($update_log, $logid);

    curl_close($ch);
    return $return;
  }

  function payment_credit_v3($data, $trans_log_id=''){
    $post_data = array(
     'service' => 'paymentcredit',
     'key' => $this->key_v3,
     'fromcode' => $data['fromcode'],
     'fromid' => $data['fromid'],
     'fromtype' => $data['fromtype'],
     'channel' => $data['channel'],
     'amount' => $data['amount'],
     'orderid' =>$data['orderid'],
     'fee' => $data['fee'],
     'addby' => $data['addby']
    );

    $ch = curl_init($this->url_api_migrate);                                                                      
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($post_data));
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
    curl_setopt($ch, CURLOPT_TIMEOUT, 30);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
     'Content-Type: application/json', 
     'Content-Length: ' . strlen(json_encode($post_data)))
    );

    $insert = array(
      'api' => 'paymentcredit',
      'url' => $this->url_api_migrate,
      'postdata' => json_encode($post_data),
      'fromcode' => $data['fromcode'],
      'fromid' => $data['fromid'],
      'fromchannel' => $data['channel'],
      'transid' => $trans_log_id,
      'status' => 'inprogress',
      'postdatetime' => date('Y-m-d H:i:s')
    );
    $logid = $this->logpaymentv3->insert_log_v3($insert);

    $content = curl_exec($ch);

    if($content){
      $return = json_decode($content, true);
    }else{
      $return = 'ERROR';
      $content = "Curl Error: ".curl_error($ch);
    }

    $update_log = array(
      'returndata' => $content,
      'status' => 'complete',
      'returndatetime' => date('Y-m-d H:i:s')
    );
    $this->logpaymentv3->update_log_v3($update_log, $logid);

    curl_close($ch);
    return $return;
  }

  function payment_credit_member_partner_v3($addby, $payment_member, $payment_partner, $trans_log_id=''){
    $post_data = array(
     'service' => 'paymentcreditmemberpartner',
     'key' => $this->key_v3,
     'payment_member' => $payment_member,
     'payment_partner' => $payment_partner,
     'trans_log_id' => $trans_log_id
    );

    $partnerid = isset($payment_partner['fromid']) ? $payment_partner['fromid'] : 0;
    $this->load->module('common/endpoint');
    $url = $this->endpoint->get($partnerid);

    $ch = curl_init($url);                                                                      
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($post_data));
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
    curl_setopt($ch, CURLOPT_TIMEOUT, 30);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
     'Content-Type: application/json', 
     'Content-Length: ' . strlen(json_encode($post_data)))
    );

    $insert = array(
      'api' => 'paymentcreditmemberpartner',
      'url' => $url,
      'postdata' => json_encode($post_data),
      'fromcode' => $payment_member['fromcode'],
      'fromid' => $payment_member['fromid'],
      'fromchannel' => $payment_member['channel'],
      'transid' => $trans_log_id,
      'status' => 'inprogress',
      'postdatetime' => date('Y-m-d H:i:s')
    );
    $logid = $this->logpaymentv3->insert_log_v3($insert);

    $content = curl_exec($ch);

    if($content){
      $return = json_decode($content, true);
    }else{
      $return = 'ERROR';
      $content = "Curl Error: ".curl_error($ch);
    }

    $update_log = array(
      'returndata' => $content,
      'status' => 'complete',
      'returndatetime' => date('Y-m-d H:i:s')
    );
    $this->logpaymentv3->update_log_v3($update_log, $logid);

    curl_close($ch);
    return $return;
  }

  function get_partner_detail_v3($data, $trans_log_id=''){
    $post_data = array(
     'service' => 'getpartnerdetail',
     'key' => $this->key_v3,
     'partnercode' => $data['partnercode'],
     'partnerid' => $data['partnerid'],
    );

    $ch = curl_init($this->url_api_migrate);                                                                      
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($post_data));
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
    curl_setopt($ch, CURLOPT_TIMEOUT, 30);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
     'Content-Type: application/json', 
     'Content-Length: ' . strlen(json_encode($post_data)))
    );

    $insert = array(
      'api' => 'getpartnerdetail',
      'url' => $this->url_api_migrate,
      'postdata' => json_encode($post_data),
      'fromcode' => $data['partnercode'],
      'fromid' => $data['partnerid'],
      'fromchannel' => '',
      'transid' => $trans_log_id,
      'status' => 'inprogress',
      'postdatetime' => date('Y-m-d H:i:s')
    );
    $logid = $this->logpaymentv3->insert_log_v3($insert);

    $content = curl_exec($ch);

    if($content){
      $return = json_decode($content, true);
    }else{
      $return = 'ERROR';
      $content = "Curl Error: ".curl_error($ch);
    }

    $update_log = array(
      'returndata' => $content,
      'status' => 'complete',
      'returndatetime' => date('Y-m-d H:i:s')
    );
    $this->logpaymentv3->update_log_v3($update_log, $logid);

    curl_close($ch);
    return $return;
  }

  function reverse_credit_v3($data, $cancel_amount, $cancelby){
    $post_data = array(
      'service' => 'reversecredit',
      'key' => $this->key_v3,
      'fromcode' => $data['fromcode'],
      'fromid' => $data['fromid'],
      'fromtype' => $data['fromtype'],
      'channel' => $data['channel'],
      'amount' => $cancel_amount,
      'orderid' =>$data['ID'],
      'fee' => $data['fee'],
      'cancelby' => $cancelby
    );

    $ch = curl_init($this->url_api_migrate);                                                                 
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($post_data));
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
    curl_setopt($ch, CURLOPT_TIMEOUT, 30);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
     'Content-Type: application/json',
     'Content-Length: ' . strlen(json_encode($post_data)))
    );

    $insert = array(
      'api' => 'reversecredit',
      'url' => $this->url_api_migrate,
      'postdata' => json_encode($post_data),
      'fromcode' => $data['fromcode'],
      'fromid' => $data['fromid'],
      'fromchannel' => $data['fromtype'],
      'transid' => $data['ID'],
      'status' => 'inprogress',
      'postdatetime' => date('Y-m-d H:i:s')
    );
    $logid = $this->logpaymentv3->insert_log_v3($insert);

    $content = curl_exec($ch);
    if($content){
      $return = json_decode($content, true);
    }else{
      $return = 'ERROR';
      $content = "Curl Error: ".curl_error($ch);
    }

    $update_log = array(
      'returndata' => $content,
      'status' => 'complete',
      'returndatetime' => date('Y-m-d H:i:s')
    );
    $this->logpaymentv3->update_log_v3($update_log, $logid);

    curl_close($ch);
    return $return;
  }

  function get_list_get_qr_powerbank($ref1, $ref2, $trans_log_id=''){
    $postdata  = array(
      "service"=>"get_list_get_qr_powerbank",
       "key"=>$this->key_v3,
       "ref1"=>$ref1,
       "ref2"=>$ref2
    );

    $insert = array(
      'api' => 'get_list_get_qr_powerbank',
      'url' => $this->url_api_server_v3,
      'postdata' => json_encode($postdata),
      'fromcode' => $ref1,
      'fromid' => $ref2,
      'fromchannel' => 'Powerbank',
      'transid' => $trans_log_id,
      'status' => 'inprogress',
      'postdatetime' => date('Y-m-d H:i:s')
    );
    $logid = $this->logpaymentv3->insert_log_v3($insert);

    $curl = curl_init();

    curl_setopt_array($curl, array(
      CURLOPT_URL => $this->url_api_server_v3,
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_ENCODING => "",
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 10,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => "POST",
      CURLOPT_POSTFIELDS => json_encode($postdata),
      CURLOPT_HTTPHEADER => array(
        "Content-Type: application/json",
        "cache-control: no-cache"
      ),
    ));

    $response = curl_exec($curl);
    $err = curl_error($curl);

    curl_close($curl);

    $update_log = array(
      'returndata' => $response.'/'.$err,
      'status' => 'complete',
      'returndatetime' => date('Y-m-d H:i:s')
    );
    $this->logpaymentv3->update_log_v3($update_log, $logid);

    if ($err) {
      return false;
    } else {
      return json_decode($response, true);
    }
  }

  function get_list_e_commerce_order($ref1, $ref2, $trans_log_id=''){
    $postdata  = array(
      "service"=>"get_list_e_commerce_order",
       "key"=>$this->key_v3,
       "ref1"=>$ref1,
       "ref2"=>$ref2
    );

    $insert = array(
      'api' => 'get_list_e_commerce_order',
      'url' => $this->url_api_server_v3,
      'postdata' => json_encode($postdata),
      'fromcode' => $ref1,
      'fromid' => $ref2,
      'fromchannel' => 'ECommerce',
      'transid' => $trans_log_id,
      'status' => 'inprogress',
      'postdatetime' => date('Y-m-d H:i:s')
    );
    $logid = $this->logpaymentv3->insert_log_v3($insert);

    $curl = curl_init();

    curl_setopt_array($curl, array(
      CURLOPT_URL => $this->url_api_server_v3,
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_ENCODING => "",
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 10,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => "POST",
      CURLOPT_POSTFIELDS => json_encode($postdata),
      CURLOPT_HTTPHEADER => array(
        "Content-Type: application/json",
        "cache-control: no-cache"
      ),
    ));

    $response = curl_exec($curl);
    $err = curl_error($curl);

    curl_close($curl);

    $update_log = array(
      'returndata' => $response.'/'.$err,
      'status' => 'complete',
      'returndatetime' => date('Y-m-d H:i:s')
    );
    $this->logpaymentv3->update_log_v3($update_log, $logid);

    if ($err) {
      return false;
    } else {
      return json_decode($response, true);
    }
  }

  function create_new_ca_from_bankinf($ref, $acc_name, $bank_code, $trans_log_id=''){
    $postdata  = array(
      "service" => "create_new_ca_from_bankinf",
      "key" => $this->key_v3,
      "ref" => $ref,
      "acc_name" => $acc_name,
      "bank_code" => $bank_code
    );

    
    $insert = array(
      'api' => 'create_new_ca_from_bankinf',
      'url' => $this->url_api_server_v3,
      'postdata' => json_encode($postdata),
      'fromcode' => $ref,
      'fromid' => $bank_code,
      'fromchannel' => 'Powerbank',
      'transid' => $trans_log_id,
      'status' => 'inprogress',
      'postdatetime' => date('Y-m-d H:i:s')
    );
    $logid = $this->logpaymentv3->insert_log_v3($insert);

    $curl = curl_init();

    curl_setopt_array($curl, array(
      CURLOPT_URL => $this->url_api_server_v3,
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_ENCODING => "",
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 10,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => "POST",
      CURLOPT_POSTFIELDS => json_encode($postdata),
      CURLOPT_HTTPHEADER => array(
        "Content-Type: application/json",
        "cache-control: no-cache"
      ),
    ));

    $response = curl_exec($curl);
    $err = curl_error($curl);

    curl_close($curl);

    $update_log = array(
      'returndata' => $response.'/'.$err,
      'status' => 'complete',
      'returndatetime' => date('Y-m-d H:i:s')
    );
    $this->logpaymentv3->update_log_v3($update_log, $logid);

    if ($err) {
      return false;
    } else {
      return json_decode($response, true);
    }
  }

  function pop_powerbank_from_bankinf($ref1, $ref2, $trans_log_id=''){
    $postdata  = array(
      "service"=>"pop_powerbank_from_bankinf",
       "key"=>$this->key_v3,
       "ref1"=>$ref1,
       "ref2"=>$ref2
    );

    $insert = array(
      'api' => 'pop_powerbank_from_bankinf',
      'url' => $this->url_api_server_v3,
      'postdata' => json_encode($postdata),
      'fromcode' => $ref1,
      'fromid' => $ref2,
      'fromchannel' => 'Powerbank',
      'transid' => $trans_log_id,
      'status' => 'inprogress',
      'postdatetime' => date('Y-m-d H:i:s')
    );
    $logid = $this->logpaymentv3->insert_log_v3($insert);

    $curl = curl_init();

    curl_setopt_array($curl, array(
      CURLOPT_URL => $this->url_api_server_v3,
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_ENCODING => "",
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 30,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => "POST",
      CURLOPT_POSTFIELDS => json_encode($postdata),
      CURLOPT_HTTPHEADER => array(
        "Content-Type: application/json",
        "cache-control: no-cache"
      ),
    ));

    $response = curl_exec($curl);
    $err = curl_error($curl);

    curl_close($curl);

    $update_log = array(
      'returndata' => $response.'/'.$err,
      'status' => 'complete',
      'returndatetime' => date('Y-m-d H:i:s')
    );
    $this->logpaymentv3->update_log_v3($update_log, $logid);

    if ($err) {
      return false;
    } else {
      return json_decode($response, true);
    }
  }

  function payment_doll($ref1, $ref2, $amount){
    $postdata  = array(
      "service"=>"payment_doll",
       "key"=>$this->key_v3,
       "ref1"=>$ref1,
       "ref2"=>$ref2,
       'amount'=>$amount
    );

    $insert = array(
      'api' => 'payment_doll',
      'url' => $this->url_api_migrate,
      'postdata' => json_encode($postdata),
      'fromcode' => $ref1,
      'fromid' => $ref2,
      'fromchannel' => 'Powerbank',
      'transid' => '',
      'status' => 'inprogress',
      'postdatetime' => date('Y-m-d H:i:s')
    );
    $logid = $this->logpaymentv3->insert_log_v3($insert);

    $curl = curl_init();

    curl_setopt_array($curl, array(
      CURLOPT_URL => $this->url_api_server_v3,
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_ENCODING => "",
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 30,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => "POST",
      CURLOPT_POSTFIELDS => json_encode($postdata),
      CURLOPT_HTTPHEADER => array(
        "Content-Type: application/json",
        "cache-control: no-cache"
      ),
    ));

    $response = curl_exec($curl);
    $err = curl_error($curl);

    curl_close($curl);

    $update_log = array(
      'returndata' => $response.'/'.$err,
      'status' => 'complete',
      'returndatetime' => date('Y-m-d H:i:s')
    );
    $this->logpaymentv3->update_log_v3($update_log, $logid);

    if ($err) {
      return false;
    } else {
      return json_decode($response, true);
    }
  }

  function create_ca_by_ref2($ref2, $acc_name, $bank_code, $trans_log_id=''){
    $postdata  = array(
      "service" => "create_ca_by_ref2",
      "key" => $this->key_v3,
      "ref2" => $ref2,
      "acc_name" => $acc_name,
      "bank_code" => $bank_code
    );

    
    $insert = array(
      'api' => 'create_ca_by_ref2',
      'url' => $this->url_api_migrate,
      'postdata' => json_encode($postdata),
      'fromcode' => $ref2,
      'fromid' => $bank_code,
      'fromchannel' => 'bankinf',
      'transid' => $trans_log_id,
      'status' => 'inprogress',
      'postdatetime' => date('Y-m-d H:i:s')
    );
    $logid = $this->logpaymentv3->insert_log_v3($insert);

    $curl = curl_init();

    curl_setopt_array($curl, array(
      CURLOPT_URL => $this->url_api_server_v3,
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_ENCODING => "",
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 10,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => "POST",
      CURLOPT_POSTFIELDS => json_encode($postdata),
      CURLOPT_HTTPHEADER => array(
        "Content-Type: application/json",
        "cache-control: no-cache"
      ),
    ));

    $response = curl_exec($curl);
    $err = curl_error($curl);

    curl_close($curl);

    $update_log = array(
      'returndata' => $response.'/'.$err,
      'status' => 'complete',
      'returndatetime' => date('Y-m-d H:i:s')
    );
    $this->logpaymentv3->update_log_v3($update_log, $logid);

    if ($err) {
      return false;
    } else {
      return json_decode($response, true);
    }
  }

  function smartapi_by_ref1_ref2($type ,$ref1, $ref2, $amount, $trans_log_id=''){
    $postdata  = array(
      "service"=>"smartapi_by_ref1_ref2",
       "key"=>$this->key_v3,
       "type" => $type,
       "ref1"=>$ref1,
       "ref2"=>$ref2,
       "amount" => $amount
    );

    $insert = array(
      'api' => 'smartapi_by_ref1_ref2',
      'url' => $this->url_api_migrate,
      'postdata' => json_encode($postdata),
      'fromcode' => $ref1,
      'fromid' => $ref2,
      'fromchannel' => 'bankinf',
      'transid' => $trans_log_id,
      'status' => 'inprogress',
      'postdatetime' => date('Y-m-d H:i:s')
    );
    $logid = $this->logpaymentv3->insert_log_v3($insert);

    $curl = curl_init();

    curl_setopt_array($curl, array(
      CURLOPT_URL => $this->url_api_server_v3,
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_ENCODING => "",
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 30,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => "POST",
      CURLOPT_POSTFIELDS => json_encode($postdata),
      CURLOPT_HTTPHEADER => array(
        "Content-Type: application/json",
        "cache-control: no-cache"
      ),
    ));

    $response = curl_exec($curl);
    $err = curl_error($curl);

    curl_close($curl);

    if($err){
      $returndata =  $err;
    }else{
      $returndata = $response;
    }

    $update_log = array(
      'returndata' => $returndata,
      'status' => 'complete',
      'returndatetime' => date('Y-m-d H:i:s')
    );
    $this->logpaymentv3->update_log_v3($update_log, $logid);

    if ($err) {
      return false;
    } else {
      return json_decode($response, true);
    }
  }

  function notify_e_commerce($ref1, $ref2, $result, $channel, $amount, $orderid, $trans_log_id){
    $postdata  = array(
      "service"=>"notify_e_commerce",
       "key"=>$this->key_v3,
       "result" => $result,
       "channel" => $channel,
       "orderid"=>$orderid,
       "amount" => $amount,
       "ref1" => $ref1,
       "ref2" => $ref2
    );

    $insert = array(
      'api' => 'notify_e_commerce',
      'url' => $this->url_api_server_v3,
      'postdata' => json_encode($postdata),
      'fromcode' => $ref1,
      'fromid' => $ref2,
      'fromchannel' => 'bankinf',
      'transid' => $trans_log_id,
      'status' => 'inprogress',
      'postdatetime' => date('Y-m-d H:i:s')
    );
    $logid = $this->logpaymentv3->insert_log_v3($insert);

    $curl = curl_init();

    curl_setopt_array($curl, array(
      CURLOPT_URL => $this->url_api_server_v3,
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_ENCODING => "",
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 30,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => "POST",
      CURLOPT_POSTFIELDS => json_encode($postdata),
      CURLOPT_HTTPHEADER => array(
        "Content-Type: application/json",
        "cache-control: no-cache"
      ),
    ));

    $response = curl_exec($curl);
    $err = curl_error($curl);

    curl_close($curl);

    if($err){
      $returndata =  $err;
    }else{
      $returndata = $response;
    }

    $update_log = array(
      'returndata' => $returndata,
      'status' => 'complete',
      'returndatetime' => date('Y-m-d H:i:s')
    );
    $this->logpaymentv3->update_log_v3($update_log, $logid);

    if ($err) {
      return false;
    } else {
      return json_decode($response, true);
    }
  }

}
