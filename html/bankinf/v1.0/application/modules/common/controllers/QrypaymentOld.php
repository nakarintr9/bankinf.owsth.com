<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Qrypayment extends MY_Controller {

  function __construct()
  {
    parent::__construct();
    $this->load->library('multipledb');
  }

  function get_partner($ref1){
    $partnerid = $ref1;
    $sql = "SELECT * FROM partner where partnerid=$partnerid";
    $qry = $this->multipledb->owstopup->query($sql)->row();
    return $qry;
  }

  function get_member($ref1, $ref2){
    $partnerid = $ref1;
    $memberid = $ref2;
    $sql = "SELECT * FROM member where partnerid=$partnerid and memberid=$memberid ";
    $qry = $this->multipledb->owstopup->query($sql)->row();
    return $qry;
  }

  function get_api_type($ref1, $ref2){
    if($ref1 == "00051" || $ref1 == "00052"){
      $api_type = 'payment';
    } 
    else if ($ref1 == "00004") {
      //For test
      if($ref2 == '00002'){
        $api_type = 'loan';
      }else{
        $api_type = 'payment';
      }
    }
    else{
      $api_type = 'payment';
    }
    $api_type = 'payment';
    return $api_type;
  }

  function inquiry_loan($kioskcode, $kioskid){
    $url = $this->url_loan_api."/inquiry_loan";

    $data = array(
      'kioskcode' => $kioskcode, 
      'kioskid' => intval($kioskid)
    );
    $postdata = json_encode($data);

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS,$postdata);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $content  = curl_exec($ch);
    
    if($content){
      $response = json_decode($content, TRUE);
    }else{
      $response['error_code'] = 'E15';
    }

    curl_close($ch);
    return  $response;
  }

  function pay_loan($kioskcode, $kioskid, $username){
    $url = $this->url_loan_api."/pay_loan";

    $data = array(
      'kioskcode' => $kioskcode, 
      'kioskid' => intval($kioskid),
      'username' => $username
    );
    $postdata = json_encode($data);

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS,$postdata);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $content  = curl_exec($ch);
    
    if($content){
      $response = json_decode($content, TRUE);
    }else{
      $response['error_code'] = 'E15';
    }

    curl_close($ch);
    return  $response;
  }

  function cancel_loan($kioskcode, $kioskid, $amount, $username){
    $url = $this->url_loan_api."/cancel_loan";

    $data = array(
      'kioskcode' => $kioskcode, 
      'kioskid' => intval($kioskid),
      'username' => $username,
      'amount'=>$amount
    );
    $postdata = json_encode($data);

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS,$postdata);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $content  = curl_exec($ch);
    
    if($content){
      $response = json_decode($content, TRUE);
    }else{
      $response['error_code'] = 'E15';
    }

    curl_close($ch);
    return  $response;
  }

  function payment_credit_member($ref1, $ref2, $amount, $member, $partner, $detail, $addby, $channel, $bankcode, $wave_fee=false, $trans_log_id=''){
    $this->load->module('common/payment');
    if($wave_fee == true){
      $member_fee = 0;
      $partner_fee = 0;
    }else{
      $member_fee = $this->payment->get_member_fee($bankcode, $ref1, $amount);
      $partner_fee = $this->payment->get_partner_fee($bankcode, $ref1, $amount); 
    }
    $data_array = array(
      'membercode' => $member->membercode,
      'memberid' => $member->memberid,
      'transferdate' => date('Y-m-d'),
      'transfertime' => date('H:i:s'),
      'detail' => $detail,
      'amount' => $amount,
      'channel' => $channel,
      'addby' => $addby,
      'member_fee' => $member_fee
    );
    $res = $this->payment->add_payment_member_to_partner($data_array);
    if($res['status'] == true){
      $error_code = 'E00';
      $payment_member = $res['payment'];
      $payment_member['ID'] = $res['id'];
      $memberpaymentid = $payment_member['ID'];
    }

    if($error_code == 'E00'){
      $payment_member['member_fee'] = $member_fee;
      $returnPamentMember = $this->payment->approve_credit_member($payment_member, $trans_log_id);

      $payment_member['partner_fee'] = $partner_fee;
      $payment_partner = $this->payment->add_payment_partner_to_admin($payment_member, $partner);

      $partnerpaymentid = $payment_partner['ID'];

      $payment_partner['res_payment'] = $returnPamentMember['res_payment'];
      $payment_partner['is_member_v3'] = $returnPamentMember['is_member_v3'];
      $payment_partner['partner_fee'] = $partner_fee;
      $returnPamentPartner = $this->payment->approve_credit_partner($payment_partner, $trans_log_id);
    }

    $response['error_code'] = 'E00';
    $response['partnerpaymentid'] = isset($partnerpaymentid) ? $partnerpaymentid : NULL;
    $response['memberpaymentid'] = isset($memberpaymentid) ? $memberpaymentid : NULL;
    return $response;
  }

  function payment_credit_partner($ref1, $amount, $partner, $detail, $addby, $channel, $bankcode, $wave_fee=false, $trans_log_id=''){
    $this->load->module('common/payment');
    if($wave_fee == true){
      $partner_fee = 0;
    }else{
      $partner_fee = $this->payment->get_partner_fee($bankcode, $ref1, $amount); 
    }
    $data_array = array(
      'toid' => $ref1,
      'transferdate' => date('Y-m-d'),
      'transfertime' => date('H:i:s'),
      'detail' => $detail,
      'amount' => $amount,
      'channel' => $channel,
      'addby' => $addby,
      'partner_fee' => $partner_fee
    );

    $payment_partner = $this->payment->add_payment_partner_to_admin($data_array, $partner);

    $partnerpaymentid = $payment_partner['ID'];

    $payment_partner['res_payment'] = true;
    $payment_partner['is_member_v3'] = true;
    $payment_partner['partner_fee'] = $partner_fee;
    $returnPamentPartner = $this->payment->approve_credit_partner($payment_partner, $trans_log_id);
    
    $response['error_code'] = 'E00';
    $response['partnerpaymentid'] = isset($partnerpaymentid) ? $partnerpaymentid : NULL;
    return $response;
  }
}