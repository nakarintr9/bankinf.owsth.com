<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Alert_line extends MY_Controller {

  function __construct()
  {
    parent::__construct();
  }

  public function notify_message($message){
    $curl = curl_init();

    curl_setopt_array($curl, array(
      CURLOPT_URL => $this->line_notify_url,
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_ENCODING => "",
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 30,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => "POST",
      // CURLOPT_POSTFIELDS => "message=".$message."&stickerPackageId=2&stickerId=34",
      CURLOPT_POSTFIELDS => "message=".$message."",
      CURLOPT_HTTPHEADER => array(
        "Authorization:Bearer ".$this->line_token,
        "Content-Type: application/x-www-form-urlencoded",
        "Postman-Token: c44ab18e-07bf-48f5-a254-2b8178364ba1",
        "cache-control: no-cache"
      ),
    ));

    $response = curl_exec($curl);
    $err = curl_error($curl);

    curl_close($curl);

    if ($err) {
      return "cURL Error #:" . $err;
    } else {
      return $response;
    }
  }

  public function notify_message_powerbank($message){
    $curl = curl_init();

    curl_setopt_array($curl, array(
      CURLOPT_URL => $this->line_notify_url,
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_ENCODING => "",
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 30,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => "POST",
      // CURLOPT_POSTFIELDS => "message=".$message."&stickerPackageId=2&stickerId=34",
      CURLOPT_POSTFIELDS => "message=".$message."",
      CURLOPT_HTTPHEADER => array(
        "Authorization:Bearer ".$this->line_token_powerbank,
        "Content-Type: application/x-www-form-urlencoded",
        "Postman-Token: c44ab18e-07bf-48f5-a254-2b8178364ba1",
        "cache-control: no-cache"
      ),
    ));

    $response = curl_exec($curl);
    $err = curl_error($curl);

    curl_close($curl);

    if ($err) {
      return "cURL Error #:" . $err;
    } else {
      return $response;
    }
  }
}