<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Retry_payment_migrate extends MY_Controller {

  function __construct() {
    parent::__construct();
    $this->load->model('common/payment_migrate_m');
    $this->load->module('common/logpaymentv3');
    // $this->url_api_migrate = $this->url_v3.'/api/v1.0/api_migrate_json/app_api';
    $this->url_api_migrate = $this->url_v3.'/api/v1.0/api_server_json/app_api';
    $this->url_api_server_v3 = $this->url_v3.'/api/v1.0/api_server_json/app_api';
  }

  function loop_retry($start, $limit, $addby){
    $sql = "select * from log_payment_v3
          where
          api = 'paymentcreditmemberpartner'  
          and postdatetime >= '2019-05-17 06:00:00' 
          and postdatetime <= '2019-05-17 07:00:00' 
          and returndata not like '%E00%'
          and transid like '".$addby."%' limit ".$start.", ".$limit;
    $qry = $this->multipledb->owstopup->query($sql)->result_array();
    foreach ($qry as $key => $value) {
      $res = $this->retry_payment_credit_member_partner_v3($value);
      var_dump($res);
      echo "<pre>";
    }
  }

  function retry_payment_credit_member_partner_v3($log){

    $sql = "select * from log_payment_v3 where transid='".$log['transid']."' and api='retry_paymentcreditmemberpartner'";
    $qry = $this->multipledb->owstopup->query($sql)->row();
    if(empty($qry)){
      $postdata = $log['postdata'];

      $ch = curl_init($this->url_api_migrate);                                                                      
      curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
      curl_setopt($ch, CURLOPT_POSTFIELDS, $postdata);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
      curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
      curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
      curl_setopt($ch, CURLOPT_TIMEOUT, 30);
      curl_setopt($ch, CURLOPT_HTTPHEADER, array(
       'Content-Type: application/json', 
       'Content-Length: ' . strlen($postdata))
      );

      $insert = array(
        'api' => 'retry_paymentcreditmemberpartner',
        'url' => $this->url_api_migrate,
        'postdata' => $postdata,
        'fromcode' => $log['fromcode'],
        'fromid' => $log['fromid'],
        'fromchannel' => $log['channel'],
        'transid' => $log['transid'],
        'status' => 'inprogress',
        'postdatetime' => date('Y-m-d H:i:s')
      );
      $logid = $this->logpaymentv3->insert_log_v3($insert);

      $content = curl_exec($ch);

      if($content){
        $return = json_decode($content, true);
      }else{
        $return = 'ERROR';
        $content = "Curl Error: ".curl_error($ch);
      }

      $update_log = array(
        'returndata' => $content,
        'status' => 'complete',
        'returndatetime' => date('Y-m-d H:i:s')
      );
      $this->logpaymentv3->update_log_v3($update_log, $logid);

      curl_close($ch);
      $return = $log['fromcode']."/".$log['fromid']." SUCCESS";
    }else{
      $return = $log['fromcode']."/".$log['fromid']." Dupplicate Retry!!";
    }
    return $return;
  }
}
