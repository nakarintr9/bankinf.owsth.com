<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Qrypayment extends MY_Controller {

  function __construct()
  {
    parent::__construct();
    $this->load->library('multipledb');
  }

  function get_partner($ref1){
    $partnerid = $ref1;
    $sql = "SELECT * FROM partner where partnerid=$partnerid";
    $qry = $this->multipledb->owstopup->query($sql)->row();
    return $qry;
  }

  function get_member($ref1, $ref2){
    $partnerid = $ref1;
    $memberid = $ref2;
    $sql = "SELECT * FROM member where partnerid=$partnerid and memberid=$memberid ";
    $qry = $this->multipledb->owstopup->query($sql)->row();
    return $qry;
  }

  function get_api_type($ref1, $ref2){
    if($ref1 == "00051" || $ref1 == "00052"){
      $api_type = 'payment';
    } 
    else if ($ref1 == "00004") {
      //For test
      if($ref2 == '00002'){
        $api_type = 'loan';
      }else{
        $api_type = 'payment';
      }
    }
    else{
      $api_type = 'payment';
    }
    $api_type = 'payment';
    return $api_type;
  }

  function inquiry_loan($kioskcode, $kioskid){
    $url = $this->url_loan_api."/inquiry_loan";

    $data = array(
      'kioskcode' => $kioskcode, 
      'kioskid' => intval($kioskid)
    );
    $postdata = json_encode($data);

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS,$postdata);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $content  = curl_exec($ch);
    
    if($content){
      $response = json_decode($content, TRUE);
    }else{
      $response['error_code'] = 'E15';
    }

    curl_close($ch);
    return  $response;
  }

  function pay_loan($kioskcode, $kioskid, $username){
    $url = $this->url_loan_api."/pay_loan";

    $data = array(
      'kioskcode' => $kioskcode, 
      'kioskid' => intval($kioskid),
      'username' => $username
    );
    $postdata = json_encode($data);

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS,$postdata);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $content  = curl_exec($ch);
    
    if($content){
      $response = json_decode($content, TRUE);
    }else{
      $response['error_code'] = 'E15';
    }

    curl_close($ch);
    return  $response;
  }

  function cancel_loan($kioskcode, $kioskid, $amount, $username){
    $url = $this->url_loan_api."/cancel_loan";

    $data = array(
      'kioskcode' => $kioskcode, 
      'kioskid' => intval($kioskid),
      'username' => $username,
      'amount'=>$amount
    );
    $postdata = json_encode($data);

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS,$postdata);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $content  = curl_exec($ch);
    
    if($content){
      $response = json_decode($content, TRUE);
    }else{
      $response['error_code'] = 'E15';
    }

    curl_close($ch);
    return  $response;
  }

  function payment_credit_member($ref1, $ref2, $amount, $member, $partner, $detail, $addby, $channel, $bankcode, $wave_fee=false, $trans_log_id=''){
    $error_code = 'E00';
    $this->load->module('common/payment');
    if($wave_fee == true){
      $member_fee = 0;
      $partner_fee = 0;
    }else{
      $member_fee = $this->payment->get_member_fee($bankcode, $ref1, $amount);
      $partner_fee = $this->payment->get_partner_fee($bankcode, $ref1, $amount); 
    }

    $creditafter = (number_format($member->credit,4,'.','')) + (number_format($amount,4,'.','')) - (number_format($member_fee,4,'.',''));

    $payment_member = array(
      'order_id' => $trans_log_id,
      'fromcode' => $member->membercode,
      'fromid' => $member->memberid,
      'fromtype' => 'member',
      'banktransfer' => '',
      'channel' => $channel,
      'paymentauto' => 0,
      'amount' => number_format($amount,4,'.',''),
      'fee' => $member_fee,
      'tocode' => $partner->partnercode,
      'toid' => $partner->partnerid,
      'totype' => 'partner',
      'detail' => $detail,
      'transferdate' => date('Y-m-d'),
      'transfertime' => date('H:i:s'),
      'addby' => $addby ,
      'adddate' => date('Y-m-d'),
      'addtime' => date('H:i:s'),
      'creditbefore' => (number_format($member->credit,4,'.','')),
      'creditafter' => (number_format($creditafter,4,'.','')),
    );
    if($this->multipledb->owstopup->insert('payment', $payment_member)){
      $payment_member['ID'] = $this->multipledb->owstopup->insert_id();
    }else{
      $error_code = "E59";
    }

    $creditafter = (number_format($partner->credit,4,'.','')) + (number_format($amount,4,'.','')) - (number_format($partner_fee,4,'.',''));

    if($error_code == "E00"){
      $payment_partner = array(
        'order_id' => $trans_log_id,
        'fromcode' => $partner->partnercode,
        'fromid' => $partner->partnerid,
        'fromtype' => 'partner',
        'banktransfer' => 'อื่นๆ',
        'tocode' => 'AM',
        'toid' => 1,
        'totype' => 'admin',
        'channel' => $channel,
        'paymentauto' => 0,
        'detail' => $detail,
        'amount' => number_format($amount,4,'.',''),
        'fee' => $partner_fee,
        'creditbefore' => (number_format($partner->credit,4,'.','')),
        'creditafter' => (number_format($creditafter,4,'.','')),
        'addby' => $addby,
        'transferdate' => date('Y-m-d'),
        'transfertime' => date('H:i:s'),
        'adddate' => date('Y-m-d'),
        'addtime' => date('H:i:s'),
      );
      if($this->multipledb->owstopup->insert('payment', $payment_partner)){
        $payment_partner['ID'] = $this->multipledb->owstopup->insert_id();
      }else{
        $error_code = "E59";
      }
    }

    if($error_code == 'E00'){
      $returnPamentMemberPartner = $this->approve_credit_member_partner($addby, $amount, $payment_member, $payment_partner, $member, $partner, $trans_log_id);
      $error_code = isset($returnPamentMemberPartner['error_code']) ? $returnPamentMemberPartner['error_code'] : 'E14';
    }

    $response['error_code'] = $error_code;
    $response['partnerpaymentid'] = isset($payment_partner['ID']) ? $payment_partner['ID'] : NULL;
    $response['memberpaymentid'] = isset($payment_member['ID']) ? $payment_member['ID'] : NULL;
    return $response;
  }

  function payment_credit_partner($ref1, $amount, $partner, $detail, $addby, $channel, $bankcode, $wave_fee=false, $trans_log_id=''){
    $this->load->module('common/payment');
    if($wave_fee == true){
      $partner_fee = 0;
    }else{
      $partner_fee = $this->payment->get_partner_fee($bankcode, $ref1, $amount); 
    }
    $data_array = array(
      'toid' => $ref1,
      'transferdate' => date('Y-m-d'),
      'transfertime' => date('H:i:s'),
      'detail' => $detail,
      'amount' => $amount,
      'channel' => $channel,
      'addby' => $addby,
      'partner_fee' => $partner_fee
    );

    $payment_partner = $this->payment->add_payment_partner_to_admin($data_array, $partner);

    $partnerpaymentid = $payment_partner['ID'];

    $payment_partner['res_payment'] = true;
    $payment_partner['is_member_v3'] = true;
    $payment_partner['partner_fee'] = $partner_fee;
    $returnPamentPartner = $this->payment->approve_credit_partner($payment_partner, $trans_log_id);
    
    $response['error_code'] = 'E00';
    $response['partnerpaymentid'] = isset($partnerpaymentid) ? $partnerpaymentid : NULL;
    return $response;
  }

  function approve_credit_member_partner($addby, $amount, $payment_member, $payment_partner, $member, $partner, $trans_log_id=''){
    $update = array(
      'approveby' => $addby, 
      'approvedate' => date('Y-m-d'), 
      'approvetime' => date('H:i:s'), 
      'status' => 1, 
    );
    $where = " ID in (".$payment_member['ID'].",".$payment_partner['ID'].")";
    $this->multipledb->owstopup->where($where);
    $this->multipledb->owstopup->set($update);
    $this->multipledb->owstopup->update('payment');

    $resPaymentV3 = $this->payment_migrate->payment_credit_member_partner_v3($addby, $payment_member, $payment_partner, $trans_log_id);
    $member->credit = isset($resPaymentV3['member']['credit']) ? $resPaymentV3['member']['credit'] : 0;
    $partner->credit = isset($resPaymentV3['partner']['credit']) ? $resPaymentV3['partner']['credit'] : 0;
    $payment_status = isset($resPaymentV3['error_code']) ? $resPaymentV3['error_code'] : 'E14';

    if($payment_status == "E00"){
      $sms = $member->username." อนุมัติเติมเครดิต ".number_format($payment_member['amount'],2)." บ. ";
      $sms.= "คงเหลือ ".number_format($member->credit, 2)." บ. ขอบคุณค่ะ";
      $data = array(
        'fromcode' => $member->membercode,
        'fromid' => $member->memberid,
        'fromtype' => 'member',
        'mobile' => $member->mobile,
        'message' => $sms
      );
      $this->sendmessage->send($data, $trans_log_id);
    }else{
      $message = "\r\nServive: $addby ไม่สำเร็จ!";
      $message.= "\r\nPartner Payment ID: ".$payment_partner['ID'];
      $message.= "\r\nMember Payment ID: ".$payment_member['ID'];
      $message.= "\r\nPartner: ".$partner->partnercode.sprintf("%05d", $partner->partnerid);
      $message.= "\r\nMember: ".$member->membercode.sprintf("%05d", $member->memberid);
      $message.= "\r\nAmount: ".$amount;
      $this->alert_line->notify_message($message);
    }
    $return['error_code'] = $payment_status;
    return $return;
  }

  function update_payment_bank($update, $where){
    $this->multipledb->owstopup->set($update);
    $this->multipledb->owstopup->where($where);
    if($this->multipledb->owstopup->update('payment')){
      $ret['status'] = true;
    }
    else{
      $ret['status'] = false;
    }
    return $ret;
  }
}