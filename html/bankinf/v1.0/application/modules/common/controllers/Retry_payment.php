<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Retry_payment extends MY_Controller {

  function __construct()
  {
    parent::__construct();
    $this->load->library('multipledb');
    $this->load->module('common/payment_migrate');
    $this->load->module('common/cashbag');
    $this->load->module('common/statement');
    $this->load->module('sms/smsscript');
    $this->load->module('common/alert_line'); 
    $this->load->module('sms/sendmessage');
  }

  function do_retry($addby, $adddate, $limit=1){
    $where = array(
      'addby' => $addby,
      'adddate' => $adddate,
    );
    $gets = $this->get_list_payment_fail($where, $limit);
    foreach ($gets as $key => $value) {
      $ex_error_detail = explode('.', $value['error_detail']);
      
    }
  }

  function get_list_payment_fail($where, $limit){
    $get = $this->multipledb->owstopup
      ->select('*')
      ->from('list_payment_fail')
      ->where($where)
      ->order_by('ID', 'ASC')
      ->limit($limit)
      ->get()
      ->result_array();
    return $get;
  }
}
