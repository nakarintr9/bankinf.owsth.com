<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Cancel_payment extends MY_Controller {

  function __construct()
  {
    parent::__construct();
    $this->load->library('multipledb');
    $this->load->module('common/payment_migrate');
    $this->load->module('common/cashbag');
    $this->load->module('common/statement');
    $this->load->module('sms/smsscript');
  }

  function cancel_payment_member($memberpaymentid, $amount, $addby, $member, $is_payment_v3){
    $error_code = 'E00';
    $sql = "select * from payment where ID = '".$memberpaymentid."' and status=1";
    $qry = $this->multipledb->owstopup->query($sql)->row();
    if(!empty($qry)){
      $payment_member = (array)$qry;
      if($payment_member['amount'] != $amount){
        $error_code = 'E08';
      }else{
        $member_fee = $payment_member['fee'];
        $cancel_amount = $amount - $member_fee;
        if($is_payment_v3== 'Y'){
          $res = $this->payment_migrate->reverse_credit_v3($payment_member, $cancel_amount, $addby);
          if($res == 'ERROR'){
            $error_code = 'E26';
          }else{
            $error_code = $res['error_code'];
            if($error_code == 'E00'){
              $this->updatePaymentCancel($payment_member['ID'], $addby);
            }
          }
        }else{
          $this->cancelPaymentMemberToPartner($payment_member, $addby);
          $operator = 'del';
          $array = array(
            'membercode' => $member->membercode,
            'memberid' => $member->memberid,
            'broker' => '',
            'ordertype' => 'payment',
            'detail' => "เครดิต : ยกเลิกอนุมัติเครดิต orderid ".$memberpaymentid,
            'orderid' => $memberpaymentid,
          );
          $this->statement->create_statement_member($member->membercode, $member->memberid, $operator, $cancel_amount, $array);
        }
      }
    }else{
      $error_code = 'E11';
    }

    $response['error_code'] = $error_code;
    $response['member_fee'] = isset($member_fee) ? $member_fee : 0;
    return $response;
  }

  function cancel_payment_partner($partnerpaymentid, $amount, $addby, $partner, $is_payment_v3){
    $error_code = 'E00';
    $sql = "select * from payment where ID = '".$partnerpaymentid."' and status=1 ";
    $qry = $this->multipledb->owstopup->query($sql)->row();
    if(!empty($qry)){
      $payment_partner = (array)$qry;
      $partner_fee = $payment_partner['fee'];
      if($amount != $payment_partner['amount']){
        $error_code = 'E08';
      }else{
        $cancel_amount = $amount - $partner_fee;
        if($is_payment_v3== 'Y'){
          $res = $this->payment_migrate->reverse_credit_v3($payment_partner, $cancel_amount, $addby);
          if($res == 'ERROR'){
            $error_code = 'E26';
          }else{
            $error_code = $res['error_code'];
            if($error_code == 'E00'){
              $this->updatePaymentCancel($payment_partner['ID'], $addby);
            }
          }
        }else{
          $this->cancelPaymentPartnerToAdmin($payment_partner, $addby);
          $array = array(
            'partnercode' => 'PN',
            'partnerid' => $partner->partnerid,
            'broker' => '',
            'ordertype' => 'payment',
            'detail' => "เครดิต : อนุมัติเครดิต orderid ".$partnerpaymentid,
            'orderid' => $partnerpaymentid,
          );
          $this->statement->create_statement_partner($partner->partnerid, $operator, $cancel_amount, $array);
        }
      }
    }else{
      $error_code = 'E11';
    }

    $response['error_code'] = $error_code;
    $response['partner_fee'] = isset($partner_fee) ? $partner_fee : 0;
    return $response;
  }

  function cancel_payment_credit_member($partnerpaymentid, $memberpaymentid, $amount, $addby, $member, $partner){
    $error_code = 'E00';
    $sql = "select * from payment where ID = '".$partnerpaymentid."' and status=1";
    $qry = $this->multipledb->owstopup->query($sql)->row();
    if(!empty($qry)){
      $payment_partner = (array)$qry;
      $partner_fee = ($payment_partner['fee'] != NULL) ? $payment_partner['fee'] : 10;
      $this->cancelPaymentPartnerToAdmin($payment_partner, $addby);
    }else{
      $error_code = 'E03';
    }
    
    if($error_code == 'E00'){
      $sql = "select * from payment where ID = '".$memberpaymentid."' and status=1 ";
      $qry = $this->multipledb->owstopup->query($sql)->row();
      $payment_member = (array)$qry;
      $member_fee = ($payment_member['fee'] != NULL) ? $payment_member['fee'] : 10;
      $this->cancelPaymentMemberToPartner($payment_member, $addby);
    }else{
      $error_code = 'E03';
    }

    if($error_code == 'E00'){
      $response = $this->payment_migrate->cancel_payment_v3($member->membercode, $member->memberid, 'member', $memberpaymentid);
      if($response != 'ERROR'){
        if($error_code == 'E00'){
          $amount_cancel_v3 = $response['amount_cancel'];
        }else{
          $amount_cancel_v3 = 0;
        }
        $amount_cancel_v2 = $amount - $amount_cancel_v3 - $member_fee;
      }else{
        $error_code = 'E03';
      }
    }

    if($error_code == 'E00' && $amount_cancel_v2 > $member_fee){
      //statement_member
      $operator = 'del';
      $array = array(
        'membercode' => $member->membercode,
        'memberid' => $member->memberid,
        'broker' => '',
        'ordertype' => 'payment',
        'detail' => "เครดิต : ยกเลิกอนุมัติเครดิต orderid ".$memberpaymentid,
        'orderid' => $memberpaymentid,
      );
      $this->statement->create_statement_member($member->membercode, $member->memberid, $operator, $amount_cancel_v2, $array);

      //statement_partner
      $array = array(
        'partnercode' => 'PN',
        'partnerid' => $partner->partnerid,
        'broker' => '',
        'ordertype' => 'payment',
        'detail' => "เครดิต : อนุมัติเครดิต orderid ".$partnerpaymentid,
        'orderid' => $partnerpaymentid,
      );
      $this->statement->create_statement_partner($partner->partnerid, $operator, $amount_cancel_v2, $array);
    }

    if($error_code == 'E00' && $member_fee > $partner_fee){
      $operator = 'del';
      //statement_partner
      $array = array(
        'partnercode' => 'PN',
        'partnerid' => $partner->partnerid,
        'broker' => '',
        'ordertype' => 'payment',
        'detail' =>'ยกเลิกการเติมเครดิตผ่านระบบ KTB Bill Payment (ดึงคืนจาก V3 แล้วเป็นเงิน '.($amount_cancel_v3).' บ.)',
        'orderid' => $partnerpaymentid,
      );
      $this->statement->create_statement_partner($partner->partnerid, $operator, $member_fee, $array);
    }

    $response['error_code'] = $error_code;

    return $response;
  }

  function cancelPaymentPartnerToAdmin($payment, $addby){
    $date = date('Y-m-d');
    $time = date('H:i:s');
    $orderid = $payment['ID'];

    $sql="update payment set ";
    $sql.="cancelby = '".$addby."' ";
    $sql.=",canceldate = '".$date."' ";
    $sql.=",canceltime = '".$time."' ";
    $sql.=",status = '2' ";
    $sql.="where ID = '".$orderid."' ";
    $qry= $this->multipledb->owstopup->query($sql);
  
    //cashbag_admin
    $partnerid = $payment['toid'];
    $bank = $payment['channel'];
    $operator = 'del';
    $amount = $payment['amount'];
    $array = array(
      'bank' => $payment['channel'],
      'cashtype' => 'payment',
      'partnercode' => $payment['fromcode'],
      'partnerid' => $payment['fromid'],
      'orderid' => $payment['ID'],
      'detail' => "เครดิต : ยกเลิกการอนุมัติเครดิต orderid ".$payment['ID'],
      'amount' => $payment['amount'],
    );
    $this->cashbag->create_cashbag_admin($bank, $operator, $amount, $array);
    
    //cashbag_partner
    $partnerid = $payment['toid'];
    $bank = $payment['channel'];
    $operator = 'add';
    $amount = $payment['amount'];
    $array = array(
      'partnercode' => $payment['tocode'],
      'partnerid' => $payment['toid'],
      'bank' => 'อื่นๆ',
      'cashtype' => 'payment',
      'membercode' => '',
      'memberid' => 0,
      'orderid' => $payment['ID'],
      'detail' => "เครดิต : ยกเลิกการอนุมัติเครดิต orderid ".$payment['ID'],
      'amount' => $payment['amount'],
    );
    $this->cashbag->create_cashbag_partner($partnerid, $bank, $operator, $amount, $array);

    //log_payment
    $insert = array(
      'paymentid' => $payment['ID'], 
      'action' => 'cancel', 
      'addby' => $payment['addby'], 
      'adddate' => date('Y-m-d'), 
      'addtime' => date('H:i:s'), 
    );
    $this->multipledb->owstopup->insert('log_payment', $insert);
  }

  function cancelPaymentMemberToPartner($payment, $addby){
    $date = date('Y-m-d');
    $time = date('H:i:s');
    $orderid = $payment['ID'];

    $sql="update payment set ";
    $sql.="cancelby = '".$addby."' ";
    $sql.=",canceldate = '".$date."' ";
    $sql.=",canceltime = '".$time."' ";
    $sql.=",status = '2' ";
    $sql.="where ID = '".$orderid."' ";
    $qry= $this->multipledb->owstopup->query($sql);

    //cashbag_partner
    $partnerid = $payment['toid'];
    $bank = $payment['channel'];
    $operator = 'del';
    $amount = $payment['amount'];
    $array = array(
      'partnercode' => $payment['tocode'],
      'partnerid' => $payment['toid'],
      'bank' => 'อื่นๆ',
      'cashtype' => 'payment',
      'membercode' => $payment['fromcode'],
      'memberid' => $payment['fromid'],
      'orderid' => $payment['ID'],
      'detail' => "เครดิต : อนุมัติเครดิต orderid ".$payment['ID'],
      'amount' => $payment['amount'],
    );
    $this->cashbag->create_cashbag_partner($partnerid, $bank, $operator, $amount, $array);
  }

  function updatePaymentCancel($orderid, $addby){
    $date = date('Y-m-d');
    $time = date('H:i:s');

    $sql="update payment set ";
    $sql.="cancelby = '".$addby."' ";
    $sql.=",canceldate = '".$date."' ";
    $sql.=",canceltime = '".$time."' ";
    $sql.=",status = '2' ";
    $sql.="where ID = '".$orderid."' ";
    $qry= $this->multipledb->owstopup->query($sql);
  }

}
