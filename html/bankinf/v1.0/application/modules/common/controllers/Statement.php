<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Statement extends MY_Controller {

  function __construct() {
    parent::__construct();

    $this->load->model('statement_m');
  }

  function create_statement_partner($partnerid, $operator, $amount, $array){

    $this->multipledb->owstopup->trans_begin();

    //Select Partner Credit (For Update)
    $quote = $this->multipledb->owstopup->query("SELECT credit FROM partner WHERE partnerid = '".$partnerid."' FOR UPDATE ;")->row('credit');
    $balance = 0;
    $is_credit_enough = true;

    //Check action
    if($operator == 'change'){
      if($quote >= $amount){
        $add_credit = 0;
        $del_credit = $quote - $amount;
      }
      else if($quote < $amount){
        $add_credit = $amount - $quote;
        $del_credit = 0;
      }
      $balance = $amount;
    }
    else if($operator == 'add'){
      $balance = $quote + $amount;
      $add_credit = $amount;
      $del_credit = 0;
    }
    else if($operator == 'del'){
      $balance = $quote - $amount;
      $add_credit = 0;
      $del_credit = $amount;
    }

    $return = false;
    if($is_credit_enough){

      //Update Partner Credit 
      $update_ok = $this->multipledb->owstopup->query("UPDATE partner SET credit = ".$balance." WHERE partnerid = '".$partnerid."';");

      //Insert Statement Partner
      if($update_ok){
        $array['qouted'] = $quote;
        $array['addcredit'] = $add_credit;
        $array['delcredit'] = $del_credit;
        $array['balance'] = $balance;
        $array['adddate'] = date('Y-m-d');
        $array['addtime'] = date('H:i:s');

        $ret = $this->statement_m->insert_statement_partner($array);

        if($ret['status']){
          $this->multipledb->owstopup->trans_commit();
          $return = true;
        }
        else{
          $this->multipledb->owstopup->trans_rollback();
        }
      }
      else{
        $this->multipledb->owstopup->trans_rollback();
      }
    }

    return $return;
  }

  function create_statement_member($membercode, $memberid, $operator, $amount, $array){

    $this->multipledb->owstopup->trans_begin();

    //Select Member Credit (For Update)
    $quote = $this->multipledb->owstopup->query("SELECT credit FROM member WHERE membercode = '".$membercode."' AND memberid = '".$memberid."' FOR UPDATE ;")->row('credit');
    $balance = 0;
    $is_credit_enough = true;

    //Check action
    if($operator == 'change'){
      if($quote >= $amount){
        $add_credit = 0;
        $del_credit = $quote - $amount;
      }
      else if($quote < $amount){
        $add_credit = $amount - $quote;
        $del_credit = 0;
      }
      $balance = $amount;
    }
    else if($operator == 'add'){
      $balance = $quote + $amount;
      $add_credit = $amount;
      $del_credit = 0;
    }
    else if($operator == 'del'){
      $balance = $quote - $amount;
      $add_credit = 0;
      $del_credit = $amount;
    }

    $return = false;
    if($is_credit_enough){

      //Update Member Credit 
      $update_ok = $this->multipledb->owstopup->query("UPDATE member SET credit = ".$balance." WHERE membercode = '".$membercode."' AND memberid = '".$memberid."';");

      //Insert Statement Member
      if($update_ok){
        $array['qouted'] = $quote;
        $array['addcredit'] = $add_credit;
        $array['delcredit'] = $del_credit;
        $array['balance'] = $balance;
        $array['adddate'] = date('Y-m-d');
        $array['addtime'] = date('H:i:s');

        $ret = $this->statement_m->insert_statement_member($array);

        if($ret['status']){
          $this->multipledb->owstopup->trans_commit();
          $return = true;
        }
        else{
          $this->multipledb->owstopup->trans_rollback();
        }
      }
      else{
        $this->multipledb->owstopup->trans_rollback();
      }
    }

    return $return;
  }

}
