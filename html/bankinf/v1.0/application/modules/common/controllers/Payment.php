<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Payment extends MY_Controller {

  function __construct()
  {
    parent::__construct();
    $this->load->library('multipledb');
    $this->load->module('common/payment_migrate');
    $this->load->module('common/cashbag');
    $this->load->module('common/statement');
    $this->load->module('sms/smsscript');
    $this->load->module('common/alert_line'); 
    $this->load->module('sms/sendmessage');
  }

  public function get_partner_fee($bankcode, $partnerid, $amount){
    $config_fee['scb']['partner_mc'] = 10;
    $config_fee['scb']['partner_non_mc'] = 10;
    $config_fee['bbl']['partner_mc'] = 5;
    $config_fee['bbl']['partner_non_mc'] = 5;
    $config_fee['ktb']['partner_mc'] = 10;
    $config_fee['ktb']['partner_non_mc'] = 10;
    $config_fee['tesco']['partner_mc'] = 10;
    $config_fee['tesco']['partner_non_mc'] = 10;
    $config_fee['thanachart']['partner_mc'] = 5;
    $config_fee['thanachart']['partner_non_mc'] = 5;
    $config_fee['scbqr']['partner_mc'] = 5;
    $config_fee['scbqr']['partner_non_mc'] = 5;

    $sql = "SELECT * FROM partner WHERE partnerid=".$partnerid." AND firstname LIKE '%media%' ";
    $row = $this->multipledb->owstopup->query($sql)->row(); 
    if(!empty($row)){
      if($bankcode == 'thanachart'){
        if($amount >= 5000){
          return 0;
          // return $config_fee[$bankcode]['partner_mc'];
        }else{
          return $config_fee[$bankcode]['partner_mc'];
        }
      }else if($bankcode == 'bbl'){
        if($amount >= 5000){
          return 0;
          // return $config_fee[$bankcode]['partner_mc'];
        }else{
          return $config_fee[$bankcode]['partner_mc'];
        }
      }else{
        if($amount >= 5000){
          // return 0;
          return $config_fee[$bankcode]['partner_mc'];
        }else{
          return $config_fee[$bankcode]['partner_mc'];
        }
      }
    }else{
      return $config_fee[$bankcode]['partner_non_mc'];
    }
  }

  public function get_member_fee($bankcode, $partnerid, $amount){
    $config_fee['scb']['member_mc'] = 10;
    $config_fee['scb']['member_non_mc'] = 10;
    $config_fee['bbl']['member_mc'] = 5;
    $config_fee['bbl']['member_non_mc'] = 5;
    $config_fee['ktb']['member_mc'] = 10;
    $config_fee['ktb']['member_non_mc'] = 10;
    $config_fee['tesco']['member_mc'] = 10;
    $config_fee['tesco']['member_non_mc'] = 10;
    $config_fee['thanachart']['member_mc'] = 5;
    $config_fee['thanachart']['member_non_mc'] = 5;
    $config_fee['scbqr']['member_mc'] = 5;
    $config_fee['scbqr']['member_non_mc'] = 5;

    $sql = "SELECT * FROM partner WHERE partnerid=".$partnerid." AND firstname LIKE '%media%' ";
    $row = $this->multipledb->owstopup->query($sql)->row(); 
    if(!empty($row)){
      if($bankcode == 'thanachart'){
        // Promotion Expire 2019-01-02
        if($amount >= 5000){
          return 0;
          // return $config_fee[$bankcode]['member_mc'];
        }else{
          return $config_fee[$bankcode]['member_mc'];
        }
      }else if($bankcode == 'bbl'){
        if($amount >= 5000){
          return 0;
          // return $config_fee[$bankcode]['member_mc'];
        }else{
          return $config_fee[$bankcode]['member_mc'];
        }
      }else{
        if($amount >= 5000){
          // return 0;
          return $config_fee[$bankcode]['member_mc'];
        }else{
          return $config_fee[$bankcode]['member_mc'];
        }
      }
    }else{
      return $config_fee[$bankcode]['member_non_mc'];
    }
  }

  function get_payment_fee($channel, $amount){
    $fee = 0;
    $where  = "amount <= $amount and channel = '$channel'";
    $get_fee = $this->multipledb->bankinf->select('fee')
      ->from('payment_fee')
      ->where($where)
      ->order_by('amount', 'desc')
      ->limit(1)
      ->get()
      ->row();
    if(!empty($get_fee)){
      $fee = $get_fee->fee;
    }else{
      $where = array(
        'channel' => $channel
      );
      $get_fee_default = $this->multipledb->bankinf->select('fee')
        ->from('payment_fee_default')
        ->where($where)
        ->get()
        ->row();
      if(!empty($get_fee_default)){
        $fee = $get_fee_default->fee;
      }else{
        $fee = 10;
      }
    }
    return $fee;
  }

  function add_payment_member_to_partner($data_array){
    $membercode = $data_array['membercode'];
    $memberid = $data_array['memberid'];
    $transfer_date = $data_array['transferdate'];
    $transfer_time = $data_array['transfertime'];
    $detail = $data_array['detail'];
    $amount = $data_array['amount'];
    $channel = $data_array['channel'];
    $addby = $data_array['addby'];
    $where = array(
      'membercode' => $membercode,
      'memberid' => $memberid,
    );
    $get_member = $this->get_member($where);  

    $insert = array(
      'fromcode' => $membercode,
      'fromid' => $memberid,
      'fromtype' => 'member',
      'banktransfer' => '',
      'channel' => $channel,
      'paymentauto' => 0,
      'amount' => $amount,
      'fee' => $data_array['member_fee'],
      'tocode' => 'PN',
      'toid' => $get_member->partnerid,
      'totype' => 'partner',
      'detail' => $detail,
      'transferdate' => $transfer_date,
      'transfertime' => $transfer_time,
      'addby' => $addby ,
      'adddate' => date('Y-m-d'),
      'addtime' => date('H:i:s'),
      'creditbefore' => $get_member->credit,
      'creditafter' => ($get_member->credit) + ($amount),
    );
    $res = $this->insert_payment($insert);
    $res['payment'] = $insert;
    return $res;
  }

  function approve_credit_member($payment, $trans_log_id=''){
    $date=date('Y-m-d');
    $time=date('H:i:s');
 
    //check member inactive
    $member_close = false;
    $where = array(
      'membercode' => $payment['fromcode'], 
      'memberid' => $payment['fromid'],
      'credit <' => 10,
      'status' => 1,
    );
    $get_member_inactive = $this->multipledb->owstopup
      ->select('*')
      ->from('member')
      ->where($where)
      ->get()
      ->result_array();
    if(count($get_member_inactive) > 0){
      $member_close = true;
    }

    //update payment
    $update = array(
      'approveby' => $payment['addby'], 
      'approvedate' => date('Y-m-d'), 
      'approvetime' => date('H:i:s'), 
      'status' => 1, 
    );
    $where = array('ID' => $payment['ID']);
    $this->multipledb->owstopup->where($where);
    $this->multipledb->owstopup->set($update);
    $this->multipledb->owstopup->update('payment');

    $data = array(
      'fromcode' => $payment['fromcode'],
      'fromid' => $payment['fromid'],
      'fromtype' => $payment['fromtype'],
      'channel' => $payment['channel'],
      'amount' => $payment['amount'],
      'fee' => $payment['member_fee'],
      'orderid' => $payment['ID'],
      'addby' => $payment['addby']
    );
    // $checkMerberV3  = $this->payment_migrate->get_member_v3($data, $trans_log_id);
    $checkMerberV3['error_code'] = 'E00';
    if($checkMerberV3 != 'ERROR'){
      if($checkMerberV3['error_code'] == 'E00'){
        // $member_detail = $checkMerberV3['data'][0];
        $is_member_v3 = true;
        $resPaymentV3 = $this->payment_migrate->payment_credit_v3($data, $trans_log_id);
        $member_detail['credit'] = $resPaymentV3['credit'];
        $member_detail['membercode'] = $resPaymentV3['membercode'];
        $member_detail['memberid'] = $resPaymentV3['memberid'];
        $member_detail['username'] = $resPaymentV3['username'];
        $member_detail['mobile'] = $resPaymentV3['mobile'];
        if($resPaymentV3 != 'ERROR'){
          if($resPaymentV3['error_code'] != 'E00'){
            $message = "\r\nPayment To V3 Fail\r\nPlease Check Payment ID: ".$payment['ID']."\r\nFromCode: ".$payment['fromcode']."\r\nFromID: ".$payment['fromid']."\r\nAmount: ".$payment['amount'];
            $this->alert_line->notify_message($message);
          }else{
            $res_payment = true;
          }
        }else{
          $message = "\r\nPayment Connection V3 Error (confirm)\r\nPlease Check Payment ID: ".$payment['ID']."\r\nFromCode: ".$payment['fromcode']."\r\nFromID: ".$payment['fromid']."\r\nAmount: ".$payment['amount'];
          $this->alert_line->notify_message($message);
        }
      }else{
        //statement_member
        $operator = 'add';
        $array = array(
          'membercode' => $payment['fromcode'],
          'memberid' => $payment['fromid'],
          'broker' => '',
          'ordertype' => 'payment',
          'detail' => "เครดิต : อนุมัติเครดิต orderid ".$payment['ID'],
          'orderid' => $payment['ID'],
        );
        $this->statement->create_statement_member($payment['fromcode'], $payment['fromid'], $operator, $payment['amount'], $array);

        //statement_member fee
        // $fee = $this->get_payment_fee($payment['addby'], $payment['amount']);
        $fee = $payment['member_fee'];
        $operator = 'del';
        $array = array(
          'membercode' => $payment['fromcode'],
          'memberid' => $payment['fromid'],
          'broker' => '',
          'ordertype' => 'payment',
          'detail' => "หักค่าบริการจากการเติมเครดิตผ่านระบบ ".$payment['addby'],
          'orderid' => $payment['ID'],
        );
        $this->statement->create_statement_member($payment['fromcode'], $payment['fromid'], $operator, $fee, $array);

        $res_payment = true;
        $where = array(
          'membercode' => $payment['fromcode'],
          'memberid' => $payment['fromid']
        );
        $get_member_detail = $this->multipledb->owstopup
          ->select('*')
          ->from('member')
          ->where($where)
          ->get()
          ->result_array();
        $member_detail = $get_member_detail[0];
      }
    } 
    else if($checkMerberV3 == 'ERROR') {
      $message = "\r\nPayment Connection V3 Error (query)\r\nPlease Check Payment ID: ".$payment['ID']."\r\nFromCode: ".$payment['fromcode']."\r\nFromID: ".$payment['fromid']."\r\nAmount: ".$payment['amount'];
      $this->alert_line->notify_message($message);
    } 
    else {
      //statement_member
      $operator = 'add';
      $array = array(
        'membercode' => $payment['fromcode'],
        'memberid' => $payment['fromid'],
        'broker' => '',
        'ordertype' => 'payment',
        'detail' => "เครดิต : อนุมัติเครดิต orderid ".$payment['ID'],
        'orderid' => $payment['ID'],
      );
      $this->statement->create_statement_member($payment['fromcode'], $payment['fromid'], $operator, $payment['amount'], $array);

      //statement_member fee
      // $fee = $this->get_payment_fee($payment['addby'], $payment['amount']);
      $fee = $payment['member_fee'];
      $operator = 'del';
      $array = array(
        'membercode' => $payment['fromcode'],
        'memberid' => $payment['fromid'],
        'broker' => '',
        'ordertype' => 'payment',
        'detail' => "หักค่าบริการจากการเติมเครดิตผ่านระบบ ".$payment['addby'],
        'orderid' => $payment['ID'],
      );
      $this->statement->create_statement_member($payment['fromcode'], $payment['fromid'], $operator, $fee, $array);

      $res_payment = true;
      $where = array(
        'membercode' => $payment['fromcode'],
        'memberid' => $payment['fromid']
      );
      $get_member_detail = $this->multipledb->owstopup
        ->select('*')
        ->from('member')
        ->where($where)
        ->get()
        ->result_array();
      $member_detail = $get_member_detail[0];
    }

    //cashbag_partner
    $partnerid = $payment['toid'];
    $bank = $payment['channel'];
    $operator = 'add';
    $amount = $payment['amount'];
    $array = array(
      'partnercode' => $payment['tocode'],
      'partnerid' => $payment['toid'],
      'bank' => 'อื่นๆ',
      'cashtype' => 'payment',
      'membercode' => $payment['fromcode'],
      'memberid' => $payment['fromid'],
      'orderid' => $payment['ID'],
      'detail' => "เครดิต : อนุมัติเครดิต orderid ".$payment['ID'],
      'amount' => $payment['amount'],
    );
    $this->cashbag->create_cashbag_partner($partnerid, $bank, $operator, $amount, $array);

    //clear call
    $update = array(
      'callone' => 0,
      'calltwo' => 0,
      'callthree' => 0
    );
    if($member_close == true){
      $update['status'] =0;
      $update['useweb'] = 0;
      $update['usetopupmobile'] = 0;
      $update['usetopupcard'] = 0; 
      $update['usetopuppublic'] = 0;
    }
    $where = array(
      'membercode' => $payment['fromcode'], 
      'memberid' => $payment['fromid'],
    );
    $this->multipledb->owstopup->where($where);
    $this->multipledb->owstopup->set($update);
    $this->multipledb->owstopup->update('member');
        
    //log_payment
    $insert = array(
      'paymentid' => $payment['ID'], 
      'action' => 'approve', 
      'addby' => $payment['addby'], 
      'adddate' => date('Y-m-d'), 
      'addtime' => date('H:i:s'),
    );
    $this->multipledb->owstopup->insert('log_payment', $insert);

    if($res_payment){
      $sms = $member_detail['username']." อนุมัติเติมเครดิต ".number_format($payment['amount'],2)." บ. คงเหลือ ".number_format($member_detail['credit'],2)." บ. ขอบคุณค่ะ";
      $data = array(
        'fromcode' => $member_detail['membercode'],
        'fromid' => $member_detail['memberid'],
        'fromtype' => 'member',
        'mobile' => $member_detail['mobile'],
        'message' => $sms
      );
      $this->sendmessage->send($data, $trans_log_id);
    }

    $return = array(
      'res_payment' => isset($res_payment) ? $res_payment : false,
      'is_member_v3' => isset($is_member_v3) ? $is_member_v3 : false,
    );
    return $return;
  }

  function add_payment_partner_to_admin($payment, $partner){

    $account_bank = (isset($payment['account_bank'])) ? $payment['account_bank'] : '';
    $transfer_date = (isset($payment['transfer_date'])) ? $payment['transfer_date'] : date('Y-m-d');
    $transfer_time = (isset($payment['transfer_time'])) ? $payment['transfer_time'] : date('H:i:s');

    $insert = array(
      'fromcode' => 'PN',
      'fromid' => $payment['toid'],
      'fromtype' => 'partner',
      'banktransfer' => 'อื่นๆ',
      'tocode' => 'AM',
      'toid' => 1,
      'totype' => 'admin',
      'channel' => $payment['channel'],
      'paymentauto' => 0,
      'detail' => $payment['detail'],
      'amount' => $payment['amount'],
      'fee' => $payment['partner_fee'],
      'creditbefore' => $partner->credit,
      'creditafter' => ($partner->credit) + ($payment['amount']),
      'addby' => $payment['addby'],
      'transferdate' => $transfer_date,
      'transfertime' => $transfer_time,
      'adddate' => date('Y-m-d'),
      'addtime' => date('H:i:s'),
    );
    $res = $this->insert_payment($insert);
    $payment_partner = $insert;
    $payment_partner['ID'] = $res['id'];
    return $payment_partner;
  }

  function approve_credit_partner($payment, $trans_log_id=''){
    $date=date('Y-m-d');
    $time=date('H:i:s');

    //update payment
    $update = array(
      'approveby' => $payment['addby'], 
      'approvedate' => date('Y-m-d'), 
      'approvetime' => date('H:i:s'), 
      'status' => 1, 
    );
    $where = array('ID' => $payment['ID']);
    $this->multipledb->owstopup->where($where);
    $this->multipledb->owstopup->set($update);
    $this->multipledb->owstopup->update('payment');

    $data = array(
      'fromcode' => $payment['fromcode'],
      'fromid' => $payment['fromid'],
      'fromtype' => $payment['fromtype'],
      'channel' => $payment['channel'],
      'amount' => $payment['amount'],
      'fee' => $payment['partner_fee'],
      'orderid' => $payment['ID'],
      'addby' => $payment['addby']
    );

    if($payment['res_payment'] == true){
      if($payment['is_member_v3']){
        $is_member_v3 = true;
        $resPaymentV3 = $this->payment_migrate->payment_credit_v3($data, $trans_log_id);
        if($resPaymentV3 != 'ERROR'){
          if($resPaymentV3['error_code'] != 'E00'){
            $message = "\r\nPayment To V3 Fail\r\nPlease Check Payment ID: ".$payment['ID']."\r\nFromCode: ".$payment['fromcode']."\r\nFromID: ".$payment['fromid']."\r\nAmount: ".$payment['amount'];
            $this->alert_line->notify_message($message);
          }else{
            $res_payment = true;
          }
        }else{
          $message = "\r\nPayment Connection V3 Error (confirm)\r\nPlease Check Payment ID: ".$payment['ID']."\r\nFromCode: ".$payment['fromcode']."\r\nFromID: ".$payment['fromid']."\r\nAmount: ".$payment['amount'];
          $this->alert_line->notify_message($message);
        }

        /*
        $where = array(
          'partnercode' => $payment['fromcode'],
          'partnerid' => $payment['fromid']
        );
        $get_partner_detail = $this->payment_migrate->get_partner_detail_v3($where, $trans_log_id);
        if($get_partner_detail != 'ERROR'){
          if($get_partner_detail['error_code'] == 'E00'){
            $partner_detail = $get_partner_detail['data'][0];
          }
        }
        */
      }else{        
        //statement_partner
        $partnerid = $payment['fromid'];
        $operator = 'add';
        $amount = $payment['amount'];
        $array = array(
          'partnercode' => 'PN',
          'partnerid' => $payment['fromid'],
          'broker' => '',
          'ordertype' => 'payment',
          'detail' => "เครดิต : อนุมัติเครดิต orderid ".$payment['ID'],
          'orderid' => $payment['ID'],
        );
        $this->statement->create_statement_partner($partnerid, $operator, $amount, $array);

        //statement_partner fee
        // $fee = $this->get_payment_fee($payment['addby'], $payment['amount']);
        $fee = $payment['partner_fee'];
        $operator = 'del';
        $array = array(
          'partnercode' => 'PN',
          'partnerid' => $payment['fromid'],
          'broker' => '',
          'ordertype' => 'payment',
          'detail' => "หักค่าบริการจากการเติมเครดิตผ่านระบบ ".$payment['addby'],
          'orderid' => $payment['ID'],
        );
        $this->statement->create_statement_partner($partnerid, $operator, $fee, $array);
        $res_payment = true;
      }

      //cashbag_admin
      $partnerid = $payment['toid'];
      $bank = $payment['channel'];
      $operator = 'add';
      $amount = $payment['amount'];
      $array = array(
        'bank' => $payment['channel'],
        'cashtype' => 'payment',
        'partnercode' => $payment['fromcode'],
        'partnerid' => $payment['fromid'],
        'orderid' => $payment['ID'],
        'detail' => "เครดิต : อนุมัติเครดิต orderid ".$payment['ID'],
        'amount' => $payment['amount'],
      );
      $this->cashbag->create_cashbag_admin($bank, $operator, $amount, $array);

      //cashbag_partner
      $partnerid = $payment['toid'];
      $bank = $payment['channel'];
      $operator = 'del';
      $amount = $payment['amount'];
      $array = array(
        'partnercode' => $payment['tocode'],
        'partnerid' => $payment['toid'],
        'bank' => 'อื่นๆ',
        'cashtype' => 'payment',
        'membercode' => '',
        'memberid' => 0,
        'orderid' => $payment['ID'],
        'detail' => "เครดิต : อนุมัติเครดิต orderid ".$payment['ID'],
        'amount' => $payment['amount'],
      );
      $this->cashbag->create_cashbag_partner($partnerid, $bank, $operator, $amount, $array);

      //clear call
      $update = array(
        'callone' => 0,
        'calltwo' => 0,
        'callthree' => 0,
      );
      $where = array('partnerid' => $payment['fromid']);
      $this->multipledb->owstopup->where($where);
      $this->multipledb->owstopup->set($update);
      $this->multipledb->owstopup->update('partner');
    }

    /*
    if(!isset($partner_detail)){
      $where = array(
        'partnercode' => $payment['fromcode'],
        'partnerid' => $payment['fromid']
      );
      $get_partner_detail = $this->multipledb->owstopup
        ->select('*')
        ->from('partner')
        ->where($where)
        ->get()
        ->result_array();
      $partner_detail = $get_partner_detail[0];
    }

    if($res_payment){
      $sms = $partner_detail['username']." อนุมัติเติมเครดิต ".number_format($payment['amount'],2)." บ. คงเหลือ ".number_format($partner_detail['credit'],2)." บ. ขอบคุณค่ะ";
      $data = array(
        'fromcode' => $partner_detail['partnercode'],
        'fromid' => $partner_detail['partnerid'],
        'fromtype' => 'partner',
        'mobile' => $partner_detail['mobile'],
        'message' => $sms
      );
      $this->sendmessage->send($data, $trans_log_id);
    }
    */

    //log_payment
    $insert = array(
      'paymentid' => $payment['ID'], 
      'action' => 'approve', 
      'addby' => $payment['addby'], 
      'adddate' => date('Y-m-d'), 
      'addtime' => date('H:i:s'), 
    );
    $this->multipledb->owstopup->insert('log_payment', $insert);

    $return = array(
      'res_payment' => isset($res_payment) ? $res_payment : false
    );
    return $return;
  }

  function insert_payment($insert){
    $query = $this->multipledb->owstopup->insert('payment', $insert);
    if($query){
      $id = $this->multipledb->owstopup->insert_id();
      $ret['status'] = true;
      $ret['id'] = $id;
    }
    else{
      $ret['status'] = false;
    }
    return $ret;
  }

  function send_sms($member_mobile,$message,$membercode,$memberid){
    $obj = new ArrayObject();
    $obj->mobile = $member_mobile;
    // $obj->mobile = '0957386086';
    $obj->message = $message;
    if($obj->mobile != '' && strlen($obj->mobile) == 10){
      $response = $this->smsscript->send_sms($obj);
      // echo "send sms : ".$message;
    }    
  }

  function get_member($where){
    $query = $this->multipledb->owstopup
      ->from('member')
      ->where($where)
      ->get()
      ->row();
    return $query;
  }

  function do_migrate($payment){
    $data = array(
      'fromcode' => $payment['fromcode'],
      'fromid' => $payment['fromid'],
      'fromtype' => $payment['fromtype'],
      'membercode' => $payment['fromcode'],
      'memberid' => $payment['fromid'],
      'partnerid' => $payment['toid'],
      'orderid' => $payment['ID'],
      'amount' => $payment['amount'],
      'detail' => 'โอนย้ายเครดิตสมาชิกจากการเติมเครดิตผ่านระบบ '.$payment['addby'].' ไปยังระบบใหม่'
    );
    $res_migrate = $this->payment_migrate->migrate_member_credit($data);
    $where = array(
      'membercode' => $data['membercode'],
      'memberid' => $data['memberid']
    );

    $member = $this->multipledb->owstopup->select('*')->from('member')->where($where)->get()->row(); 
    $username = $data['membercode'].str_pad($data['memberid'],5,"0",STR_PAD_LEFT);
    if(isset($res_migrate['error_code'])){
      if($res_migrate['error_code'] == 'E00'){
        $sms = $username." อนุมัติเติมเครดิต ".number_format($data['amount'],2)." บ. คงเหลือ ".number_format($res_migrate['m_credit']+$member->credit,2)." บ. ขอบคุณค่ะ";
      }
      else{
        $sms = $username." อนุมัติเติมเครดิต ".number_format($data['amount'],2)." บ. คงเหลือ ".number_format($member->credit,2)." บ. ขอบคุณค่ะ";
      }
    }else{
      $sms = $username." อนุมัติเติมเครดิต ".number_format($data['amount'],2)." บ. คงเหลือ ".number_format($member->credit,2)." บ. ขอบคุณค่ะ";
    }
    $this->send_sms($member->mobile, $sms, $data['membercode'], $data['memberid']);
  }

  function transfer_credit_partner_to_v3($payment){
    $data = array(
      'fromcode' => $payment['fromcode'],
      'fromid' => $payment['fromid'],
      'fromtype' => $payment['fromtype'],
      'orderid' => $payment['ID'],
      'amount' => $payment['amount'],
      'detail' => 'โอนย้ายเครดิตสมาชิกจากการเติมเครดิตผ่านระบบ '.$payment['addby'].' ไปยังระบบใหม่'
    );
    $res_migrate = $this->payment_migrate->migrate_partner_credit($data);
    $where = array(
      'partnercode' => $data['fromcode'],
      'partnerid' => $data['fromid']
    );
    $partner = $this->multipledb->owstopup->select('*')->from('partner')->where($where)->get()->row(); 
    $username = $data['fromcode'].str_pad($data['fromid'],5,"0",STR_PAD_LEFT);
    if(isset($res_migrate['error_code'])){
      if($res_migrate['error_code'] == 'E00'){
        $sms = $username." อนุมัติเติมเครดิต ".number_format($data['amount'],2)." บ. คงเหลือ ".number_format($res_migrate['m_credit']+$partner->credit,2)." บ. ขอบคุณค่ะ";
      }
      else{
        $sms = $username." อนุมัติเติมเครดิต ".number_format($data['amount'],2)." บ. คงเหลือ ".number_format($partner->credit,2)." บ. ขอบคุณค่ะ";
      }
    }else{
      $sms = $username." อนุมัติเติมเครดิต ".number_format($data['amount'],2)." บ. คงเหลือ ".number_format($partner->credit,2)." บ. ขอบคุณค่ะ";
    }
    $this->send_sms($partner->mobile, $sms, $data['fromcode'], $data['fromid']);
  }
}
