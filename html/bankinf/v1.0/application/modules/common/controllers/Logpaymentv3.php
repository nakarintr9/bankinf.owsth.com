<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Logpaymentv3 extends MY_Controller {

  function __construct()
  {
    parent::__construct();
    $this->load->library('multipledb');
  }

  function update_log_v3($update_log, $logid){
    $where = array(
      'ID'=>$logid,
    );
    $this->multipledb->owstopup->where($where);
    $this->multipledb->owstopup->set($update_log);
    $res = $this->multipledb->owstopup->update('log_payment_v3');
    return $res;
  }

  function insert_log_v3($insert){
    $this->multipledb->owstopup->insert('log_payment_v3', $insert);
    $insert_id = $this->multipledb->owstopup->insert_id();
    return $insert_id;
  }
}