<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Genscbqr extends MY_Controller {

  function __construct() {
    parent::__construct();
    $this->load->module('scb/generateqrcode');
  }

  function index(){
    $postdata = file_get_contents("php://input");
    $post = json_decode($postdata);
    $body = (array)$post;
    $ref1 = isset($body['ref1']) ? $body['ref1'] : '00004';
    $ref1 = date('ymdHis').rand(10,99).$ref1;
    $ref2 = isset($body['ref2']) ? $body['ref2'] : '00001';
    $ref3 = 'MEC';
    $amount = isset($body['amount']) ? $body['amount'] : 1;
    $merchant_name= isset($body['merchant_name']) ? $body['merchant_name'] : '';
    $res = $this->generateqrcode->call($ref1, $ref2, $ref3, $amount, $merchant_name='');
    $return = (array) $res;
    header('Content-Type: application/json');
    echo json_encode($return);
  }
}
