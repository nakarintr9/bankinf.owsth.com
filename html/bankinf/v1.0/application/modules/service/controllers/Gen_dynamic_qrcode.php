<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Gen_dynamic_qrcode extends MY_Controller {

  function __construct() {
    parent::__construct();
    $this->load->library('errorcode');
  }

  function index(){
    $postdata = file_get_contents("php://input");
    $post = json_decode($postdata, true);
    $body = (array)$post;
    $channel = isset($body['channel']) ? $post['channel'] : '';
    if($channel == 'alipay' || $channel == 'wechat'){
      $this->load->module('ksherpay/generateqrcode');
      $res = $this->generateqrcode->gen($body);
      $return = (array) $res;
    }
    else if($channel == 'krungsri'){
      $this->load->module('krungsri/generateqrcode');
      $res = $this->generateqrcode->gen($body);
      $return = (array) $res;
    }
    else if($channel == 'bay'){
      $this->load->module('bay/generateqrcode');
      $res = $this->generateqrcode->gen($body);
      $return = (array) $res;
    }
    else if($channel == 'bbl'){
      $this->load->module('bbl/generateqrcode');
      $res = $this->generateqrcode->gen($body);
      $return = (array) $res;
    }
    else{
      $error_code = 'E03';
      $return = array(
        'error_code' => $error_code,
        'error_text' => $this->errorcode->$error_code,
        'qr_image' => '',
        'broker_error' => ''
      );
    }
    header('Content-Type: application/json');
    echo json_encode($return);
  }
}
