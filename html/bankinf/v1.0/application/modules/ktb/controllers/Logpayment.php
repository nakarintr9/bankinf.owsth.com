<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Logpayment extends MY_Controller {

  function __construct()
  {
    parent::__construct();
    $this->load->library('multipledb');
  }

  function get_log($transid, $ref1, $ref2, $api_type, $action, $logid=''){
    $where = array(
      'request_transid'=>$transid,
      'ref1'=>$ref1,
      'ref2'=>$ref2,
      'api_type'=>$api_type,
      'action'=>$action
    );
    if(!empty($logid)){
      $where['ID !='] = $logid;
    }
    $qry = $this->multipledb->owstopup->select('*')
      ->from('log_ktb_ows')
      ->where($where)
      ->get()
      ->row();
    return $qry;
  }

  function update_log($update_log, $transid, $ref1, $ref2, $api_type, $action){
    $where = array(
      'request_transid'=>$transid,
      'ref1'=>$ref1,
      'ref2'=>$ref2,
      'api_type'=>$api_type,
      'action'=>$action
    );
    $this->multipledb->owstopup->where($where);
    $this->multipledb->owstopup->set($update_log);
    $res = $this->multipledb->owstopup->update('log_ktb_ows');
    return $res;
  }

  function update_log_id($update_log, $logid){
    $where = array(
      'ID'=>$logid,
    );
    $this->multipledb->owstopup->where($where);
    $this->multipledb->owstopup->set($update_log);
    $res = $this->multipledb->owstopup->update('log_ktb_ows');
    return $res;
  }

  function insert_log($insert){
    $this->multipledb->owstopup->insert('log_ktb_ows', $insert);
    $insert_id = $this->multipledb->owstopup->insert_id();
    return $insert_id;
  }

  function get_log_where($where){
    $qry = $this->multipledb->owstopup->select('*')
      ->from('log_ktb_ows')
      ->where($where)
      ->get()
      ->row();
    return $qry;
  }

  function get_log_join_payment_partner_by_bankref($bankref){
    $sql="select lg.amount,lg.partnerpaymentid,ifnull(pp.fee,10) as partner_fee
          from log_ktb_ows lg
          join payment pp on pp.ID = lg.partnerpaymentid and pp.status = 1 and pp.fromtype = 'partner' and pp.addby='KTB Bill Payment'
          where lg.bankref = '".$bankref."' and lg.api_type= 'payment' ";
    $qry = $this->multipledb->owstopup->query($sql)->row();
    return $qry;
  }

  function get_log_join_payment_partner_member_by_bankref($bankref){
    $sql="select lg.amount,lg.partnerpaymentid,lg.memberpaymentid,ifnull(pp.fee,10) as partner_fee, ifnull(pm.fee,10) as member_fee
        from log_ktb_ows lg
        join payment pp on pp.ID = lg.partnerpaymentid and pp.status = 1 and pp.fromtype = 'partner' and pp.addby='KTB Bill Payment'
        join payment pm on pm.ID = lg.memberpaymentid and pm.status = 1 and pm.fromtype = 'member' and pm.addby='KTB Bill Payment' 
        where lg.bankref = '".$bankref."' and lg.api_type= 'payment' ";
    $qry = $this->multipledb->owstopup->query($sql)->row();
    return $qry;
  }
}
