<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Billpayment extends MY_Controller {

  function __construct() 
  {
    parent::__construct();

    $this->load->library("nusoap_library"); //load the library here
    $obj_base_url = include(getcwd().'/../../../config/bankinf/base_url.php');

    $endPoint = $obj_base_url->base_url."/ktb/billpayment";
    // $endPoint = $obj_base_url->base_url."/callback/ktb/billpayment";
    $_SERVER['PHP_SELF'] = $endPoint;

    $this->nusoap_server = new soap_server();
    $this->nusoap_server->configureWSDL("Billpayment", "urn:Billpayment", $endPoint);
    $this->nusoap_server->wsdl->schemaTargetNamespace = 'urn:Billpayment';

    //DATA TYPES Inquiry
    $this->nusoap_server->wsdl->addComplexType(
      'InquiryRequestData',
      'complexType',
      'struct',
      'all',
      '',
      array(
        'user' => array('name' => 'user', 'type' => 'xsd:string'),
        'password' => array('name' => 'password', 'type' => 'xsd:string'), 
        'comcode' => array('name' => 'comcode', 'type' =>'xsd:string'), 
        'prodcode' => array('name' => 'prodcode', 'type' =>'xsd:string'), 
        'command' => array('name' => 'command', 'type' =>'xsd:string'), 
        'bankcode' => array('name' => 'bankcode', 'type' =>'xsd:decimal'), 
        'bankref' => array('name' => 'bankref', 'type' =>'xsd:string'), 
        'datetime' => array('name' => 'datetime', 'type' =>'xsd:string'), 
        'effdate' => array('name' => 'effdate', 'type' =>'xsd:string'), 
        'channel' => array('name' => 'channel', 'type' =>'xsd:string'), 
        'ref1' => array('name' => 'ref1', 'type' =>'xsd:string'), 
        'ref2' => array('name' => 'ref2', 'type' =>'xsd:string'), 
        'ref3' => array('name' => 'ref3', 'type' =>'xsd:string'), 
        'ref4' => array('name' => 'ref4', 'type' =>'xsd:string')
      )
    );

    $this->nusoap_server->wsdl->addComplexType(
      'InquiryResponseData',
      'complexType',
      'struct',
      'all',
      '',
      array(
        'tranxid' => array('name' => 'tranxid', 'type' => 'xsd:string'), 
        'bankref' => array('name' => 'bankref', 'type' => 'xsd:string'), 
        'respcode' => array('name' => 'respcode', 'type' => 'xsd:decimal'), 
        'respmsg' => array('name' => 'respmsg', 'type' => 'xsd:string'), 
        'balance' => array('name' => 'balance', 'type' => 'xsd:decimal'), 
        'cusname' => array('name' => 'cusname', 'type' => 'xsd:string'), 
        'print1' => array('name' => 'print1', 'type' => 'xsd:string'), 
        'print2' => array('name' => 'print2', 'type' => 'xsd:string'), 
        'print3' => array('name' => 'print3', 'type' => 'xsd:string'), 
        'print4' => array('name' => 'print4', 'type' => 'xsd:string'), 
        'print5' => array('name' => 'print5', 'type' => 'xsd:string'), 
        'print6' => array('name' => 'print6', 'type' => 'xsd:string'), 
        'print7' => array('name' => 'print7', 'type' => 'xsd:string')
      )
    );

    //DATA TYPES Approval
    $this->nusoap_server->wsdl->addComplexType(
      'ApprovalRequestData',
      'complexType',
      'struct',
      'sequence',
      '',
      array(
        'user' => array('name' => 'user', 'type' => 'xsd:string'),
        'password' => array('name' => 'password', 'type' => 'xsd:string'), 
        'comcode' => array('name' => 'comcode', 'type' =>'xsd:string'), 
        'prodcode' => array('name' => 'prodcode', 'type' =>'xsd:string'), 
        'command' => array('name' => 'command', 'type' =>'xsd:string'), 
        'bankcode' => array('name' => 'bankcode', 'type' =>'xsd:decimal'), 
        'bankref' => array('name' => 'bankref', 'type' =>'xsd:string'), 
        'datetime' => array('name' => 'datetime', 'type' =>'xsd:string'), 
        'effdate' => array('name' => 'effdate', 'type' =>'xsd:string'), 
        'amount' => array('name' => 'amount', 'type' =>'xsd:decimal'), 
        'channel' => array('name' => 'channel', 'type' =>'xsd:string'), 
        'cusname' => array('name' => 'cusname', 'type' =>'xsd:string'), 
        'ref1' => array('name' => 'ref1', 'type' =>'xsd:string'), 
        'ref2' => array('name' => 'ref2', 'type' =>'xsd:string'), 
        'ref3' => array('name' => 'ref3', 'type' =>'xsd:string'), 
        'ref4' => array('name' => 'ref4', 'type' =>'xsd:string')
      )
    );

    $this->nusoap_server->wsdl->addComplexType(
      'ApprovalResponseData',
      'complexType',
      'struct',
      'sequence',
      '',
      array(
        'tranxid' => array('name' => 'tranxid', 'type' => 'xsd:string'), 
        'bankref' => array('name' => 'bankref', 'type' => 'xsd:string'), 
        'respcode' => array('name' => 'respcode', 'type' => 'xsd:decimal'), 
        'respmsg' => array('name' => 'respmsg', 'type' => 'xsd:string'), 
        'balance' => array('name' => 'balance', 'type' => 'xsd:decimal'), 
        'cusname' => array('name' => 'cusname', 'type' => 'xsd:string'), 
        'info' => array('name' => 'info', 'type' => 'xsd:string'),
        'print1' => array('name' => 'print1', 'type' => 'xsd:string'), 
        'print2' => array('name' => 'print2', 'type' => 'xsd:string'), 
        'print3' => array('name' => 'print3', 'type' => 'xsd:string'), 
        'print4' => array('name' => 'print4', 'type' => 'xsd:string'), 
        'print5' => array('name' => 'print5', 'type' => 'xsd:string'), 
        'print6' => array('name' => 'print6', 'type' => 'xsd:string'), 
        'print7' => array('name' => 'print7', 'type' => 'xsd:string')
      )
    );

    //DATA TYPES Payment
    $this->nusoap_server->wsdl->addComplexType(
      'PaymentRequestData',
      'complexType',
      'struct',
      'sequence',
      '',
      array(
        'user' => array('name' => 'user', 'type' => 'xsd:string'),
        'password' => array('name' => 'password', 'type' => 'xsd:string'), 
        'comcode' => array('name' => 'comcode', 'type' =>'xsd:string'), 
        'prodcode' => array('name' => 'prodcode', 'type' =>'xsd:string'), 
        'command' => array('name' => 'command', 'type' =>'xsd:string'), 
        'bankcode' => array('name' => 'bankcode', 'type' =>'xsd:decimal'), 
        'bankref' => array('name' => 'bankref', 'type' =>'xsd:string'), 
        'tranxid' => array('name' => 'tranxid', 'type' =>'xsd:string'), 
        'datetime' => array('name' => 'datetime', 'type' =>'xsd:string'), 
        'effdate' => array('name' => 'effdate', 'type' =>'xsd:string'), 
        'amount' => array('name' => 'amount', 'type' =>'xsd:decimal'), 
        'channel' => array('name' => 'channel', 'type' =>'xsd:string'), 
        'cusname' => array('name' => 'cusname', 'type' =>'xsd:string'), 
        'ref1' => array('name' => 'ref1', 'type' =>'xsd:string'), 
        'ref2' => array('name' => 'ref2', 'type' =>'xsd:string'), 
        'ref3' => array('name' => 'ref3', 'type' =>'xsd:string'), 
        'ref4' => array('name' => 'ref4', 'type' =>'xsd:string')
      )
    );

    $this->nusoap_server->wsdl->addComplexType(
      'PaymentResponseData',
      'complexType',
      'struct',
      'sequence',
      '',
      array(
        'tranxid' => array('name' => 'tranxid', 'type' => 'xsd:string'), 
        'bankref' => array('name' => 'bankref', 'type' => 'xsd:string'), 
        'respcode' => array('name' => 'respcode', 'type' => 'xsd:decimal'), 
        'respmsg' => array('name' => 'respmsg', 'type' => 'xsd:string'), 
        'balance' => array('name' => 'balance', 'type' => 'xsd:decimal'), 
        'cusname' => array('name' => 'cusname', 'type' => 'xsd:string'), 
        'info' => array('name' => 'info', 'type' => 'xsd:string'),
        'print1' => array('name' => 'print1', 'type' => 'xsd:string'), 
        'print2' => array('name' => 'print2', 'type' => 'xsd:string'), 
        'print3' => array('name' => 'print3', 'type' => 'xsd:string'), 
        'print4' => array('name' => 'print4', 'type' => 'xsd:string'), 
        'print5' => array('name' => 'print5', 'type' => 'xsd:string'), 
        'print6' => array('name' => 'print6', 'type' => 'xsd:string'), 
        'print7' => array('name' => 'print7', 'type' => 'xsd:string')
      )
    );

    //DATA TYPES Approve Reversal
    $this->nusoap_server->wsdl->addComplexType(
      'ApproveReversalRequestData',
      'complexType',
      'struct',
      'sequence',
      '',
      array(
        'user' => array('name' => 'user', 'type' => 'xsd:string'),
        'password' => array('name' => 'password', 'type' => 'xsd:string'), 
        'comcode' => array('name' => 'comcode', 'type' =>'xsd:string'), 
        'prodcode' => array('name' => 'prodcode', 'type' =>'xsd:string'), 
        'command' => array('name' => 'command', 'type' =>'xsd:string'), 
        'bankcode' => array('name' => 'bankcode', 'type' =>'xsd:decimal'), 
        'bankref' => array('name' => 'bankref', 'type' =>'xsd:string'), 
        'tranxid' => array('name' => 'tranxid', 'type' =>'xsd:string'), 
        'datetime' => array('name' => 'datetime', 'type' =>'xsd:string'), 
        'effdate' => array('name' => 'effdate', 'type' =>'xsd:string'), 
        'amount' => array('name' => 'amount', 'type' =>'xsd:decimal'), 
        'channel' => array('name' => 'channel', 'type' =>'xsd:string'), 
        'cusname' => array('name' => 'cusname', 'type' =>'xsd:string'), 
        'ref1' => array('name' => 'ref1', 'type' =>'xsd:string'), 
        'ref2' => array('name' => 'ref2', 'type' =>'xsd:string'), 
        'ref3' => array('name' => 'ref3', 'type' =>'xsd:string'), 
        'ref4' => array('name' => 'ref4', 'type' =>'xsd:string')
      )
    );

    $this->nusoap_server->wsdl->addComplexType(
      'ApproveReversalResponseData',
      'complexType',
      'struct',
      'sequence',
      '',
      array(
        'tranxid' => array('name' => 'tranxid', 'type' => 'xsd:string'), 
        'bankref' => array('name' => 'bankref', 'type' => 'xsd:string'), 
        'respcode' => array('name' => 'respcode', 'type' => 'xsd:decimal'), 
        'respmsg' => array('name' => 'respmsg', 'type' => 'xsd:string')
      )
    );

    //DATA TYPES Reversal
    $this->nusoap_server->wsdl->addComplexType(
      'ReversalRequestData',
      'complexType',
      'struct',
      'sequence',
      '',
      array(
        'user' => array('name' => 'user', 'type' => 'xsd:string'),
        'password' => array('name' => 'password', 'type' => 'xsd:string'), 
        'comcode' => array('name' => 'comcode', 'type' =>'xsd:string'), 
        'prodcode' => array('name' => 'prodcode', 'type' =>'xsd:string'), 
        'command' => array('name' => 'command', 'type' =>'xsd:string'), 
        'bankcode' => array('name' => 'bankcode', 'type' =>'xsd:decimal'), 
        'bankref' => array('name' => 'bankref', 'type' =>'xsd:string'), 
        'tranxid' => array('name' => 'tranxid', 'type' =>'xsd:string'), 
        'datetime' => array('name' => 'datetime', 'type' =>'xsd:string'), 
        'effdate' => array('name' => 'effdate', 'type' =>'xsd:string'), 
        'amount' => array('name' => 'amount', 'type' =>'xsd:decimal'), 
        'channel' => array('name' => 'channel', 'type' =>'xsd:string'), 
        'cusname' => array('name' => 'cusname', 'type' =>'xsd:string'), 
        'ref1' => array('name' => 'ref1', 'type' =>'xsd:string'), 
        'ref2' => array('name' => 'ref2', 'type' =>'xsd:string'), 
        'ref3' => array('name' => 'ref3', 'type' =>'xsd:string'), 
        'ref4' => array('name' => 'ref4', 'type' =>'xsd:string')
      )
    );

    $this->nusoap_server->wsdl->addComplexType(
      'ReversalResponseData',
      'complexType',
      'struct',
      'sequence',
      '',
      array(
        'tranxid' => array('name' => 'tranxid', 'type' => 'xsd:string'), 
        'bankref' => array('name' => 'bankref', 'type' => 'xsd:string'), 
        'respcode' => array('name' => 'respcode', 'type' => 'xsd:decimal'), 
        'respmsg' => array('name' => 'respmsg', 'type' => 'xsd:string')
      )
    );


    $InquiryRequestData =  array(
      'user' => 'xsd:string',
      'password' => 'xsd:string', 
      'comcode' => 'xsd:string', 
      'prodcode' => 'xsd:string', 
      'command' => 'xsd:string', 
      'bankcode' => 'xsd:decimal', 
      'bankref' => 'xsd:string', 
      'datetime' => 'xsd:string', 
      'effdate' => 'xsd:string', 
      'channel' => 'xsd:string', 
      'ref1' => 'xsd:string', 
      'ref2' => 'xsd:string', 
      'ref3' => 'xsd:string', 
      'ref4' => 'xsd:string'
    );

    $InquiryResponseData =  array(
      'tranxid' => 'xsd:string', 
      'bankref' => 'xsd:string', 
      'respcode' => 'xsd:decimal', 
      'respmsg' => 'xsd:string', 
      'balance' => 'xsd:decimal', 
      'cusname' => 'xsd:string', 
      'print1' => 'xsd:string', 
      'print2' => 'xsd:string', 
      'print3' => 'xsd:string', 
      'print4' => 'xsd:string', 
      'print5' => 'xsd:string', 
      'print6' => 'xsd:string', 
      'print7' => 'xsd:string'
    );

    //REGISTRATION Inquiry
    $this->nusoap_server->register(
      'inquiry',
      array('request' => 'tns:InquiryRequestData'),  //parameters
      array('response' => 'tns:InquiryResponseData'),  //output
      'urn:Billpayment',   //namespace
      'urn:Billpayment#inquiry',  //soapaction
      'rpc', // style
      'encoded', // use
      'Inquiry Data' //description
    );

    //REGISTRATION Approval
    $this->nusoap_server->register(
      'approval',
      array('request' => 'tns:ApprovalRequestData'),  //parameters
      array('return' => 'tns:ApprovalResponseData'),  //output
      'urn:Billpayment',   //namespace
      'urn:Billpayment#approval',  //soapaction
      'rpc', // style
      'encoded', // use
      'Approval data' //description
    );

    //REGISTRATION Payment
    $this->nusoap_server->register(
      'payment',
      array('request' => 'tns:PaymentRequestData'),  //parameters
      array('return' => 'tns:PaymentResponseData'),  //output
      'urn:Billpayment',   //namespace
      'urn:Billpayment#payment',  //soapaction
      'rpc', // style
      'encoded', // use
      'Payment data' //description
    );

    //REGISTRATION Approve Reversal
    $this->nusoap_server->register(
      'approvereversal',
      array('request' => 'tns:ApproveReversalRequestData'),  //parameters
      array('return' => 'tns:ApproveReversalResponseData'),  //output
      'urn:Billpayment',   //namespace
      'urn:Billpayment#approvereversal',  //soapaction
      'rpc', // style
      'encoded', // use
      'Approval Reversal data' //description
    );

    //REGISTRATION Reversal
    $this->nusoap_server->register(
      'reversal',
      array('request' => 'tns:ReversalRequestData'),  //parameters
      array('return' => 'tns:ReversalResponseData'),  //output
      'urn:Billpayment',   //namespace
      'urn:Billpayment#reversal',  //soapaction
      'rpc', // style
      'encoded', // use
      'Reversal data' //description
    );

    //IMPLEMENTATION

    function validate($request){
      $user = 'mcktb2017';
      $password = 'M3d!4@C3NT3R';
      if($request['user'] == $user && $request['password'] = $password){
        return true;
      }else{
        return false;
      }
    }

    // function inquiry($user,$password,$comcode,$prodcode,$command,
    //                 $bankcode,$bankref,$datetime,$effdate,$channel,
    //                 $ref1,$ref2,$ref3,$ref4
    //   ){
    function inquiry($request){
      $checkuser = validate($request);
      if($checkuser){
         $response = inquiry_agent_api($request);
       }else{
        $respcode = '101';
        $resmsg = get_res_msg($respcode);
        $response = array(
          'tranxid' => ' ', 
          'bankref' => $request['bankref'], 
          'respcode' => $respcode, 
          'respmsg' =>  $resmsg, 
          'balance' => '0.00', 
          'cusname' => ' ',
          'print1' => " ", 
          'print2' => ' ', 
          'print3' => ' ', 
          'print4' => ' ', 
          'print5' => ' ', 
          'print6' => ' ', 
          'print7' => ' '
        );
       }
      return $response;
    }

    function get_res_msg($respcode){
      $res_msg = array(
          '0' => 'Successful',
         '99' => 'Communication Error',
        '100' => 'Unable to process transaction',
        '101' => 'Invalid username/password',
        '102' => 'Require field or parameter is null',
        '103' => 'Transaction amount or number of transaction is over limit',
        '104' => 'Invalid reference',
        '105' => 'Invalid transaction number',
        '106' => 'Transaction number duplicate',
        '107' => 'Not business hour',
        '108' => 'Invalid price or amount',
        '999' => 'Other error'
      );
      return $res_msg[$respcode];
    }

    function inquiry_agent_api($request){
      $obj_ows_ktb_api_url = include(getcwd().'/../../../config/bankinf/ows_ktb_api_url.php');
      $api_url = $obj_ows_ktb_api_url->url;

      $request['apitype'] = 'inquiry';
      $response = curl($api_url, json_encode($request));
      return $response;
    }

    function approval($request){
      $checkuser = validate($request);
      if($checkuser){
        $response = approval_agent_api($request);
       }else{
        $respcode = '101';
        $resmsg = get_res_msg($respcode);
        $response = array(
          'tranxid' => ' ', 
          'bankref' => $request['bankref'], 
          'respcode' => $respcode, 
          'respmsg' =>  $resmsg, 
          'balance' => '0.00', 
          'cusname' => ' ',
          'print1' => " ", 
          'print2' => ' ', 
          'print3' => ' ', 
          'print4' => ' ', 
          'print5' => ' ', 
          'print6' => ' ', 
          'print7' => ' '
        );
       }
      return $response;
    }

    function approval_agent_api($request){
      $obj_ows_ktb_api_url = include(getcwd().'/../../../config/bankinf/ows_ktb_api_url.php');
      $api_url = $obj_ows_ktb_api_url->url;

      $request['apitype'] = 'approval';
      $response = curl($api_url, json_encode($request));
      return $response;
    }

    function payment($request){
      $checkuser = validate($request);
      if($checkuser){
        $response = payment_agent_api($request);
      }else{
        $respcode = '101';
        $resmsg = get_res_msg($respcode);
        $response = array(
          'tranxid' => ' ', 
          'bankref' => $request['bankref'], 
          'respcode' => $respcode, 
          'respmsg' =>  $resmsg, 
          'balance' => '0.00', 
          'cusname' => ' ', 
          'info' => ' ',
          'print1' => " ", 
          'print2' => ' ', 
          'print3' => ' ', 
          'print4' => ' ', 
          'print5' => ' ', 
          'print6' => ' ', 
          'print7' => ' '
        );
      }
      return $response;
    }

    function payment_agent_api($request){
      $obj_ows_ktb_api_url = include(getcwd().'/../../../config/bankinf/ows_ktb_api_url.php');
      $api_url = $obj_ows_ktb_api_url->url;

      $request['apitype'] = 'payment';
      $response = curl($api_url, json_encode($request));
      return $response;
    }

    function approvereversal($request){
      $checkuser = validate($request);
      if($checkuser){
        $response = approvereversal_agent_api($request);
       }else{
        $respcode = '101';
        $resmsg = get_res_msg($respcode);
        $response = array(
          'tranxid' => ' ', 
          'bankref' => $request['bankref'], 
          'respcode' => $respcode, 
          'respmsg' =>  $resmsg
        );
       }
      return $response;
    }

    function approvereversal_agent_api($request){
      $obj_ows_ktb_api_url = include(getcwd().'/../../../config/bankinf/ows_ktb_api_url.php');
      $api_url = $obj_ows_ktb_api_url->url;

      $request['apitype'] = 'approve-reversal';
      $response = curl($api_url, json_encode($request));
      return $response;
    }

    function reversal($request){
      $checkuser = validate($request);
      if($checkuser){
        $response = reversal_agent_api($request);
       }else{
        $respcode = '101';
        $resmsg = get_res_msg($respcode);
        $response = array(
          'tranxid' => ' ', 
          'bankref' => $request['bankref'], 
          'respcode' => $respcode, 
          'respmsg' =>   $resmsg
        );
       }
      return $response;
    }

    function reversal_agent_api($request){
      $obj_ows_ktb_api_url = include(getcwd().'/../../../config/bankinf/ows_ktb_api_url.php');
      $api_url = $obj_ows_ktb_api_url->url;

      $request['apitype'] = 'reversal';
      $response = curl($api_url, json_encode($request));
      return $response;
    }

    function curl($url, $data_json){
      $request = json_decode($data_json, true);
      $ch = curl_init();
      curl_setopt($ch, CURLOPT_URL, $url);
      curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
      curl_setopt($ch, CURLOPT_POST, 1);
      curl_setopt($ch, CURLOPT_POSTFIELDS,$data_json);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
      curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
      curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
      $response  = curl_exec($ch);
      if(curl_error($ch)){
        return get_return_error($request);
      }else{
        return json_decode($response, true);
      }
      curl_close($ch);
    }

    function get_return_error($request){
      $response = array(
        'respcode' => '99', 
        'respmsg' => 'Communication Error'
      );
      return $response;
    }

  }

  function index() {    
    $POST_DATA = isset($GLOBALS['HTTP_RAW_POST_DATA']) ? $GLOBALS['HTTP_RAW_POST_DATA'] : '';
    if(empty($POST_DATA)){
      $POST_DATA = (null != file_get_contents("php://input")) ? file_get_contents("php://input") : '';
    }
    $this->nusoap_server->service($POST_DATA); 
  }

  function test(){
    $payment = new stdClass();
    $payment->tranxid='16';
    $payment->user='user';
    $payment->password='password'; 
    $payment->comcode='1010'; 
    $payment->prodcode='1010'; 
    $payment->command='payment'; 
    $payment->bankcode='6'; 
    $payment->bankref='K00002 00004391'; 
    $payment->datetime=date('YmdHis'); 
    $payment->effdate=date('Ymd');
    $payment->amount='500.00'; 
    $payment->cusname=''; 
    $payment->channel='2'; 
    $payment->ref1='00004'; 
    $payment->ref2=''; 
    $payment->ref3='-'; 
    $payment->ref4='-';



    $obj_ows_ktb_api_url = include(getcwd().'/../../../config/bankinf/ows_ktb_api_url.php');
    $api_url = $obj_ows_ktb_api_url->url;
    $payment->apitype = 'reversal';
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $api_url);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS,json_encode($payment));
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $response  = curl_exec($ch);
    curl_close($ch);
    var_dump($response);
  }

}
  
  