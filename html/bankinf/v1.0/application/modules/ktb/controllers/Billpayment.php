<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Billpayment extends MY_Controller {

  function __construct() 
  {
    parent::__construct();

    $this->load->library("nusoap_library"); //load the library here
    $obj_base_url = include(getcwd().'/../../../config/bankinf/base_url.php');

    $endPoint = $obj_base_url->base_url."/ktb/billpayment";
    // $endPoint = $obj_base_url->base_url."/callback/ktb/billpayment";
    $_SERVER['PHP_SELF'] = $endPoint;

    $this->nusoap_server = new soap_server();
    $this->nusoap_server->configureWSDL("Billpayment", "urn:Billpayment", $endPoint);
    $this->nusoap_server->wsdl->schemaTargetNamespace = 'urn:Billpayment';

    //DATA TYPES Inquiry
    $this->nusoap_server->wsdl->addComplexType(
      'InquiryRequestData',
      'complexType',
      'struct',
      'all',
      '',
      array(
        'user' => array('name' => 'user', 'type' => 'xsd:string'),
        'password' => array('name' => 'password', 'type' => 'xsd:string'), 
        'comcode' => array('name' => 'comcode', 'type' =>'xsd:string'), 
        'prodcode' => array('name' => 'prodcode', 'type' =>'xsd:string'), 
        'command' => array('name' => 'command', 'type' =>'xsd:string'), 
        'bankcode' => array('name' => 'bankcode', 'type' =>'xsd:decimal'), 
        'bankref' => array('name' => 'bankref', 'type' =>'xsd:string'), 
        'datetime' => array('name' => 'datetime', 'type' =>'xsd:string'), 
        'effdate' => array('name' => 'effdate', 'type' =>'xsd:string'), 
        'channel' => array('name' => 'channel', 'type' =>'xsd:string'), 
        'ref1' => array('name' => 'ref1', 'type' =>'xsd:string'), 
        'ref2' => array('name' => 'ref2', 'type' =>'xsd:string'), 
        'ref3' => array('name' => 'ref3', 'type' =>'xsd:string'), 
        'ref4' => array('name' => 'ref4', 'type' =>'xsd:string')
      )
    );

    $this->nusoap_server->wsdl->addComplexType(
      'InquiryResponseData',
      'complexType',
      'struct',
      'all',
      '',
      array(
        'tranxid' => array('name' => 'tranxid', 'type' => 'xsd:string'), 
        'bankref' => array('name' => 'bankref', 'type' => 'xsd:string'), 
        'respcode' => array('name' => 'respcode', 'type' => 'xsd:decimal'), 
        'respmsg' => array('name' => 'respmsg', 'type' => 'xsd:string'), 
        'balance' => array('name' => 'balance', 'type' => 'xsd:decimal'), 
        'cusname' => array('name' => 'cusname', 'type' => 'xsd:string'), 
        'print1' => array('name' => 'print1', 'type' => 'xsd:string'), 
        'print2' => array('name' => 'print2', 'type' => 'xsd:string'), 
        'print3' => array('name' => 'print3', 'type' => 'xsd:string'), 
        'print4' => array('name' => 'print4', 'type' => 'xsd:string'), 
        'print5' => array('name' => 'print5', 'type' => 'xsd:string'), 
        'print6' => array('name' => 'print6', 'type' => 'xsd:string'), 
        'print7' => array('name' => 'print7', 'type' => 'xsd:string')
      )
    );

    //DATA TYPES Approval
    $this->nusoap_server->wsdl->addComplexType(
      'ApprovalRequestData',
      'complexType',
      'struct',
      'sequence',
      '',
      array(
        'user' => array('name' => 'user', 'type' => 'xsd:string'),
        'password' => array('name' => 'password', 'type' => 'xsd:string'), 
        'comcode' => array('name' => 'comcode', 'type' =>'xsd:string'), 
        'prodcode' => array('name' => 'prodcode', 'type' =>'xsd:string'), 
        'command' => array('name' => 'command', 'type' =>'xsd:string'), 
        'bankcode' => array('name' => 'bankcode', 'type' =>'xsd:decimal'), 
        'bankref' => array('name' => 'bankref', 'type' =>'xsd:string'), 
        'datetime' => array('name' => 'datetime', 'type' =>'xsd:string'), 
        'effdate' => array('name' => 'effdate', 'type' =>'xsd:string'), 
        'amount' => array('name' => 'amount', 'type' =>'xsd:decimal'), 
        'channel' => array('name' => 'channel', 'type' =>'xsd:string'), 
        'cusname' => array('name' => 'cusname', 'type' =>'xsd:string'), 
        'ref1' => array('name' => 'ref1', 'type' =>'xsd:string'), 
        'ref2' => array('name' => 'ref2', 'type' =>'xsd:string'), 
        'ref3' => array('name' => 'ref3', 'type' =>'xsd:string'), 
        'ref4' => array('name' => 'ref4', 'type' =>'xsd:string')
      )
    );

    $this->nusoap_server->wsdl->addComplexType(
      'ApprovalResponseData',
      'complexType',
      'struct',
      'sequence',
      '',
      array(
        'tranxid' => array('name' => 'tranxid', 'type' => 'xsd:string'), 
        'bankref' => array('name' => 'bankref', 'type' => 'xsd:string'), 
        'respcode' => array('name' => 'respcode', 'type' => 'xsd:decimal'), 
        'respmsg' => array('name' => 'respmsg', 'type' => 'xsd:string'), 
        'balance' => array('name' => 'balance', 'type' => 'xsd:decimal'), 
        'cusname' => array('name' => 'cusname', 'type' => 'xsd:string'), 
        'info' => array('name' => 'info', 'type' => 'xsd:string'),
        'print1' => array('name' => 'print1', 'type' => 'xsd:string'), 
        'print2' => array('name' => 'print2', 'type' => 'xsd:string'), 
        'print3' => array('name' => 'print3', 'type' => 'xsd:string'), 
        'print4' => array('name' => 'print4', 'type' => 'xsd:string'), 
        'print5' => array('name' => 'print5', 'type' => 'xsd:string'), 
        'print6' => array('name' => 'print6', 'type' => 'xsd:string'), 
        'print7' => array('name' => 'print7', 'type' => 'xsd:string')
      )
    );

    //DATA TYPES Payment
    $this->nusoap_server->wsdl->addComplexType(
      'PaymentRequestData',
      'complexType',
      'struct',
      'sequence',
      '',
      array(
        'user' => array('name' => 'user', 'type' => 'xsd:string'),
        'password' => array('name' => 'password', 'type' => 'xsd:string'), 
        'comcode' => array('name' => 'comcode', 'type' =>'xsd:string'), 
        'prodcode' => array('name' => 'prodcode', 'type' =>'xsd:string'), 
        'command' => array('name' => 'command', 'type' =>'xsd:string'), 
        'bankcode' => array('name' => 'bankcode', 'type' =>'xsd:decimal'), 
        'bankref' => array('name' => 'bankref', 'type' =>'xsd:string'), 
        'tranxid' => array('name' => 'tranxid', 'type' =>'xsd:string'), 
        'datetime' => array('name' => 'datetime', 'type' =>'xsd:string'), 
        'effdate' => array('name' => 'effdate', 'type' =>'xsd:string'), 
        'amount' => array('name' => 'amount', 'type' =>'xsd:decimal'), 
        'channel' => array('name' => 'channel', 'type' =>'xsd:string'), 
        'cusname' => array('name' => 'cusname', 'type' =>'xsd:string'), 
        'ref1' => array('name' => 'ref1', 'type' =>'xsd:string'), 
        'ref2' => array('name' => 'ref2', 'type' =>'xsd:string'), 
        'ref3' => array('name' => 'ref3', 'type' =>'xsd:string'), 
        'ref4' => array('name' => 'ref4', 'type' =>'xsd:string')
      )
    );

    $this->nusoap_server->wsdl->addComplexType(
      'PaymentResponseData',
      'complexType',
      'struct',
      'sequence',
      '',
      array(
        'tranxid' => array('name' => 'tranxid', 'type' => 'xsd:string'), 
        'bankref' => array('name' => 'bankref', 'type' => 'xsd:string'), 
        'respcode' => array('name' => 'respcode', 'type' => 'xsd:decimal'), 
        'respmsg' => array('name' => 'respmsg', 'type' => 'xsd:string'), 
        'balance' => array('name' => 'balance', 'type' => 'xsd:decimal'), 
        'cusname' => array('name' => 'cusname', 'type' => 'xsd:string'), 
        'info' => array('name' => 'info', 'type' => 'xsd:string'),
        'print1' => array('name' => 'print1', 'type' => 'xsd:string'), 
        'print2' => array('name' => 'print2', 'type' => 'xsd:string'), 
        'print3' => array('name' => 'print3', 'type' => 'xsd:string'), 
        'print4' => array('name' => 'print4', 'type' => 'xsd:string'), 
        'print5' => array('name' => 'print5', 'type' => 'xsd:string'), 
        'print6' => array('name' => 'print6', 'type' => 'xsd:string'), 
        'print7' => array('name' => 'print7', 'type' => 'xsd:string')
      )
    );

    //DATA TYPES Approve Reversal
    $this->nusoap_server->wsdl->addComplexType(
      'ApproveReversalRequestData',
      'complexType',
      'struct',
      'sequence',
      '',
      array(
        'user' => array('name' => 'user', 'type' => 'xsd:string'),
        'password' => array('name' => 'password', 'type' => 'xsd:string'), 
        'comcode' => array('name' => 'comcode', 'type' =>'xsd:string'), 
        'prodcode' => array('name' => 'prodcode', 'type' =>'xsd:string'), 
        'command' => array('name' => 'command', 'type' =>'xsd:string'), 
        'bankcode' => array('name' => 'bankcode', 'type' =>'xsd:decimal'), 
        'bankref' => array('name' => 'bankref', 'type' =>'xsd:string'), 
        'tranxid' => array('name' => 'tranxid', 'type' =>'xsd:string'), 
        'datetime' => array('name' => 'datetime', 'type' =>'xsd:string'), 
        'effdate' => array('name' => 'effdate', 'type' =>'xsd:string'), 
        'amount' => array('name' => 'amount', 'type' =>'xsd:decimal'), 
        'channel' => array('name' => 'channel', 'type' =>'xsd:string'), 
        'cusname' => array('name' => 'cusname', 'type' =>'xsd:string'), 
        'ref1' => array('name' => 'ref1', 'type' =>'xsd:string'), 
        'ref2' => array('name' => 'ref2', 'type' =>'xsd:string'), 
        'ref3' => array('name' => 'ref3', 'type' =>'xsd:string'), 
        'ref4' => array('name' => 'ref4', 'type' =>'xsd:string')
      )
    );

    $this->nusoap_server->wsdl->addComplexType(
      'ApproveReversalResponseData',
      'complexType',
      'struct',
      'sequence',
      '',
      array(
        'tranxid' => array('name' => 'tranxid', 'type' => 'xsd:string'), 
        'bankref' => array('name' => 'bankref', 'type' => 'xsd:string'), 
        'respcode' => array('name' => 'respcode', 'type' => 'xsd:decimal'), 
        'respmsg' => array('name' => 'respmsg', 'type' => 'xsd:string')
      )
    );

    //DATA TYPES Reversal
    $this->nusoap_server->wsdl->addComplexType(
      'ReversalRequestData',
      'complexType',
      'struct',
      'sequence',
      '',
      array(
        'user' => array('name' => 'user', 'type' => 'xsd:string'),
        'password' => array('name' => 'password', 'type' => 'xsd:string'), 
        'comcode' => array('name' => 'comcode', 'type' =>'xsd:string'), 
        'prodcode' => array('name' => 'prodcode', 'type' =>'xsd:string'), 
        'command' => array('name' => 'command', 'type' =>'xsd:string'), 
        'bankcode' => array('name' => 'bankcode', 'type' =>'xsd:decimal'), 
        'bankref' => array('name' => 'bankref', 'type' =>'xsd:string'), 
        'tranxid' => array('name' => 'tranxid', 'type' =>'xsd:string'), 
        'datetime' => array('name' => 'datetime', 'type' =>'xsd:string'), 
        'effdate' => array('name' => 'effdate', 'type' =>'xsd:string'), 
        'amount' => array('name' => 'amount', 'type' =>'xsd:decimal'), 
        'channel' => array('name' => 'channel', 'type' =>'xsd:string'), 
        'cusname' => array('name' => 'cusname', 'type' =>'xsd:string'), 
        'ref1' => array('name' => 'ref1', 'type' =>'xsd:string'), 
        'ref2' => array('name' => 'ref2', 'type' =>'xsd:string'), 
        'ref3' => array('name' => 'ref3', 'type' =>'xsd:string'), 
        'ref4' => array('name' => 'ref4', 'type' =>'xsd:string')
      )
    );

    $this->nusoap_server->wsdl->addComplexType(
      'ReversalResponseData',
      'complexType',
      'struct',
      'sequence',
      '',
      array(
        'tranxid' => array('name' => 'tranxid', 'type' => 'xsd:string'), 
        'bankref' => array('name' => 'bankref', 'type' => 'xsd:string'), 
        'respcode' => array('name' => 'respcode', 'type' => 'xsd:decimal'), 
        'respmsg' => array('name' => 'respmsg', 'type' => 'xsd:string')
      )
    );


    $InquiryRequestData =  array(
      'user' => 'xsd:string',
      'password' => 'xsd:string', 
      'comcode' => 'xsd:string', 
      'prodcode' => 'xsd:string', 
      'command' => 'xsd:string', 
      'bankcode' => 'xsd:decimal', 
      'bankref' => 'xsd:string', 
      'datetime' => 'xsd:string', 
      'effdate' => 'xsd:string', 
      'channel' => 'xsd:string', 
      'ref1' => 'xsd:string', 
      'ref2' => 'xsd:string', 
      'ref3' => 'xsd:string', 
      'ref4' => 'xsd:string'
    );

    $InquiryResponseData =  array(
      'tranxid' => 'xsd:string', 
      'bankref' => 'xsd:string', 
      'respcode' => 'xsd:decimal', 
      'respmsg' => 'xsd:string', 
      'balance' => 'xsd:decimal', 
      'cusname' => 'xsd:string', 
      'print1' => 'xsd:string', 
      'print2' => 'xsd:string', 
      'print3' => 'xsd:string', 
      'print4' => 'xsd:string', 
      'print5' => 'xsd:string', 
      'print6' => 'xsd:string', 
      'print7' => 'xsd:string'
    );

    //REGISTRATION Inquiry
    $this->nusoap_server->register(
      'inquiry',
      array('request' => 'tns:InquiryRequestData'),  //parameters
      array('response' => 'tns:InquiryResponseData'),  //output
      'urn:Billpayment',   //namespace
      'urn:Billpayment#inquiry',  //soapaction
      'rpc', // style
      'encoded', // use
      'Inquiry Data' //description
    );

    //REGISTRATION Approval
    $this->nusoap_server->register(
      'approval',
      array('request' => 'tns:ApprovalRequestData'),  //parameters
      array('return' => 'tns:ApprovalResponseData'),  //output
      'urn:Billpayment',   //namespace
      'urn:Billpayment#approval',  //soapaction
      'rpc', // style
      'encoded', // use
      'Approval data' //description
    );

    //REGISTRATION Payment
    $this->nusoap_server->register(
      'payment',
      array('request' => 'tns:PaymentRequestData'),  //parameters
      array('return' => 'tns:PaymentResponseData'),  //output
      'urn:Billpayment',   //namespace
      'urn:Billpayment#payment',  //soapaction
      'rpc', // style
      'encoded', // use
      'Payment data' //description
    );

    //REGISTRATION Approve Reversal
    $this->nusoap_server->register(
      'approvereversal',
      array('request' => 'tns:ApproveReversalRequestData'),  //parameters
      array('return' => 'tns:ApproveReversalResponseData'),  //output
      'urn:Billpayment',   //namespace
      'urn:Billpayment#approvereversal',  //soapaction
      'rpc', // style
      'encoded', // use
      'Approval Reversal data' //description
    );

    //REGISTRATION Reversal
    $this->nusoap_server->register(
      'reversal',
      array('request' => 'tns:ReversalRequestData'),  //parameters
      array('return' => 'tns:ReversalResponseData'),  //output
      'urn:Billpayment',   //namespace
      'urn:Billpayment#reversal',  //soapaction
      'rpc', // style
      'encoded', // use
      'Reversal data' //description
    );

    //IMPLEMENTATION

    function validate($request){
      $user = 'mcktb2017';
      $password = 'M3d!4@C3NT3R';
      if($request['user'] == $user && $request['password'] = $password){
        return true;
      }else{
        return false;
      }
    }

    function inquiry($request){
      $ci =& get_instance();
      $ci->load->module('ktb/logpayment');
      $ref1 = isset($request['ref1']) ? $request['ref1'] : '';
      $ref2 = isset($request['ref2']) ? $request['ref2'] : '';
      $request_transid = isset($request['tranxid']) ? $request['tranxid'] : '';
      $bankref = isset($request['bankref']) ? $request['bankref'] : '';
      $response_transid = gen_response_trans_id();
      if($ref2== '00000'){
        $paymenttype = 'partner';
      }else{
        $paymenttype = 'member';
      }

      $insert_log = array(
        'api_type' => 'inquiry',
        'action' => 'process',
        'ref1' => $ref1,
        'ref2' => $ref2,
        'amount' => 0,
        'bankref'=> $bankref,
        'request_transid'=>$request_transid,
        'response_transid'=>$response_transid,
        'res_code'=>'',
        'res_message'=>'',
        'paymenttype'=>$paymenttype,
        'partnerpaymentid'=>'',
        'memberpaymentid'=>'',
        'postdata'=> json_encode($request),
        'returndata'=>'',
        'adddate' => date('Y-m-d'),
        'addtime' => date('H:i:s')
      );
      $logid = $ci->logpayment->insert_log($insert_log);

      $checkuser = validate($request);
      if($checkuser){
         $response = inquiry_agent_api($request, $response_transid);
       }else{
        $respcode = '101';
        $resmsg = get_res_msg($respcode);
        $response = array(
          'tranxid' => ' ', 
          'bankref' => $request['bankref'], 
          'respcode' => $respcode, 
          'respmsg' =>  $resmsg, 
          'balance' => '0.00', 
          'cusname' => ' ',
          'print1' => " ", 
          'print2' => ' ', 
          'print3' => ' ', 
          'print4' => ' ', 
          'print5' => ' ', 
          'print6' => ' ', 
          'print7' => ' '
        );
      }

      $update_log = array(
        'res_code'=>$response['respcode'],
        'res_message'=>$response['respmsg'],
        'returndata'=>json_encode($response),
      );
      $ci->logpayment->update_log_id($update_log, $logid);

      return $response;
    }

    function gen_response_trans_id(){
      $t = microtime(true);
      $micro = sprintf("%06d",($t - floor($t)) * 1000000);
      $d = new DateTime( date('Y-m-d H:i:s.'.$micro, $t) );
      $time = $d->format("u");
      $rand = rand(100,999);
      $response_transid = $d->getTimestamp().$time.$rand;
      return $response_transid;
    }

    function get_res_msg($respcode){
      $res_msg = array(
          '0' => 'Successful',
         '99' => 'Communication Error',
        '100' => 'Unable to process transaction',
        '101' => 'Invalid username/password',
        '102' => 'Require field or parameter is null',
        '103' => 'Transaction amount or number of transaction is over limit',
        '104' => 'Invalid reference',
        '105' => 'Invalid transaction number',
        '106' => 'Transaction number duplicate',
        '107' => 'Not business hour',
        '108' => 'Invalid price or amount',
        '999' => 'Other error'
      );
      return $res_msg[$respcode];
    }

    function inquiry_agent_api($request, $response_transid){
      $ci =& get_instance();
      $ci->load->module('common/qrypayment');
      $respcode = '0';
      $cusname = '';
      $ref1 = isset($request['ref1']) ? $request['ref1'] : '';
      $ref2 = isset($request['ref2']) ? $request['ref2'] : '';
      $request_transid = isset($request['tranxid']) ? $request['tranxid'] : '';
      $bankref = isset($request['bankref']) ? $request['bankref'] : '';

      if($ref2== '00000'){
        $paymenttype = 'partner';
      }else{
        $paymenttype = 'member';
      }

      //chk partner
      if($respcode=='0'){
        $partner = $ci->qrypayment->get_partner($ref1);
        if(!empty($partner)){
          $cusname = $partner->firstname.' '.$partner->lastname;
        }else{
          $resCode = '104';
        }
      }
        
      //chk member
      if($paymenttype == 'member' && $respcode == '0') {
        $member = $ci->qrypayment->get_member($ref1, $ref2);
        if(!empty($member)){
          $cusname = $member->firstname.' '.$member->lastname;
          /*
          $out_of_service = array(
            'AMO','PIS','ION','FTR','PIA','MTB','AMN','MCP',
            'OWS', 'PIA','WOL','MCP','ION','ION','SMA','AMN','FTR',
            'TMD', 'MCS',
            'TSE', 'NMC',
            'SEB', 'TSD'
          );

          if(in_array($member->membercode, $out_of_service)){
            if($member->membercode == 'SEB' && $member->memberid >= 2500){
              $respcode = '0';
            }else{
              $respcode = '104';
            }
          }
          */

        }else{
          $respcode = '104';
        }
      }

      $resmsg = get_res_msg($respcode);
      $response =  array(
        'tranxid' => $response_transid, 
        'bankref' => $bankref, 
        'respcode' => $respcode, 
        'respmsg' =>  $resmsg, 
        'cusname' => $cusname,
        'print1' => " ", 
        'print2' => ' ', 
        'print3' => ' ', 
        'print4' => ' ', 
        'print5' => ' ', 
        'print6' => ' ', 
        'print7' => ' '
      );
      return $response;
    }

    function approval($request){
      $ci =& get_instance();
      $ci->load->module('ktb/logpayment');
      $ref1 = isset($request['ref1']) ? $request['ref1'] : '';
      $ref2 = isset($request['ref2']) ? $request['ref2'] : '';
      $request_transid = isset($request['tranxid']) ? $request['tranxid'] : '';
      $amount = isset($request['amount']) ? $request['amount'] : '';
      $bankref = isset($request['bankref']) ? $request['bankref'] : '';
      $response_transid = gen_response_trans_id();
      if($ref2== '00000'){
        $paymenttype = 'partner';
      }else{
        $paymenttype = 'member';
      }

      $insert_log = array(
        'api_type' => 'approval',
        'action' => 'process',
        'ref1' => $ref1,
        'ref2' => $ref2,
        'amount' => $amount,
        'bankref'=> $bankref,
        'request_transid'=>$request_transid,
        'response_transid'=>$response_transid,
        'res_code'=>'',
        'res_message'=>'',
        'paymenttype'=>$paymenttype,
        'partnerpaymentid'=>'',
        'memberpaymentid'=>'',
        'postdata'=> json_encode($request),
        'returndata'=>'',
        'adddate' => date('Y-m-d'),
        'addtime' => date('H:i:s')
      );
      $logid = $ci->logpayment->insert_log($insert_log);

      $checkuser = validate($request);
      if($checkuser){
        $response = approval_agent_api($request, $response_transid);
       }else{
        $respcode = '101';
        $resmsg = get_res_msg($respcode);
        $response = array(
          'tranxid' => ' ', 
          'bankref' => $request['bankref'], 
          'respcode' => $respcode, 
          'respmsg' =>  $resmsg, 
          'balance' => '0.00', 
          'cusname' => ' ',
          'print1' => " ", 
          'print2' => ' ', 
          'print3' => ' ', 
          'print4' => ' ', 
          'print5' => ' ', 
          'print6' => ' ', 
          'print7' => ' '
        );
      }

      $update_log = array(
        'res_code'=>$response['respcode'],
        'res_message'=>$response['respmsg'],
        'returndata'=>json_encode($response),
      );
      $ci->logpayment->update_log_id($update_log, $logid);

      return $response;
    }

    function approval_agent_api($request, $response_transid){
      $ci =& get_instance();
      $ci->load->module('common/qrypayment');
      $respcode = '0';
      $cusname = '';
      $ref1 = isset($request['ref1']) ? $request['ref1'] : '';
      $ref2 = isset($request['ref2']) ? $request['ref2'] : '';
      $request_transid = isset($request['tranxid']) ? $request['tranxid'] : '';
      $amount = isset($request['amount']) ? $request['amount'] : '';
      $bankref = isset($request['bankref']) ? $request['bankref'] : '';

      if($ref2== '00000'){
        $paymenttype = 'partner';
      }else{
        $paymenttype = 'member';
      }

      //chk partner
      if($respcode=='0'){
        $partner = $ci->qrypayment->get_partner($ref1);
        if(!empty($partner)){
          $cusname = $partner->firstname.' '.$partner->lastname;
        }else{
          $resCode = '104';
        }
      }
        
      //chk member
      if($paymenttype == 'member' && $respcode == '0') {
        $member = $ci->qrypayment->get_member($ref1, $ref2);
        if(!empty($member)){
          $cusname = $member->firstname.' '.$member->lastname;
          /*
          $out_of_service = array(
            'AMO','PIS','ION','FTR','PIA','MTB','AMN','MCP',
            'OWS', 'PIA','WOL','MCP','ION','ION','SMA','AMN','FTR',
            'TMD', 'MCS',
            'TSE', 'NMC',
            'SEB', 'TSD'
          );

          if(in_array($member->membercode, $out_of_service)){
            if($member->membercode == 'SEB' && $member->memberid >= 2500){
              $respcode = '0';
            }else{
              $respcode = '104';
            }
          }
          */
        }else{
          $respcode = '104';
        }
      }

      if($respcode == '0'){
        if($amount>= 1000 && $amount <= 50000){
          $respcode = '0';
        }else{
          $respcode = '108';
        }
      }

      $resmsg = get_res_msg($respcode);
      $response =  array(
        'tranxid' => $response_transid, 
        'bankref' => $bankref, 
        'respcode' => $respcode, 
        'respmsg' => $resmsg, 
        'cusname' => $cusname,
        'print1' => " ", 
        'print2' => ' ', 
        'print3' => ' ', 
        'print4' => ' ', 
        'print5' => ' ', 
        'print6' => ' ', 
        'print7' => ' '
      );
      return $response;
    }

    function payment($request){
      $ci =& get_instance();
      $ci->load->module('ktb/logpayment');
      $ref1 = isset($request['ref1']) ? $request['ref1'] : '';
      $ref2 = isset($request['ref2']) ? $request['ref2'] : '';
      $request_transid = isset($request['tranxid']) ? $request['tranxid'] : '';
      $amount = isset($request['amount']) ? $request['amount'] : '';
      $bankref = isset($request['bankref']) ? $request['bankref'] : '';
      $response_transid = gen_response_trans_id();
      if($ref2== '00000'){
        $paymenttype = 'partner';
      }else{
        $paymenttype = 'member';
      }

      $insert_log = array(
        'api_type' => 'payment',
        'action' => 'process',
        'ref1' => $ref1,
        'ref2' => $ref2,
        'amount' => $amount,
        'bankref'=> $bankref,
        'request_transid'=>$request_transid,
        'response_transid'=>$response_transid,
        'res_code'=>'',
        'res_message'=>'',
        'paymenttype'=>$paymenttype,
        'partnerpaymentid'=>'',
        'memberpaymentid'=>'',
        'postdata'=> json_encode($request),
        'returndata'=>'',
        'adddate' => date('Y-m-d'),
        'addtime' => date('H:i:s')
      );
      $logid = $ci->logpayment->insert_log($insert_log);

      $checkuser = validate($request);
      if($checkuser){
        $response = payment_agent_api($request, $response_transid, $logid);
      }else{
        $respcode = '101';
        $resmsg = get_res_msg($respcode);
        $response = array(
          'tranxid' => ' ', 
          'bankref' => $request['bankref'], 
          'respcode' => $respcode, 
          'respmsg' =>  $resmsg, 
          'balance' => '0.00', 
          'cusname' => ' ', 
          'info' => ' ',
          'print1' => " ", 
          'print2' => ' ', 
          'print3' => ' ', 
          'print4' => ' ', 
          'print5' => ' ', 
          'print6' => ' ', 
          'print7' => ' '
        );
      }

      $update_log = array(
        'res_code'=>$response['respcode'],
        'res_message'=>$response['respmsg'],
        'returndata'=>json_encode($response),
      );
      $ci->logpayment->update_log_id($update_log, $logid);

      return $response;
    }

    function payment_agent_api($request, $response_transid, $logid){
      $ci =& get_instance();
      $ci->load->module('common/qrypayment');
      $ci->load->module('ktb/logpayment');
      $respcode = '0';
      $cusname = '';
      $ref1 = isset($request['ref1']) ? $request['ref1'] : '';
      $ref2 = isset($request['ref2']) ? $request['ref2'] : '';
      $request_transid = isset($request['tranxid']) ? $request['tranxid'] : '';
      $amount = isset($request['amount']) ? $request['amount'] : '0';
      $amount = number_format($amount,4,'.','');
      $bankref = isset($request['bankref']) ? $request['bankref'] : '';
      $detail = 'เติมเครดิตด้วย KTB BILL PAYMENT';
      $addby= 'KTB Bill Payment';
      $channel = 'อื่นๆ';
      $username = "KTBBillPayment";
      $bankcode = 'ktb';
      $wave_fee = false;
      $trans_log_id = 'KTB'.date('YmdHisu').rand(100,999);

      if($ref2== '00000'){
        $paymenttype = 'partner';
      }else{
        $paymenttype = 'member';
      }

      //chk partner
      if($respcode=='0'){
        $partner = $ci->qrypayment->get_partner($ref1);
        if(!empty($partner)){
          $cusname = $partner->firstname.' '.$partner->lastname;
        }else{
          $resCode = '104';
        }
      }
        
      //chk member
      if($paymenttype == 'member' && $respcode == '0') {
        $member = $ci->qrypayment->get_member($ref1, $ref2);
        if(!empty($member)){
          $cusname = $member->firstname.' '.$member->lastname;
        }else{
          $respcode = '104';
        }
      }

      //chk duplicate
      if($respcode = '0'){
        $logPayment = $ci->logpayment->get_log($request_transid, $ref1, $ref2, $api_type='payment', $action, $logid);
        if(!empty($logPayment)){
          $respcode = '106';
        }
      }

      //chk approval
      if($respcode = '0'){
        $logApproval = $ci->logpayment->get_log($request_transid, $ref1, $ref2, $api_type='approval', $action, $logid='');
        if(empty($logApproval)){
          $respcode = '105';
        }
      }

      if($respcode == '0'){
        if($amount>= 1000 && $amount <= 50000){
          $respcode = '0';
        }else{
          $respcode = '108';
        }
      }

      if($respcode == '0'){
        if($paymenttype == 'partner'){
          $res = $ci->qrypayment->payment_credit_partner($ref1, $amount, $partner, $detail, $addby, $channel, $bankcode, $wave_fee, $trans_log_id);
        }else{
          $res = $ci->qrypayment->payment_credit_member($ref1, $ref2, $amount, $member, $partner, $detail, $addby, $channel, $bankcode, $wave_fee, $trans_log_id);
        }
        if($res['error_code'] == 'E00'){
          $respcode = '0';
          $partnerpaymentid = isset($res['partnerpaymentid']) ? $res['partnerpaymentid'] : '';
          $memberpaymentid = isset($res['memberpaymentid']) ? $res['memberpaymentid'] : '';

          $update_log = array(
            'partnerpaymentid'=> $partnerpaymentid,
            'memberpaymentid'=> $memberpaymentid,
          );
          $ci->logpayment->update_log_id($update_log, $logid);
        }else{
          $respcode = '999';
        }
      }

      $resmsg = get_res_msg($respcode);
      $response =  array(
        'tranxid' => $response_transid, 
        'bankref' => $bankref, 
        'respcode' => $respcode, 
        'respmsg' => $resmsg, 
        'cusname' => $cusname,
        'print1' => " ", 
        'print2' => ' ', 
        'print3' => ' ', 
        'print4' => ' ', 
        'print5' => ' ', 
        'print6' => ' ', 
        'print7' => ' '
      );
      return $response;
    }

    function approvereversal($request){
      $ci =& get_instance();
      $ci->load->module('ktb/logpayment');
      $ref1 = isset($request['ref1']) ? $request['ref1'] : '';
      $ref2 = isset($request['ref2']) ? $request['ref2'] : '';
      $request_transid = isset($request['tranxid']) ? $request['tranxid'] : '';
      $amount = isset($request['amount']) ? $request['amount'] : '';
      $bankref = isset($request['bankref']) ? $request['bankref'] : '';
      $response_transid = gen_response_trans_id();
      if($ref2== '00000'){
        $paymenttype = 'partner';
      }else{
        $paymenttype = 'member';
      }

      $insert_log = array(
        'api_type' => 'approve-reversal',
        'action' => 'process',
        'ref1' => $ref1,
        'ref2' => $ref2,
        'amount' => $amount,
        'bankref'=> $bankref,
        'request_transid'=>$request_transid,
        'response_transid'=>$response_transid,
        'res_code'=>'',
        'res_message'=>'',
        'paymenttype'=>$paymenttype,
        'partnerpaymentid'=>'',
        'memberpaymentid'=>'',
        'postdata'=> json_encode($request),
        'returndata'=>'',
        'adddate' => date('Y-m-d'),
        'addtime' => date('H:i:s')
      );
      $logid = $ci->logpayment->insert_log($insert_log);

      $checkuser = validate($request);
      if($checkuser){
        $response = approvereversal_agent_api($request, $response_transid);
       }else{
        $respcode = '101';
        $resmsg = get_res_msg($respcode);
        $response = array(
          'tranxid' => ' ', 
          'bankref' => $request['bankref'], 
          'respcode' => $respcode, 
          'respmsg' =>  $resmsg
        );
      }

      $update_log = array(
        'res_code'=>$response['respcode'],
        'res_message'=>$response['respmsg'],
        'returndata'=>json_encode($response),
      );
      $ci->logpayment->update_log_id($update_log, $logid);

      return $response;
    }

    function approvereversal_agent_api($request, $response_transid){
      $ci =& get_instance();
      $ci->load->module('common/qrypayment');
      $ci->load->module('ktb/logpayment');
      $respcode = '0';
      $cusname = '';
      $ref1 = isset($request['ref1']) ? $request['ref1'] : '';
      $ref2 = isset($request['ref2']) ? $request['ref2'] : '';
      $request_transid = isset($request['tranxid']) ? $request['tranxid'] : '';
      $amount = isset($request['amount']) ? $request['amount'] : '0';
      $amount = number_format($amount,4,'.','');
      $bankref = isset($request['bankref']) ? $request['bankref'] : '';

      if($ref2== '00000'){
        $paymenttype = 'partner';
      }else{
        $paymenttype = 'member';
      }

      //chk partner
      if($respcode=='0'){
        $partner = $ci->qrypayment->get_partner($ref1);
        if(!empty($partner)){
          $cusname = $partner->firstname.' '.$partner->lastname;
        }else{
          $resCode = '104';
        }
      }
        
      //chk member
      if($paymenttype == 'member' && $respcode == '0') {
        $member = $ci->qrypayment->get_member($ref1, $ref2);
        if(!empty($member)){
          $cusname = $member->firstname.' '.$member->lastname;
        }else{
          $respcode = '104';
        }
      }

      //chk duplicate
      if($respcode = '0'){
        $where = array(
          'bankref' => $bankref, 
          'api_type' => 'reversal'
        );
        $logReversal = $ci->logpayment->get_log_where($where);
        if(!empty($logReversal)){
          $respcode = '106';
        }
      }

      //chk payment
      if($respcode = '0'){
        $where = array(
          'bankref' => $bankref, 
          'api_type' => 'payment'
        );
        $logPayment = $ci->logpayment->get_log_where($where);
        if(empty($logPayment)){
          $respcode = '105';
        }
      }

      $resmsg = get_res_msg($respcode);
      $response =  array(
        'tranxid' => $response_transid, 
        'bankref' => $bankref, 
        'respcode' => $respcode, 
        'respmsg' => $resmsg, 
        'cusname' => $cusname,
        'print1' => " ", 
        'print2' => ' ', 
        'print3' => ' ', 
        'print4' => ' ', 
        'print5' => ' ', 
        'print6' => ' ', 
        'print7' => ' '
      );
      return $response;
    }

    function reversal($request){
      $ci =& get_instance();
      $ci->load->module('ktb/logpayment');
      $ref1 = isset($request['ref1']) ? $request['ref1'] : '';
      $ref2 = isset($request['ref2']) ? $request['ref2'] : '';
      $request_transid = isset($request['tranxid']) ? $request['tranxid'] : '';
      $amount = isset($request['amount']) ? $request['amount'] : '0';
      $amount = number_format($amount,4,'.','');
      $bankref = isset($request['bankref']) ? $request['bankref'] : '';
      $response_transid = gen_response_trans_id();
      if($ref2== '00000'){
        $paymenttype = 'partner';
      }else{
        $paymenttype = 'member';
      }

      $insert_log = array(
        'api_type' => 'reversal',
        'action' => 'process',
        'ref1' => $ref1,
        'ref2' => $ref2,
        'amount' => $amount,
        'bankref'=> $bankref,
        'request_transid'=>$request_transid,
        'response_transid'=>$response_transid,
        'res_code'=>'',
        'res_message'=>'',
        'paymenttype'=>$paymenttype,
        'partnerpaymentid'=>'',
        'memberpaymentid'=>'',
        'postdata'=> json_encode($request),
        'returndata'=>'',
        'adddate' => date('Y-m-d'),
        'addtime' => date('H:i:s')
      );
      $logid = $ci->logpayment->insert_log($insert_log);

      $checkuser = validate($request);
      if($checkuser){
        $response = reversal_agent_api($request, $response_transid);
       }else{
        $respcode = '101';
        $resmsg = get_res_msg($respcode);
        $response = array(
          'tranxid' => ' ', 
          'bankref' => $request['bankref'], 
          'respcode' => $respcode, 
          'respmsg' =>   $resmsg
        );
      }

      $update_log = array(
        'res_code'=>$response['respcode'],
        'res_message'=>$response['respmsg'],
        'returndata'=>json_encode($response),
      );
      $ci->logpayment->update_log_id($update_log, $logid);

      return $response;
    }

    function reversal_agent_api($request, $response_transid){
      $ci =& get_instance();
      $ci->load->module('common/qrypayment');
      $ci->load->module('ktb/logpayment');
      $ci->load->module('common/alert_line');
      $ci->load->module('common/cancel_payment');
      $respcode = '0';
      $cusname = '';
      $ref1 = isset($request['ref1']) ? $request['ref1'] : '';
      $ref2 = isset($request['ref2']) ? $request['ref2'] : '';
      $request_transid = isset($request['tranxid']) ? $request['tranxid'] : '';
      $amount = isset($request['amount']) ? $request['amount'] : '0';
      $amount = number_format($amount,4,'.','');
      $bankref = isset($request['bankref']) ? $request['bankref'] : '';

      $addby= 'KTB Bill Payment';
      $username = "KTBBillPayment";
      $partnerpaymentid = '';
      $memberpaymentid = '';

      $status_cancel = "FAIL";
      $alert_message = "\r\nPayment Cancel: KTB\r\nRef1: $ref1\r\nRef2: $ref2\r\nAmount: $amount\r\nStatus Cancel: $status_cancel";

      if($ref2== '00000'){
        $paymenttype = 'partner';
      }else{
        $paymenttype = 'member';
      }

      //chk partner
      if($respcode=='0'){
        $partner = $ci->qrypayment->get_partner($ref1);
        if(!empty($partner)){
          $cusname = $partner->firstname.' '.$partner->lastname;
        }else{
          $resCode = '104';
        }
      }
        
      //chk member
      if($paymenttype == 'member' && $respcode == '0') {
        $member = $ci->qrypayment->get_member($ref1, $ref2);
        if(!empty($member)){
          $cusname = $member->firstname.' '.$member->lastname;
        }else{
          $respcode = '104';
        }
      }

      //chk duplicate
      if($respcode = '0'){
        $where = array(
          'bankref' => $bankref, 
          'request_transid' => $request_transid,
          'api_type' => 'reversal'
        );
        $logReversal = $ci->logpayment->get_log_where($where);
        if(!empty($logReversal)){
          $respcode = '106';
        }
      }

      //chk approval
      if($respcode = '0'){
        $where = array(
          'bankref' => $bankref, 
          'response_transid' => $request_transid,
          'api_type' => 'approve-reversal'
        );
        $logPayment = $ci->logpayment->get_log_where($where);
        if(empty($logPayment)){
          $respcode = '105';
        }
      }

      if($respcode == '0'){
        if($paymenttype == 'partner'){
          $paymentPartner = $ci->logpayment->get_log_join_payment_partner_by_bankref($bankref);
          if(!empty($paymentPartner)){
            $paymentPartner = (array)$paymentPartner;
            if($paymentPartner['amount'] != $amount){
              $respcode = '108';
            }else{
              $is_payment_v3 = 'Y';
              $partnerpaymentid = $paymentPartner['partnerpaymentid']; 
              $res = $ci->cancel_payment->cancel_payment_partner($partnerpaymentid, $amount, $addby, $partner, $is_payment_v3); 
              if($res['error_code'] == 'E00'){
                $partner_fee = $res['partner_fee'];
                $respcode = '0';
              }else{
                $respcode = '999';
              }
            }
          }else{
            $respcode = '100';
          }
        }else{
          $data = array(
            'fromcode' => $member->membercode, 
            'fromid' => $member->memberid,
          );
          $checkMerberV3  = $ci->payment_migrate->get_member_v3($data);
          if($checkMerberV3 != 'ERROR'){
            $paymentPartnerMember = $ci->logpayment->get_log_join_payment_partner_member_by_bankref($bankref);
            if(!empty($paymentPartnerMember)){
              $paymentPartnerMember = (array)$paymentPartnerMember;
              $memberpaymentid = $paymentPartnerMember['memberpaymentid'];
              $partnerpaymentid = $paymentPartnerMember['partnerpaymentid'];
              if($checkMerberV3['error_code'] == 'E00'){
                $is_payment_v3 = 'Y';
              }else{
                $is_payment_v3 = 'N';
              }

              if($paymentPartnerMember['amount'] != $amount){
                $respcode = '108';
              }else{
                $res = $ci->cancel_payment->cancel_payment_member($memberpaymentid, $amount, $addby, $member, $is_payment_v3);
                if($res['error_code'] == 'E00'){
                  $member_fee = $res['member_fee'];
                  $res = $ci->cancel_payment->cancel_payment_partner($partnerpaymentid, $amount, $addby, $partner, $is_payment_v3);
                  if($res['error_code'] == 'E00'){
                    $partner_fee = $res['partner_fee'];
                    $respcode = '0';
                  }else{
                     $respcode = '999';
                  }
                }else{
                  $respcode = '999';
                }
              }
            }
          }else{
            $respcode = '100';
          }
        }

        if($respcode == '0'){
          $status_cancel = "SUCCESS";
        }else{
          $respcode = '100';
        }
      }

      $alert_message = "\r\nPayment Cancel: KTB\r\nRef1: $ref1\r\nRef2: $ref2\r\nPartner Payment ID: $partnerpaymentid\r\nMember Payment ID: $memberpaymentid\r\nAmount: $amount\r\nStatus Cancel: $status_cancel";

      $ci->alert_line->notify_message($alert_message);

      $resmsg = get_res_msg($respcode);
      $response =  array(
        'tranxid' => $response_transid, 
        'bankref' => $bankref, 
        'respcode' => $respcode, 
        'respmsg' => $resmsg, 
        'cusname' => $cusname,
        'print1' => " ", 
        'print2' => ' ', 
        'print3' => ' ', 
        'print4' => ' ', 
        'print5' => ' ', 
        'print6' => ' ', 
        'print7' => ' '
      );
      return $response;
    }

    function curl($url, $data_json){
      $request = json_decode($data_json, true);
      $ch = curl_init();
      curl_setopt($ch, CURLOPT_URL, $url);
      curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
      curl_setopt($ch, CURLOPT_POST, 1);
      curl_setopt($ch, CURLOPT_POSTFIELDS,$data_json);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
      curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
      curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
      $response  = curl_exec($ch);
      if(curl_error($ch)){
        return get_return_error($request);
      }else{
        return json_decode($response, true);
      }
      curl_close($ch);
    }

    function get_return_error($request){
      $response = array(
        'respcode' => '99', 
        'respmsg' => 'Communication Error'
      );
      return $response;
    }

  }

  function index() {    
    $POST_DATA = isset($GLOBALS['HTTP_RAW_POST_DATA']) ? $GLOBALS['HTTP_RAW_POST_DATA'] : '';
    if(empty($POST_DATA)){
      $POST_DATA = (null != file_get_contents("php://input")) ? file_get_contents("php://input") : '';
    }
    $this->nusoap_server->service($POST_DATA); 
  }

  function test(){
    $payment = new stdClass();
    $payment->tranxid='16';
    $payment->user='user';
    $payment->password='password'; 
    $payment->comcode='1010'; 
    $payment->prodcode='1010'; 
    $payment->command='payment'; 
    $payment->bankcode='6'; 
    $payment->bankref='K00002 00004391'; 
    $payment->datetime=date('YmdHis'); 
    $payment->effdate=date('Ymd');
    $payment->amount='500.00'; 
    $payment->cusname=''; 
    $payment->channel='2'; 
    $payment->ref1='00004'; 
    $payment->ref2=''; 
    $payment->ref3='-'; 
    $payment->ref4='-';



    $obj_ows_ktb_api_url = include(getcwd().'/../../../config/bankinf/ows_ktb_api_url.php');
    $api_url = $obj_ows_ktb_api_url->url;
    $payment->apitype = 'reversal';
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $api_url);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS,json_encode($payment));
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $response  = curl_exec($ch);
    curl_close($ch);
    var_dump($response);
  }

}
  
  