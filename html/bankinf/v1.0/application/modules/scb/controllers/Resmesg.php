<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Resmesg extends MY_Controller {

  function __construct()
  {
    parent::__construct();
  }

  function get_response_mesg($resCode){
    $resMesg['00'] = 'success';
    $resMesg['11'] = 'Invalid reference1';
    $resMesg['12'] = 'Invalid reference2';
    $resMesg['13'] = 'Invalid reference3';
    $resMesg['14'] = 'Invalid amount';
    $resMesg['21'] = 'Duplicate transaction';
    $resMesg['99'] = 'Other Error';
    return $resMesg[$resCode];
  }


}
