<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Notifypayment extends MY_Controller {

  function __construct()
  {
    parent::__construct();
    $this->load->module('scb/resmesg');
    $this->load->module('common/qrypayment');
    $this->load->module('scb/logpayment');
    $this->load->module('common/payment_migrate');
    $scb_qr_config = include(getcwd().'/../../../config/bankinf/scbqr_config.php');
    $this->scb_qr_config = (array)$scb_qr_config;
  }

  function index(){
    $postdata = file_get_contents("php://input");
    $post = json_decode($postdata);
    $body = (array)$post;

    $resCode = '00';

    $TRX_REF = isset($body['transactionId']) ? $body['transactionId'] : '';
    $AMOUNT = isset($body['amount']) ? $body['amount'] : 0;
    $FROM_ACC_NAME = isset($body['payerName']) ? $body['payerName'] : '';
    $FROM_ACC_NO = isset($body['payerAccountNumber']) ? $body['payerAccountNumber'] : '';
    $FROM_BANK_CODE = isset($body['sendingBankCode']) ? $body['sendingBankCode'] : '';
    $TRX_DATE_TIME = isset($body['transactionDateandTime']) ? $body['transactionDateandTime'] : '';

    $REF1_LOG = isset($body['billPaymentRef1']) ? $body['billPaymentRef1'] : '';
    $REF2_LOG = isset($body['billPaymentRef2']) ? $body['billPaymentRef2'] : '';
    $REF3_LOG = isset($body['billPaymentRef3']) ? $body['billPaymentRef3'] : '';

    $REF1 = substr($REF1_LOG, 14);
    $REF2 = isset($body['billPaymentRef2']) ? $body['billPaymentRef2'] : '';
    $REF3 = isset($body['billPaymentRef3']) ? $body['billPaymentRef3'] : '';

    $amount = $AMOUNT;
    $ref1 = !empty($REF1) ? $REF1 : '999999';
    $ref2 = !empty($REF2) ? $REF2 : '999999';
    $transid = $TRX_REF;
    $api_type = $this->qrypayment->get_api_type($ref1, $ref2);
    $detail = 'เติมเครดิตด้วย SCBQR BILL PAYMENT';
    $addby='SCBQR Bill Payment';
    $channel = 'อื่นๆ';
    $username = "SCBQRBillPayment";
    $bankcode = 'scbqr';
    $action = 'notify';
    $powerbankFlag = false;
    $wave_fee = false;
    $smartapi = false;
    $trans_log_id = 'SCBQR'.date('YmdHisu').rand(100,99);

    if($ref1 == "100001"){
      $list_get_qr_powerbank = $this->payment_migrate->get_list_get_qr_powerbank($ref1, $ref2, $trans_log_id);
      if($list_get_qr_powerbank){
        if($list_get_qr_powerbank['error_code'] == 'E00'){
          $ref1 = $list_get_qr_powerbank['list_get_qr_powerbank']['ca_ref1'];
          $ref2 = $list_get_qr_powerbank['list_get_qr_powerbank']['ca_ref2'];
          $powerbankFlag = true;
          $wave_fee =  true;
        }
      }
    } 
    else if($ref1 == "100002"){
      $list_get_qr_powerbank = $this->payment_migrate->create_new_ca_from_bankinf($REF2, $FROM_ACC_NAME, $FROM_BANK_CODE, $trans_log_id);
      if($list_get_qr_powerbank){
        if($list_get_qr_powerbank['error_code'] == 'E00'){
          $ref1 = $list_get_qr_powerbank['list_get_qr_powerbank']['ca_ref1'];
          $ref2 = $list_get_qr_powerbank['list_get_qr_powerbank']['ca_ref2'];
          $powerbankFlag = true;
          $wave_fee =  true;
        }
        else{
          $ref1 = $list_get_qr_powerbank['ca_ref1'];
          $ref2 = $list_get_qr_powerbank['ca_ref2'];
          $wave_fee =  true;
        }
      }
    } 
    else if($ref1 == "100003" || $ref1 == "100004" || $ref1 == "100005" || $ref1 == "100006" || $ref1 == "100007"){
      // 100003 = smartwash
      // 100004 = smartscale
      if($ref1 == '100003'){
        $smartname = "Smart Wash";
      }else if($ref1 == "100004"){
        $smartname = "Smart BMI";
      }else if($ref1 == "100005"){
        $smartname = "Smart Coin";
      }else if($ref1 == "100006"){
        $smartname = "Smart Withdraw";
      }else if($ref1 == "100007"){
        $smartname = "Smart Game";
      }else{
        $smartname = "Smarts NULL";
      }
      $ca_detail = $this->payment_migrate->create_ca_by_ref2($REF2, $FROM_ACC_NAME, $FROM_BANK_CODE, $trans_log_id);

      if($ca_detail){
        $ref1 = $ca_detail['ca_ref1'];
        $ref2 = $ca_detail['ca_ref2'];
        $smart_type = $REF1;
        $smart_ref1 = substr($REF2, 0, 5).'-'.substr($REF2, 5, 5);
        $smart_ref2 = sprintf("%05d",$ref1).'-'.sprintf("%05d",$ref2);
        if($REF1 == "100007"){
          $smart_ref2.= '-'.substr($REF2, 10, 5);
        }
        $smartapi = true;
        $wave_fee =  true;
      }
    }

    if($ref2 == '00000'){
      $paymenttype = 'partner';
    }else{
      $paymenttype = 'member';
    }

    $insert_log = array(
      'api_type' => $api_type,
      'action' => $action,
      'ref1' => $REF1,
      'ref2' => $REF2,
      'ref3' => $REF3,
      'request_ref1' => $REF1_LOG,
      'request_ref2' => $REF2_LOG,
      'request_ref3' => $REF3_LOG,
      'from_acc_name' => $FROM_ACC_NAME,
      'from_acc_no' => $FROM_ACC_NO,
      'amount' => number_format($amount,4,'.',''),
      'paymenttype' => $paymenttype,
      'status' => 0,
      'request_transid' => $transid,
      'postdata' => json_encode($body),
      'adddate' => date('Y-m-d'),
      'addtime' => date('H:i:s'),
      'ip' => $this->get_client_ip()
    );
    $logid = $this->logpayment->insert_log($insert_log);


    if($resCode == '00'){
      $logconfirm = $this->logpayment->get_log($transid, $REF1, $REF2, $api_type, $action, $logid);
      if(empty($logconfirm)){
        $resCode = '00';
      }else{
        $resCode = '21';
      }
    }

    if($resCode == '00'){
      $partner = $this->qrypayment->get_partner($ref1);
      if(!empty($partner)){
        $resCode = '00';
      }else{
        $resCode = '11';
      }
    }

    if($resCode == '00' && $paymenttype == 'member'){
      $member = $this->qrypayment->get_member($ref1, $ref2);
      if(!empty($member)){
        $resCode = '00';
      }else{
        $resCode = '12';
      }
    }

    if($resCode == '00' && $api_type != 'loan'){
      if($paymenttype == 'partner'){
        $res = $this->qrypayment->payment_credit_partner($ref1, $amount, $partner, $detail, $addby, $channel, $bankcode, $wave_fee, $trans_log_id);
      }else{
        $res = $this->qrypayment->payment_credit_member($ref1, $ref2, $amount, $member, $partner, $detail, $addby, $channel, $bankcode, $wave_fee, $trans_log_id);
      }
      if($res['error_code'] == 'E00'){
        $resCode = '00';
        $partnerpaymentid = isset($res['partnerpaymentid']) ? $res['partnerpaymentid'] : '';
        $memberpaymentid = isset($res['memberpaymentid']) ? $res['memberpaymentid'] : '';
        if($powerbankFlag){
          $payment_type = $list_get_qr_powerbank['list_get_qr_powerbank']['payment_type'];
          $order_id = $list_get_qr_powerbank['list_get_qr_powerbank']['id'];
          $device_id = $list_get_qr_powerbank['list_get_qr_powerbank']['device_id'];
          $pooup_result = $this->payment_migrate->pop_powerbank_from_bankinf($payment_type, $order_id, $trans_log_id);
          $this->load->module('common/alert_line');
          $alert_message = "\r\nService: Powerbank ($addby)";
          $alert_message.= "\r\nAmount: $amount";
          $alert_message.= "\r\nDevice ID: $device_id";
          $alert_message.= "\r\nRef1: ".sprintf("%05d",$ref1);
          $alert_message.= "\r\nRef2: ".sprintf("%05d",$ref2);
          $alert_message.= "\r\nResult Code: ".$pooup_result['error_code'];
          $alert_message.= "\r\nResult Text: ".$pooup_result['error_text'];
          $this->alert_line->notify_message_powerbank($alert_message);
        }
        else if($smartapi){
          $smartapiRes = $this->payment_migrate->smartapi_by_ref1_ref2($smart_type, $smart_ref1, $smart_ref2, $amount, $trans_log_id);
          $this->load->module('common/alert_line');
          $alert_message = "\r\nService: $smartname ($addby)";
          $alert_message.= "\r\nAmount: $amount";
          $alert_message.= "\r\nDevice ID: $smart_ref1";
          $alert_message.= "\r\nRef1: ".sprintf("%05d",$ref1);
          $alert_message.= "\r\nRef2: ".sprintf("%05d",$ref2);
          $alert_message.= "\r\nResult Code: ".$smartapiRes['error_code'];
          $alert_message.= "\r\nResult Text: ".$smartapiRes['error_text'];
          $this->alert_line->notify_message_powerbank($alert_message);
        }
        else{
          if($ref1 == '00102'){
            $payment_doll_result = $this->payment_migrate->payment_doll($ref1, $ref2, $amount);
          }
        } 
      }else{
        $resCode = '99';
      }
    }

    if($resCode == '00' && $api_type == 'loan' && $paymenttype == 'member'){
      $kioskcode = "K".$member->membercode;
      $kioskid = $ref2;
      $res = $this->qrypayment->inquiry_loan($kioskcode, $kioskid);
      if($res['error_code'] == 'E00'){
        $resCode = '00';
        $loan_amount = $res['loan_detail']['pay_per_month'];
        $this->load->module('common/payment');
        $member_fee = $this->payment->get_member_fee($bankcode, $ref1, $amount);
        $loan_amount = $loan_amount+$member_fee;
        if($amount < $loan_amount){
          $resCode = '14';
        }else{
          $res = $this->qrypayment->payment_credit_member($ref1, $ref2, $amount, $member, $partner, $detail, $addby, $channel, $bankcode);
          if($res['error_code'] == 'E00'){
            $resCode = '00';
            $partnerpaymentid = $res['partnerpaymentid'];
            $memberpaymentid = $res['memberpaymentid'];
            $res = $this->qrypayment->pay_loan($kioskcode, $kioskid, $username);
            if($res['error_code'] == 'E00'){
              $resCode = '00';
            }else{
              $resCode = '00';
            }
          }else{
            $resCode = '99';
          }
        }
      }else if($res['error_code'] == 'E15'){
        $resCode = '99';
      }else{
        $resCode = '99';
      }
    }

    
    $RESPONSE_CODE = '00';
    $RESPONSE_MESSAGE = $this->resmesg->get_response_mesg($RESPONSE_CODE);

    $response = array(
      'resCode' => $RESPONSE_CODE,
      'resDesc' => $RESPONSE_MESSAGE,
      'transactionId' => strtoupper($transid),
      'confirmId' => $REF1_LOG
    );

    if($resCode == '00'){
      $status = 1;
    }else{
      $status = 2;
    }

    if($status == 2 && ($smartapi==true or $powerbankFlag==true)){
      $this->load->module('common/alert_line');
      if($powerbankFlag){
        $device_id = isset($list_get_qr_powerbank['list_get_qr_powerbank']['device_id']) ? $list_get_qr_powerbank['list_get_qr_powerbank']['device_id'] : '';
      }else{
        $device_id = isset($smart_ref1) ? $smart_ref1 : '';
      }
      $alert_message = "\r\nService: $smartname ($addby) ไม่สำเร็จ!";
      $alert_message.= "\r\nAmount: $amount";
      $alert_message.= "\r\nDevice ID: $device_id";
      $alert_message.= "\r\nRef1: ".sprintf("%05d",$ref1);
      $alert_message.= "\r\nRef2: ".sprintf("%05d",$ref2);
      $this->alert_line->notify_message_powerbank($alert_message);
    }
    else if($status == 2){
      if($api_type == 'loan'){
        $typeAlert = " LOAN";
      }else{
        $typeAlert = "";
      }
      $this->load->module('common/alert_line');
      $alert_message = "\r\nService : $addby\r\nRef1: $ref1\r\nRef2: $ref2\r\nRef3: $REF3\r\nAmount: $amount\r\nStatus :$typeAlert FAIL";
      $this->alert_line->notify_message($alert_message);
    }

    $update_log = array(
      'partnerpaymentid' => isset($partnerpaymentid) ? $partnerpaymentid : NULL,
      'memberpaymentid' => isset($memberpaymentid) ? $memberpaymentid : NULL,
      'response_transid' => strtoupper($transid),
      'res_code' => $resCode,
      'res_message' => $this->resmesg->get_response_mesg($resCode),
      'status' => $status,
      'returndata' => json_encode($response),
      'returndate' => date('Y-m-d'),
      'returntime' => date('H:i:s')
    );
    $this->logpayment->update_log_id($update_log, $logid);

    header('Content-Type: application/json');
    echo json_encode($response);
  }

  function get_client_ip() {
    $ipaddress = '';
    if (getenv('HTTP_CLIENT_IP'))
        $ipaddress = getenv('HTTP_CLIENT_IP');
    else if(getenv('HTTP_X_FORWARDED_FOR'))
        $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
    else if(getenv('HTTP_X_FORWARDED'))
        $ipaddress = getenv('HTTP_X_FORWARDED');
    else if(getenv('HTTP_FORWARDED_FOR'))
        $ipaddress = getenv('HTTP_FORWARDED_FOR');
    else if(getenv('HTTP_FORWARDED'))
       $ipaddress = getenv('HTTP_FORWARDED');
    else if(getenv('REMOTE_ADDR'))
        $ipaddress = getenv('REMOTE_ADDR');
    else
        $ipaddress = 'UNKNOWN';
    return $ipaddress;
  }
}