<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Generateqrcode extends MY_Controller {

  function __construct()
  {
    parent::__construct();
    $this->load->module('scb/resmesg');
    $this->load->module('common/qrypayment');
    $this->load->module('scb/logpayment');
    $this->load->module('common/payment_migrate');
    $this->load->module('common/generateuuidv4');
    $this->load->library('errorcode');
    $scb_qr_config = include(getcwd().'/../../../config/bankinf/scbqr_config.php');
    $this->scb_qr_config = (array)$scb_qr_config;
  }

  function call($ref1, $ref2, $ref3, $amount, $merchant_name=''){
    $requestUID = $this->generateuuidv4->gen_uuid_v4();
    $transid = $requestUID;
    $resourceOwnerID = !empty($merchant_name) ? $merchant_name : $this->scb_qr_config['merchant_name'];
    $apikey = $this->scb_qr_config['apikey'];
    $apisecret = $this->scb_qr_config['apisecret'];
    $amount = number_format($amount,2,'.','');
    $QRCode = '';
    $body = array(
      "response_type"=> "DATA",
      "biller_id"=> $this->scb_qr_config['biller_id'], 
      "merchant_name"=> $merchant_name, 
      "promptpay_type" => "BILLERID", 
      "amount"=> $amount,
      "ref_1"=> $ref1,
      "ref_2"=> $ref2,
      "ref_3"=> $ref3
    );

    $ch = curl_init($this->scb_qr_config['url_gen_qr']);                                                                      
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($body));
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
    curl_setopt($ch, CURLOPT_TIMEOUT, 30);
    curl_setopt($ch, CURLINFO_HEADER_OUT, true);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
     'Content-Type: application/json',
     'apikey: '.$apikey,
     'apisecret: '.$apisecret,
     'requestUID: '.$requestUID,
     'resourceOwnerID: '.$resourceOwnerID,
    ));

    $insert_log = array(
      'api_type' => "REQUEST",
      'action' => "Generateqrcode",
      'ref1' => $ref1,
      'ref2' => $ref2,
      'ref3' => $ref3,
      'amount' => $amount,
      'request_transid' => $transid,
      'postdata' => json_encode($body),
      'adddate' => date('Y-m-d'),
      'addtime' => date('H:i:s')
    );
    $logid = $this->logpayment->insert_log($insert_log);

    $content = curl_exec($ch);
    // $information = curl_getinfo($ch);
    // var_dump($information);

    if($content){
      $return = json_decode($content, true);
      $resCode = isset($return['code']) ? $return['code'] : '99';
      if($resCode == '00'){
        $status = 1;
        $QRCode = isset($return['data']['qr_image']) ? 'data:image/png;base64,'.$return['data']['qr_image'] : '';
        $error_code = 'E00';
      }else{
        $error_code = 'E64';
        $status = 2;
      }
      $broker_error = isset($return['code']) ? $return['code'] : '';

    }else{
      $content = "Curl Error: ".curl_error($ch);
      $status = 2;
      $resCode = '99';
      $error_code = 'E14';
    }

    $update_log = array(
      'data' => isset($data) ? $data : '',
      'res_code' => $resCode,
      'res_message' => $this->resmesg->get_response_mesg($resCode),
      'status' => $status,
      'returndata' => $content,
      'returndate' => date('Y-m-d'),
      'returntime' => date('H:i:s')
    );
    $this->logpayment->update_log_id($update_log, $logid);

    return (object) array(
      'error_code' => $error_code,
      'error_text' => $this->errorcode->$error_code,
      'qr_image' => $QRCode,
      'broker_error' => isset($broker_error) ? $broker_error : ''
    );
  }
}