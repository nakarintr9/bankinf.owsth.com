<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Notifypayment extends MY_Controller {

  function __construct()
  {
    parent::__construct();
    $this->load->module('thanachart/resmesg');
    $this->load->module('common/qrypayment');
    $this->load->module('thanachart/logpayment');
    $this->load->module('common/payment_migrate');
    $thanachart_config = include(getcwd().'/../../../config/bankinf/thanachart_config.php');
    $this->thanachart_config = (array)$thanachart_config;
  }

  function index(){
    $postdata = file_get_contents("php://input");
    $post = json_decode($postdata);
    $body = (array)$post;

    $SECRETEKEY = $this->thanachart_config['SECRETEKEY'];
    $TRX_REF = isset($body['TRX_REF']) ? $body['TRX_REF'] : '';
    $TO_PROXY_TYPE = isset($body['TO_PROXY_TYPE']) ? $body['TO_PROXY_TYPE'] : '';
    $TO_PROXY_ID = isset($body['TO_PROXY_ID']) ? $body['TO_PROXY_ID'] : '';
    $AMOUNT = isset($body['AMOUNT']) ? $body['AMOUNT'] : '';
    $FROM_ACC_NAME = isset($body['FROM_ACC_NAME']) ? $body['FROM_ACC_NAME'] : '';
    $FROM_BANK_CODE = isset($body['FROM_BANK_CODE']) ? $body['FROM_BANK_CODE'] : '';
    $REF1 = isset($body['REF1']) ? $body['REF1'] : '';
    $REF2 = isset($body['REF2']) ? $body['REF2'] : '';
    $TERMINAL_ID = isset($body['TERMINAL_ID']) ? $body['TERMINAL_ID'] : '';
    $TRX_DATE_TIME = isset($body['TRX_DATE_TIME']) ? $body['TRX_DATE_TIME'] : '';
    $CHECK_SUM = isset($body['CHECK_SUM']) ? $body['CHECK_SUM'] : '';

    $resCode = '000';
    $amount = intval($AMOUNT) / 100;
    $ref1 = $REF1;
    $ref2 = $REF2;
    $transid = $TRX_REF;
    $api_type = $this->qrypayment->get_api_type($ref1, $ref2);
    $detail = 'เติมเครดิตด้วย Thanachart BILL PAYMENT';
    $addby='Thanachart Bill Payment';
    $channel = 'อื่นๆ';
    $username = "ThanachartBillPayment";
    $bankcode = 'thanachart';
    $action = 'notify';
    $powerbankFlag = false;
    $wave_fee = false;
    $smartapi = false;
    $trans_log_id = 'TBank'.date('YmdHisu').rand(100,999);

    if($ref1 == "100001"){
      $list_get_qr_powerbank = $this->payment_migrate->get_list_get_qr_powerbank($ref1, $ref2, $trans_log_id);
      if($list_get_qr_powerbank){
        if($list_get_qr_powerbank['error_code'] == 'E00'){
          $ref1 = $list_get_qr_powerbank['list_get_qr_powerbank']['ca_ref1'];
          $ref2 = $list_get_qr_powerbank['list_get_qr_powerbank']['ca_ref2'];
          $powerbankFlag = true;
          $wave_fee =  true;
        }
      }
    } 
    else if($ref1 == "100002"){
      $list_get_qr_powerbank = $this->payment_migrate->create_new_ca_from_bankinf($REF2, $FROM_ACC_NAME, $FROM_BANK_CODE, $trans_log_id);
      if($list_get_qr_powerbank){
        if($list_get_qr_powerbank['error_code'] == 'E00'){
          $ref1 = $list_get_qr_powerbank['list_get_qr_powerbank']['ca_ref1'];
          $ref2 = $list_get_qr_powerbank['list_get_qr_powerbank']['ca_ref2'];
          $powerbankFlag = true;
          $wave_fee =  true;
        }
        else{
          $ref1 = $list_get_qr_powerbank['ca_ref1'];
          $ref2 = $list_get_qr_powerbank['ca_ref2'];
          $wave_fee =  true;
        }
      }
    } 
    else if($ref1 == "100003" || $ref1 == "100004" || $ref1 == "100005" || $ref1 == "100006" || $ref1 == "100007"){
      // 100003 = smartwash
      // 100004 = smartscale
      if($ref1 == '100003'){
        $smartname = "Smart Wash";
      }else if($ref1 == "100004"){
        $smartname = "Smart BMI";
      }else if($ref1 == "100005"){
        $smartname = "Smart Coin";
      }else if($ref1 == "100006"){
        $smartname = "Smart Withdraw";
      }else if($ref1 == "100007"){
        $smartname = "Smart Game";
      }else{
        $smartname = "Smarts NULL";
      }
      $ca_detail = $this->payment_migrate->create_ca_by_ref2($REF2, $FROM_ACC_NAME, $FROM_BANK_CODE, $trans_log_id);

      if($ca_detail){
        $ref1 = $ca_detail['ca_ref1'];
        $ref2 = $ca_detail['ca_ref2'];
        $smart_type = $REF1;
        $smart_ref1 = substr($REF2, 0, 5).'-'.substr($REF2, 5, 5);
        $smart_ref2 = sprintf("%05d",$ref1).'-'.sprintf("%05d",$ref2);
        if($REF1 == "100007"){
          $smart_ref2.= '-'.substr($REF2, 10, 5);
        }
        $smartapi = true;
        $wave_fee =  true;
      }
    }

    if($ref2 == '00000'){
      $paymenttype = 'partner';
    }else{
      $paymenttype = 'member';
    }

    $insert_log = array(
      'api_type' => $api_type,
      'action' => $action,
      'ref1' => $ref1,
      'ref2' => $ref2,
      'amount' => number_format($amount,4,'.',''),
      'min_amount' => number_format($amount,4,'.',''),
      'max_amount' => number_format($amount,4,'.',''),
      'paymenttype' => $paymenttype,
      'status' => 0,
      'request_transid' => $transid,
      'postdata' => json_encode($body),
      'adddate' => date('Y-m-d'),
      'addtime' => date('H:i:s')
    );
    $logid = $this->logpayment->insert_log($insert_log);

    $CONCAT = "";
    $CONCAT.= $TRX_REF;
    $CONCAT.= $TO_PROXY_TYPE;
    $CONCAT.= $TO_PROXY_ID;
    $CONCAT.= $AMOUNT;
    $CONCAT.= $FROM_ACC_NAME;
    $CONCAT.= $FROM_BANK_CODE;
    $CONCAT.= $REF1;
    $CONCAT.= $REF2;
    $CONCAT.= $TERMINAL_ID;
    $CONCAT.= $TRX_DATE_TIME;

    $CHECK_SUM_REQUEST = md5($CONCAT.$SECRETEKEY);

    if($CHECK_SUM == $CHECK_SUM_REQUEST){
      $resCode = '000';
    }else{
      $resCode = '001';
    }

    if($resCode == '000'){
      $logconfirm = $this->logpayment->get_log($transid, $ref1, $ref2, $api_type, $action, $logid);
      if(empty($logconfirm)){
        $resCode = '000';
      }else{
        $resCode = '998';
      }
    }

    if($resCode == '000'){
      $partner = $this->qrypayment->get_partner($ref1);
      if(!empty($partner)){
        $resCode = '000';
      }else{
        $resCode = '004';
      }
    }

    if($resCode == '000' && $paymenttype == 'member'){
      $member = $this->qrypayment->get_member($ref1, $ref2);
      if(!empty($member)){
        $resCode = '000';
      }else{
        $resCode = '004';
      }
    }

    if($resCode == '000' && $api_type != 'loan'){
      if($paymenttype == 'partner'){
        $res = $this->qrypayment->payment_credit_partner($ref1, $amount, $partner, $detail, $addby, $channel, $bankcode, $wave_fee, $trans_log_id);
      }else{
        $res = $this->qrypayment->payment_credit_member($ref1, $ref2, $amount, $member, $partner, $detail, $addby, $channel, $bankcode, $wave_fee, $trans_log_id);
      }
      if($res['error_code'] == 'E00'){
        $resCode = '000';
        $partnerpaymentid = isset($res['partnerpaymentid']) ? $res['partnerpaymentid'] : '';
        $memberpaymentid = isset($res['memberpaymentid']) ? $res['memberpaymentid'] : '';
        if($powerbankFlag){
          $payment_type = $list_get_qr_powerbank['list_get_qr_powerbank']['payment_type'];
          $order_id = $list_get_qr_powerbank['list_get_qr_powerbank']['id'];
          $device_id = $list_get_qr_powerbank['list_get_qr_powerbank']['device_id'];
          $pooup_result = $this->payment_migrate->pop_powerbank_from_bankinf($payment_type, $order_id, $trans_log_id);
          $this->load->module('common/alert_line');
          $alert_message = "\r\nService: Powerbank (QR-Payment)";
          $alert_message.= "\r\nAmount: $amount";
          $alert_message.= "\r\nDevice ID: $device_id";
          $alert_message.= "\r\nRef1: ".sprintf("%05d",$ref1);
          $alert_message.= "\r\nRef2: ".sprintf("%05d",$ref2);
          $alert_message.= "\r\nResult Code: ".$pooup_result['error_code'];
          $alert_message.= "\r\nResult Text: ".$pooup_result['error_text'];
          $this->alert_line->notify_message_powerbank($alert_message);
        }
        else if($smartapi){
          $smartapiRes = $this->payment_migrate->smartapi_by_ref1_ref2($smart_type, $smart_ref1, $smart_ref2, $amount, $trans_log_id);
          $this->load->module('common/alert_line');
          $alert_message = "\r\nService: $smartname (QR-Payment)";
          $alert_message.= "\r\nAmount: $amount";
          $alert_message.= "\r\nDevice ID: $smart_ref1";
          $alert_message.= "\r\nRef1: ".sprintf("%05d",$ref1);
          $alert_message.= "\r\nRef2: ".sprintf("%05d",$ref2);
          $alert_message.= "\r\nResult Code: ".$smartapiRes['error_code'];
          $alert_message.= "\r\nResult Text: ".$smartapiRes['error_text'];
          $this->alert_line->notify_message_powerbank($alert_message);
        }
        else{
          if($ref1 == '00102'){
            $payment_doll_result = $this->payment_migrate->payment_doll($ref1, $ref2, $amount);
          }
        } 
      }else{
        $resCode = '999';
      }
    }

    if($resCode == '000' && $api_type == 'loan' && $paymenttype == 'member'){
      $kioskcode = "K".$member->membercode;
      $kioskid = $ref2;
      $res = $this->qrypayment->inquiry_loan($kioskcode, $kioskid);
      if($res['error_code'] == 'E00'){
        $resCode = '000';
        $loan_amount = $res['loan_detail']['pay_per_month'];
        $this->load->module('common/payment');
        $member_fee = $this->payment->get_member_fee($bankcode, $ref1, $amount);
        $loan_amount = $loan_amount+$member_fee;
        if($amount < $loan_amount){
          $resCode = '003';
        }else{
          $res = $this->qrypayment->payment_credit_member($ref1, $ref2, $amount, $member, $partner, $detail, $addby, $channel, $bankcode);
          if($res['error_code'] == 'E00'){
            $resCode = '000';
            $partnerpaymentid = $res['partnerpaymentid'];
            $memberpaymentid = $res['memberpaymentid'];
            $res = $this->qrypayment->pay_loan($kioskcode, $kioskid, $username);
            if($res['error_code'] == 'E00'){
              $resCode = '000';
            }else{
              $resCode = '000';
            }
          }else{
            $resCode = '999';
          }
        }
      }else if($res['error_code'] == 'E15'){
        $resCode = '999';
      }else{
        $resCode = '999';
      }
    }

    $RESPONSE_CODE = '000';
    $RESPONSE_MESSAGE = $this->resmesg->get_response_mesg($RESPONSE_CODE);

    $CHECK_SUM = md5($RESPONSE_CODE.$RESPONSE_MESSAGE.$CONCAT.$SECRETEKEY);
    
    $response = array(
      'TRX_REF' => $body['TRX_REF'],
      'TO_PROXY_TYPE' => $body['TO_PROXY_TYPE'],
      'TO_PROXY_ID' => $body['TO_PROXY_ID'],
      'AMOUNT' => $body['AMOUNT'],
      'FROM_ACC_NAME' => $body['FROM_ACC_NAME'],
      'FROM_BANK_CODE' => $body['FROM_BANK_CODE'],
      'REF1' => $body['REF1'],
      'REF2' => $body['REF2'],
      'TERMINAL_ID' => $body['TERMINAL_ID'],
      'TRX_DATE_TIME' => $body['TRX_DATE_TIME'],
      'RESPONSE_CODE' => $RESPONSE_CODE,
      'RESPONSE_MESSAGE' => $RESPONSE_MESSAGE,
      'CHECK_SUM' => $CHECK_SUM
    );

    if($resCode == '000'){
      $status = 1;
    }else{
      $status = 2;
    }

    if($status == 2 && ($smartapi==true or $powerbankFlag==true)){
      $this->load->module('common/alert_line');
      if($powerbankFlag){
        $device_id = isset($list_get_qr_powerbank['list_get_qr_powerbank']['device_id']) ? $list_get_qr_powerbank['list_get_qr_powerbank']['device_id'] : '';
      }else{
        $device_id = isset($smart_ref1) ? $smart_ref1 : '';
      }
      $alert_message = "\r\nService: $smartname (QR-Payment) ไม่สำเร็จ!";
      $alert_message.= "\r\nAmount: $amount";
      $alert_message.= "\r\nDevice ID: $device_id";
      $alert_message.= "\r\nRef1: ".sprintf("%05d",$ref1);
      $alert_message.= "\r\nRef2: ".sprintf("%05d",$ref2);
      $this->alert_line->notify_message_powerbank($alert_message);
    }

    $update_log = array(
      'partnerpaymentid' => isset($partnerpaymentid) ? $partnerpaymentid : NULL,
      'memberpaymentid' => isset($memberpaymentid) ? $memberpaymentid : NULL,
      'response_transid' => $transid,
      'res_code' => $resCode,
      'res_message' => $this->resmesg->get_response_mesg($resCode),
      'status' => $status,
      'returndata' => json_encode($response),
      'returndate' => date('Y-m-d'),
      'returntime' => date('H:i:s')
    );
    $this->logpayment->update_log_id($update_log, $logid);

    header('Content-Type: application/json');
    echo json_encode($response);
  }
}