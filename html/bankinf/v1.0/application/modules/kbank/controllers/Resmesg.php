<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Resmesg extends MY_Controller {

  function __construct()
  {
    parent::__construct();
  }

  function get_response_mesg($resCode){
    $resMesg['000'] = 'SUCCESS'; 
    $resMesg['001'] = 'INVALID MESSAGE';  
    $resMesg['002'] = 'INVALID BILLER ID';  
    $resMesg['003'] = 'INVALID AMOUNT';  
    $resMesg['004'] = 'INVALID REFERENCE'; 
    $resMesg['998'] = 'OTHER ERROR'; 
    $resMesg['999'] = 'SYSTEM ERROR'; 
    return $resMesg[$resCode];
  }


}
