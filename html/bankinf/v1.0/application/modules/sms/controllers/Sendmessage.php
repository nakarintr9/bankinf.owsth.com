<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Sendmessage extends MY_Controller
{
  function __construct()
  {
    parent::__construct();
    $this->load->module('common/logpaymentv3');
  }

  public function send($data, $trans_log_id=''){
    $url = $this->url_v3.'/api/v1.0/api_server_json/app_api';
    $post_data = array(
      "service" => "notifymessagebyadmin",
      "key" => $this->key_v3,
      "fromcode" => $data['fromcode'],
      "fromid" => $data['fromid'],
      "fromtype" => $data['fromtype'],
      "mobile" => $data['mobile'],
      "message" => $data['message']
    );

    $ch = curl_init($url);                                                                      
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($post_data));
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
    // curl_setopt($ch, CURLOPT_TIMEOUT, 1);
    curl_setopt($ch, CURLOPT_TIMEOUT_MS, 500);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
     'Content-Type: application/json', 
     'Content-Length: ' . strlen(json_encode($post_data)))
    );

    $insert = array(
      'api' => 'notifymessagebyadmin',
      'url' => $url,
      'postdata' => json_encode($post_data),
      'fromcode' => $data['fromcode'],
      'fromid' => $data['fromid'],
      'fromchannel' => '',
      'transid' => $trans_log_id,
      'status' => 'inprogress',
      'postdatetime' => date('Y-m-d H:i:s')
    );
    $logid = $this->logpaymentv3->insert_log_v3($insert);

    $content = curl_exec($ch);

    if($content){
      $return = json_decode($content, true);
    }else{
      $return = 'ERROR';
      $content = "Curl Error: ".curl_error($ch);
    }

    $update_log = array(
      'returndata' => $content,
      'status' => 'complete',
      'returndatetime' => date('Y-m-d H:i:s')
    );
    $this->logpaymentv3->update_log_v3($update_log, $logid);

    curl_close($ch);
    return $return;
  }
}
