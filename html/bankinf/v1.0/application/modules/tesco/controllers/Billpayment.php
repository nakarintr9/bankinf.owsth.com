<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Billpayment extends MY_Controller {

  function __construct()
  {
    parent::__construct();
    $this->load->module('common/qrypayment');
    $this->load->module('tesco/resmesg');
    $this->load->module('common/cancel_payment');
    $this->load->module('tesco/logpayment');

    $tesco_config = include(getcwd().'/../../../config/bankinf/tesco_config.php');
    $this->tesco_config = (array)$tesco_config;
  }

  function check_key($key, $passKey){
    if($key  == $this->tesco_config['key'] && $passKey == $this->tesco_config['passKey']){
      return true;
    }else{
      return false;
    }
  }

  function inquiry(){
    $postdata = file_get_contents("php://input");
    $body = json_decode($postdata, true);

    $key = isset($body['key']) ? $body['key'] : '';
    $passKey = isset($body['passKey']) ? $body['passKey'] : '';
    $res_check_key = $this->check_key($key, $passKey);
    if($res_check_key){
      $resCode = '0000';
    }else{
      $resCode = '1004';
    }

    $Min = 0;
    $Max = 0;
    $amount = 0;
    $ref1 = $body['reference1'];
    $ref2 = $body['reference2'];
    $transid = $body['tranID'];
    $api_type = $this->qrypayment->get_api_type($ref1, $ref2);
    $action = 'inquiry';
    $bankcode = 'tesco';

    if($ref2 == '00000'){
      $paymenttype = 'partner';
    }else{
      $paymenttype = 'member';
    }

    $insert_log = array(
      'api_type' => $api_type,
      'action' => $action,
      'ref1' => $ref1,
      'ref2' => $ref2,
      'amount' => $amount,
      'min_amount' => $Min,
      'max_amount' => $Max,
      'request_transid' => $transid,
      'paymenttype' => $paymenttype,
      'status' => 0,
      'postdata' => json_encode($body),
      'adddate' => date('Y-m-d'),
      'addtime' => date('H:i:s')
    );
    $logid = $this->logpayment->insert_log($insert_log);

    if($resCode == '0000'){
      $log = $this->logpayment->get_log($transid, $ref1, $ref2, $api_type, $action, $logid);
      if(empty($log)){
        $resCode = '0000';
      }else{
        $resCode = '2001';
      }
    }

    if($resCode == '0000'){
      $partner = $this->qrypayment->get_partner($ref1);
      if(!empty($partner)){
        $resCode = '0000';
      }else{
        $resCode = '1001';
      }
    }

    if($resCode == '0000' && $paymenttype == 'member'){
      $member = $this->qrypayment->get_member($ref1, $ref2);
      if(!empty($member)){
        $resCode = '0000';
      }else{
        $resCode = '1002';
      }
    }

    if($resCode == '0000'){
      if($api_type == 'loan'){
        $kioskcode = "K".$member->membercode;
        $kioskid = $ref2;
        $res = $this->qrypayment->inquiry_loan($kioskcode, $kioskid);
        if($res['error_code'] == 'E00'){
          $resCode = '0000';
          $Min = $res['loan_detail']['pay_per_month'];
          $Max = $res['loan_detail']['pay_per_month'];
          $amount = $res['loan_detail']['pay_per_month'];
          $this->load->module('common/payment');
          $member_fee = $this->payment->get_member_fee($bankcode, $ref1, $amount);
          $amount = $amount+$member_fee;
          $Min = $amount;
          $Max = $amount;
        }else if($res['error_code'] == 'E15'){
          $resCode = '3001';
        }else{
          $resCode = '2003';
        }
      }else{
        $resCode = '0000';
        $Min = 100;
        $Max = 30000;
      }
    }

    $response = array(
      'resCode' => $resCode,
      'resMesg' => $this->resmesg->get_response_mesg($resCode),
      'tranID' => $transid,
      'Min'=> $Min,
      'Max'=> $Max
    );

    if($resCode == '0000'){
      $status = 0;
    }else{
      $status = 2;
    }

    $update_log = array(
      'amount' => $amount,
      'min_amount' => $Min,
      'max_amount' => $Max,
      'response_transid' => $transid,
      'res_code' => $resCode,
      'res_message' => $this->resmesg->get_response_mesg($resCode),
      'status' => $status,
      'returndata' => json_encode($response),
      'returndate' => date('Y-m-d'),
      'returntime' => date('H:i:s')
    );
    $this->logpayment->update_log_id($update_log, $logid);

    header('Content-Type: application/json');
    echo json_encode($response);
  }

  function confirm(){
    $postdata = file_get_contents("php://input");
    $body = json_decode($postdata, true);

    $key = isset($body['key']) ? $body['key'] : '';
    $passKey = isset($body['passKey']) ? $body['passKey'] : '';
    $res_check_key = $this->check_key($key, $passKey);
    if($res_check_key){
      $resCode = '0000';
    }else{
      $resCode = '1004';
    }

    $amount = $body['amount'];
    $ref1 = $body['reference1'];
    $ref2 = $body['reference2'];
    $transid = $body['tranID'];
    $api_type = $this->qrypayment->get_api_type($ref1, $ref2);
    $api_type = 'payment';
    $action = 'confirm';
    $detail = 'เติมเครดิตด้วย Tesco BILL PAYMENT';
    $addby= 'Tesco Bill Payment';
    $channel = 'อื่นๆ';
    $username = "TescoBillPayment";
    $bankcode = 'tesco';
    $wave_fee = false;
    $trans_log_id = 'Tesco'.date('YmdHisu').rand(100,999);

    if($ref2 == '00000'){
      $paymenttype = 'partner';
    }else{
      $paymenttype = 'member';
    }

    if($api_type == 'loan'){
      $Min = $amount;
      $Max = $amount;
    }else{
      $Min = 100;
      $Max = 30000;
    }

    $insert_log = array(
      'api_type' => $api_type,
      'action' => $action,
      'ref1' => $ref1,
      'ref2' => $ref2,
      'amount' => $amount,
      'min_amount' => $Min,
      'max_amount' => $Max,
      'paymenttype' => $paymenttype,
      'status' => 0,
      'request_transid' => $transid,
      'postdata' => json_encode($body),
      'adddate' => date('Y-m-d'),
      'addtime' => date('H:i:s')
    );
    $logid = $this->logpayment->insert_log($insert_log);

    if($resCode == '0000'){
      $logquiry = $this->logpayment->get_log($transid, $ref1, $ref2, $api_type, 'inquiry');
      if(!empty($logquiry)){
        if($logquiry->status == 0){
          $resCode = '0000';
          $status = 1;
          $update_log = array(
            'status' => $status
          );
          $this->logpayment->update_log($update_log, $transid, $ref1, $ref2, $api_type, 'inquiry');
        }else{
          $resCode = '2001';
        }
      }else{
        $resCode = '2002';
      }
    }

    if($resCode == '0000'){
      $logconfirm = $this->logpayment->get_log($transid, $ref1, $ref2, $api_type, $action, $logid);
      if(empty($logconfirm)){
        $resCode = '0000';
      }else{
        $resCode = '2001';
      }
    }

    if($resCode == '0000'){
      $partner = $this->qrypayment->get_partner($ref1);
      if(!empty($partner)){
        $resCode = '0000';
      }else{
        $resCode = '1001';
      }
    }

    if($resCode == '0000' && $paymenttype == 'member'){
      $member = $this->qrypayment->get_member($ref1, $ref2);
      if(!empty($member)){
        $resCode = '0000';
      }else{
        $resCode = '1002';
      }
    }
    
    if($resCode == '0000' && $api_type != 'loan'){
      if($paymenttype == 'partner'){
        $res = $this->qrypayment->payment_credit_partner($ref1, $amount, $partner, $detail, $addby, $channel, $bankcode, $wave_fee, $trans_log_id);
      }else{
        $res = $this->qrypayment->payment_credit_member($ref1, $ref2, $amount, $member, $partner, $detail, $addby, $channel, $bankcode, $wave_fee, $trans_log_id);
      }
      if($res['error_code'] == 'E00'){
        $resCode = '0000';
        $partnerpaymentid = isset($res['partnerpaymentid']) ? $res['partnerpaymentid'] : '';
        $memberpaymentid = isset($res['memberpaymentid']) ? $res['memberpaymentid'] : '';
      }else{
        $resCode = '3002';
      }
    }

    if($resCode == '0000' && $api_type == 'loan' && $paymenttype == 'member'){
      $kioskcode = "K".$member->membercode;
      $kioskid = $ref2;
      $res = $this->qrypayment->inquiry_loan($kioskcode, $kioskid);
      if($res['error_code'] == 'E00'){
        $resCode = '0000';
        $loan_amount = $res['loan_detail']['pay_per_month'];
        $this->load->module('common/payment');
        $member_fee = $this->payment->get_member_fee($bankcode, $ref1, $amount);
        $loan_amount = $loan_amount+$member_fee;
        if($amount < $loan_amount){
          $resCode = '1003';
        }else{
          $res = $this->qrypayment->payment_credit_member($ref1, $ref2, $amount, $member, $partner, $detail, $addby, $channel, $bankcode);
          if($res['error_code'] == 'E00'){
            $resCode = '0000';
            $partnerpaymentid = $res['partnerpaymentid'];
            $memberpaymentid = $res['memberpaymentid'];
            $res = $this->qrypayment->pay_loan($kioskcode, $kioskid, $username);
            if($res['error_code'] == 'E00'){
              $resCode = '0000';
            }else{
              $resCode = '0000';
            }
          }else{
            $resCode = '3002';
          }
        }
      }else if($res['error_code'] == 'E15'){
        $resCode = '3001';
      }else{
        $resCode = '2003';
      }
    }

    $response = array(
      'resCode' => $resCode,
      'resMesg' => $this->resmesg->get_response_mesg($resCode),
      'tranID' => $body['tranID']
    );

    if($resCode == '0000'){
      $status = 1;
    }else{
      $status = 2;
    }

    $update_log = array(
      'partnerpaymentid' => isset($partnerpaymentid) ? $partnerpaymentid : NULL,
      'memberpaymentid' => isset($memberpaymentid) ? $memberpaymentid : NULL,
      'response_transid' => $transid,
      'res_code' => $resCode,
      'res_message' => $this->resmesg->get_response_mesg($resCode),
      'status' => $status,
      'returndata' => json_encode($response),
      'returndate' => date('Y-m-d'),
      'returntime' => date('H:i:s')
    );
    $this->logpayment->update_log_id($update_log, $logid);

    
    header('Content-Type: application/json');
    echo json_encode($response);
  }

  function reverse(){
    $postdata = file_get_contents("php://input");
    $body = json_decode($postdata, true);

    $key = isset($body['key']) ? $body['key'] : '';
    $passKey = isset($body['passKey']) ? $body['passKey'] : '';
    $res_check_key = $this->check_key($key, $passKey);
    if($res_check_key){
      $resCode = '0000';
    }else{
      $resCode = '1004';
    }
    
    $amount = $body['amount'];
    $ref1 = $body['reference1'];
    $ref2 = $body['reference2'];
    $transid = $body['tranID'];
    $api_type = $this->qrypayment->get_api_type($ref1, $ref2);
    $action = 'reverse';
    $paymenttype = 'member';
    $addby = 'Tesco Bill Payment';
    $username = "TescoBillPayment";
    $partnerpaymentid = '';
    $memberpaymentid = '';

    if($ref2== '00000'){
      $paymenttype = 'partner';
    }else{
      $paymenttype = 'member';
    }

    $this->load->module('common/alert_line');
    $status_cancel = "FAIL";
    $alert_message = "\r\nPayment Cancel: Tesco\r\nRef1: $ref1\r\nRef2: $ref2\r\nAmount: $amount\r\nStatus Cancel: $status_cancel";

    $insert_log = array(
      'api_type' => $api_type,
      'action' => $action,
      'ref1' => $ref1,
      'ref2' => $ref2,
      'amount' => $amount,
      'min_amount' => 0,
      'max_amount' => 0,
      'paymenttype' => $paymenttype,
      'status' => 0,
      'request_transid' => $transid,
      'postdata' => json_encode($body),
      'adddate' => date('Y-m-d'),
      'addtime' => date('H:i:s')
    );
    $logid = $this->logpayment->insert_log($insert_log);

    if($resCode == '0000'){
      $logconfirm = $this->logpayment->get_log($transid, $ref1, $ref2, $api_type, 'confirm');
      if(!empty($logconfirm)){
        if($logconfirm->status == 1){
          $resCode = '0000';
          $status = 3;
          $update_log = array(
            'status' => $status
          );
          $this->logpayment->update_log($update_log, $transid, $ref1, $ref2, $api_type, 'confirm');
          $partnerpaymentid = $logconfirm->partnerpaymentid;
          $memberpaymentid = $logconfirm->memberpaymentid;
          $amount_log = $logconfirm->amount;
          if($amount_log != $amount){
            $resCode = '1003';
          }
        }else{
          $resCode = '2001';
        }
      }else{
        $resCode = '2002';
      }
    }

    if($resCode == '0000'){
      $logreverse = $this->logpayment->get_log($transid, $ref1, $ref2, $api_type, $action, $logid);
      if(empty($logreverse)){
        $resCode = '0000';
      }else{
        $resCode = '2001';
      }
    }

    if($resCode == '0000'){
      $partner = $this->qrypayment->get_partner($ref1);
      if(!empty($partner)){
        $resCode = '0000';
      }else{
        $resCode = '1001';
      }
    }

    if($resCode == '0000' && $paymenttype == 'member'){
      $member = $this->qrypayment->get_member($ref1, $ref2);
      if(!empty($member)){
        $resCode = '0000';
      }else{
        $resCode = '1002';
      }
    }

    if($resCode == '0000'){
      if($paymenttype == 'partner'){
        $is_payment_v3 = 'Y';
        $res = $this->cancel_payment->cancel_payment_partner($partnerpaymentid, $amount, $addby, $partner, $is_payment_v3);
      }else{
        $data = array(
          'fromcode' => $member->membercode, 
          'fromid' => $member->memberid,
        );
        $checkMerberV3  = $this->payment_migrate->get_member_v3($data);
        if($checkMerberV3 != 'ERROR'){
          if($checkMerberV3['error_code'] == 'E00'){
            $is_payment_v3 = 'Y';
          }else{
            $is_payment_v3 = 'N';
          }
          $res = $this->cancel_payment->cancel_payment_member($memberpaymentid, $amount, $addby, $member, $is_payment_v3);
          if($res['error_code'] == 'E00'){
            $member_fee = $res['member_fee'];
            $res = $this->cancel_payment->cancel_payment_partner($partnerpaymentid, $amount, $addby, $partner, $is_payment_v3);
            if($res['error_code'] == 'E00'){
              $partner_fee = $res['partner_fee'];
            }
          }
        }else{
          $res['error_code'] = 'E02';
        }
      }

      if($res['error_code'] == 'E00'){
        $resCode = '0000';
        $status_cancel = "SUCCESS";
      }else{
        $resCode = '3002';
      }
    }

    if($resCode == '0000' && $api_type == 'loan'){
      $kioskcode = "K".$member->membercode;
      $kioskid = $ref2;
      $res = $this->qrypayment->inquiry_loan($kioskcode, $kioskid);
      if($res['error_code'] == 'E00'){
        $resCode = '0000';
        $loan_amount = $amount - $member_fee;
        $res = $this->qrypayment->cancel_loan($kioskcode, $kioskid, $loan_amount);
        if($res['error_code'] == 'E00'){
          $resCode = '0000';
          $status_cancel = "SUCCESS";
        }else{
          $resCode = '0001';
          $status_cancel = "CANCEL LOAN FAIL";
        }
      }else if($res['error_code'] == 'E15'){
        $resCode = '3001';
      }else{
        $resCode = '2003';
      }
    }

    $response = array(
      'resCode' => $resCode,
      'resMesg' => $this->resmesg->get_response_mesg($resCode),
      'tranID' => $body['tranID']
    );

    if($resCode == '0000'){
      $status = 1;
    }else{
      $status = 2;
    }

    $alert_message = "\r\nPayment Cancel: Tesco\r\nRef1: $ref1\r\nRef2: $ref2\r\nPartner Payment ID: $partnerpaymentid\r\nMember Payment ID: $memberpaymentid\r\nAmount: $amount\r\nStatus Cancel: $status_cancel";

    $this->alert_line->notify_message($alert_message);

    $update_log = array(
      'partnerpaymentid' => isset($partnerpaymentid) ? $partnerpaymentid : NULL,
      'memberpaymentid' => isset($memberpaymentid) ? $memberpaymentid : NULL,
      'response_transid' => $transid,
      'res_code' => $resCode,
      'res_message' => $this->resmesg->get_response_mesg($resCode),
      'status' => $status,
      'returndata' => json_encode($response),
      'returndate' => date('Y-m-d'),
      'returntime' => date('H:i:s')
    );
    $this->logpayment->update_log_id($update_log, $logid);

    header('Content-Type: application/json');
    echo json_encode($response);
  }

}