<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Qrypayment extends MY_Controller {

  function __construct()
  {
    parent::__construct();
    $this->load->library('multipledb');
  }

  function get_partner($ref1){
    $partnerid = $ref1;
    $sql = "SELECT * FROM partner where partnerid=$partnerid";
    $qry = $this->multipledb->owstopup->query($sql)->row();
    return $qry;
  }

  function get_member($ref1, $ref2){
    $partnerid = $ref1;
    $memberid = $ref2;
    $sql = "SELECT * FROM member where partnerid=$partnerid and memberid=$memberid ";
    $qry = $this->multipledb->owstopup->query($sql)->row();
    return $qry;
  }

  function get_log($transid, $ref1, $ref2, $api_type, $action, $logid=''){
    $where = array(
      'request_transid'=>$transid,
      'ref1'=>$ref1,
      'ref2'=>$ref2,
      'api_type'=>$api_type,
      'action'=>$action
    );
    if(!empty($logid)){
      $where['ID !='] = $logid;
    }
    $qry = $this->multipledb->owstopup->select('*')
      ->from('log_tesco_ows')
      ->where($where)
      ->get()
      ->row();
    return $qry;
  }

  function update_log($update_log, $transid, $ref1, $ref2, $api_type, $action){
    $where = array(
      'request_transid'=>$transid,
      'ref1'=>$ref1,
      'ref2'=>$ref2,
      'api_type'=>$api_type,
      'action'=>$action
    );
    $this->multipledb->owstopup->where($where);
    $this->multipledb->owstopup->set($update_log);
    $res = $this->multipledb->owstopup->update('log_tesco_ows');
    return $res;
  }

  function update_log_id($update_log, $logid){
    $where = array(
      'ID'=>$logid,
    );
    $this->multipledb->owstopup->where($where);
    $this->multipledb->owstopup->set($update_log);
    $res = $this->multipledb->owstopup->update('log_tesco_ows');
    return $res;
  }

  function insert_log($insert){
    $this->multipledb->owstopup->insert('log_tesco_ows', $insert);
    $insert_id = $this->multipledb->owstopup->insert_id();
    return $insert_id;
  }

  function get_api_type($ref1, $ref2){
    if($ref1 > 10000){
      $api_type = 'loan';
    }
    else if($ref1 == "00051" || $ref1 == "00052"){
      $api_type = 'loan';
    } 
    else if ($ref1 == "00004") {
      //For test
      if($ref2 == '00002'){
        $api_type = 'loan';
      }else{
        $api_type = 'payment';
      }
    }
    else{
      $api_type = 'payment';
    }
    return $api_type;
  }

  function inquiry_loan($kioskcode, $kioskid){
    $url = $this->url_loan_api."/inquiry_loan";

    $data = array(
      'kioskcode' => $kioskcode, 
      'kioskid' => intval($kioskid)
    );
    $postdata = json_encode($data);

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS,$postdata);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $content  = curl_exec($ch);
    
    if($content){
      $response = json_decode($content, TRUE);
    }else{
      $response['error_code'] = 'E15';
    }

    curl_close($ch);
    return  $response;
  }

  function pay_loan($kioskcode, $kioskid){
    $url = $this->url_loan_api."/pay_loan";

    $data = array(
      'kioskcode' => $kioskcode, 
      'kioskid' => intval($kioskid),
      'username' => "TescoBillPayment"
    );
    $postdata = json_encode($data);

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS,$postdata);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $content  = curl_exec($ch);
    
    if($content){
      $response = json_decode($content, TRUE);
    }else{
      $response['error_code'] = 'E15';
    }

    curl_close($ch);
    return  $response;
  }

  function payment_credit_member($ref1, $ref2, $amount, $transid, $member, $partner){
    $this->load->module('common/payment');
    $member_fee = $this->payment->get_member_fee('tesco', $ref1, $amount);
    $partner_fee = $this->payment->get_partner_fee('tesco', $ref1, $amount);
    $data_array = array(
      'membercode' => $member->membercode,
      'memberid' => $member->memberid,
      'transferdate' => date('Y-m-d'),
      'transfertime' => date('H:i:s'),
      'detail' => 'เติมเครดิตด้วย Tesco BILL PAYMENT',
      'amount' => $amount,
      'channel' => 'อื่นๆ',
      'addby' => 'Tesco Bill Payment',
      'member_fee' => $member_fee
    );
    $res = $this->payment->add_payment_member_to_partner($data_array);
    if($res['status'] == true){
      $error_code = 'E00';
      $payment_member = $res['payment'];
      $payment_member['ID'] = $res['id'];
      $memberpaymentid = $payment_member['ID'];
    }

    if($error_code == 'E00'){
      $payment_member['member_fee'] = $member_fee;
      $this->payment->approve_credit_member($payment_member);

      $payment_member['partner_fee'] = $partner_fee;
      $payment_partner = $this->payment->add_payment_partner_to_admin($payment_member);

      $partnerpaymentid = $payment_partner['ID'];

      $payment_partner['partner_fee'] = $partner_fee;
      $this->payment->approve_credit_partner($payment_partner);
    }

    if($error_code == 'E00'){
      //statement_member fee
      // $fee = $this->payment->get_payment_fee($data_array['addby'], $amount);
      $fee = $member_fee;
      $payment_member['amount'] = $amount - $fee;
      $this->payment->do_migrate($payment_member);
    }

    $response['error_code'] = 'E00';
    $response['partnerpaymentid'] = isset($partnerpaymentid) ? $partnerpaymentid : NULL;
    $response['memberpaymentid'] = isset($memberpaymentid) ? $memberpaymentid : NULL;
    return $response;
  }

  function cancel_loan($kioskcode, $kioskid, $amount){
    $url = $this->url_loan_api."/cancel_loan";

    $data = array(
      'kioskcode' => $kioskcode, 
      'kioskid' => intval($kioskid),
      'username' => "TescoBillPayment",
      'amount'=>$amount
    );
    $postdata = json_encode($data);

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS,$postdata);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $content  = curl_exec($ch);
    
    if($content){
      $response = json_decode($content, TRUE);
    }else{
      $response['error_code'] = 'E15';
    }

    curl_close($ch);
    return  $response;
  }

}
