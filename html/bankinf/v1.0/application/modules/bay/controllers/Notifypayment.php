<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Notifypayment extends MY_Controller {

  function __construct()
  {
    parent::__construct();
    $this->load->module('bay/resmesg');
    $this->load->module('common/qrypayment');
    $this->load->module('bay/logpayment');
    $this->load->module('common/payment_migrate');
    $bay_config = include(getcwd().'/../../../config/bankinf/bay_config.php');
    $this->bay_config = (array)$bay_config;
    $this->load->module('bay/baysignature');
  }

  function log_file($contents='')
  {
    $path_file = getcwd()."/../../../log/BAY_Callbak_Log.txt";
    file_put_contents($path_file, $contents."\r\n", FILE_APPEND);
  }

  function index(){
    $result = "SUCCESS";
    $msg = "Successful reception";

    $postdata = file_get_contents("php://input");
    $body = json_decode($postdata, true);

    $dateTimeLog = date('Y-m-d H:i:s');

    /*
    $contents = json_encode($_POST);
    $this->log_file($dateTimeLog.':POST_DATA:'.$contents);

    $contents = json_encode($_SERVER);
    $this->log_file($dateTimeLog.':SERVER_DATA:'.$contents);

    $contents = json_encode($_REQUEST);
    $this->log_file($dateTimeLog.':REQUEST_DATA:'.$contents);

    $contents = json_encode($postdata);
    $this->log_file($dateTimeLog.':INPUT_DATA:'.$contents);
    */

    $sign = isset($body['sign']) ? $body['sign'] : '';
    if(empty($sign)){
      $verifySign = false;
    }else{
      $verifySign = $this->baysignature->verify($body, $sign);
    }

    // $verifySign = true;

    if(!$verifySign){
      $result = "FAIL";
      $msg = "Verify Signature Failed";
    }
    else{
      $trxId = isset($body['trxId']) ? $body['trxId'] : '';
      $logGetQR = $this->logpayment->get_log_transaction($trxId, 'getqrcode');
      if(!empty($logGetQR)){
        if($logGetQR->status != 1){
          $result = "FAIL";
          $msg = "Create Transaction Fail";
        }else{
          $logConfirm = $this->logpayment->get_log_transaction($trxId, 'notify');
          if(!empty($logConfirm)){
            $result = "FAIL";
            $msg = "Dubplicate Transaction";
          }
        }
      }else{
        $result = "FAIL";
        $msg = "Not Found Transaction";
      }
    }

    $trxStatus = isset($body['trxStatus']) ? $body['trxStatus'] : 'NULL';
    if($trxStatus != 1){
      $result = "FAIL";
      $msg = "Callback trxStatus from bank is ".$trxStatus;
    }

    $username = "BAYBillPayment";
    $bankcode = 'bay';
    $detail = 'เติมเครดิตด้วย BAY Bill PAYMENT';
    $addby= 'BAY Bill Payment';
    $channel = 'อื่นๆ';
    $REF1 = isset($logGetQR->ref1) ? $logGetQR->ref1 : '999999';
    $REF2 = isset($logGetQR->ref2) ? $logGetQR->ref2 : '999999';
    $ref1 = isset($logGetQR->ref1) ? $logGetQR->ref1 : '999999';
    $ref2 = isset($logGetQR->ref2) ? $logGetQR->ref2 : '999999';
    $bank_ref1 = isset($body['trxId']) ? $body['trxId']: '';
    $bank_ref2 = '';
    $bank_ref3 = '';
    $amount =  isset($body['amount']) ? $body['amount'] : 0;
    $paymenttype = 'member';
    $powerbankFlag = false;
    $wave_fee = true;
    $smartapi = false;
    $e_commerce = false;

    $trans_log_id = 'BAY'.date('YmdHisu').rand(100,999);

    $transid = isset($body['trxId']) ? $body['trxId'] : '';

    $fromAccount = isset($body['fromAccount']) ? $body['fromAccount'] : '';
    $FROM_BANK_CODE = $bankcode;
    $FROM_ACC_NAME = $fromAccount.' '.$FROM_BANK_CODE;

    $insert_log = array(
      'process' => 'notify',
      'ref1' => $ref1,
      'ref2' => $ref2,
      'amount'=> $amount,
      'from_acc_no'=> $fromAccount,
      'trans_log_id' => $trans_log_id,
      'trx_id' => isset($body['trxId']) ? $body['trxId'] : '',
      'paymenttype' => $paymenttype,
      'status' => 0,
      'postdata' => $postdata,
      'adddate' => date('Y-m-d'),
      'addtime' => date('H:i:s')
    );
    $logid = $this->logpayment->insert_log($insert_log);
    sleep(50);

    if($result == "SUCCESS" && strlen($ref1) == 6){
      if($REF1 == "100001"){
        $smartname = "Powerbank";
        $list_get_qr_powerbank = $this->payment_migrate->get_list_get_qr_powerbank($REF1, $REF2, $trans_log_id);
        if($list_get_qr_powerbank){
          if($list_get_qr_powerbank['error_code'] == 'E00'){
            $ref1 = $list_get_qr_powerbank['list_get_qr_powerbank']['ca_ref1'];
            $ref2 = $list_get_qr_powerbank['list_get_qr_powerbank']['ca_ref2'];
            $powerbankFlag = true;
            $wave_fee =  true;
          }
        }
        else{
          $result = "FAIL";
          $msg = "Internal Error (Create CA Failed)";
        }
      } 
      else if($REF1 == "100002"){
        $smartname = "Powerbank";
        $list_get_qr_powerbank = $this->payment_migrate->create_new_ca_from_bankinf($REF2, $FROM_ACC_NAME, $FROM_BANK_CODE, $trans_log_id);
        if($list_get_qr_powerbank){
          if($list_get_qr_powerbank['error_code'] == 'E00'){
            $ref1 = $list_get_qr_powerbank['list_get_qr_powerbank']['ca_ref1'];
            $ref2 = $list_get_qr_powerbank['list_get_qr_powerbank']['ca_ref2'];
            $powerbankFlag = true;
            $wave_fee =  true;
          }
          else{
            $ref1 = $list_get_qr_powerbank['ca_ref1'];
            $ref2 = $list_get_qr_powerbank['ca_ref2'];
            $wave_fee =  true;
          }
        }
        else{
          $result = "FAIL";
          $msg = "Internal Error (Create CA Failed)";
        }
      } 
      else if($REF1 == "100003" || $REF1 == "100004" || $REF1 == "100005" || $REF1 == "100007"){
        if($REF1 == '100003'){
          $smartname = "Smart Wash";
        }else if($REF1 == "100004"){
          $smartname = "Smart BMI";
        }else if($REF1 == "100005"){
          $smartname = "Smart Coin";
        }else if($REF1 == "100007"){
          $smartname = "Smart Game";
        }else{
          $smartname = "Smarts NULL";
        }
        $ca_detail = $this->payment_migrate->create_ca_by_ref2($REF2, $FROM_ACC_NAME, $FROM_BANK_CODE, $trans_log_id);
        if($ca_detail){
          $ref1 = $ca_detail['ca_ref1'];
          $ref2 = $ca_detail['ca_ref2'];
          $smart_type = $REF1;
          $smart_ref1 = substr($REF2, 0, 5).'-'.substr($REF2, 5, 5);
          $smart_ref2 = sprintf("%05d",$ref1).'-'.sprintf("%05d",$ref2);
          if($REF1 == "100007"){
            $smart_ref2.= '-'.substr($REF2, 10, 5);
          }
          $bank_ref2 = $smart_ref2;
          $bank_ref3 = $smart_ref1;
          $smartapi = true;
          $wave_fee =  true;
        }
        else{
          $result = "FAIL";
          $msg = "Internal Error (Create CA Failed)";
        }
      }
      else if($REF1 == "100008"){
        $ca_detail = $this->payment_migrate->create_ca_by_ref2($REF2, $FROM_ACC_NAME, $FROM_BANK_CODE, $trans_log_id);
        if($ca_detail){
          $ref1 = $ca_detail['ca_ref1'];
          $ref2 = $ca_detail['ca_ref2'];
          $smart_ref2 = sprintf("%05d",$ref1).'-'.sprintf("%05d",$ref2);

          $e_commerce = true;
          $e_commerce_order = $this->payment_migrate->get_list_e_commerce_order($REF1, $REF2, $trans_log_id);
          if($e_commerce_order){
            if($e_commerce_order['error_code'] == 'E00'){
              $payee_ref1 = $e_commerce_order['e_commerce_order']['ref1'];
              $payee_ref2 = $e_commerce_order['e_commerce_order']['ref2'];
              $smart_ref1 = sprintf("%05d",$payee_ref1).'-'.sprintf("%05d",$payee_ref2);
              $bank_ref2 = $smart_ref2;
              $bank_ref3 = $smart_ref1;
            }else{
              $result = "FAIL";
              $msg = "Internal Error (Get Order Failed) 1";
            }
          }
          else{
            $result = "FAIL";
            $msg = "Internal Error (Get Order Failed) 2";
          }
        }
        else{
          $result = "FAIL";
          $msg = "Internal Error (Create CA Failed)";
        }
      } 
      else{
        $result = "FAIL";
        $msg = "Invalid Attach Data (Service ".$REF1." is not available)";
      }
    }

    if($result == "SUCCESS"){
      $partner = $this->qrypayment->get_partner($ref1);
      if(empty($partner)){
        $result = "FAIL";
        $msg = "Unknown Reference Data (Internal CA REF_1)";
      }
    }

    if($result == "SUCCESS"){
      $member = $this->qrypayment->get_member($ref1, $ref2);
      if(empty($member)){
        $result = "FAIL";
        $msg = "Unknown Reference Data (Internal CA REF_2)";
      }
    }

    if($result == "SUCCESS"){
      $res = $this->qrypayment->payment_credit_member($ref1, $ref2, $amount, $member, $partner, $detail, $addby, $channel, $bankcode, $wave_fee, $trans_log_id);
      if($res['error_code'] == 'E00'){
        $partnerpaymentid = isset($res['partnerpaymentid']) ? $res['partnerpaymentid'] : '';
        $memberpaymentid = isset($res['memberpaymentid']) ? $res['memberpaymentid'] : '';
        if($powerbankFlag){
          $payment_type = $list_get_qr_powerbank['list_get_qr_powerbank']['payment_type'];
          $order_id = $list_get_qr_powerbank['list_get_qr_powerbank']['id'];
          $device_id = $list_get_qr_powerbank['list_get_qr_powerbank']['device_id'];
          $pooup_result = $this->payment_migrate->pop_powerbank_from_bankinf($payment_type, $order_id, $trans_log_id);
          $this->load->module('common/alert_line');
          $alert_message = "\r\nService: Powerbank (".$addby.")";
          $alert_message.= "\r\nAmount: $amount";
          $alert_message.= "\r\nDevice ID: $device_id";
          $alert_message.= "\r\nRef1: ".sprintf("%05d",$ref1);
          $alert_message.= "\r\nRef2: ".sprintf("%05d",$ref2);
          $alert_message.= "\r\nResult Code: ".$pooup_result['error_code'];
          $alert_message.= "\r\nResult Text: ".$pooup_result['error_text'];
          $this->alert_line->notify_message_powerbank($alert_message);
        }
        else if($smartapi){
          $smartapiRes = $this->payment_migrate->smartapi_by_ref1_ref2($smart_type, $smart_ref1, $smart_ref2, $amount, $trans_log_id);
          $this->load->module('common/alert_line');
          $alert_message = "\r\nService: $smartname (".$addby.")";
          $alert_message.= "\r\nAmount: $amount";
          $alert_message.= "\r\nDevice ID: $smart_ref1";
          $alert_message.= "\r\nRef1: ".sprintf("%05d",$ref1);
          $alert_message.= "\r\nRef2: ".sprintf("%05d",$ref2);
          $alert_message.= "\r\nResult Code: ".$smartapiRes['error_code'];
          $alert_message.= "\r\nResult Text: ".$smartapiRes['error_text'];
          $this->alert_line->notify_message_powerbank($alert_message);
        }
        else{
          if($ref1 == '00102'){
            $payment_doll_result = $this->payment_migrate->payment_doll($ref1, $ref2, $amount);
          }
        } 
      }else{
        $result = "FAIL";
        $msg = "Internal Error (Payment Fail)";
      }
    }

    if($e_commerce){

      $notify_e_commerce = $this->payment_migrate->notify_e_commerce($ref1, $ref2, $result, $FROM_BANK_CODE, $amount, $REF2, $trans_log_id);
      $this->load->module('common/alert_line');
      $alert_message = "\r\nService: E-Commerce Payment (".$addby.")";
      $alert_message.= "\r\nAmount: $amount";
      $alert_message.= "\r\nPayee: ".$smart_ref1;
      $alert_message.= "\r\nPayer: ".$smart_ref2;
      $alert_message.= "\r\nResult Code: ".$notify_e_commerce['error_code'];
      $alert_message.= "\r\nResult Text: ".$notify_e_commerce['error_text'];
      $this->alert_line->notify_message_powerbank($alert_message);
    }

    if($result == "SUCCESS"){
      $status = 1;
    }else{
      $status = 2;
    }

    if($status == 2 && ($smartapi==true or $powerbankFlag==true)){
      $this->load->module('common/alert_line');
      if($powerbankFlag){
        $device_id = isset($list_get_qr_powerbank['list_get_qr_powerbank']['device_id']) ? $list_get_qr_powerbank['list_get_qr_powerbank']['device_id'] : '';
      }else{
        $device_id = isset($smart_ref1) ? $smart_ref1 : '';
      }
      $alert_message = "\r\nService: $smartname (KSher-Payment-".$FROM_BANK_CODE.") ไม่สำเร็จ!";
      $alert_message.= "\r\nAmount: $amount";
      $alert_message.= "\r\nDevice ID: $device_id";
      $alert_message.= "\r\nRef1: ".sprintf("%05d",$ref1);
      $alert_message.= "\r\nRef2: ".sprintf("%05d",$ref2);
      $this->alert_line->notify_message_powerbank($alert_message);
    }

    if(!empty($partnerpaymentid) && !empty($memberpaymentid) && is_numeric($partnerpaymentid) && is_numeric($memberpaymentid)){
      $update = array(
        'bank_ref1' => $bank_ref1,
        'bank_ref2' => $bank_ref2,
        'bank_ref3' => $bank_ref3,
      );
      $where = ' ID in ('.$partnerpaymentid.','.$memberpaymentid.') ';
      $this->qrypayment->update_payment_bank($update, $where);
    }

    $returnCode = '10000';
    $response = array(
      'returnCode' => $returnCode,
      'message' => $msg,
    );
    $sign = $this->baysignature->sign($response);
    $response['sign'] = $sign;

    $update_log = array(
      'partnerpaymentid' => isset($partnerpaymentid) ? $partnerpaymentid : NULL,
      'memberpaymentid' => isset($memberpaymentid) ? $memberpaymentid : NULL,
      'res_code' => $result,
      'res_message' => $msg,
      'status' => $status,
      'returndata' => json_encode($response),
      'returndate' => date('Y-m-d'),
      'returntime' => date('H:i:s')
    );
    $this->logpayment->update_log_id($update_log, $logid);

    header('Content-Type: application/json');
    echo json_encode($response);
  }
}