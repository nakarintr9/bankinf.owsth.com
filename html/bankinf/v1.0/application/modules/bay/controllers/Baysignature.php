<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Baysignature extends MY_Controller {
  public function __construct(){
    parent::__construct();    
    $this->load->library('rsacrypt');
    $this->bay_config = include(getcwd().'/../../../config/bankinf/bay_config.php');
    $this->load->module('common/generateuuidv4');
  }

  function test_sign(){
    $requestUID = $this->generateuuidv4->gen_uuid_v4();
    $data = array(
      'pc' => 'ccc',
      'pa' => 'aaa',
      'pb' => 'bbb',
      'bizMchId' => $this->bay_config->bizMchId
    );
    $sign = $this->sign($data);
    $data['sign'] = $sign;

    $curl = curl_init();

    curl_setopt_array($curl, array(
      CURLOPT_URL => $this->bay_config->base_url."/trans/testsign",
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_ENCODING => "",
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 30,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => "POST",
      CURLOPT_POSTFIELDS => json_encode($data),
      CURLOPT_HTTPHEADER => array(
        "Content-Type: application/json",
        "API-Key: ".$this->bay_config->apikey,
        "X-Client-Transaction-ID: ".$requestUID
      ),
    ));

    $response = curl_exec($curl);
    $err = curl_error($curl);

    curl_close($curl);

    if ($err) {
      echo "cURL Error #:" . $err;
    } else {
      echo $response;
    }
  }

  function sign($data){
    ksort($data); # array key sorting

    $message = "";
    foreach ($data as $key => $value) $message .= $key."=".$value.'&';
    $message = rtrim($message, '&');

    // echo 'message : '.$message;
    // echo '<br>';

    $rsa = new Crypt_RSA();
    $rsa->loadKey($this->bay_config->publickey_krunsri); 

    $message= hash('SHA256', $message);

    $rsa->setEncryptionMode(CRYPT_RSA_ENCRYPTION_PKCS1);
    $ciphertext = $rsa->encrypt($message);

    // echo "ciphertext : ".$ciphertext;
    // echo "<br>";

    $encoded_sign = base64_encode($ciphertext);

    return $encoded_sign;
  }

  function verify($data, $signature){
    unset($data['sign']);

    ksort($data); # array key sorting

    $message = "";
    foreach ($data as $key => $value) $message .= $key."=".$value.'&';
    $message = rtrim($message, '&');

    $rsa = new Crypt_RSA();
    $rsa->setEncryptionMode(CRYPT_RSA_ENCRYPTION_PKCS1);
    $rsa->loadKey($this->bay_config->publickey_krunsri); // public key
    $hash_message= hash('SHA256', $message);

    // echo $hash_message;
    // echo "<br>";
    // var_dump($signature);
    $hash_sign =  $rsa->decrypt(base64_decode($signature));

    // echo $hash_sign;
    // echo "<br>";

    return ($hash_message == $hash_sign) ? true : false;
  }

  function create_key_pair(){
    $rsa = new Crypt_RSA();
    $rsa->setPrivateKeyFormat(CRYPT_RSA_PRIVATE_FORMAT_PKCS1);
    $rsa->setPublicKeyFormat(CRYPT_RSA_PUBLIC_FORMAT_PKCS1);

    //define('CRYPT_RSA_EXPONENT', 65537);
    //define('CRYPT_RSA_SMALLEST_PRIME', 64); // makes it so multi-prime RSA is used
    $x = extract($rsa->createKey()); // == $rsa->createKey(1024) where 1024 is the key size
    echo "<pre>";
    var_dump($rsa->createKey());
  }
}