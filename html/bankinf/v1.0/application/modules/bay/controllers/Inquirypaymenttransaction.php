<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Inquirypaymenttransaction extends MY_Controller {

  function __construct()
  {
    parent::__construct();
    $this->bay_config = include(getcwd().'/../../../config/bankinf/bay_config.php');
    $this->load->module('bay/baysignature');
    $this->load->module('common/generateuuidv4');
  }

  function test_(){
    echo "Server OK";
  }

  function call($trxId){
    $x_client_transaction_id = $this->generateuuidv4->gen_uuid_v4();
    $data = array(
      'bizMchId' => $this->bay_config->bizMchId,
      'trxId' => $trxId,
    );    
    $sign = $this->baysignature->sign($data);
    $data['sign'] = $sign;

    $postdata =  json_encode($data);

    $curl = curl_init();

    curl_setopt_array($curl, array(
      CURLOPT_URL => $this->bay_config->base_url."/trans/detail",
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_ENCODING => "",
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 30,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => "POST",
      CURLOPT_POSTFIELDS => $postdata,
      // CURLINFO_HEADER_OUT => true,
      CURLOPT_HTTPHEADER => array(
        "Content-Type: application/json",
        "API-Key: ".$this->bay_config->apikey,
        "X-Client-Transaction-ID: ".$x_client_transaction_id
      ),
    ));

    // $information = curl_getinfo($curl);

    $response = curl_exec($curl);
    $err = curl_error($curl);

    curl_close($curl);

    /*
      0 = inprogress
      1 = success
      2 = fail
      3 = api error
    */

    if(!$err){
      $res = json_decode($response, true);
      $trxStatus = isset($res['transaction']['trxStatus']) ? $res['transaction']['trxStatus'] : 3;
    }else{
      $trxStatus = 3;
    }

    $return = array(
      'trxStatus' => $trxStatus,
      'trxMessage' => $this->get_trans_msg($trxStatus)
    );

    header('Content-Type: application/json');
    echo json_encode($return);
  }

  function get_trans_msg($trxStatus){
    $list_msg = array(
      '0'=>'the transaction is processing ',
      '1'=>'the transaction is successfully paid',
      '2'=>'the transaction processes fail',
      '3'=>'api error'
    );
    return $list_msg[$trxStatus];
  }
}