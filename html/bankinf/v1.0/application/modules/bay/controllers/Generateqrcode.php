<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Generateqrcode extends MY_Controller {

  function __construct()
  {
    parent::__construct();
    $this->load->module('bay/resmesg');
    $this->load->module('common/qrypayment');
    $this->load->module('bay/logpayment');
    $this->load->module('common/payment_migrate');
    $this->bay_config = include(getcwd().'/../../../config/bankinf/bay_config.php');
    $this->load->module('bay/baysignature');
    $this->load->library('errorcode');
    $this->load->module('common/generateuuidv4');
  }

  function gen($body){
    $status = 2;
    $x_client_transaction_id = $this->generateuuidv4->gen_uuid_v4();
    $terminalId = isset($body['terminal_id']) ? $body['terminal_id'] : $this->bay_config->terminalId;

    $orderid = isset($body['orderid']) ? $body['orderid'] : '';
    $ref1 = isset($body['ref1']) ? $body['ref1'] : '';
    $ref2 = isset($body['ref2']) ? $body['ref2'] : '';
    if(empty($ref1) && empty($ref2)){
      $ref1 = '100008';
      $ref2 = $orderid;
    }

    $amount = isset($body['amount']) ? number_format($body['amount'],2,'.','') : 1; 
    $remark = isset($body['remark']) ? $body['remark'] : 'OK'; 

    $data = array(
      'bizMchId' => $this->bay_config->bizMchId,
      "billerId" => $this->bay_config->billerId,
      "reference1" => $ref1,
      "reference2" => $ref2,
      "terminalId" => $terminalId,
      "amount" => $amount,
      "remark" => $remark,
    );

    $sign = $this->baysignature->sign($data);

    $data['sign'] = $sign;

    $postdata = json_encode($data);

    /*
    echo $postdata;
    echo "<br>";
    */

    $insert_log = array(
      'process' => "getqrcode",
      'ref1' => $ref1,
      'ref2' => $ref2,
      'amount' => $amount,
      'x_client_transaction_id' => isset($x_client_transaction_id) ? $x_client_transaction_id : '',
      'postdata' => $postdata,
      'adddate' => date('Y-m-d'),
      'addtime' => date('H:i:s'),
      'status' => 0,
    );
    $logid = $this->logpayment->insert_log($insert_log);

    $curl = curl_init();

    curl_setopt_array($curl, array(
      CURLOPT_URL => $this->bay_config->base_url."/trans/precreate",
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_ENCODING => "",
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 30,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => "POST",
      CURLOPT_POSTFIELDS => $postdata,
      // CURLINFO_HEADER_OUT => true,
      CURLOPT_HTTPHEADER => array(
        "Content-Type: application/json",
        "API-Key: ".$this->bay_config->apikey,
        "X-Client-Transaction-ID: ".$x_client_transaction_id
      ),
    ));

    // $information = curl_getinfo($curl);

    $response = curl_exec($curl);
    $err = curl_error($curl);

    curl_close($curl);

    if(!$err){
      $res = json_decode($response, true);
      $sign = isset($res['sign']) ? $res['sign'] : '';

      if(empty($sign)){
        $verifySign = false;
      }else{
        $verifySign = $this->baysignature->verify($res, $sign);
      }

      if($verifySign){
        $returnCode = isset($res['returnCode']) ? $res['returnCode'] : '';
        $broker_error = isset($res['message']) ? $res['message'] : '';
        $trxId = isset($res['trxId']) ? $res['trxId'] : '';
        if($returnCode == '10000'){
          $qr_content = isset($res['qrcodeContent']) ? $res['qrcodeContent'] : '';
          $QRCode = file_get_contents($this->bay_config->url_create_qr_code_base_64.'/'.$qr_content);
          $error_code = 'E00';
          $status = 1;
        }else{
          $error_code = 'E64';
        }  
      }else{
        $error_code = 'E14';
        $broker_error = 'verify sign fail.';
      }
    }else{
      $error_code = 'E14';
      $broker_error = 'cURL Error: '.$err;
    }

    $return = array(
      'error_code' => $error_code,
      'error_text' => $this->errorcode->$error_code,
      'qr_image' => isset($QRCode) ? $QRCode : '',
      'broker_error' => isset($broker_error) ? $broker_error : '',
      'broker_trx_id' => isset($res['trxId']) ? $res['trxId'] : ''
    );

    $update_log = array(
      'trx_id' => isset($trxId) ? $trxId : '',
      'qr_content' => isset($qr_content) ? $qr_content : '',
      'res_code' => isset($returnCode) ? $returnCode : '',
      'res_message' => isset($broker_error) ? $broker_error : '',
      'returndata' => $response,
      'returndate' => date('Y-m-d'),
      'returntime' => date('H:i:s'),
      'status' => $status,
    );
    $this->logpayment->update_log_id($update_log, $logid);

    return (object)$return;
  }
}