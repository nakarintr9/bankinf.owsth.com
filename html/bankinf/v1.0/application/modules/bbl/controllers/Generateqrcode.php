<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Generateqrcode extends MY_Controller {

  function __construct()
  {
    parent::__construct();
    $this->bay_config = include(getcwd().'/../../../config/bankinf/bay_config.php');
    $this->load->library('errorcode');
    define('CRC16POLYN', 0x1021);
  }

  function gen($body){
    $orderid = isset($body['orderid']) ? $body['orderid'] : '';
    $ref1 = isset($body['ref1']) ? $body['ref1'] : '';
    $ref2 = isset($body['ref2']) ? $body['ref2'] : '';
    $amount = isset($body['amount']) ? $body['amount'] : 0; 

    if(empty($ref1) && empty($ref2)){
      $ref1 = '100008';
      $ref2 = $orderid;
    }

    $qr_content = $this->gen_qrpay_by_order($ref1,$ref2,$amount);
    $QRCode = file_get_contents($this->bay_config->url_create_qr_code_base_64.'/'.$qr_content);
    $error_code = "E00";

    $return = array(
      'error_code' => $error_code,
      'error_text' => $this->errorcode->$error_code,
      'qr_image' => isset($QRCode) ? $QRCode : '',
      'broker_error' => '',
      'broker_trx_id' => ''
    );

    return (object)$return;
  }

  function gen_qrpay_by_order($ref1,$ref2,$amount){
    $Suffix= "10";
    $amount = number_format($amount,2,'.','');
    $Count_amount  = str_pad(strlen($amount), 2, 0, STR_PAD_LEFT);
    $Transaction_Amount = "54".$Count_amount.$amount;

    $AID = "00"."16"."A000000677010112";
    $TXID = "0105550024194";
    $Count_txid = strlen($TXID.$Suffix);
    $Biller_ID = "01".$Count_txid.$TXID.$Suffix;
    $Reference_1 = "02".sprintf('%02s',strlen($ref1)).$ref1;
    $Reference_2 = "03".sprintf('%02s',strlen($ref2)).$ref2;

    $Count_Merchant_identifier = strlen($AID.$Biller_ID.$Reference_1.$Reference_2);
    $Merchant_identifier = "30".$Count_Merchant_identifier.$AID.$Biller_ID.$Reference_1.$Reference_2;

    $Str_confirm_page = "Media Center";
    $Count_str_confirm_page = iconv_strlen($Str_confirm_page);
    $Merchant_Name = "59".$Count_str_confirm_page.$Str_confirm_page;
    $data_array = array(
      'Payload_Format_Indicator' => "00"."02"."01",
      'Point_of_Initiation_method' => "01"."02"."11",
      'Merchant_identifier' => $Merchant_identifier,
      'Transaction_Currency_Code' => "53"."03"."764",
      'Transaction_Amount' => $Transaction_Amount,
      'Tip_or_convenience_indicator' => "",
      'Value_of_convenience_fee_fixed' => "",
      'Value_of_convenience_fee_pecentage' => "",
      'Country_Code' => "58"."02"."TH",
      'Merchant_Name' => $Merchant_Name,
      'Merchant_City' => "",
      'Postal_Code' => "",
      'Additional_Data_Field_Template' => "",
      'Merchant_Information_Language_Template' => "",
      'RFU_for_EMVCo' => "",
      'VAT_TQRC' => "",
      'Unreserved_Templated' => "",
      'CRC' => "",
    );
    
    $data = "";
    foreach ($data_array as $key => $value) {
      $data .= $value;
    }
    $data.="6304";

    $check_sum  = dechex($this->CRC16Normal(($data)));
    $data .= strtoupper(sprintf("%04s",$check_sum));

    return $data;
  }

  function CRC16Normal($buffer) {
    $result = 0xFFFF;
    if (($length = strlen($buffer)) > 0) {
        for ($offset = 0; $offset < $length; $offset++) {
            $result ^= (ord($buffer[$offset]) << 8);
            for ($bitwise = 0; $bitwise < 8; $bitwise++) {
                if (($result <<= 1) & 0x10000) $result ^= CRC16POLYN;
                $result &= 0xFFFF;
            }
        }
    }
    return $result;
  }
}