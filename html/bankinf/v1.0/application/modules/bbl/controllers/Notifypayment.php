<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Notifypayment extends MY_Controller {

  function __construct()
  {
    parent::__construct();
    $this->load->module('bbl/resmesg');
    $this->load->module('common/qrypayment');
    $this->load->module('bbl/logpayment');
    $this->load->module('common/payment_migrate');
    $bbl_config = include(getcwd().'/../../../config/bankinf/bbl_config.php');
    $this->bbl_config = (array)$bbl_config;
  }

  function providerId($providerId){
    $postdata = file_get_contents("php://input");
    $post = json_decode($postdata);
    $body = (array)$post;

    $transid = isset($_SERVER['HTTP_REQUEST_REF']) ? $_SERVER['HTTP_REQUEST_REF'] : '';
    $request_date_time = isset($_SERVER['HTTP_TRANSMIT_DATE_TIME']) ? $_SERVER['HTTP_TRANSMIT_DATE_TIME'] : date('Y-m-d\TH:i:s.000\+07:00');
    $User = isset($_SERVER['PHP_AUTH_USER']) ? $_SERVER['PHP_AUTH_USER'] : '';
    $Password = isset($_SERVER['PHP_AUTH_PW']) ? $_SERVER['PHP_AUTH_PW'] : '';

    $this->providerId = $this->bbl_config['providerId'];
    $this->User = $this->bbl_config['User'];
    $this->Password = $this->bbl_config['Password'];

    if($User != $this->User && $Password != $this->Password){
      // $resCode =  '211';
      $resCode = '000';
    }else{
      $resCode = '000';
    }

    $billerId = isset($body['billerId']) ? $body['billerId'] : '';
    $transDate = isset($body['transDate']) ? $body['transDate'] : '';
    $transTime = isset($body['transTime']) ? $body['transTime'] : '';
    $termType = isset($body['termType']) ? $body['termType'] : '';
    $amount = isset($body['amount']) ? $body['amount'] : 0;
    $reference1 = isset($body['reference1']) ? $body['reference1'] : '999999';
    $reference2 = isset($body['reference2']) ? $body['reference2'] : '999999';


    $bank_ref1 = isset($body['reference1']) ? $body['reference1'] : '999999';
    $bank_ref2 = isset($body['reference2']) ? $body['reference2'] : '999999';
    $bank_ref3 = '';
    
    $reference3 = isset($body['reference3']) ? $body['reference3'] : '999999';
    $fromBank = isset($body['fromBank']) ? $body['fromBank'] : '';
    $fromName = isset($body['fromName']) ? $body['fromName'] : '';
    $bankRef = isset($body['bankRef']) ? $body['bankRef'] : '';
    $approvalCode = isset($body['approvalCode']) ? $body['approvalCode'] : '';
    $retryFlag = isset($body['retryFlag']) ? $body['retryFlag'] : 'N';
    $ref2 = '';
    $trans_log_id = 'BBL'.date('YmdHisu').rand(100,999);

    if($resCode == '000'){
      $ref1 = $reference1;
      $ref2 = $reference2;
      $api_type = $this->qrypayment->get_api_type($ref1, $ref2);
      $detail = 'เติมเครดิตด้วย BBL BILL PAYMENT';
      $addby='BBL Bill Payment';
      $channel = 'อื่นๆ';
      $username = "BBLBillPayment";
      $bankcode = 'bbl';
      $action = 'notify';
      $powerbankFlag = false;
      $wave_fee = false;
      $smartapi = false;
      $e_commerce = false;

      
      if($ref1 == "100001"){
        $list_get_qr_powerbank = $this->payment_migrate->get_list_get_qr_powerbank($ref1, $ref2, $trans_log_id);
        if($list_get_qr_powerbank){
          if($list_get_qr_powerbank['error_code'] == 'E00'){
            $ref1 = $list_get_qr_powerbank['list_get_qr_powerbank']['ca_ref1'];
            $ref2 = $list_get_qr_powerbank['list_get_qr_powerbank']['ca_ref2'];
            $powerbankFlag = true;
            $wave_fee =  true;
          }
        }
      } 
      else if($ref1 == "100002"){
        $list_get_qr_powerbank = $this->payment_migrate->create_new_ca_from_bankinf($reference2, $fromName, $fromBank, $trans_log_id);
        if($list_get_qr_powerbank){
          if($list_get_qr_powerbank['error_code'] == 'E00'){
            $ref1 = $list_get_qr_powerbank['list_get_qr_powerbank']['ca_ref1'];
            $ref2 = $list_get_qr_powerbank['list_get_qr_powerbank']['ca_ref2'];
            $powerbankFlag = true;
            $wave_fee =  true;
          }
          else{
            $ref1 = $list_get_qr_powerbank['ca_ref1'];
            $ref2 = $list_get_qr_powerbank['ca_ref2'];
            $wave_fee =  true;
          }
        }
      } 
      else if($ref1 == "100003" || $ref1 == "100004" || $ref1 == "100005" || $ref1 == "100006" || $ref1 == "100007"){
        // 100003 = smartwash
        // 100004 = smartscale
        if($ref1 == '100003'){
          $smartname = "Smart Wash";
        }else if($ref1 == "100004"){
          $smartname = "Smart BMI";
        }else if($ref1 == "100005"){
          $smartname = "Smart Coin";
        }else if($ref1 == "100006"){
          $smartname = "Smart Withdraw";
        }else if($ref1 == "100007"){
          $smartname = "Smart Game";
        }else{
          $smartname = "Smarts NULL";
        }
        $ca_detail = $this->payment_migrate->create_ca_by_ref2($reference2, $fromName, $fromBank, $trans_log_id);
        if($ca_detail){
          $ref1 = $ca_detail['ca_ref1'];
          $ref2 = $ca_detail['ca_ref2'];
          $smart_type = $reference1;
          $smart_ref1 = substr($reference2, 0, 5).'-'.substr($reference2, 5, 5);
          $smart_ref2 = sprintf("%05d",$ref1).'-'.sprintf("%05d",$ref2);
          if($reference1 == "100007"){
            $smart_ref2.= '-'.substr($reference2, 10, 5);
          }
          $smartapi = true;
          $wave_fee =  true;
        }
      }
      else if($ref1 == "100008"){
        $ca_detail = $this->payment_migrate->create_ca_by_ref2($reference2, $fromName, $fromBank, $trans_log_id);
        if($ca_detail){
          $ref1 = $ca_detail['ca_ref1'];
          $ref2 = $ca_detail['ca_ref2'];
          $smart_ref2 = sprintf("%05d",$ref1).'-'.sprintf("%05d",$ref2);

          $e_commerce = true;
          $e_commerce_order = $this->payment_migrate->get_list_e_commerce_order($reference1, $reference2, $trans_log_id);
          if($e_commerce_order){
            if($e_commerce_order['error_code'] == 'E00'){
              $payee_ref1 = $e_commerce_order['e_commerce_order']['ref1'];
              $payee_ref2 = $e_commerce_order['e_commerce_order']['ref2'];
              $smart_ref1 = sprintf("%05d",$payee_ref1).'-'.sprintf("%05d",$payee_ref2);
              $wave_fee =  true;
            }else{
              $result = "FAIL";
              $msg = "Internal Error (Get Order Failed) 1";
            }
          }
          else{
            $result = "FAIL";
            $msg = "Internal Error (Get Order Failed) 2";
          }
        }
        else{
          $result = "FAIL";
          $msg = "Internal Error (Create CA Failed)";
        }
      }
    }

    if($ref2 == '00000'){
      $paymenttype = 'partner';
    }else{
      $paymenttype = 'member';
    }

    if($_SERVER['SERVER_NAME'] == 'bankinf.mcpay.in'){
      $base_url = 'https://bankinf.mcpay.in:22446/bankinf';
    }else{
      $base_url = 'https://owsth.com/bankinf';
    }

    $insert_log = array(
      'api_type' => $api_type,
      'action' => $action,
      'ref1' => $ref1,
      'ref2' => $ref2,
      'amount' => $amount,
      'bankref' => $bankRef,
      'providerid' => $providerId,
      'request_date_time' => $request_date_time,
      'paymenttype' => $paymenttype,
      'status' => 0,
      'request_transid' => $transid,
      'postdata' => json_encode($body),
      'adddate' => date('Y-m-d'),
      'addtime' => date('H:i:s'),
      'process_id' => getmygid(),
      'base_url' => $base_url
    );
    $logid = $this->logpayment->insert_log($insert_log);


    if($resCode == '000'){
      $logconfirm = $this->logpayment->get_log($transid, $ref1, $ref2, $api_type, $action, $logid);
      if(empty($logconfirm)){
        $resCode = '000';
      }else{
        $resCode = '888';
      }
    }

    if($resCode == '000'){
      $partner = $this->qrypayment->get_partner($ref1);
      if(!empty($partner)){
        $resCode = '000';
      }else{
        $resCode = '052';
      }
    }

    if($resCode == '000' && $paymenttype == 'member'){
      $member = $this->qrypayment->get_member($ref1, $ref2);
      if(!empty($member)){
        $resCode = '000';
      }else{
        $resCode = '052';
      }
    }

    if($resCode == '000' && $api_type != 'loan'){
      if($paymenttype == 'partner'){
        $res = $this->qrypayment->payment_credit_partner($ref1, $amount, $partner, $detail, $addby, $channel, $bankcode, $wave_fee, $trans_log_id);
      }else{
        $res = $this->qrypayment->payment_credit_member($ref1, $ref2, $amount, $member, $partner, $detail, $addby, $channel, $bankcode, $wave_fee, $trans_log_id);
      }
      if($res['error_code'] == 'E00'){
        $resCode = '000';
        $partnerpaymentid = isset($res['partnerpaymentid']) ? $res['partnerpaymentid'] : '';
        $memberpaymentid = isset($res['memberpaymentid']) ? $res['memberpaymentid'] : '';
        if($powerbankFlag){
          $payment_type = $list_get_qr_powerbank['list_get_qr_powerbank']['payment_type'];
          $order_id = $list_get_qr_powerbank['list_get_qr_powerbank']['id'];
          $pooup_result = $this->payment_migrate->pop_powerbank_from_bankinf($payment_type, $order_id, $trans_log_id);
          $this->load->module('common/alert_line');
          $alert_message = "\r\nService: Powerbank (BBL-QR-Payment)\r\nAmount: $amount\r\nResult Code: ".$pooup_result['error_code']."\r\nResult Text: ".$pooup_result['error_text'];;
          $this->alert_line->notify_message_powerbank($alert_message);
        }
        else if($smartapi){
          $smartapiRes = $this->payment_migrate->smartapi_by_ref1_ref2($smart_type, $smart_ref1, $smart_ref2, $amount, $trans_log_id);
          $this->load->module('common/alert_line');
          $alert_message = "\r\nService: $smartname (BBL-QR-Payment)";
          $alert_message.= "\r\nDevice ID: $smart_ref1";
          $alert_message.= "\r\nRef1: ".sprintf("%05d",$ref1);
          $alert_message.= "\r\nRef2: ".sprintf("%05d",$ref2);
          $alert_message.= "\r\nAmount: $amount\r\nResult Code: ".$smartapiRes['error_code']."\r\nResult Text: ".$smartapiRes['error_text'];
          $this->alert_line->notify_message_powerbank($alert_message);
        }
        else{
          if($ref1 == '00102'){
            $payment_doll_result = $this->payment_migrate->payment_doll($ref1, $ref2, $amount);
          }
        } 

        if(!empty($partnerpaymentid) && !empty($memberpaymentid) && is_numeric($partnerpaymentid) && is_numeric($memberpaymentid)){
          $update = array(
            'bank_ref1' => $bank_ref1,
            'bank_ref2' => $bank_ref2,
            'bank_ref3' => $bank_ref3,
          );
          $where = ' ID in ('.$partnerpaymentid.','.$memberpaymentid.') ';
          $this->qrypayment->update_payment_bank($update, $where);
        }
      }else{
        $resCode = '999';
      }
    }

    if($e_commerce){
      if($resCode == '000'){
        $result = 'SUCCESS';
      }else{
        $result = "FAIL";
      }
      $notify_e_commerce = $this->payment_migrate->notify_e_commerce($ref1, $ref2, $result, 'bbl', $amount, $reference2, $trans_log_id);
      $this->load->module('common/alert_line');
      $alert_message = "\r\nService: E-Commerce Payment (".$addby.")";
      $alert_message.= "\r\nAmount: $amount";
      $alert_message.= "\r\nPayee: ".$smart_ref1;
      $alert_message.= "\r\nPayer: ".$smart_ref2;
      $alert_message.= "\r\nResult Code: ".$notify_e_commerce['error_code'];
      $alert_message.= "\r\nResult Text: ".$notify_e_commerce['error_text'];
      $this->alert_line->notify_message_powerbank($alert_message);
    }
    
    $response = array(
      'responseCode' => $resCode,
      'responseMesg' => $this->resmesg->get_response_mesg($resCode)
    );

    if($resCode == '000'){
      $status = 1;
    }else{
      $status = 2;
      $status_payment = "FAIL";
      if($api_type == 'loan'){
        $typeAlert = " LOAN";
      }else{
        $typeAlert = "";
      }
      $this->load->module('common/alert_line');
      $alert_message = "\r\nService : BBL QR-Payment\r\nRef1: $ref1\r\nRef2: $ref2\r\nAmount: $amount\r\nStatus :$typeAlert FAIL";
      $this->alert_line->notify_message($alert_message);
    }

    $response_transid = $transid;
    $response_date_time = $this->get_microtime();
    $update_log = array(
      'partnerpaymentid' => isset($partnerpaymentid) ? $partnerpaymentid : NULL,
      'memberpaymentid' => isset($memberpaymentid) ? $memberpaymentid : NULL,
      'response_transid' => $response_transid,
      'response_date_time' => $response_date_time, 
      'res_code' => $resCode,
      'res_message' => $this->resmesg->get_response_mesg($resCode),
      'status' => $status,
      'returndata' => json_encode($response),
      'returndate' => date('Y-m-d'),
      'returntime' => date('H:i:s')
    );
    $this->logpayment->update_log_id($update_log, $logid);
    header('Content-Type: application/json');
    header('Request-Ref: '.$response_transid);
    header('Transmit-Date-Time: '.$response_date_time);
    echo json_encode($response);
  }

  function get_microtime(){
    $t = microtime(true);
    $micro = sprintf("%06d",($t - floor($t)) * 1000000);
    $d = new DateTime( date('Y-m-d H:i:s.'.$micro, $t) );
    $time = $d->format("Y-m-d\TH:i:s.vP");
    return $time;
  }

}