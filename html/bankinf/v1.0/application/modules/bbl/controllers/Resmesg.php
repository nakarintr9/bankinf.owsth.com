<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Resmesg extends MY_Controller {

  function __construct()
  {
    parent::__construct();
  }

  function get_response_mesg($resCode){
    $resMesg['000'] = 'Success'; 
    $resMesg['052'] = 'Biller ID or Service Code not register';  
    $resMesg['054'] = 'System Unavailable';  
    $resMesg['210'] = 'Time out';  
    $resMesg['211'] = 'Invalid data'; 
    $resMesg['341'] = 'Service Provider not ready'; 
    $resMesg['888'] = 'Other Error'; 
    return $resMesg[$resCode];
  }


}
