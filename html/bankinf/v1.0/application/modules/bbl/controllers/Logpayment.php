<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Logpayment extends MY_Controller {

  function __construct()
  {
    parent::__construct();
    $this->load->library('multipledb');
  }

  function get_log($transid, $ref1, $ref2, $api_type, $action, $logid=''){
    $where = array(
      'request_transid'=>$transid,
      'ref1'=>$ref1,
      'ref2'=>$ref2,
      'api_type'=>$api_type,
      'action'=>$action
    );
    if(!empty($logid)){
      $where['ID !='] = $logid;
    }
    $qry = $this->multipledb->owstopup->select('*')
      ->from('log_bbl_ows')
      ->where($where)
      ->get()
      ->row();
    return $qry;
  }

  function update_log($update_log, $transid, $ref1, $ref2, $api_type, $action){
    $where = array(
      'request_transid'=>$transid,
      'ref1'=>$ref1,
      'ref2'=>$ref2,
      'api_type'=>$api_type,
      'action'=>$action
    );
    $this->multipledb->owstopup->where($where);
    $this->multipledb->owstopup->set($update_log);
    $res = $this->multipledb->owstopup->update('log_bbl_ows');
    return $res;
  }

  function update_log_id($update_log, $logid){
    $where = array(
      'ID'=>$logid,
    );
    $this->multipledb->owstopup->where($where);
    $this->multipledb->owstopup->set($update_log);
    $res = $this->multipledb->owstopup->update('log_bbl_ows');
    return $res;
  }

  function insert_log($insert){
    $this->multipledb->owstopup->insert('log_bbl_ows', $insert);
    $insert_id = $this->multipledb->owstopup->insert_id();
    return $insert_id;
  }
}