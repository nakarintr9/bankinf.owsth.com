<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Inquirypaymenttransaction extends MY_Controller {

  function __construct()
  {
    parent::__construct();
    $bbl_config = include(getcwd().'/../../../config/bankinf/bbl_config.php');
    $this->bbl_config = (array)$bbl_config;
    $this->BBL_URL = $this->bbl_config['BBL_URL'];
    $this->BBL_AUTH = $this->bbl_config['BBL_AUTH'];
    $this->BBL_PORT = $this->bbl_config['BBL_PORT'];
  }

  function test_(){
    echo "Server OK";
  }

  function test(){

    $curl = curl_init();

    curl_setopt_array($curl, array(
      CURLOPT_PORT => "5443",
      CURLOPT_URL => "https://119.46.64.166:5443/fwdgateway/rss/bbl/v1/transType/QRC_PYMT_TXN/transCode/INQUIRY_PPBP/clientId/MCCQR/providerId/QRCGW",
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_ENCODING => "",
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 30,
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_SSL_VERIFYPEER => false,
      CURLOPT_SSL_VERIFYHOST => false,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => "POST",
      CURLOPT_POSTFIELDS => "{\n    \"billerId\": \"010555002419415\",\n    \"transDate\": \"2019-02-13\",\n    \"amount\": \"100\",\n    \"reference1\": \"00004\",\n    \"reference2\": \"00001\"\n}",
      CURLOPT_HTTPHEADER => array(
        "Authorization: Basic d3N1c2VyLm1jY3FydHFyOlBhc3N3b3JkMTIz",
        "Content-Type: application/json",
        "Postman-Token: 0df9f528-9c38-4c2f-81c1-7da6e6c59003",
        "Request-Ref: 2019021311075101000170708",
        "Transmit-Date-Time: 2019-02-13T17:07:52.000+07:00",
        "cache-control: no-cache"
      ),
    ));

    $response = curl_exec($curl);
    $err = curl_error($curl);

    curl_close($curl);

    if ($err) {
      echo "cURL Error #:" . $err;
    } else {
      echo $response;
    }
  }


  function call($transDate, $ref1, $ref2, $amount, $RequestRef="2019021311075101000170708"){
    $billerId = "010555002419415";
    $transmit = date('Y-m-d\TH:i:s.000\+07:00');
    
    $data = array(
      'billerId' => $billerId,
      'transDate' => $transDate,
      'reference1' => $ref1,
      'reference2' => $ref2,
      'amount' => $amount
    );

    $postdata =  json_encode($data);

    $curl = curl_init();

    curl_setopt_array($curl, array(  
      CURLOPT_PORT => $this->BBL_PORT,
      CURLOPT_URL => $this->BBL_URL,
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_ENCODING => "",
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 30,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => "POST",
      CURLOPT_POSTFIELDS => $postdata,
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_SSL_VERIFYPEER => false,
      CURLOPT_SSL_VERIFYHOST => false,
      CURLOPT_HTTPHEADER => array(
        "Authorization: Basic ".$this->BBL_AUTH,
        "Content-Type: application/json",
        "Request-Ref: ".$RequestRef,
        "Transmit-Date-Time: ".$transmit,
      ),
    ));

    $response = curl_exec($curl);
    $err = curl_error($curl);

    curl_close($curl);

    if ($err) {
      echo "cURL Error #:" . $err;
    } else {
      echo "<pre>";
      var_dump(json_decode($response,true));
    }
  }
}