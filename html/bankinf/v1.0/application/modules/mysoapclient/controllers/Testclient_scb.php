<?php
class Testclient_scb extends MY_Controller {

  public function __construct() {
    parent::__construct();
    $this->load->library("nusoaplib"); //load the library here
  }

  public function test($value=''){
    $this->inquiry_api();
    echo "<pre>";
    echo "------------------------------------";
    $this->payment_api();
    echo "<pre>";
    echo "------------------------------------";
    $this->cancel_api();
    echo "<pre>";
    echo "------------------------------------";
  }

  public function inquiry_api(){
    global $wsdl, $client;

    $wsdl = 'http://alpha.owsth.com:5080/owstopup/scb_api/ows_inquiry_api.php?wsdl';

    $client = new nusoap_client($wsdl, true);
    // $inquiry = new stdClass();
    // $inquiry->tranID=date('YmdHis');
    // $inquiry->tranDate=date('Y-m-d H:i:s'); 
    // $inquiry->account='1113060335'; 
    // $inquiry->amount='100'; 
    // $inquiry->reference1='00001'; 
    // $inquiry->reference2='00004'; 
    // $inquiry->reference3=''; 
    $params = array(
      'tranID' => "123456",
      'tranDate' => "2015-05-30T18:00:00",
      'channel' => "ATM",
      'account' => "654321",
      'amount' => "20.25",
      'reference1' => "001",
      'reference2' => "00004",
      'reference3' => "00001"
    );
        
    try {
      $result = $client->call('verify', $params);
      echo '<h2>Result verify</h2><pre>';
      print_r($result);
      echo '</pre>';
    } catch (SoapFault $exception) {
      echo $exception;
    }
    echo '<h2>Request verify</h2><pre>' . htmlspecialchars($client->request, ENT_QUOTES) . '</pre>';
    echo '<h2>Response verify</h2><pre>' . htmlspecialchars($client->response, ENT_QUOTES) . '</pre>';
  }

  public function payment_api(){
    global $wsdl, $client;

    $wsdl = 'https://alpha.owsth.com/owstopup/scb_api/ows_payment_api.php?wsdl';

    $client = new nusoap_client($wsdl, 'wsdl');
    $inquiry = new stdClass();
    $inquiry->tranID='11789122';
    $inquiry->tranDate='2015-05-30T18:01:00'; 
    $inquiry->account='1113060335'; 
    $inquiry->amount='100'; 
    $inquiry->reference1='00001'; 
    $inquiry->reference2='00004'; 
    $inquiry->reference3=''; 
        
    try {
      $result = $client->call('confirm',array('request'=>$inquiry));
      echo '<h2>Result confirm</h2><pre>';
      print_r($result);
      echo '</pre>';
    } catch (SoapFault $exception) {
      echo $exception;
    }
    echo '<h2>Request confirm</h2><pre>' . htmlspecialchars($client->request, ENT_QUOTES) . '</pre>';
    echo '<h2>Response confirm</h2><pre>' . htmlspecialchars($client->response, ENT_QUOTES) . '</pre>';
  }

  public function cancel_api(){
    global $wsdl, $client;

    $wsdl = 'https://alpha.owsth.com/owstopup/scb_api/ows_cancel_api.php?wsdl';

    $client = new nusoap_client($wsdl, 'wsdl');
    $inquiry = new stdClass();
    $inquiry->tranID='11789122';
    $inquiry->tranDate='2015-05-30T18:01:00'; 
    $inquiry->account='1113060335'; 
    $inquiry->amount='100'; 
    $inquiry->reference1='00001'; 
    $inquiry->reference2='00004'; 
    $inquiry->reference3=''; 
        
    try {
      $result = $client->call('cancel',array('request'=>$inquiry));
      echo '<h2>Result cancel</h2><pre>';
      print_r($result);
      echo '</pre>';
    } catch (SoapFault $exception) {
      echo $exception;
    }
    echo '<h2>Request cancel</h2><pre>' . htmlspecialchars($client->request, ENT_QUOTES) . '</pre>';
    echo '<h2>Response cancel</h2><pre>' . htmlspecialchars($client->response, ENT_QUOTES) . '</pre>';
  }

}