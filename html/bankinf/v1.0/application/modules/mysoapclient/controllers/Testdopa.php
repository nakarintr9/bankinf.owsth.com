<?php
class Testdopa extends MY_Controller {

  public function __construct() {
    parent::__construct();
    $this->load->library("nusoap_library"); //load the library here

  }

  public function call($req, $params, $service){
    global $wsdl, $client;

    // $wsdl = 'https://idcard.bora.dopa.go.th/CheckCardStatus/CheckCardService.asmx?WSDL';
    $wsdl = getcwd().'/../../../config/dopa.wsdl';
    $client = new nusoap_client($wsdl, 'wsdl');

    try {
      $result = $client->call($service, $params);
    } catch (SoapFault $exception) {
      echo $exception;
    }
    echo '<h2>Request</h2><pre>' . htmlspecialchars($client->request, ENT_QUOTES) . '</pre>';
    echo '<h2>Response</h2><pre>' . htmlspecialchars($client->response, ENT_QUOTES) . '</pre>';
    $Desc =$this->get_string_between($client->response, '<Desc>', '</Desc>');
    // echo "<h2>Response To Array</h2><pre>";
    // var_dump($result);

    echo "<h2>Request Params</h2><pre>";
    foreach ($params as $key => $value) {
      echo $key." : ".$value."\r\n";
    }
    echo '</pre>';
    echo "<h2>Response</h2><pre>";
    echo $Desc;
    echo '</pre>';
  }

  function CheckCardByLaser($req){
    $service = 'CheckCardByLaser';
    $params =  array(
      'PID' => $req['NationalID'],
      'FirstName' => $req['ThaiFirstName'], 
      'LastName' => $req['ThaiLastName'], 
      'BirthDay' => $req['Birthdate'], 
      'Laser' => $req['LaserID'], 
    );
    $this->call($req, $params, $service);
  }

  function CheckCardByCID($req){
    $service = 'CheckCardByCID';
    $params = array(
      'ChipNo' => $req['ChipID'],
      'pid' => $req['NationalID'],
      'bp1no' => $req['RequestNo']
    );
    $this->call($req, $params, $service);
  }

  function test(){
    // $req[0] = array(
    //   "AtrString"=>"3B791800005448204E4944203130",
    //   "ChipID"=>"53502614301B2C83",
    //   "LaserID"=>"JT3103670712",
    //   "NationalID"=>"1909800113908",
    //   "ThaiFirstName"=>"1", 
    //   "ThaiLastName"=>"แก้วคำ", 
    //   "Birthdate"=>"25281009",
    //   "RequestNo"=>"10374297754/07061045",
    //   "PhotoRefNo"=>"10370407061045"
    // );
    // $req[1] = array(
    //   "AtrString"=>"3B781800005448204E49442039",
    //   "ChipID"=>"43432515260E28CF",
    //   "LaserID"=>"JT2090201581",
    //   "NationalID"=>"1909800113908",
    //   "ThaiFirstName"=>"ฐานันดร์", 
    //   "ThaiLastName"=>"แก้วคำ",
    //   "Birthdate"=>"25281009",
    //   "RequestNo"=>"61014058946/06220947",
    //   "PhotoRefNo"=>"61010306220947"
    // );
    // $req[2] = array(
    //   "AtrString"=>"3B799600005448204E4944203131",
    //   "ChipID"=>"7012FE126415269D",
    //   "LaserID"=>"ME0114630125",
    //   "NationalID"=>"1909800113908",
    //   "ThaiFirstName"=>"ฐานันดร์", 
    //   "ThaiLastName"=>"แก้วคำ",
    //   "Birthdate"=>"25281009",
    //   "RequestNo"=>"10174364578/05291349",
    //   "PhotoRefNo"=>"10170405291349"
    // );
    // $req[3] = array(
    //   "AtrString"=>"3B6800005448204E49442036",
    //   "ChipID"=>"2326028726964536",
    //   "LaserID"=>"JC2076747701",
    //   "NationalID"=>"1729800017854",
    //   "ThaiFirstName"=>"สุนีย์", 
    //   "ThaiLastName"=>"ขันทอง",
    //   "Birthdate"=>"25290218",
    //   "RequestNo"=>"10414245887/05081202",
    //   "PhotoRefNo"=>"10410405081202"
    // );
    // $req[0] = array(
    //   "AtrString"=>"",
    //   "ChipID"=>"",
    //   "LaserID"=>"ME1128783190",
    //   "NationalID"=>"1160100355649",
    //   "ThaiFirstName"=>"นครินทร์", 
    //   "ThaiLastName"=>"ธนมี",
    //   "Birthdate"=>"25350122",
    //   "RequestNo"=>"10414245887/05081202",
    //   "PhotoRefNo"=>"10410405081202"
    // );

    $req[0] = array(
      "AtrString"=>"",
      "ChipID"=>"",
      "LaserID"=>"JT3101174403",
      "NationalID"=>"1100501087980",
      "ThaiFirstName"=>"สุทธิชัย", 
      "ThaiLastName"=>"เฮี้ยะพงษ์",
      "Birthdate"=>"25370405",
      "RequestNo"=>"10414245887/05081202",
      "PhotoRefNo"=>"10410405081202"
    );
    foreach ($req as $key => $value) {
      echo "<h2>Original Params</h2><pre>";
      foreach ($value as $key2 => $value2) {
        echo $key2." : ".$value2."\r\n";
      }
      echo '</pre>';
      echo '<h2 style="color:#069">Service : CheckCardByCID</h2><pre>';
      echo '</pre>';
      $this->CheckCardByCID($value);
      echo '<h2 style="color:#1079">Service : CheckCardByLaser</h2><pre>';
      echo '</pre>';
      $this->CheckCardByLaser($value);
    }
  }

  function get_string_between($string, $start, $end){
    $string = ' ' . $string;
    $ini = strpos($string, $start);
    if ($ini == 0) return '';
    $ini += strlen($start);
    $len = strpos($string, $end, $ini) - $ini;
    return substr($string, $ini, $len);
  }

}