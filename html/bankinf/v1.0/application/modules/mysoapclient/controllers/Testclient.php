<?php
class Testclient extends MY_Controller {

  public function __construct() {
    parent::__construct();
    $this->load->library("nusoap_library"); //load the library here
    $t = microtime(true);
    $micro = sprintf("%06d",($t - floor($t)) * 1000000);
    $d = new DateTime( date('Y-m-d H:i:s.'.$micro, $t) );
    $time = $d->format("u");
    $rand = rand(100,999);
    $bankref = $d->getTimestamp().$time.$rand;
    $this->bankref1 = '1 '.$bankref;
    $this->bankref2 = '2 '.$bankref;
    $this->bankref3 = '3 '.$bankref;
    $this->tranxid = $bankref;
  }

  public function call(){
    global $wsdl, $client;

    // $wsdl = 'https://v5tst1.owsth.com:21329/callback/ktb/billpayment?wsdl';
    $wsdl = 'https://owsth.com/bankinf/ktb/billpayment?wsdl';

    $client = new nusoap_client($wsdl, 'wsdl');

    // $InquiryRequestData =  array(
    //   'user' => 'user',
    //   'password' => 'password', 
    //   'comcode' => '1010', 
    //   'prodcode' => '1010', 
    //   'command' => 'Inquiry', 
    //   'bankcode' => '6', 
    //   'bankref' => 'K00002 00004391', 
    //   'datetime' => date('YmdHis'), 
    //   'effdate' => date('Ymd'), 
    //   'channel' => '2', 
    //   'ref1' => '00004', 
    //   'ref2' => '00001', 
    //   'ref3' => '-', 
    //   'ref4' => '-'
    // );

    $inquiry = new stdClass();
    $inquiry->user='mcktb2017';
    $inquiry->password='M3d!4@C3NT3R'; 
    $inquiry->comcode='1010'; 
    $inquiry->prodcode='1010'; 
    $inquiry->command='Inquiry'; 
    $inquiry->bankcode='6'; 
    $inquiry->bankref=$this->bankref1; 
    $inquiry->datetime=date('YmdHis'); 
    $inquiry->effdate=date('Ymd'); 
    $inquiry->channel='2'; 
    $inquiry->ref1='00004'; 
    $inquiry->ref2='00001'; 
    $inquiry->ref3='-'; 
    $inquiry->ref4='-';
        
    try {
      $result = $client->call('inquiry',array('request'=>$inquiry));
      echo '<h2>Result Inquiry</h2><pre>';
      print_r($result);
      echo '</pre>';
    } catch (SoapFault $exception) {
      echo $exception;
    }
    echo '<h2>Request Inquiry</h2><pre>' . htmlspecialchars($client->request, ENT_QUOTES) . '</pre>';
    echo '<h2>Response Inquiry</h2><pre>' . htmlspecialchars($client->response, ENT_QUOTES) . '</pre>';
    //echo '<h2>Debug</h2><pre>' . htmlspecialchars($client->debug_str, ENT_QUOTES) . '</pre>'; //this generates a lot of text!

    $approval = new stdClass();
    $approval->user='mcktb2017';
    $approval->password='M3d!4@C3NT3R'; 
    $approval->comcode='1010'; 
    $approval->prodcode='1010'; 
    $approval->command='Approval'; 
    $approval->bankcode='6'; 
    $approval->bankref=$this->bankref2; 
    $approval->datetime=date('YmdHis'); 
    $approval->effdate=date('Ymd');
    $approval->amount='1000.00'; 
    $approval->channel='2'; 
    $approval->ref1='00004'; 
    $approval->ref2='00001'; 
    $approval->ref3='-'; 
    $approval->ref4='-';
    
    try {
      $resultApproval = $client->call('approval',array('request'=>$approval));
      echo '<h2>Result Approval</h2><pre>';
      print_r($resultApproval);
      echo '</pre>';
    } catch (SoapFault $exception) {
      echo $exception;
    }
    echo '<h2>Request Approval</h2><pre>' . htmlspecialchars($client->request, ENT_QUOTES) . '</pre>';
    echo '<h2>Response Approval</h2><pre>' . htmlspecialchars($client->response, ENT_QUOTES) . '</pre>';

    $payment = new stdClass();
    $payment->tranxid=$resultApproval['tranxid'];
    $payment->user='mcktb2017';
    $payment->password='M3d!4@C3NT3R'; 
    $payment->comcode='1010'; 
    $payment->prodcode='1010'; 
    $payment->command='payment'; 
    $payment->bankcode='6'; 
    $payment->bankref=$this->bankref3; 
    $payment->datetime=date('YmdHis'); 
    $payment->effdate=date('Ymd');
    $payment->amount='1000.00'; 
    $payment->cusname=$resultApproval['cusname']; 
    $payment->channel='2'; 
    $payment->ref1='00004'; 
    $payment->ref2='00001'; 
    $payment->ref3='-'; 
    $payment->ref4='-';
    
    try {
      $result_payment = $client->call('payment',array('request'=>$payment));
      echo '<h2>Result Payment</h2><pre>';
      print_r($result_payment);
      echo '</pre>';
    } catch (SoapFault $exception) {
      echo $exception;
    }
    echo '<h2>Request Payment</h2><pre>' . htmlspecialchars($client->request, ENT_QUOTES) . '</pre>';
    echo '<h2>Response Payment</h2><pre>' . htmlspecialchars($client->response, ENT_QUOTES) . '</pre>';


    $approve_reversal = new stdClass();
    $approve_reversal->tranxid=$this->tranxid;
    $approve_reversal->user='mcktb2017';
    $approve_reversal->password='M3d!4@C3NT3R'; 
    $approve_reversal->comcode='1010'; 
    $approve_reversal->prodcode='1010'; 
    $approve_reversal->command='approvereversal'; 
    $approve_reversal->bankcode='6'; 
    $approve_reversal->bankref=$this->bankref3; 
    $approve_reversal->datetime=date('YmdHis'); 
    $approve_reversal->effdate=date('Ymd');
    $approve_reversal->amount='1000.00'; 
    $approve_reversal->channel='2'; 
    $approve_reversal->ref1='00004'; 
    $approve_reversal->ref2='00001'; 
    $approve_reversal->ref3='-'; 
    $approve_reversal->ref4='-';
    
    try {
      $result_approve_reversal = $client->call('approvereversal',array('request'=>$approve_reversal));
      echo '<h2>Result Approve Reversal</h2><pre>';
      print_r($result_approve_reversal);
      echo '</pre>';
    } catch (SoapFault $exception) {
      echo $exception;
    }
    echo '<h2>Request Approve Reversal</h2><pre>' . htmlspecialchars($client->request, ENT_QUOTES) . '</pre>';
    echo '<h2>Response Approve Reversal</h2><pre>' . htmlspecialchars($client->response, ENT_QUOTES) . '</pre>';

    $reversal = new stdClass();
    $reversal->tranxid=$result_approve_reversal['tranxid'];
    $reversal->user='mcktb2017';
    $reversal->password='M3d!4@C3NT3R'; 
    $reversal->comcode='1010'; 
    $reversal->prodcode='1010'; 
    $reversal->command='reversal'; 
    $reversal->bankcode='6'; 
    $reversal->bankref=$this->bankref3; 
    $reversal->datetime=date('YmdHis'); 
    $reversal->effdate=date('Ymd');
    $reversal->amount='1000.00'; 
    $reversal->cusname=''; 
    $reversal->channel='2'; 
    $reversal->ref1='00004'; 
    $reversal->ref2='00001'; 
    $reversal->ref3='-'; 
    $reversal->ref4='-';
    
    try {
      $result = $client->call('reversal',array('request'=>$reversal));
      echo '<h2>Result Reversal</h2><pre>';
      print_r($result);
      echo '</pre>';
    } catch (SoapFault $exception) {
      echo $exception;
    }
    echo '<h2>Request Reversal</h2><pre>' . htmlspecialchars($client->request, ENT_QUOTES) . '</pre>';
    echo '<h2>Response Reversal</h2><pre>' . htmlspecialchars($client->response, ENT_QUOTES) . '</pre>';   


  }

}