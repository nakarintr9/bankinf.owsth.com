<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Test extends MY_Controller {

  function __construct() {
    parent::__construct();
    $test_payment_config = include(getcwd().'/../../../config/bankinf/test_payment_config.php');
    $this->test_payment_config = (array)$test_payment_config;
  }

  function payment_ksherpay1($ref1="100002", $ref2="0009900001", $amount="490", $channel="KSher"){
    $attach = $ref1.$ref2;
    $order = date('YmdHis').rand(1000,9999);
    $data = array (
      'channel' => $channel,
      'openid' => 'o2G4c08HOgiY-xQoglXDC9g-lh08',
      'channel_order_no' => '4200000282201904020362907975',
      'operator_id' => 3428,
      'cash_fee_type' => 'CNY',
      'ksher_order_no' => $order,
      'nonce_str' => 'dGVz2aVJ7buKo7F3PN55r5Jhu2CGwVmC',
      'time_end' => '2019-04-02 22:18:53',
      'fee_type' => 'THB',
      'attach' => $attach,
      'rate' => 0.2126,
      'result' => 'SUCCESS',
      'total_fee' => $amount*100,
      'appid' => 'mch21377',
      'operation' => 'JSAPI-PAY',
      'device_id' => '',
      'cash_fee' => '1',
      'order_no' => $order,
      'mch_order_no' => $order,
    );

    $post = array (
      'code' => 0,
      'version' => '2.0.0',
      'status_code' => '',
      'msg' => 'ok',
      'time_stamp' => '',
      'status_msg' => '',
      'data' => $data,
      'sign' => '3ac888e05375b9c74054917f88b458a0452abd53a583c127f1773fb82e11efe163a9c67d1e9816791eb9f2ad54d5e4cee448306a44f52966c2187ee6b9a858a5',
    );

    $curl = curl_init();

    curl_setopt_array($curl, array(
      CURLOPT_URL => $this->test_payment_config['base_url_test']."/ksherpay/notifypayment",
      CURLOPT_RETURNTRANSFER => true,      
      CURLOPT_SSL_VERIFYPEER => false,
      CURLOPT_SSL_VERIFYHOST => false,
      CURLOPT_ENCODING => "",
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 30,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => "POST",
      CURLOPT_POSTFIELDS => json_encode($post),
      CURLOPT_HTTPHEADER => array(
        "Content-Type: application/application/json"
      ),
    ));

    $response = curl_exec($curl);
    $err = curl_error($curl);

    curl_close($curl);

    if ($err) {
      echo "cURL Error #:" . $err;
    } else {
      echo $response;
    }
  }

  function refund_ksherpay($channel= "wechat", $channel_order_no='1', $order_no='1', $refund_fee=1, $total_fee=1){
    $curl = curl_init();

    curl_setopt_array($curl, array(
      CURLOPT_URL => $this->test_payment_config['base_url_test']."/ksherpay/refundorder/refund",
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_ENCODING => "",
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 30,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => "POST",
      CURLOPT_POSTFIELDS => "channel=".$channel."&channel_order_no=".$channel_order_no."&ksher_order_no=".$order_no."&mch_order_no=".$order_no."&refund_fee=".$refund_fee."&total_fee=".$total_fee,
      CURLOPT_HTTPHEADER => array(
        "Content-Type: application/x-www-form-urlencoded",
        "cache-control: no-cache"
      ),
    ));

    $response = curl_exec($curl);
    $err = curl_error($curl);

    curl_close($curl);

    if ($err) {
      echo "cURL Error #:" . $err;
    } else {
      echo $response;
    }
  }

  function payment_ksherpay(){
    $appid="123456789";
    $sub_openid="aadfsdfsdfs";
    $channel_sub_appid="sfasdfasdfasdf";
    $this->load->module('ksherpay/kshersignature');
    $data = array(
        'version'           => '2.0.0',
        'time_stamp'        => date("YmdHis",time()),
        'appid'             => $appid,
        'nonce_str'         => $this->kshersignature->generate_nonce_str(16),
        'mch_order_no'      => date("YmdHis",time()),
        'fee_type'          => 'THB',
        'channel'           => 'wechat',
    );
    
    var_dump($data);

    $sign = $this->kshersignature->makeSign($data);
    $request_data = array(
      'sign' => $sign,
      'data' => $data
    );

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_URL, $this->test_payment_config['base_url_test']."/ksherpay/notifypayment");
    curl_setopt($ch, CURLOPT_POSTFIELDS, $request_data);
    $output = curl_exec($ch);
    curl_close($ch);

    var_dump($output);
  }
}
