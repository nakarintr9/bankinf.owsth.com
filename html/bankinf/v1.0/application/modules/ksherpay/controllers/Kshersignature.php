<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Kshersignature extends MY_Controller {
  public function __construct(){
    parent::__construct();    
    $this->load->library('Php_signature');
    $this->private_key_content = file_get_contents(getcwd().'/../../../config/bankinf/ksherpay/mch_privkey.pem');
    $this->public_key_content = file_get_contents(getcwd().'/../../../config/bankinf/ksherpay/dh_pubkey.pem');
  }

  public function makeSign($data){
      $message = "";
      ksort($data); # array key sorting
      foreach ($data as $key => $value) $message .= $key."=".$value;

      $message = mb_convert_encoding($message, "UTF-8");

      $private_key = openssl_get_privatekey($this->private_key_content);
      openssl_sign($message, $encoded_sign, $private_key,OPENSSL_ALGO_MD5);
      openssl_free_key($private_key);
      $encoded_sign = bin2hex($encoded_sign);
      return $encoded_sign;
  }

  public function makeVerify($data, $sign){
      $sign = pack("H*",$sign);

      $message = "";
      ksort($data); # array key sorting
      foreach ($data as $key => $value) $message .= $key."=".$value;

      $message = mb_convert_encoding($message, "UTF-8");

      $res = openssl_get_publickey($this->public_key_content);
      $result = openssl_verify($message, $sign, $res,OPENSSL_ALGO_MD5);
      openssl_free_key($res);
      return $result;
  }

  function generate_nonce_str($len) {
    $nonce_str = "";
    $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"; 
    for ( $i = 0; $i < $len; $i++ ) { 
      $nonce_str .= $chars[ mt_rand(0, strlen($chars) - 1) ]; 
    } 
    return $nonce_str;
  }

}