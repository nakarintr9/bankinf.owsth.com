<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Refundorder extends MY_Controller {

  function __construct()
  {
    parent::__construct();
    $this->load->module('ksherpay/resmesg');
    $this->load->module('common/qrypayment');
    $this->load->module('ksherpay/logpayment');
    $this->load->module('common/payment_migrate');
    $ksherpay_config = include(getcwd().'/../../../config/bankinf/ksherpay_config.php');
    $this->ksherpay_config = (array)$ksherpay_config;
    $this->load->module('ksherpay/kshersignature');
  }

  function refund(){
    $body = [];
    $rawPost = file_get_contents('php://input');
    mb_parse_str($rawPost, $body);

    $appid="mch26495";
    $version= isset($body['version']) ? $body['version'] : '2.0.0';
    $channel= isset($body['channel']) ? $body['channel'] : '';
    $channel_order_no = isset($body['channel_order_no']) ? $body['channel_order_no'] : '';
    $ksher_order_no = isset($body['ksher_order_no']) ? $body['ksher_order_no'] : '';
    $mch_order_no = isset($body['mch_order_no']) ? $body['mch_order_no'] : '';
    $refund_fee = isset($body['refund_fee']) ? $body['refund_fee'] : 0;
    $total_fee = isset($body['total_fee']) ? $body['total_fee'] : 0;

    $production = 0;
    $nonce_str = $this->kshersignature->generate_nonce_str(32);
    $refund_no = date("YmdHis").time();
    $timestamp = date("YmdHis").time();

    $data = array(
      'appid' => $appid,
      'channel' => $channel,
      'channel_order_no' => $channel_order_no,
      'fee_type' => 'THB',
      'ksher_order_no' => $ksher_order_no,
      'mch_refund_no' => $refund_no,
      'mch_order_no' => $mch_order_no,
      'nonce_str' => $nonce_str,
      'refund_fee' => 5,
      'total_fee' => 5,
      'time_stamp' => $timestamp,
      'version' => $version,
    );
    $signature = $this->kshersignature->makeSign($data);

    $data['sign'] = $signature;

    echo "*** REQUEST API BELOW ***";
    echo "<pre>";
    print_r($data);
    echo "<pre>";

    $micro_getrefund_url = "http://api.mch.ksher.net/KsherPay/order_refund";

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_URL, $micro_getrefund_url);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
    $output = curl_exec($ch);

    $itemreturn = json_decode($output,true);
    echo "*** RETURNS API BELOW ***";
    echo "<pre>";
    print_r($itemreturn); 
    echo "<pre>";
  }
}