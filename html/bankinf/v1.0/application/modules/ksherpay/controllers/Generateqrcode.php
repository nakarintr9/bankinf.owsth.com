<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Generateqrcode extends MY_Controller {

  function __construct()
  {
    parent::__construct();
    $this->load->module('ksherpay/resmesg');
    $this->load->module('common/qrypayment');
    $this->load->module('ksherpay/logpayment');
    $this->load->module('common/payment_migrate');
    $this->ksherpay_config = include(getcwd().'/../../../config/bankinf/ksherpay_config.php');
    $this->load->module('ksherpay/kshersignature');
    $this->load->library('errorcode');
  }

  function gen($body){
    $appid = $this->ksherpay_config->appid;
    $version = $this->ksherpay_config->version;
    $notify_url = $this->ksherpay_config->notify_url;
    $orderid = isset($body['orderid']) ? $body['orderid'] : date('YmdHis');
    $channel = $body['channel']; 
    $amount = ($body['amount'])*100;
    $amount = number_format($amount,0,'','');
    $timestamp = date("YmdHis").time();
    $nonce_str = $this->kshersignature->generate_nonce_str(32);
    $ref1 = isset($body['ref1']) ? $body['ref1'] : '';
    $ref2 = isset($body['ref2']) ? $body['ref2'] : ''; 
    $attach = $ref1.$ref2;
    $data = array(
      'appid' => $appid,
      'channel' => $channel,
      'fee_type' => 'THB',
      'img_type' => "png",
      'mch_order_no' => $orderid,
      'nonce_str' => $nonce_str,
      'notify_url' => $notify_url,
      'total_fee' => $amount,
      'attach' => $attach,
      'time_stamp' => $timestamp,
      'version' => $version,
    );

    $signature = $this->kshersignature->makeSign($data);

    $data['sign'] = $signature;

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_URL, $this->ksherpay_config->generate_qrcode_url);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
    $output = curl_exec($ch);

    $err = curl_error($ch);
    if(!$err){
      $itemreturn = json_decode($output,true);
      $QRCode = isset($itemreturn['data']['imgdat']) ? $itemreturn['data']['imgdat'] : '';
      $broker_error = isset($itemreturn['data']['err_msg']) ? $itemreturn['data']['err_msg'] : '';
      if(!empty($QRCode)){
        $error_code = 'E00';
      }else{
        $error_code = 'E64';
      }      
    }else{
      $error_code = 'E14';
    }

    
    $return = array(
      'error_code' => $error_code,
      'error_text' => $this->errorcode->$error_code,
      'qr_image' => $QRCode,
      'broker_error' => isset($broker_error) ? $broker_error : ''
    );

    return (object)$return;
    
    /*
    echo "*** RETURNS API BELOW ***";
    echo "<pre>";
    print_r($itemreturn); 
    echo "<pre>";

    echo '<img src="'.$QRCode.'">';
    */
    
  }
}