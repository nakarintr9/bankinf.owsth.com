<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Notifypayment extends MY_Controller {

  function __construct()
  {
    parent::__construct();
    $this->load->module('ksherpay/resmesg');
    $this->load->module('common/qrypayment');
    $this->load->module('ksherpay/logpayment');
    $this->load->module('common/payment_migrate');
    $ksherpay_config = include(getcwd().'/../../../config/bankinf/ksherpay_config.php');
    $this->ksherpay_config = (array)$ksherpay_config;
    $this->load->module('ksherpay/kshersignature');
  }
  
  function index(){
    $postdata = file_get_contents("php://input");
    $body = json_decode($postdata, true);

    // $body = [];
    // $rawPost = file_get_contents('php://input');
    // mb_parse_str($rawPost, $body);

    $data = $body['data'];
    $sign = $body['sign'];

    $username = "KSherBillPayment";
    $bankcode = isset($data['channel']) ? $data['channel'] : 'ksher';

    $Channel_name = "KSher";

    if($bankcode == "alipay"){
      $username = "AlipayBillPayment";
      $bankcode = "alipay";
      $Channel_name = "Alipay";
    }
    else if($bankcode == "wechat"){
      $username = "WechatBillPayment";
      $bankcode = "wechat";
      $Channel_name = "Wechat";
    }
    else{
      $username = "KSherBillPayment";
      $bankcode = "ksher";
      $Channel_name = "KSher";
    }

    $result = "SUCCESS";
    $msg = "OK";
    $api_type = 'payment';
    $detail = 'เติมเครดิตด้วย '.$Channel_name.' Bill PAYMENT';
    $addby= $Channel_name.' Bill Payment';
    $channel = 'อื่นๆ';
    $action = 'notify';
    $REF1 = '';
    $REF2 = '';
    $ref1 = '';
    $ref2 = '';
    $amount = isset($data['total_fee']) ? $data['total_fee']/100 : 0;
    $amount = number_format($amount,4,'.','');
    $paymenttype = 'member';
    $powerbankFlag = false;
    $wave_fee = true;
    $smartapi = false;
    $e_commerce = false;
    $trans_log_id = 'KSher'.date('YmdHisu').rand(100,999);
    $transid = isset($data['order_no']) ? $data['order_no'] : '';
    $openid = isset($data['openid']) ? $data['openid'] : '';
    $FROM_BANK_CODE = $bankcode;
    $FROM_ACC_NAME = $openid." ".$FROM_BANK_CODE;
    $smart_ref1 = isset($data['attach']) ? $data['attach'] : '';
    
    $verify = $this->kshersignature->makeVerify($data, $sign);
    if(!$verify){
      $result = "FAIL";
      $msg = "Verify Signature Failed";
    }
    

    if($result == "SUCCESS"){
      $attach = isset($data['attach']) ? $data['attach'] : '';
      if(!empty($attach)){
        if(strlen($attach) >= 6){
          $REF1 = substr($attach, 0, 6);
        }else{
          $result = "FAIL";
          $msg = "Invalid Attach Format ".$attach;
        }
      }else{
        $attach = '100008'.$transid;      
        $REF1 = substr($attach, 0, 6);
      }
    }

    if($result == "SUCCESS"){
      if(strlen($attach) >= 16){
        $REF2 = substr($attach, 6, 10);
      }else{
        $result = "FAIL";
        $msg = "Invalid Attach Format For Service ".$REF1;
      }
    }

    if($result == "SUCCESS"){
      if($REF1 == "100001"){
        $smartname = "Powerbank";
        $list_get_qr_powerbank = $this->payment_migrate->get_list_get_qr_powerbank($REF1, $REF2, $trans_log_id);
        if($list_get_qr_powerbank){
          if($list_get_qr_powerbank['error_code'] == 'E00'){
            $ref1 = $list_get_qr_powerbank['list_get_qr_powerbank']['ca_ref1'];
            $ref2 = $list_get_qr_powerbank['list_get_qr_powerbank']['ca_ref2'];
            $powerbankFlag = true;
            $wave_fee =  true;
          }
        }
        else{
          $result = "FAIL";
          $msg = "Internal Error (Create CA Failed)";
        }
      } 
      else if($REF1 == "100002"){
        $smartname = "Powerbank";
        $list_get_qr_powerbank = $this->payment_migrate->create_new_ca_from_bankinf($REF2, $FROM_ACC_NAME, $FROM_BANK_CODE, $trans_log_id);
        if($list_get_qr_powerbank){
          if($list_get_qr_powerbank['error_code'] == 'E00'){
            $ref1 = $list_get_qr_powerbank['list_get_qr_powerbank']['ca_ref1'];
            $ref2 = $list_get_qr_powerbank['list_get_qr_powerbank']['ca_ref2'];
            $powerbankFlag = true;
            $wave_fee =  true;
          }
          else{
            $ref1 = $list_get_qr_powerbank['ca_ref1'];
            $ref2 = $list_get_qr_powerbank['ca_ref2'];
            $wave_fee =  true;
          }
        }
        else{
          $result = "FAIL";
          $msg = "Internal Error (Create CA Failed)";
        }
      } 
      else if($REF1 == "100003" || $REF1 == "100004" || $REF1 == "100005" || $REF1 == "100007"){
        if($REF1 == '100003'){
          $smartname = "Smart Wash";
        }else if($REF1 == "100004"){
          $smartname = "Smart BMI";
        }else if($REF1 == "100005"){
          $smartname = "Smart Coin";
        }else if($REF1 == "100007"){
          $smartname = "Smart Game";
        }else{
          $smartname = "Smarts NULL";
        }
        $ca_detail = $this->payment_migrate->create_ca_by_ref2($REF2, $FROM_ACC_NAME, $FROM_BANK_CODE, $trans_log_id);
        if($ca_detail){
          $ref1 = $ca_detail['ca_ref1'];
          $ref2 = $ca_detail['ca_ref2'];
          $smart_type = $REF1;
          $smart_ref1 = substr($REF2, 0, 5).'-'.substr($REF2, 5, 5);
          $smart_ref2 = sprintf("%05d",$ref1).'-'.sprintf("%05d",$ref2);
          if($REF1 == "100007"){
            $smart_ref2.= '-'.substr($REF2, 10, 5);
          }
          $smartapi = true;
          $wave_fee =  true;
        }
        else{
          $result = "FAIL";
          $msg = "Internal Error (Create CA Failed)";
        }
      }
      else if($REF1 == "100008"){
        $ca_detail = $this->payment_migrate->create_ca_by_ref2($REF2, $FROM_ACC_NAME, $FROM_BANK_CODE, $trans_log_id);
        if($ca_detail){
          $ref1 = $ca_detail['ca_ref1'];
          $ref2 = $ca_detail['ca_ref2'];
          $smart_ref2 = sprintf("%05d",$ref1).'-'.sprintf("%05d",$ref2);

          $e_commerce = true;
          $REF2 = substr($attach, 6, (strlen($attach)-6));
          $e_commerce_order = $this->payment_migrate->get_list_e_commerce_order($REF1, $REF2, $trans_log_id);
          if($e_commerce_order){
            if($e_commerce_order['error_code'] == 'E00'){
              $payee_ref1 = $e_commerce_order['e_commerce_order']['ref1'];
              $payee_ref2 = $e_commerce_order['e_commerce_order']['ref2'];
              $smart_ref1 = sprintf("%05d",$payee_ref1).'-'.sprintf("%05d",$payee_ref2);
              if($bankcode == "ksher"){
                $bankcode = $e_commerce_order['e_commerce_order']['channel'];
                if($bankcode == "alipay"){
                  $username = "AlipayBillPayment";
                  $bankcode = "alipay";
                  $Channel_name = "Alipay";
                }
                else if($bankcode == "wechat"){
                  $username = "WechatBillPayment";
                  $bankcode = "wechat";
                  $Channel_name = "Wechat";
                }
                else{
                  $username = "KSherBillPayment";
                  $bankcode = "ksher";
                  $Channel_name = "KSher";
                }
                $detail = 'เติมเครดิตด้วย '.$Channel_name.' Bill PAYMENT';
                $addby= $Channel_name.' Bill Payment';
                $FROM_BANK_CODE = $bankcode;
                $FROM_ACC_NAME = $openid." ".$FROM_BANK_CODE;
              }
            }else{
              $result = "FAIL";
              $msg = "Internal Error (Get Order Failed) 1";
            }
          }
          else{
            $result = "FAIL";
            $msg = "Internal Error (Get Order Failed) 2";
          }
        }
        else{
          $result = "FAIL";
          $msg = "Internal Error (Create CA Failed)";
        }
      } 
      else{
        $result = "FAIL";
        $msg = "Invalid Attach Data (Service ".$REF1." is not available)";
      }
    }

    $order_no = isset($data['order_no']) ? $data['order_no'] : '';
    $ksher_order_no = isset($data['ksher_order_no']) ? $data['ksher_order_no'] : '';
    if(empty($order_no) || empty($ksher_order_no)){
      $result = "FAIL";
      $msg = "order_no or ksher_order_no is empty";
    }

    $insert_log = array(
      'version' =>  isset($body['version']) ? $body['version'] : '',
      'channel' => $bankcode,
      'openid' => isset($data['openid']) ? $data['openid'] : '',
      'channel_order_no' => isset($data['channel_order_no']) ? $data['channel_order_no'] : '',
      'operator_id' => isset($data['operator_id']) ? $data['operator_id'] : '',
      'cash_fee_type' => isset($data['cash_fee_type']) ? $data['cash_fee_type'] : '',
      'ksher_order_no' => isset($data['ksher_order_no']) ? $data['ksher_order_no'] : '',
      'nonce_str' => isset($data['nonce_str']) ? $data['nonce_str'] : '',
      'time_end' => isset($data['time_end']) ? $data['time_end'] : '',
      'fee_type' => isset($data['fee_type']) ? $data['fee_type'] : '',
      'attach' => isset($data['attach']) ? $data['attach'] : '',
      'rate' => isset($data['rate']) ? $data['rate'] : '',
      'result' => isset($data['result']) ? $data['result'] : '',
      'total_fee' => isset($data['total_fee']) ? $data['total_fee'] : '',
      'appid' => isset($data['appid']) ? $data['appid'] : '',
      'operation' => isset($data['operation']) ? $data['operation'] : '',
      'device_id' => isset($data['device_id']) ? $data['device_id'] : '',
      'cash_fee' => isset($data['cash_fee']) ? $data['cash_fee'] : '',
      'order_no' => isset($data['order_no']) ? $data['order_no'] : '',
      'mch_order_no' => isset($data['mch_order_no']) ? $data['mch_order_no'] : '',
      'api_type' => $api_type,
      'action' => $REF1,
      'ref1' => isset($ref1) ? $ref1 : '',
      'ref2' => isset($ref2) ? $ref2 : '',
      'amount' => number_format($amount,4,'.',''),
      'min_amount' => number_format($amount,4,'.',''),
      'max_amount' => number_format($amount,4,'.',''),
      'paymenttype' => $paymenttype,
      'status' => 0,
      'request_transid' => $transid,
      'postdata' => json_encode($body),
      'adddate' => date('Y-m-d'),
      'addtime' => date('H:i:s')
    );

    $logid = $this->logpayment->insert_log($insert_log);

    if($result == "SUCCESS"){
      $logconfirm = $this->logpayment->get_log($ksher_order_no, $logid);
      if(!empty($logconfirm)){
        $result = "FAIL";
        $msg = "Dubplicate Transaction ".$transid;
      }
    }

    if($result == "SUCCESS"){
      $partner = $this->qrypayment->get_partner($ref1);
      if(empty($partner)){
        $result = "FAIL";
        $msg = "Unknown Reference Data (Internal CA REF_1)";
      }
    }

    if($result == "SUCCESS"){
      $member = $this->qrypayment->get_member($ref1, $ref2);
      if(empty($member)){
        $result = "FAIL";
        $msg = "Unknown Reference Data (Internal CA REF_2)";
      }
    }

    if($result == "SUCCESS"){
      $res = $this->qrypayment->payment_credit_member($ref1, $ref2, $amount, $member, $partner, $detail, $addby, $channel, $bankcode, $wave_fee, $trans_log_id);
      if($res['error_code'] == 'E00'){
        $partnerpaymentid = isset($res['partnerpaymentid']) ? $res['partnerpaymentid'] : '';
        $memberpaymentid = isset($res['memberpaymentid']) ? $res['memberpaymentid'] : '';
        if($powerbankFlag){
          $payment_type = $list_get_qr_powerbank['list_get_qr_powerbank']['payment_type'];
          $order_id = $list_get_qr_powerbank['list_get_qr_powerbank']['id'];
          $device_id = $list_get_qr_powerbank['list_get_qr_powerbank']['device_id'];
          $pooup_result = $this->payment_migrate->pop_powerbank_from_bankinf($payment_type, $order_id, $trans_log_id);
          $this->load->module('common/alert_line');
          $alert_message = "\r\nService: Powerbank (".$addby.")";
          $alert_message.= "\r\nAmount: $amount";
          $alert_message.= "\r\nDevice ID: $device_id";
          $alert_message.= "\r\nRef1: ".sprintf("%05d",$ref1);
          $alert_message.= "\r\nRef2: ".sprintf("%05d",$ref2);
          $alert_message.= "\r\nResult Code: ".$pooup_result['error_code'];
          $alert_message.= "\r\nResult Text: ".$pooup_result['error_text'];
          $this->alert_line->notify_message_powerbank($alert_message);
        }
        else if($smartapi){
          $smartapiRes = $this->payment_migrate->smartapi_by_ref1_ref2($smart_type, $smart_ref1, $smart_ref2, $amount, $trans_log_id);
          $this->load->module('common/alert_line');
          $alert_message = "\r\nService: $smartname (".$addby.")";
          $alert_message.= "\r\nAmount: $amount";
          $alert_message.= "\r\nDevice ID: $smart_ref1";
          $alert_message.= "\r\nRef1: ".sprintf("%05d",$ref1);
          $alert_message.= "\r\nRef2: ".sprintf("%05d",$ref2);
          $alert_message.= "\r\nResult Code: ".$smartapiRes['error_code'];
          $alert_message.= "\r\nResult Text: ".$smartapiRes['error_text'];
          $this->alert_line->notify_message_powerbank($alert_message);
        }
        else{
          if($ref1 == '00102'){
            $payment_doll_result = $this->payment_migrate->payment_doll($ref1, $ref2, $amount);
          }
        } 

        if(!empty($partnerpaymentid) && !empty($memberpaymentid) && is_numeric($partnerpaymentid) && is_numeric($memberpaymentid)){
          $update = array(
            'bank_ref1' => isset($data['order_no']) ? $data['order_no'] : '',
            'bank_ref2' => isset($data['channel_order_no']) ? $data['channel_order_no'] : '',
            'bank_ref3' => isset($smart_ref1) ? $smart_ref1 : ''
          );
          $where = ' ID in ('.$partnerpaymentid.','.$memberpaymentid.') ';
          $this->qrypayment->update_payment_bank($update, $where);
        }
      }else{
        $result = "FAIL";
        $msg = "Internal Error (Payment Fail)";
      }
    }

    if($e_commerce){

      $notify_e_commerce = $this->payment_migrate->notify_e_commerce($ref1, $ref2, $result, $FROM_BANK_CODE, $amount, $REF2, $trans_log_id);
      $this->load->module('common/alert_line');
      $alert_message = "\r\nService: E-Commerce Payment (".$addby.")";
      $alert_message.= "\r\nAmount: $amount";
      $alert_message.= "\r\nPayee: ".$smart_ref1;
      $alert_message.= "\r\nPayer: ".$smart_ref2;
      $alert_message.= "\r\nResult Code: ".$notify_e_commerce['error_code'];
      $alert_message.= "\r\nResult Text: ".$notify_e_commerce['error_text'];
      $this->alert_line->notify_message_powerbank($alert_message);
    }

    if($result == "SUCCESS"){
      $status = 1;
    }else{
      $status = 2;
    }

    if($status == 2 && ($smartapi==true or $powerbankFlag==true)){
      $this->load->module('common/alert_line');
      if($powerbankFlag){
        $device_id = isset($list_get_qr_powerbank['list_get_qr_powerbank']['device_id']) ? $list_get_qr_powerbank['list_get_qr_powerbank']['device_id'] : '';
      }else{
        $device_id = isset($smart_ref1) ? $smart_ref1 : '';
      }
      $alert_message = "\r\nService: $smartname (KSher-Payment-".$FROM_BANK_CODE.") ไม่สำเร็จ!";
      $alert_message.= "\r\nAmount: $amount";
      $alert_message.= "\r\nDevice ID: $device_id";
      $alert_message.= "\r\nRef1: ".sprintf("%05d",$ref1);
      $alert_message.= "\r\nRef2: ".sprintf("%05d",$ref2);
      $this->alert_line->notify_message_powerbank($alert_message);
    }

    $response = array(
      'result'=>$result,
      'msg'=>$msg
    );

    $update_log = array(
      'partnerpaymentid' => isset($partnerpaymentid) ? $partnerpaymentid : NULL,
      'memberpaymentid' => isset($memberpaymentid) ? $memberpaymentid : NULL,
      'response_transid' => $transid,
      'res_code' => $result,
      'res_message' => $msg,
      'status' => $status,
      'returndata' => json_encode($response),
      'returndate' => date('Y-m-d'),
      'returntime' => date('H:i:s')
    );
    $this->logpayment->update_log_id($update_log, $logid);

    header('Content-Type: application/json');
    echo json_encode($response);
  }
}