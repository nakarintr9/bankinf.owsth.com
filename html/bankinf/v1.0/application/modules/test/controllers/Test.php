<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Test extends MY_Controller {

  function __construct() {
    parent::__construct();
    $test_payment_config = include(getcwd().'/../../../config/bankinf/test_payment_config.php');
    $this->test_payment_config = (array)$test_payment_config;
  }

  function test_call_to_v3(){
    $base_url = 'https://'.$_SERVER['SERVER_NAME'];
    echo "<pre>";
    echo $_SERVER['SERVER_PORT'];
    echo "<pre>";
    if($_SERVER['SERVER_PORT'] != '80'){
      $base_url.= ':'.$_SERVER['SERVER_PORT'];
    }
    echo $base_url;
    echo "<pre>";

    $postdata = '';
    $url = "tcp://0.tcp.ngrok.io:15248";
    $url = "https://v3api.mcpay.in:15248/api/v1.0/api_migrate_json/app_api";
    $url = "https://v3api.mcpay.in:21446/api/v1.0/api_migrate_json/app_api";
    $url = "http://v3-vs:21346/api/v1.0/api_server_json/app_api";
    $ch = curl_init($url);                                                                      
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
    curl_setopt($ch, CURLOPT_POSTFIELDS, $postdata);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
    curl_setopt($ch, CURLOPT_TIMEOUT, 30);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
     'Content-Type: application/json', 
     'Content-Length: ' . strlen($postdata))
    );

    $content = curl_exec($ch);

    if($content){
      $return = json_decode($content, true);
    }else{
      $content = "Curl Error: ".curl_error($ch);
    }
    echo $url;
    var_dump($content);
  }

  function payment_thanachart($ref1, $ref2, $amount){
    $thanachart_config = include(getcwd().'/../../../config/bankinf/thanachart_config.php');
    $this->thanachart_config = (array)$thanachart_config;
    $SECRETEKEY = $this->thanachart_config['SECRETEKEY'];
    $amount = str_replace(',', '', $amount);
    $amount = number_format($amount,2,'.','');
    $amount = str_replace('.', '', $amount);
    $amount = sprintf('%012d',$amount);

    $data = array(
      "TRX_REF" => "TST".date('YmdHis'),
      "TO_PROXY_TYPE" => "BILLERID",
      "TO_PROXY_ID" => "TST".date('mdHi').rand(100,999),
      "AMOUNT" => $amount,
      "FROM_ACC_NAME" => "MR. Nakarin Tanamee",
      "FROM_BANK_CODE" => "024",
      "REF1" => $ref1,
      "REF2" => $ref2,
      "TERMINAL_ID" => "123456789",
      "TRX_DATE_TIME" => date('Ymd H:i:s'),
    );

    $CONCAT = '';
    foreach ($data as $key => $value) {
      $CONCAT.= $value;
    }

    $data['CHECK_SUM'] = md5($CONCAT.$SECRETEKEY);

    $postdata = json_encode($data);

    $curl = curl_init();

    curl_setopt_array($curl, array(
      CURLOPT_URL => $this->test_payment_config['base_url_test']."/thanachart/Notifypayment",
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_SSL_VERIFYPEER => false,
      CURLOPT_SSL_VERIFYHOST => false,
      CURLOPT_ENCODING => "",
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 30,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => "POST",
      CURLOPT_POSTFIELDS => $postdata,
      CURLOPT_HTTPHEADER => array(
        "Content-Type: application/json",
        "cache-control: no-cache"
      ),
    ));

    $response = curl_exec($curl);
    $err = curl_error($curl);

    curl_close($curl);

    if ($err) {
      echo "cURL Error #:" . $err;
    } else {
      $response = json_decode($response, true);
      echo "<pre>";
      var_dump($response);
    }
  }

  function payment_tesco($ref1, $ref2, $amount, $action=''){
    echo "RESPONSE QUERY";
    echo "<pre>";
    $response = $this->query_payment_tesco($ref1, $ref2);
    var_dump($response);
    echo "<pre>";
    if($response['resCode'] == '0000' && $action == 'confirm'){
      echo "RESPONSE CONFIRM";
      echo "<pre>";
      $response = $this->confirm_payment_tesco($ref1, $ref2, $amount, $response['tranID']);
      var_dump($response);
      echo "<pre>";
    }
    if($response['resCode'] == '0000' && $action == 'reverse'){
      echo "RESPONSE REVERSE";
      echo "<pre>";
      $response = $this->reverse_payment_tesco($ref1, $ref2, $amount, $response['tranID']);
      var_dump($response);
      echo "<pre>";
    }
    if($response['resCode'] == '0000'  && $action == 'full'){
      echo "RESPONSE CONFIRM";
      echo "<pre>";
      $response = $this->confirm_payment_tesco($ref1, $ref2, $amount, $response['tranID']);
      var_dump($response);
      echo "<pre>";
      if($response['resCode'] == '0000'){
        echo "RESPONSE REVERSE";
        echo "<pre>";
        $response = $this->reverse_payment_tesco($ref1, $ref2, $amount, $response['tranID']);
        var_dump($response);
        echo "<pre>";
      }
    }
  }

  function query_payment_tesco($ref1, $ref2){
    $tesco_config = include(getcwd().'/../../../config/bankinf/tesco_config.php');
    $this->tesco_config = (array)$tesco_config;
    $key = $this->tesco_config['key'];
    $passKey = $this->tesco_config['passKey'];
    $ref1 = sprintf('%05d',$ref1);
    $ref2 = sprintf('%05d',$ref2);
    $tranID = "TST".date('mdHi').rand(100,999);

    $data = array(
      "key"=>$key,
      "passKey"=>$passKey,
      "tranID"=> $tranID,
      "reference1"=>$ref1,
      "reference2"=>$ref2
    );

    echo "REQUEST QUERY";
    echo "<pre>";
    var_dump($data);
    echo "<pre>";

    $postdata = json_encode($data);

    $curl = curl_init();

    curl_setopt_array($curl, array(
      CURLOPT_URL => $this->test_payment_config['base_url_test']."/tesco/billpayment/inquiry",
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_ENCODING => "",
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 30,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => "POST",
      CURLOPT_POSTFIELDS => $postdata,
      CURLOPT_HTTPHEADER => array(
        "Content-Type: application/json",
        "cache-control: no-cache"
      ),
    ));

    $response = curl_exec($curl);
    $err = curl_error($curl);

    curl_close($curl);

    if ($err) {
      return "cURL Error #:" . $err;
    } else {
      return json_decode($response, true);
    }
  }

  function confirm_payment_tesco($ref1, $ref2, $amount, $tranID){
    $tesco_config = include(getcwd().'/../../../config/bankinf/tesco_config.php');
    $this->tesco_config = (array)$tesco_config;
    $key = $this->tesco_config['key'];
    $passKey = $this->tesco_config['passKey'];
    $amount = str_replace(',', '', $amount);
    $amount = number_format($amount,2,'.','');
    $ref1 = sprintf('%05d',$ref1);
    $ref2 = sprintf('%05d',$ref2);

    $data = array(
      "key"=>$key,
      "passKey"=>$passKey,
      "tranID"=> $tranID,
      "amount" => $amount,
      "reference1"=>$ref1,
      "reference2"=>$ref2
    );

    $postdata = json_encode($data);

    $curl = curl_init();

    curl_setopt_array($curl, array(
      CURLOPT_URL => $this->test_payment_config['base_url_test']."/tesco/billpayment/confirm",
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_ENCODING => "",
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 30,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => "POST",
      CURLOPT_POSTFIELDS => $postdata,
      CURLOPT_HTTPHEADER => array(
        "Content-Type: application/json",
        "cache-control: no-cache"
      ),
    ));

    $response = curl_exec($curl);

    $err = curl_error($curl);

    curl_close($curl);

    if ($err) {
      return "cURL Error #:" . $err;
    } else {
      return json_decode($response, true);
    }
  }

  function reverse_payment_tesco($ref1, $ref2, $amount, $tranID){
    $tesco_config = include(getcwd().'/../../../config/bankinf/tesco_config.php');
    $this->tesco_config = (array)$tesco_config;
    $key = $this->tesco_config['key'];
    $passKey = $this->tesco_config['passKey'];
    $amount = str_replace(',', '', $amount);
    $amount = number_format($amount,2,'.','');
    $ref1 = sprintf('%05d',$ref1);
    $ref2 = sprintf('%05d',$ref2);

    $data = array(
      "key"=>$key,
      "passKey"=>$passKey,
      "tranID"=> $tranID,
      "amount" => $amount,
      "reference1"=>$ref1,
      "reference2"=>$ref2
    );

    $postdata = json_encode($data);

    $curl = curl_init();

    curl_setopt_array($curl, array(
      CURLOPT_URL => $this->test_payment_config['base_url_test']."/tesco/billpayment/reverse",
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_ENCODING => "",
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 30,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => "POST",
      CURLOPT_POSTFIELDS => $postdata,
      CURLOPT_HTTPHEADER => array(
        "Content-Type: application/json",
        "cache-control: no-cache"
      ),
    ));

    $response = curl_exec($curl);

    $err = curl_error($curl);

    curl_close($curl);

    if ($err) {
      return "cURL Error #:" . $err;
    } else {
      return json_decode($response, true);
    }
  }

  function payment_ktb($ref1, $ref2, $amount, $action=''){
    $t = microtime(true);
    $micro = sprintf("%06d",($t - floor($t)) * 1000000);
    $d = new DateTime( date('Y-m-d H:i:s.'.$micro, $t) );
    $time = $d->format("u");
    $rand = rand(100,999);
    $bankref = $d->getTimestamp().$time.$rand;
    $ref1 = sprintf('%05d',$ref1);
    $ref2 = sprintf('%05d',$ref2);
    $this->bankref1 = '1 '.$bankref;
    $this->bankref2 = '2 '.$bankref;
    $this->bankref3 = '3 '.$bankref;
    $this->tranxid = $bankref;
    if($action == 'query'){
      echo "RESPONSE QUERY";
      echo "<pre>";
      $response = $this->query_payment_ktb($ref1, $ref2);
      var_dump($response);
      echo "<pre>";
    }
    else if($action == 'approval'){
      echo "RESPONSE APPROVAL";
      echo "<pre>";
      $response = $this->approval_payment_ktb($ref1, $ref2, $amount);
      var_dump($response);
      echo "<pre>";
    }
    else if($action == 'payment'){
      echo "RESPONSE APPROVAL";
      echo "<pre>";
      $response = $this->approval_payment_ktb($ref1, $ref2, $amount);
      var_dump($response);
      echo "<pre>";

      if($response['respcode'] == '0'){
        $tranID = $response['tranxid'];
        echo "RESPONSE PAYMENT";
        echo "<pre>";
        $response = $this->payment_payment_ktb($ref1, $ref2, $amount, $tranID);
        var_dump($response);
        echo "<pre>";
      }
    }
    else if($action == 'full'){
      echo "RESPONSE APPROVAL";
      echo "<pre>";
      $response = $this->approval_payment_ktb($ref1, $ref2, $amount);
      var_dump($response);
      echo "<pre>";

      if($response['respcode'] == '0'){
        $tranID = $response['tranxid'];
        echo "RESPONSE PAYMENT";
        echo "<pre>";
        $response = $this->payment_payment_ktb($ref1, $ref2, $amount, $tranID);
        var_dump($response);
        echo "<pre>";
      }

      if($response['respcode'] == '0'){
        $bankref = $response['bankref'];
        echo "RESPONSE APPROVE REVERSAL";
        echo "<pre>";
        $response = $this->approve_reversal_payment_ktb($ref1, $ref2, $amount, $bankref);
        var_dump($response);
        echo "<pre>";
      }

      if($response['respcode'] == '0'){
        $tranID = $response['tranxid'];
        $bankref = $response['bankref'];
        echo "RESPONSE REVERSAL";
        echo "<pre>";
        $response = $this->reversal_payment_ktb($ref1, $ref2, $amount, $bankref, $tranID);
        var_dump($response);
        echo "<pre>";
      }
    }
  }

  function query_payment_ktb($ref1, $ref2){
    $this->wsdl = 'http://alpha.owsth.com/bankinf/v1.0/ktb/billpayment';
    $this->load->library("nusoap_library");
    $client = new nusoap_client($this->wsdl);
    $inquiry = new stdClass();
    $inquiry->user='mcktb2017';
    $inquiry->password='M3d!4@C3NT3R'; 
    $inquiry->comcode='1010'; 
    $inquiry->prodcode='1010'; 
    $inquiry->command='Inquiry'; 
    $inquiry->bankcode='6'; 
    $inquiry->bankref=$this->bankref1; 
    $inquiry->datetime=date('YmdHis'); 
    $inquiry->effdate=date('Ymd'); 
    $inquiry->channel='2'; 
    $inquiry->ref1= $ref1; 
    $inquiry->ref2= $ref2; 
    $inquiry->ref3='-'; 
    $inquiry->ref4='-';

    try {
      $result = $client->call('inquiry',array('request'=>$inquiry));
      echo '<h2>Request Inquiry</h2><pre>' . htmlspecialchars($client->request, ENT_QUOTES) . '</pre>';
      echo '<h2>Response Inquiry</h2><pre>' . htmlspecialchars($client->response, ENT_QUOTES) . '</pre>';
      return $result;
    } catch (SoapFault $exception) {
      return $exception;
    }
  }

  function approval_payment_ktb($ref1, $ref2, $amount){
    $this->wsdl = 'http://alpha.owsth.com/bankinf/v1.0/ktb/billpayment';
    $this->load->library("nusoap_library");
    $client = new nusoap_client($this->wsdl);
    $approval = new stdClass();
    $approval->user='mcktb2017';
    $approval->password='M3d!4@C3NT3R'; 
    $approval->comcode='1010'; 
    $approval->prodcode='1010'; 
    $approval->command='Approval'; 
    $approval->bankcode='6'; 
    $approval->bankref=$this->bankref2; 
    $approval->datetime=date('YmdHis'); 
    $approval->effdate=date('Ymd');
    $approval->amount=$amount; 
    $approval->channel='2'; 
    $approval->ref1=$ref1; 
    $approval->ref2=$ref2; 
    $approval->ref3='-'; 
    $approval->ref4='-';

    try {
      $result = $client->call('approval',array('request'=>$approval));
      echo '<h2>Request Approval</h2><pre>' . htmlspecialchars($client->request, ENT_QUOTES) . '</pre>';
      echo '<h2>Response Approval</h2><pre>' . htmlspecialchars($client->response, ENT_QUOTES) . '</pre>';
      return $result;
    } catch (SoapFault $exception) {
      return $exception;
    }
  }

  function payment_payment_ktb($ref1, $ref2, $amount, $tranID){
    $this->wsdl = 'http://alpha.owsth.com/bankinf/v1.0/ktb/billpayment';
    $this->load->library("nusoap_library");
    $client = new nusoap_client($this->wsdl);
    $payment = new stdClass();
    $payment->tranxid=$tranID;
    $payment->user='mcktb2017';
    $payment->password='M3d!4@C3NT3R'; 
    $payment->comcode='1010'; 
    $payment->prodcode='1010'; 
    $payment->command='payment'; 
    $payment->bankcode='6'; 
    $payment->bankref=$this->bankref3; 
    $payment->datetime=date('YmdHis'); 
    $payment->effdate=date('Ymd');
    $payment->amount=$amount; 
    $payment->cusname=''; 
    $payment->channel='2'; 
    $payment->ref1=$ref1; 
    $payment->ref2=$ref2; 
    $payment->ref3='-'; 
    $payment->ref4='-';

    try {
      $result = $client->call('payment',array('request'=>$payment));
      echo '<h2>Request Payment</h2><pre>' . htmlspecialchars($client->request, ENT_QUOTES) . '</pre>';
      echo '<h2>Response Payment</h2><pre>' . htmlspecialchars($client->response, ENT_QUOTES) . '</pre>';
      return $result;
    } catch (SoapFault $exception) {
      return $exception;
    }
  }

  function approve_reversal_payment_ktb($ref1, $ref2, $amount, $bankref){
    $this->wsdl = 'http://alpha.owsth.com/bankinf/v1.0/ktb/billpayment';
    $this->load->library("nusoap_library");
    $client = new nusoap_client($this->wsdl);
    $approve_reversal = new stdClass();
    $approve_reversal->user='mcktb2017';
    $approve_reversal->password='M3d!4@C3NT3R'; 
    $approve_reversal->comcode='1010'; 
    $approve_reversal->prodcode='1010'; 
    $approve_reversal->command='approvereversal'; 
    $approve_reversal->bankcode='6'; 
    $approve_reversal->bankref=$bankref; 
    $approve_reversal->datetime=date('YmdHis'); 
    $approve_reversal->effdate=date('Ymd');
    $approve_reversal->amount=$amount; 
    $approve_reversal->channel='2'; 
    $approve_reversal->ref1=$ref1; 
    $approve_reversal->ref2=$ref2; 
    $approve_reversal->ref3='-'; 
    $approve_reversal->ref4='-';

    try {
      $result = $client->call('approvereversal',array('request'=>$approve_reversal));
      echo '<h2>Request Approve Reversal</h2><pre>' . htmlspecialchars($client->request, ENT_QUOTES) . '</pre>';
      echo '<h2>Response Approve Reversal</h2><pre>' . htmlspecialchars($client->response, ENT_QUOTES) . '</pre>';
      return $result;
    } catch (SoapFault $exception) {
      return $exception;
    }
  }

  function reversal_payment_ktb($ref1, $ref2, $amount, $bankref, $tranID){
    $this->wsdl = 'http://alpha.owsth.com/bankinf/v1.0/ktb/billpayment';
    $this->load->library("nusoap_library");
    $client = new nusoap_client($this->wsdl);
    $reversal = new stdClass();
    $reversal->tranxid=$tranID;
    $reversal->user='mcktb2017';
    $reversal->password='M3d!4@C3NT3R'; 
    $reversal->comcode='1010'; 
    $reversal->prodcode='1010'; 
    $reversal->command='reversal'; 
    $reversal->bankcode='6'; 
    $reversal->bankref=$bankref; 
    $reversal->datetime=date('YmdHis'); 
    $reversal->effdate=date('Ymd');
    $reversal->amount=$amount; 
    $reversal->cusname=''; 
    $reversal->channel='2'; 
    $reversal->ref1=$ref1; 
    $reversal->ref2=$ref2; 
    $reversal->ref3='-'; 
    $reversal->ref4='-';

    try {
      $result = $client->call('reversal',array('request'=>$reversal));
      echo '<h2>Request Reversal</h2><pre>' . htmlspecialchars($client->request, ENT_QUOTES) . '</pre>';
      echo '<h2>Response Reversal</h2><pre>' . htmlspecialchars($client->response, ENT_QUOTES) . '</pre>';
      return $result;
    } catch (SoapFault $exception) {
      return $exception;
    }
  }

  function payment_ksherpay1($ref1="4", $ref2="1", $appid="123456789", $channel="wechat", $amount="100"){
    $ref1 = sprintf('%05d',$ref1);
    $ref2 = sprintf('%05d',$ref2);
    $attach = $ref1.$ref2;
    $device_id = 1;
    $operator_id = 1;
    $cash_fee = 0;
    $rate = 0;
    $openid = 1;

    $postfields = "";
    $postfields.= "code=0";
    $postfields.= "&sign=0";
    $postfields.= "&result=SUCCESS";
    $postfields.= "&appid=".$appid;
    $postfields.= "&mch_order_no=".date('YmdHis').rand(10000,99999);
    $postfields.= "&ksher=".date('YmdHis').rand(10000,99999);
    $postfields.= "&channel_order_no=".$channel;
    $postfields.= "&total_fee=".$amount;
    $postfields.= "&fee_type=THB";
    $postfields.= "&time_end=".date('YmdHis');
    $postfields.= "&attach=".$attach;
    $postfields.= "&device_id=".$device_id;
    $postfields.= "&operator_id=".$operator_id;
    $postfields.= "&cash_fee_type=THB";
    $postfields.= "&cash_fee=".$cash_fee;
    $postfields.= "&rate=".$rate;
    $postfields.= "&openid=".$openid;
    $postfields.= "&order_no=".date('YmdHis').rand(10000,99999);


    $curl = curl_init();

    curl_setopt_array($curl, array(
      CURLOPT_URL => $this->test_payment_config['base_url_test']."/ksherpay/notifypayment",
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_ENCODING => "",
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 30,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => "POST",
      CURLOPT_POSTFIELDS => $postfields,
      CURLOPT_HTTPHEADER => array(
        "Content-Type: application/x-www-form-urlencoded",
        "cache-control: no-cache"
      ),
    ));

    $response = curl_exec($curl);
    $err = curl_error($curl);

    curl_close($curl);

    if ($err) {
      echo "cURL Error #:" . $err;
    } else {
      echo $response;
    }
  }

  function refund_ksherpay($channel= "wechat", $channel_order_no='1', $order_no='1', $refund_fee=1, $total_fee=1){
    $curl = curl_init();

    curl_setopt_array($curl, array(
      CURLOPT_URL => $this->test_payment_config['base_url_test']."/ksherpay/refundorder/refund",
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_ENCODING => "",
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 30,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => "POST",
      CURLOPT_POSTFIELDS => "channel=".$channel."&channel_order_no=".$channel_order_no."&ksher_order_no=".$order_no."&mch_order_no=".$order_no."&refund_fee=".$refund_fee."&total_fee=".$total_fee,
      CURLOPT_HTTPHEADER => array(
        "Content-Type: application/x-www-form-urlencoded",
        "cache-control: no-cache"
      ),
    ));

    $response = curl_exec($curl);
    $err = curl_error($curl);

    curl_close($curl);

    if ($err) {
      echo "cURL Error #:" . $err;
    } else {
      echo $response;
    }
  }

  function payment_ksherpay(){
    $appid="123456789";
    $sub_openid="aadfsdfsdfs";
    $channel_sub_appid="sfasdfasdfasdf";
    $this->load->module('ksherpay/kshersignature');
    $data = array(
        'version'           => '2.0.0',
        'time_stamp'        => date("YmdHis",time()),
        'appid'             => $appid,
        'nonce_str'         => $this->kshersignature->generate_nonce_str(16),
        'mch_order_no'      => date("YmdHis",time()),
        'fee_type'          => 'THB',
        'channel'           => 'wechat',
    );
    
    var_dump($data);

    $sign = $this->kshersignature->makeSign($data);
    $request_data = array(
      'sign' => $sign,
      'data' => $data
    );

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_URL, $this->test_payment_config['base_url_test']."/ksherpay/notifypayment");
    curl_setopt($ch, CURLOPT_POSTFIELDS, $request_data);
    $output = curl_exec($ch);
    curl_close($ch);

    var_dump($output);
  }
}
