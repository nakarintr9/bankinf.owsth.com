<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Create_payment_member extends API_Controller {

  function __construct()
  {
    parent::__construct();
    $this->load->library('multipledb');
  }

  function index_post(){
    $ret = $this->index($this->post());
    $this->response($ret);
  }

  function index($data_array){
    $membercode = $data_array['membercode'];
    $memberid = $data_array['memberid'];
    $transfer_date = $data_array['transferdate'];
    $transfer_time = $data_array['transfertime'];
    $detail = $data_array['detail'];
    $amount = $data_array['amount'];
    $channel = $data_array['channel'];
    $addby = $data_array['addby'];
    $where = array(
      'membercode' => $membercode,
      'memberid' => $memberid,
    );
    $get_member = $this->get_member($where);  

    $insert = array(
      'fromcode' => $membercode,
      'fromid' => $memberid,
      'fromtype' => 'member',
      'banktransfer' => '',
      'channel' => $channel,
      'paymentauto' => 0,
      'amount' => $amount,
      'tocode' => 'PN',
      'toid' => $get_member->partnerid,
      'totype' => 'partner',
      'detail' => $detail,
      'transferdate' => $transfer_date,
      'transfertime' => $transfer_time,
      'addby' => $addby ,
      'adddate' => date('Y-m-d'),
      'addtime' => date('H:i:s'),
      'creditbefore' => $get_member->credit,
      'creditafter' => ($get_member->credit) + ($amount),
    );
    $res = $this->insert_payment($insert);
    //update
    $update = array('image' => 'img'.$res['id'].'.jpg');
    $where = array('ID' => $res['id']);
    $this->update_payment($update, $where);

    return $res;
  }

  function get_member($where){
    $query = $this->multipledb->owstopup
      ->from('member')
      ->where($where)
      ->get()
      ->row();
    return $query;
  }

  function insert_payment($insert){
    $query = $this->multipledb->owstopup->insert('payment', $insert);
    if($query){
      $id = $this->multipledb->owstopup->insert_id();
      $ret['status'] = true;
      $ret['id'] = $id;
    }
    else{
      $ret['status'] = false;
    }
    return $ret;
  }

  function update_payment($update, $where){
    $this->multipledb->owstopup->set($update);
    $this->multipledb->owstopup->where($where);
    if($this->multipledb->owstopup->update('payment')){
      $ret['status'] = true;
    }
    else{
      $ret['status'] = false;
    }
    return $ret;
  }

}