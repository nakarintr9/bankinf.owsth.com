<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Createsmspayintest extends MY_Controller {

  function __construct()
  {
    parent::__construct();
    $this->load->library('multipledb');
  }

  function create($amount){
    $this->create_sms_kbank1($amount);
    $this->create_sms_kbank2($amount);
    $this->create_sms_ktb($amount);
    $this->create_sms_scb($amount);
  }

  function create_sms_kbank1($amount, $account='X278322X'){
    $smsdate = date('Y-m-d H:i:s');
    $dateformat = new DateTime($smsdate);

    $dateformat_th = strtotime('+ 543 year', strtotime($smsdate));
    $smsdate_th = date('d/m/y H:i', $dateformat_th);

    $amount = number_format($amount,2,'.','');

    $sms = $smsdate_th." บชX278322X รับโอนจากX144149X ".$amount."บ";

    echo "<pre>";
    echo "KBank1 ".$sms;

    $insert = array(
      'verifyid' => date('YmdHis'),
      'username' => 'payin',
      'password' => 'express2015',
      'sms' => $sms,
      'mobile' => 'KBank',
      'amount' => 0,
      'adddate' => $dateformat->format('Y-m-d'),
      'addtime' => $dateformat->format('H:i:s'),
      'status' => 0
    );
    $this->multipledb->owstopup->insert('list_smspayin', $insert);
  }

  function create_sms_kbank2($amount, $account='X230018X'){
    $smsdate = date('Y-m-d H:i:s');
    $dateformat = new DateTime($smsdate);

    $dateformat_th = strtotime('+ 543 year', strtotime($smsdate));
    $smsdate_th = date('d/m/y H:i', $dateformat_th);

    $amount = number_format($amount,2,'.','');
    $sms = $smsdate_th." บช".$account." รับโอนจากX281830X ".$amount." คงเหลือ100112.01บ";

    echo "<pre>";
    echo "KBank2 ".$sms;

    $insert = array(
      'verifyid' => date('YmdHis'),
      'username' => 'payin',
      'password' => 'express2015',
      'sms' => $sms,
      'mobile' => 'KBank',
      'amount' => 0,
      'adddate' => $dateformat->format('Y-m-d'),
      'addtime' => $dateformat->format('H:i:s'),
      'status' => 0
    );
    $this->multipledb->owstopup->insert('list_smspayin', $insert);
  }

  function create_sms_ktb($amount, $account='X72836X'){
    $smsdate = date('Y-m-d H:i:s');

    $dateformat = new DateTime($smsdate);
    
    $smsdate = $dateformat->format('d-m-y@H:i');

    $amount = number_format($amount,2);

    $sms =  $smsdate." บช ".$account.":เงินเข้า  ".$amount." บ. ใช้ได้ 185,879.00 บ.";

    echo "<pre>";
    echo "KTB ".$sms;

    $insert = array(
      'verifyid' => date('YmdHis'),
      'username' => 'payin',
      'password' => 'express2015',
      'sms' => $sms,
      'mobile' => 'KTB',
      'amount' => 0,
      'adddate' => $dateformat->format('Y-m-d'),
      'addtime' => $dateformat->format('H:i:s'),
      'status' => 0
    );
    $this->multipledb->owstopup->insert('list_smspayin', $insert);
  }

  function create_sms_scb($amount, $account='x376989'){
    $smsdate = date('Y-m-d H:i:s');

    $dateformat = new DateTime($smsdate);
    
    $smsdate = $dateformat->format('d/m@H:i');

    $amount = number_format($amount,2);

    $sms = "เงินโอนจาก PRASONG PHANP ยอด ".$amount." บ. เข้าบ/ช".$account." วันที่ ".$smsdate."  ใช้ได้ 84,878.39 บ.";

    echo "<pre>";
    echo "SCB ".$sms;

    $insert = array(
      'verifyid' => date('YmdHis'),
      'username' => 'payin',
      'password' => 'express2015',
      'sms' => $sms,
      'mobile' => '027777777',
      'amount' => 0,
      'adddate' => $dateformat->format('Y-m-d'),
      'addtime' => $dateformat->format('H:i:s'),
      'status' => 0
    );
    $this->multipledb->owstopup->insert('list_smspayin', $insert);
  }

}