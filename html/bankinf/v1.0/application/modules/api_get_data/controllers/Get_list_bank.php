<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Get_list_bank extends API_Controller {

  function __construct()
  {
    parent::__construct();
    $this->load->model('api_get_data/data_model');
  }

  function index_post(){
    $ret = $this->index($this->post());
    $this->response($ret);
  }

  function index($body){
    $params = $body['params'];
    $ret = $this->data_model->get_bank($params);
    $response = array(
    	'error_code' => 'E00',
    	'error_text' => 'OK',
    	'data' => $ret
    );
    return $response;
  }


}