<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Data_model extends MY_Model {

  function __construct()
  {
    parent::__construct();
    $this->load->library('multipledb');
  }

  function get_bank($params=''){
    if(isset($params['where'])){
      foreach($params['where'] as $key => $value){
        $this->multipledb->bankinf->where($key, $value);
      }
    }
    if(isset($params['order'])){
      if(!isset($params['sort'])){
        $params['sort'] = 'ASC';
      }
      $this->multipledb->bankinf->order_by($params['order'], $params['sort']);
    }    
    $query = $this->multipledb->bankinf
      ->select('payment_bank.*')
      ->from('payment_bank')
      ->get()
      ->result_array();
    return $query;
  }

  function get_channel_bank($params=''){
    if(isset($params['where'])){
      foreach($params['where'] as $key => $value){
        $this->multipledb->bankinf->where($key, $value);
      }
    }
    if(isset($params['order'])){
      if(!isset($params['sort'])){
        $params['sort'] = 'ASC';
      }
      $this->multipledb->bankinf->order_by($params['order'], $params['sort']);
    }    
    $query = $this->multipledb->bankinf
      ->select('payment_channel_bank.*')
      ->from('payment_channel_bank')
      ->get()
      ->result_array();
    return $query;
  }
}
