<?php
  $CI =& get_instance();

  $getlang = $CI->db
    ->select('language.*')
    ->from('language')
    ->get()
    ->result_array();
  $lang = array();
  foreach($getlang as $glang){
    $key = $glang['key'];
    $value = $glang['value_thai'];
    $lang[$key] = $value;
  }
?>

